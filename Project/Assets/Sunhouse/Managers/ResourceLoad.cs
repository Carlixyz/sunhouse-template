using UnityEngine;
public class ResourceLoad
{
	public enum Prefabs
	{
		CUBE=0,
		FIREBALL=1,
		SHIP=2,
		SOMEDESERTPREFAB=3,
		FIREBALL_SHADER=4,
		AUDIO=5,
		FIREPOOL=6,
		GAMEOBJECT=7,
		LOCALIZATION=8,
		REGISTER=9,
		SCREENLOG=10,
		TOUCH=11,
		COUNT=12
	}
	public static readonly string[] Paths = 
	{
		"Cube",
		"Fireball",
		"Ship",
		"SomeDesertPrefab",
		"Materials/Fireball (Shader)",
		"Prefabs/Audio",
		"Prefabs/FirePool",
		"Prefabs/GameObject",
		"Prefabs/Localization",
		"Prefabs/Register",
		"Prefabs/ScreenLog",
		"Prefabs/Touch",
	};
	public static readonly string[] Extensions = 
	{
		".prefab",
		".prefab",
		".prefab",
		".prefab",
		".prefab",
		".prefab",
		".prefab",
		".prefab",
		".prefab",
		".prefab",
		".prefab",
		".prefab",
	};
	public static readonly string[] PrefabNames = 
	{
		"Cube",
		"Fireball",
		"Ship",
		"SomeDesertPrefab",
		"Fireball (Shader)",
		"Audio",
		"FirePool",
		"GameObject",
		"Localization",
		"Register",
		"ScreenLog",
		"Touch",
	};
}
