﻿using System;
using System.Collections.Generic;
using System.Reflection;
using UnityEngine;


namespace Sun.Serialization
{

    public static class Adapter
    {
        public static object ToSerializableObject(object obj)
        {
            if (obj == null) return null;

            if (obj is Color)
                return new sColor((Color)obj);
            else if (obj is Vector2)
                return new sVector2((Vector2)obj);
            else if (obj is Vector3)
                return new sVector3((Vector3)obj);
            else if (obj is Vector4)
                return new sVector4((Vector4)obj);
            else if (obj is Quaternion)
                return new sQuaternion((Quaternion)obj);
            //else if (/*obj is Data ||*/ Utils.IsTypeDerivedFrom(obj.GetType(), typeof(Data)))
            //    return new sData((Data)obj);
            else if (obj is MonoBehaviour)
                return new sBehaviour((MonoBehaviour)obj);
            //else if (obj is GameObject)
            //    return new sGameObject((GameObject)obj);

            return obj;
        }

        public static object FromSerializableObject(object obj)
        {
            if (obj == null) return null;

            if (obj is sColor)
                return ((sColor)obj).getColor();
            else if (obj is sVector2)
                return ((sVector2)obj).getVector();
            else if (obj is sVector3)
                return ((sVector3)obj).getVector3();
            else if (obj is sVector4)
                return ((sVector4)obj).getVector4();
            else if (obj is sQuaternion)
                return ((sQuaternion)obj).getQuaternion();
            //else if (obj is sData)
            //    return ((sData)obj).GetData();
            //else if (obj is sGameObject)
            //    return ((sGameObject)obj).getGO();

            return obj;
        }

    }

    [Serializable]
    public class sColor
    {
        public float r { get; set; }
        public float g { get; set; }
        public float b { get; set; }
        public float a { get; set; }
        public sColor() { }
        public sColor(Color color)
        {
            this.r = color.r;
            this.g = color.g;
            this.b = color.b;
            this.a = color.a;
        }
        public Color getColor() { return new Color(r, g, b, a); }
        public override string ToString() { return string.Format("{0}, {1}, {2}, {3}", r, g, b, a); }
    }

    [Serializable]
    public class sVector2
    {
        public float x { get; set; }
        public float y { get; set; }
        public sVector2() { }
        public sVector2(Vector2 vector2)
        {
            x = vector2.x;
            y = vector2.y;
        }
        public Vector2 getVector() { return new Vector2(x, y); }
        public override string ToString() { return string.Format("{0}, {1}", x, y); }
        public static implicit operator Vector2(sVector2 rValue)
        {
            return new Vector2(rValue.x, rValue.y);
        }
        public static implicit operator sVector2(Vector2 rValue)
        {
            return new sVector2(rValue);
        }
    }

    [Serializable]
    public class sVector3
    {
        public float x { get; set; }
        public float y { get; set; }
        public float z { get; set; }
        public sVector3() { }
        public sVector3(Vector3 vector3)
        {
            x = vector3.x;
            y = vector3.y;
            z = vector3.z;
        }
        public sVector3(float x, float y, float z)
        {
            this.x = x;
            this.y = y;
            this.z = z;
        }
        public Vector3 getVector3() { return new Vector3(x, y, z); }
        public override string ToString() { return string.Format("{0}, {1}, {2}", x, y, z); }
        public static implicit operator Vector3(sVector3 rValue)
        {
            return new Vector3(rValue.x, rValue.y, rValue.z);
        }
        public static implicit operator sVector3(Vector3 rValue)
        {
            return new sVector3(rValue);
        }
    }

    [Serializable]
    public class sVector4
    {
        public float x { get; set; }
        public float y { get; set; }
        public float z { get; set; }
        public float w { get; set; }
        public sVector4() { }
        public sVector4(Vector4 vector4)
        {
            x = vector4.x;
            y = vector4.y;
            z = vector4.z;
            w = vector4.w;
        }
        public Vector4 getVector4() { return new Vector4(x, y, z, w); }
        public override string ToString() { return string.Format("{0}, {1}, {2}, {3}", x, y, z, w); }
        public static implicit operator Vector4(sVector4 rValue)
        {
            return new Vector4(rValue.x, rValue.y, rValue.z, rValue.w);
        }
        public static implicit operator sVector4(Vector4 rValue)
        {
            return new sVector4(rValue);
        }
    }

    [Serializable]
    public class sQuaternion
    {
        public float x { get; set; }
        public float y { get; set; }
        public float z { get; set; }
        public float w { get; set; }
        public sQuaternion() { }
        public sQuaternion(Quaternion quaternion)
        {
            x = quaternion.x;
            y = quaternion.y;
            z = quaternion.z;
            w = quaternion.w;
        }
        public Quaternion getQuaternion() { return new Quaternion(x, y, z, w); }
        public override string ToString() { return string.Format("{0}, {1}, {2}, {3}", x, y, z, w); }
        public static implicit operator Quaternion(sQuaternion rValue)
        {
            return new Quaternion(rValue.x, rValue.y, rValue.z, rValue.w);
        }
        public static implicit operator sQuaternion(Quaternion rValue)
        {
            return new sQuaternion(rValue);
        }
    }

    [Serializable]
    public class sTransform
    {
        public sVector3 lPosition { get; set; }
        public sQuaternion lRotation { get; set; }
        public sVector3 lScale { get; set; }

        public sTransform(Transform tr)
        {
            lPosition = tr.transform.localPosition;
            lRotation = tr.transform.localRotation;
            lScale =    tr.transform.localScale;
        }

        public override string ToString() { return string.Format("{0}, {1}, {2}", lPosition, lRotation, lScale); }
        public static implicit operator sTransform(Transform rValue)
        {
            return new sTransform(rValue);
        }
    }

    [Serializable]
    public class sGameObject
    {
        public string sName { get; set; }               //public float sID { get; set; }

        public sTransform lTransform { get; set; }

        public List<Type> componentsTypes { get; set; }

        public sGameObject() { }
        public sGameObject(GameObject go)
        {
            sName = go.name;
            lTransform = new sTransform(go.transform);
            componentsTypes = new List<Type>();
            foreach (MonoBehaviour mb in go.GetComponents<MonoBehaviour>())
                componentsTypes.Add( mb.GetType() );
        }

        public GameObject getGO(GameObject go = null)   // Gives an old or else new GO from this serialized data    
        {
            if (go == null )
                if( Resources.Load(sName) == null )
                {
                    go = new GameObject(sName);

                    foreach (Type mbType in componentsTypes)
                        go.AddComponent(mbType);
                }
                else
                    go = GameObject.Instantiate(Resources.Load<GameObject>(sName));

            go.transform.localPosition  = lTransform.lPosition;
            go.transform.localRotation  = lTransform.lRotation;
            go.transform.localScale     = lTransform.lScale;
            
            return go;
        }
        public override string ToString() { return string.Format("{0}, {1}", sName, lTransform.ToString()); }
        public static implicit operator GameObject(sGameObject rValue)
        {
            return rValue.getGO();
        }
        public static implicit operator sGameObject(GameObject rValue)
        {
            return new sGameObject(rValue);
        }
    }

    //[Serializable]
    //public class sData                            
    //{
    //    /// It's a shortcut because SharpSerializer only reads all inside properties...
    //    //public Dictionary<string, System.Object> dataFields { get; set; }
    //    public List<System.Object> dataFields { get; set; }

    //    public string sType { get; set; }    //object instance =System.Activator.CreateInstance(Type.GetType(name));             

    //    public sData() { dataFields = new List<System.Object>(); }

    //    public sData(Data data)
    //    {
    //        dataFields = new List<System.Object>();
    //        sType = data.GetType().Name;
    //        GetFields(data);
    //    }

    //    public void GetFields(Data data)
    //    {
    //        dataFields = new List<System.Object>();

    //        FieldInfo[] dFields = data.GetType().GetFields( BindingFlags.NonPublic |
    //                                                        BindingFlags.Public |
    //                                                        BindingFlags.Instance);
    //        foreach (var field in dFields)
    //        {
    //            System.Type fieldType = field.FieldType;

    //            if( fieldType.IsClass || fieldType != typeof(string) &&
    //                fieldType.IsArray || fieldType.IsGenericType &&
    //                fieldType.GetGenericTypeDefinition() == typeof(List<>)  )
    //                De.LogWarning("Serializing inner List & Classes fields with SharpSerializer,\n" +
    //                    "Consider use Properties in those classes instead");

    //                dataFields.Add(
    //                Sun.Serialization.Adapter.ToSerializableObject(field.GetValue(data)));
    //        }
    //    }

    //    public void SetFields(Data data)
    //    {
    //        FieldInfo[] fields = data.GetType().GetFields(BindingFlags.NonPublic |
    //                                                        BindingFlags.Public |
    //                                                        BindingFlags.Instance);

    //        for (int i = 0, total = fields.Length; i < total; i++)
    //        {
    //            fields[i].SetValue(data,
    //                Sun.Serialization.Adapter.FromSerializableObject(dataFields[i]));
    //        }
    //    }



    //    public Data GetData()
    //    {
    //        Data instance = (Data)System.Activator.CreateInstance(Type.GetType(sType));
    //        SetFields(instance);
    //        return instance;
    //    }

    //}

    [Serializable]
    public class sBehaviour 
    {
        public Dictionary<string, System.Object> SubstData { get; set; }
        public string id { get; set; }

        public sBehaviour() {; }
        //{ SubstData = new Dictionary<string, System.Object>(); }

        public sBehaviour(MonoBehaviour mb)   // Save ALL Component's fields (publics & privates)               
        { GetFields(mb); }

        public T setMonoBehaviour<T>(GameObject go) where T : MonoBehaviour
        {
            // creating the object instance
            T original = go.GetComponent<T>() ?? go.AddComponent<T>();

            //and initializing it
            FieldInfo[] fields = original.GetType().GetFields(BindingFlags.NonPublic |
                                                               BindingFlags.Public |
                                                               BindingFlags.Instance);
            foreach (var field in fields)
            {
                System.Type t = field.FieldType;
                if (SubstData.ContainsKey(field.Name))
                    if (t.IsPrimitive || t.IsValueType || (t == typeof(string)))
                        field.SetValue(original, SubstData[field.Name]);
                    else
                        field.SetValue(original, 
                            Sun.Serialization.Adapter.FromSerializableObject(SubstData[field.Name]));
            }

            Debug.Log("ReCreatedOriginal " + typeof(T).Name);

            return original;
        }       // Restore Component's field values

        public void GetFields(MonoBehaviour mb)
        {
            SubstData = new Dictionary<string, System.Object>();
            //Copy properties from the original object
            FieldInfo[] mbFields = mb.GetType().GetFields(BindingFlags.NonPublic |
                                                            BindingFlags.Public |
                                                            BindingFlags.Instance);
            foreach (var field in mbFields)
            {
                System.Type t = field.FieldType;
                if ((t.IsPrimitive || t.IsValueType || (t == typeof(string))))
                    SubstData.Add(field.Name, field.GetValue(mb));
                else
                    SubstData.Add(field.Name,
                        Sun.Serialization.Adapter.ToSerializableObject(field.GetValue(mb)));
                //Debug.Log(" Reading field " + field.Name);
            }

            //////////////////////////////////////////////////////////////////////////////////

            Debug.Log("CreatedSubstitute " + mb.GetType().Name);
        }

        public void GetProperties(MonoBehaviour mb)     // Save Properties in a substitute MonoBehaviour 
        {
            SubstData = new Dictionary<string, System.Object>();

            // Copy properties from the original object
            PropertyInfo[] mbProps = mb.GetType().GetProperties(BindingFlags.NonPublic |
                                                                BindingFlags.Public |
                                                                BindingFlags.Instance |
                                                                BindingFlags.GetProperty |
                                                                BindingFlags.DeclaredOnly);
            // Copy Properties to Substitute
            foreach (var prop in mbProps)
            {
                System.Type t = prop.PropertyType;
                if ((t.IsPrimitive || t.IsValueType || (t == typeof(string))))
                    SubstData.Add(prop.Name, prop.GetValue(mb, null));
                else
                    SubstData.Add(prop.Name,
                        Sun.Serialization.Adapter.ToSerializableObject(prop.GetValue(mb, null)));
            }

            Debug.Log("GetProperties: Stored in Substitute " + mb.GetType().Name);
        }     

        public void SetProperties(MonoBehaviour mb)
        {
            PropertyInfo[] props = mb.GetType().GetProperties(BindingFlags.NonPublic |
                                                            BindingFlags.Public |
                                                            BindingFlags.Instance |
                                                            BindingFlags.SetProperty |
                                                            BindingFlags.DeclaredOnly);
            foreach (var prop in props)
            {
                System.Type t = prop.PropertyType;
                if (SubstData.ContainsKey(prop.Name))
                    if (t.IsPrimitive || t.IsValueType || (t == typeof(string)))
                        prop.SetValue(mb, SubstData[prop.Name], null);
                    else
                        prop.SetValue(mb, Sun.Serialization.Adapter.FromSerializableObject(
                            SubstData[prop.Name]), null);
            }

            SubstData = null;
            Debug.Log("SetProperties: Restored Original MonoBehaviour" + mb.GetType().Name);
        }     // Restore Properties to Original MonoBehaviour

    }
}
