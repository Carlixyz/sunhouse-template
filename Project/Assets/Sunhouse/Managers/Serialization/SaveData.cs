#region UsingStatements

using UnityEngine;
using System.Collections.Generic;
//using System.Collections;
//using System.Xml;
//using System.Xml.Serialization;
using System.IO;
using Polenter.Serialization;

#endregion

namespace Sun.Serialization
{

/// <summary>
/// 	- Simple, flexible SaveGroup class for serializing many data types.
/// 	- Only serializes public, non-static fields and classes.
/// 	- Does not support direct serialization of classes derived from Component object 
/// 	- (Transforms, Renderers, Monobehaviours, AudioSources, ect)
/// </summary>
    [System.Serializable]
    public class SaveGroup
    {
	    #region PublicFields
	
	    /// <summary>
	    /// 	- The saved file's extension.
	    /// </summary>
	    public static readonly string extension = ".xml";
	
	    /// <summary>
	    /// 	- The name of the file.
	    /// </summary>
	    public string fileName = "SavedData";
	
        #endregion

        #region PublicParameters

        /// <summary>
        /// Gets or sets the <see cref="SaveGroup"/> with the specified key.
        /// </summary>
        /// <param name='key'>
        /// 	- Key to set.
        /// </param>
        public System.Object this[string key]
	    {
            get { return _data[key]; }
            set
            {
                if (typeof(Component).IsAssignableFrom(value.GetType())) throw new System.InvalidOperationException("Cannot serialize classes derived from Component!");
                _data[key] = value;
            }
        }
	
	    #endregion
	
	    #region PrivateFields
	
	    /// <summary>
	    /// 	- Actual data storage.
	    /// </summary>
	    private Dictionary<string, System.Object> _data = new Dictionary<string, object>();

        public Dictionary<string, System.Object> DataSerialed { get; set; }

            //public Dictionary<string, System.Object> SimpleData { get; set; }

            //public Dictionary<string, System.Object> ComplexData { get; set; }
 

        #endregion

        #region Constructors

        /// <summary>
        /// 	- Initializes a new instance of the <see cref="SaveGroup"/> class.
        /// </summary>
        public SaveGroup(){}
	
	    /// <summary>
	    /// 	- Initializes a new instance of the <see cref="SaveGroup"/> class.
	    /// </summary>
	    /// <param name='fileName'>
	    /// 	- The name of the saved file without any extension
	    /// </param>
	    public SaveGroup(string fileName)
	    {
		    this.fileName = fileName;
	    }
	
	    #endregion
	
	    #region PublicStaticFunctions
	
	    /// <summary>
	    /// 	- Loads specified file from streaming assets folder.
	    /// </summary>
	    /// <param name='fileName'>
	    /// 	- The file to load from Application.streamingAssetsPath.
	    /// </param>
	    public static SaveGroup LoadFromStreamingAssets(string fileName)
	    {
		    return Load(Application.streamingAssetsPath+"\\"+fileName);
	    }

        /// <summary>
        /// 	- Loads a file from the specified path.
        /// </summary>
        /// <param name='path'>
        /// 	- The path to load from.
        /// </param>
        /// <exception cref='System.InvalidOperationException'>
        /// 	- Is thrown when the passed path does not exist or has the wrong extension.
        /// </exception>
        public static SaveGroup Load(string path, bool inBinaryFormat = false)
        {
            if (File.Exists(path))
            {
                SharpSerializer serializer;
                if (!inBinaryFormat)
                    serializer = new SharpSerializer();
                else
                    serializer = new SharpSerializer(
                        new SharpSerializerBinarySettings(BinarySerializationMode.SizeOptimized));

                    //serializer.PropertyProvider = new MyCustomPropertyProvider();

                try
                {
                    SaveGroup instance = (SaveGroup)serializer.Deserialize(path);

                        //foreach (var pair in instance.DataSerialed)
                        //        instance._data[pair.Key] = Adapter.FromSerializableObject(pair.Value);

                        foreach (var pair in instance.DataSerialed)
                            if (pair.Value.GetType().IsPrimitive)
                                instance._data[pair.Key] = pair.Value;
                            else
                                instance._data[pair.Key] = Adapter.FromSerializableObject(pair.Value);

                        instance.DataSerialed.Clear(); 
                        instance.DataSerialed = null;

                    return instance;
                }
                catch (System.Exception exception)
                {
                    Debug.LogError(exception.Message);
                    return default(SaveGroup);
                }
            }
            else
            {
                throw new System.ArgumentNullException("File not found to load");
            }
        }

        /// <summary>
        /// 	- Deletes a file from the specified path.
        /// </summary>
        /// <param name='path'>
        /// 	- The path to seek from.
        /// </param>
        public static bool Delete(string path)
        {
            try
            {
                File.Delete(path);
            }
            catch (System.Exception exception)
            {
                Debug.LogWarning(exception.Message);
                return false;
            }
            return true;
        }

        #endregion

        #region PublicFunctions

        /// <summary>
        /// 	- Determines whether this instance has the specified key.
        /// </summary>
        /// <returns>
        /// 	- <c>true</c> if this instance has the specified key; otherwise, <c>false</c>.
        /// </returns>
        /// <param name='key'>
        /// 	- Key to check for.
        /// </param>
        public bool HasKey(string key)
	    {
		    return _data.ContainsKey(key);	
	    }
	
	    /// <summary>
	    /// 	- Gets value associated with the key.
	    /// </summary>
	    /// <returns>
	    /// 	- The value.
	    /// </returns>
	    /// <param name='key'>
	    /// 	- Key to get value of.
	    /// </param>
	    /// <typeparam name='T'>
	    /// 	- The type of the returned value.
	    /// </typeparam>
	    public T GetValue<T>(string key)
	    {
		    return (T) _data[key] ;
		    //return (T)SerializableObjects.Adapter.FromSerializableObject(_data[key]);
	    }
	
	    /// <summary>
	    /// 	- Tries to get the value associated with the key.
	    /// </summary>
	    /// <returns>
	    /// 	- <c>true</c> if the instance contains the key.
	    /// </returns>
	    /// <param name='key'>
	    /// 	- Key to get value of.
	    /// </param>
	    /// <param name='result'>
	    /// 	- If set to <c>true</c>, the value associated with the key.
	    /// </param>
	    public bool TryGetValue(string key, out System.Object result)
	    {
		    return _data.TryGetValue(key, out result);
	    }
	
	    /// <summary>
	    /// 	- Tries to get the value associated with the key.
	    /// </summary>
	    /// <returns>
	    /// 	- <c>true</c> if the instance contains the key.
	    /// </returns>
	    /// <param name='key'>
	    /// 	- Key to get value of.
	    /// </param>
	    /// <param name='result'>
	    /// 	- If set to <c>true</c>, the value associated with the key.
	    /// </param>
	    /// <typeparam name='T'>
	    /// 	- The type of the returned value.
	    /// </typeparam>
	    public bool TryGetValue<T>(string key, out T result)
	    {
		    System.Object resultOut;
		
		    if(_data.TryGetValue(key, out resultOut) && resultOut.GetType() == typeof(T))
		    {
			    result = (T)resultOut;
			    return true;
		    }
		
		    else
		    {
			    result = default(T);
			    return false;
		    }
	    }
	
	    /// <summary>
	    /// 	- Saves this instance to the Streaming Assets path.
	    /// </summary>
	    public void Save() { Save(Application.streamingAssetsPath+"\\"+fileName+extension); }


        /// <summary>
        /// 	- Saves this instance to the specified path.
        /// </summary>
        /// <param name='path'>
        /// 	- Path to save to.
        /// </param>
        public void Save(string path, bool inBinaryFormat = false)
        {
                DataSerialed = new Dictionary<string, object>();

                //foreach (var pair in _data)
                //    DataSerialed[pair.Key] = Adapter.ToSerializableObject(pair.Value);

                foreach (var pair in _data)
                    if (pair.Value.GetType().IsPrimitive)
                        DataSerialed[pair.Key] = pair.Value;
                    else
                        DataSerialed[pair.Key] = Adapter.ToSerializableObject(pair.Value);

                SharpSerializer serializer;
                if (!inBinaryFormat)
                    serializer = new SharpSerializer();
                else
                    serializer = new SharpSerializer(
                        new SharpSerializerBinarySettings(BinarySerializationMode.SizeOptimized));

                serializer.PropertyProvider.AttributesToIgnore.Clear();
                serializer.PropertyProvider.AttributesToIgnore.Add(typeof(System.Xml.Serialization.XmlIgnoreAttribute));

                try
                {
                    serializer.Serialize(this, path);
                }
                catch (System.Exception exception)
                {
                    Debug.LogError(exception.Message);
                }

                DataSerialed = null;



            }

        #endregion

        #region Utility

        /// <summary>
        /// 	- Serializable data container, used for saving and loading.
        /// </summary>
        [System.Serializable]
        public class DataContainer
	    {
            [SerializeField, HideInInspector]
		    public string key;
            [SerializeField]
            public System.Object value; // Unity Don't know what type is during Inspector Serialization

            public DataContainer(){}
		    public DataContainer(string key, System.Object value)
		    {
			    this.key = key;
			    this.value = value;
		    }

	    }

        #endregion
    }

}

#region Code Example

/// <summary>
/// 	How to use:
///	1) Code 'SaveGroup.Load(string filePath)' or 'SaveGroup.LoadFromStreamingAssets(string fileName)' 
///	2) Get or set saved data using SaveData[string index] indexer, or SaveData.GetValue() overloads.
///	3) Call 'SaveGroup.Save()' to save data either to a specified path or to the streaming assets path
///
/// - All saved objects MUST have[System.Serializable] attribute
/// - All saved fields MUST be public
/// - Components CANNOT be saved
/// </summary>
/*
public class SaveDataExample : MonoBehaviour
{
    public string fileName;
    private SaveGroup save;

    void Start()
    {
        //Create data instance
        save = new SaveGroup(fileName);

        //Add keys with significant names and values
        save["Name"] = "Steve";
        save["Dude"] = "Tom";
        save["Key"] = true;
        save["HealthPotions"] = 10;
        save["Position"] = new Vector3(20, 3, -5);
        save["Rotation"] = new Quaternion(0.1f, 0.1f, 0.1f, 1);

        //Save the data
        save.Save();

        //Load the data we just saved
        save = SaveGroup.Load(Application.streamingAssetsPath + "\\" + fileName + ".uml");

        int potions; //variable for out value

        //Use data
        Debug.Log("Name : " + data.GetValue<string>("Name"));
        Debug.Log("Has health potions : " + save.TryGetValue<int>("HealthPotions", out potions));
        Debug.Log("Health potion count : " + potions);
        Debug.Log("Has buddy : " + save.HasKey("Dude"));
        Debug.Log("Buddy's name : " + save.GetValue<string>("Dude"));
        Debug.Log("Current position : " + save.GetValue<Vector3>("Position"));
        Debug.Log("Has key : " + save.GetValue<bool>("Key"));
        Debug.Log("Rotation : " + save.GetValue<Quaternion>("Rotation"));
    }
}
*/
#endregion