﻿using System.IO;
using UnityEngine;
using UnityEditor;
using System;

namespace Sun.Serialization
{

public class SerializerWindow : UnityEditor.EditorWindow
{
    private int selectIndex;
    private const float WINDOW_MIN_WIDTH = 400.0f, WINDOW_MIN_HEIGHT = 200.0f;
    public UnityEngine.Object thing;
    public MonoBehaviour mb;
    public string fileName = "FileName";
    public bool isMonoBehaviour = true;

    [MenuItem("Tools/Serializer Info Window")]
    public static void GetWindow()
    {
        Type projectType = typeof(SceneView);
        SerializerWindow window = GetWindow<SerializerWindow>("Serializer Info", true, projectType);
        window.minSize = new Vector2(WINDOW_MIN_WIDTH, WINDOW_MIN_HEIGHT);
        //mbList = 
        //window.Show();
    }

    void OnGUI()
    {
        EditorGUILayout.Separator();
        GUILayout.BeginVertical("HelpBox");

        var format = (Serializer.Formats)DrawDrop("Format", Serializer.Format);
        if(format !=  Serializer.Format)
                Serializer.Initialize(format, Serializer.SavePath);

        //Serializer.Format = (Serializer.Formats)DrawDrop("Format", Serializer.Format);
        Serializer.SavePath = (Serializer.PathType)DrawDrop("Save Path", Serializer.SavePath);
        Serializer.FileExtension = DrawTextField("File Extension", Serializer.FileExtension);
        DrawSelectlabel("Current Serializer ", Serializer.CurrentSerializer.ToString());

        //

        GUILayout.EndVertical();
        EditorGUILayout.Separator();
        GUILayout.BeginVertical("HelpBox");

        DrawSelectlabel("Streaming Assets Path ", Serializer.StreamingAssetsPath);
        DrawSelectlabel("Persistent Data Path ", Serializer.PersistentDataPath);
        DrawSelectlabel("DataPath ", Serializer.DataPath);
        GUILayout.EndVertical();

        EditorGUILayout.Separator();
        //DrawObjectSerializer();
        DrawMonoSerializer();

        }

    void DrawMonoSerializer()
    {

        EditorGUILayout.Separator();
        GUILayout.BeginVertical("HelpBox");
        EditorGUILayout.Separator();

        mb = (MonoBehaviour)EditorGUILayout.ObjectField("MonoBehaviour", mb, typeof(MonoBehaviour), true);

        //fileName = EditorGUILayout.TextField("FileName", fileName);
        fileName = DrawTextField("File Name", fileName);

        EditorGUILayout.Separator();
        GUILayout.BeginHorizontal();
        EditorGUILayout.SelectableLabel( Serializer.GetFilePath( fileName , Serializer.SavePath) );

        if (GUILayout.Button("Serialize MonoBehaviour", EditorStyles.miniButton))
        {
            if (mb != null)
            {
                var sObj = new Serialization.sBehaviour();
                sObj.GetProperties((MonoBehaviour)mb);
                Serializer.Save(sObj, fileName);
                //AssetDatabase.Refresh();
                //SceneView.RepaintAll();
            }
        }
        GUILayout.EndHorizontal();

        GUILayout.EndVertical();
    }

    void DrawObjectSerializer()
    {

            EditorGUILayout.Separator();
            GUILayout.BeginVertical("HelpBox");
            EditorGUILayout.Separator();
            ////////////////////////

            thing = EditorGUILayout.ObjectField("Object ", thing, typeof(UnityEngine.Object), true);

            fileName = EditorGUILayout.TextField("FileName", fileName);

            EditorGUILayout.Separator();
            GUILayout.BeginHorizontal();
            EditorGUILayout.SelectableLabel(Serializer.GetFilePath(fileName, Serializer.SavePath));

        if (GUILayout.Button("Serialize this object ", EditorStyles.miniButton))
        {
                //var sObj = new Serialization.sBehaviour();
                //sObj.GetProperties(thing as MonoBehaviour);
                //var sObj = new Serialization.sBehaviour(thing as MonoBehaviour);
                //Serializer.Save(sObj, fileName);
                //if (mb.GetType().IsPrimitive != true)
                //{
                //    De.Log("GOING HERE!");
                //    var sObj = new Serialization.sBehaviour();
                //    sObj.GetProperties((MonoBehaviour)mb);
                //    Serializer.Save(sObj, fileName);
                //}
                //else
                Serializer.Save(thing, fileName);

                //AssetDatabase.Refresh();
            }
        GUILayout.EndHorizontal();

        GUILayout.EndVertical();

    }

    Enum DrawDrop(string title, Enum enumParam)
    {
        GUILayout.BeginHorizontal(/*"HelpBox"*/);
        EditorGUILayout.PrefixLabel(title);
        EditorGUILayout.Separator();
        var enumTemp = EditorGUILayout.EnumPopup( /*title,*/ enumParam);
        GUILayout.EndHorizontal();
        EditorGUILayout.Separator();

        return enumTemp;
    }

    void DrawSelectlabel(string title, string text)
    {
        GUILayout.BeginHorizontal(/*"HelpBox"*/);
        EditorGUILayout.PrefixLabel(title);
        EditorGUILayout.Separator();
        EditorGUILayout.SelectableLabel(text);
        GUILayout.EndHorizontal();
    }

    string DrawTextField(string title, string text)
    {
        GUILayout.BeginHorizontal();
        EditorGUILayout.PrefixLabel(title);
        EditorGUILayout.Separator();
        var t = GUILayout.TextField(text);
        GUILayout.EndHorizontal();
        EditorGUILayout.Separator();
        return t;
    }



}
}
