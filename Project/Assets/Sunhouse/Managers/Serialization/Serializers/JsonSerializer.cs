﻿using System;
using System.IO;
using System.Text;
using UnityEngine;

namespace Sun.Serialization
{
    public class JsonSerializer : Serializer.ISerializer
    {
        public string DefaultExtension
        {
            get { return ".json"; }
        }

        public void Serialize(object obj, string filePath)
        {
            string data = JsonUtility.ToJson(obj);
            try
            {
                if (!File.Exists(filePath))
                {
                    File.Create(filePath);
                }
                else
                {
                    byte[] bytes = Encoding.UTF8.GetBytes(data);
                    data = Convert.ToBase64String(bytes);
                    File.WriteAllText(filePath, data);
                }
            }
            catch (Exception ex)
            {
                Debug.LogException(ex);
            }
        }

        public T Deserialize<T>(string filePath)
        {
            try
            {
                string fileContents = File.ReadAllText(filePath);
                byte[] bytes = Convert.FromBase64String(fileContents);
                fileContents = Encoding.UTF8.GetString(bytes);
                return JsonUtility.FromJson<T>(fileContents);
            }
            catch (Exception ex)
            {
                Debug.LogException(ex);
            }
            return default(T);
        }
    }

}
