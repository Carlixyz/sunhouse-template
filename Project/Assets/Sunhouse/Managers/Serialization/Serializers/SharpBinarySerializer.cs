﻿using UnityEngine;
using Polenter.Serialization;
using System.IO;
using System;

namespace Sun.Serialization
{
    /// <summary>
    /// Remember This Serializer Reads only properties (I won't get any field)
    /// </summary>
    public class SharpBinarySerializer : Serializer.ISerializer
    {

        public string DefaultExtension { get { return ".bin"; } }

        public void Serialize(object obj, string filePath) // Serialize specified object to specified file path.
        {
            FileStream file = File.Create(filePath);
            try
            {
                SharpSerializer binaryFormatter = new SharpSerializer(
                        new SharpSerializerBinarySettings(BinarySerializationMode.SizeOptimized));

                binaryFormatter.PropertyProvider.AttributesToIgnore.Clear();
                binaryFormatter.PropertyProvider.AttributesToIgnore.Add(typeof(System.Xml.Serialization.XmlIgnoreAttribute));

                binaryFormatter.Serialize( obj, file);
            }
            catch (Exception ex)
            {
                Debug.LogException(ex);
            }

            file.Close();
        }

        public T Deserialize<T>(string filePath)          // Deserialize object from the specified filePath.
        {
            if (!File.Exists(filePath))
                throw new ArgumentNullException("File not found to load");

            FileStream file = File.OpenRead(filePath);
            T obj = default(T);
            try
            {
                SharpSerializer serializer = new SharpSerializer(
                        new SharpSerializerBinarySettings(BinarySerializationMode.SizeOptimized));
                obj = (T)serializer.Deserialize(file);
            }
            catch (Exception ex)
            {
                Debug.LogException(ex);
            }

            file.Close();
            return obj;
        }
    }
}
