﻿using System;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine;

namespace Sun.Serialization
{
    public class BinarySerializer : Serializer.ISerializer
    {
        public string DefaultExtension
        {
            get { return ".bin"; }
        }

        //public string GetDefaultExtension() { return ".bin"; }

        public void Serialize(object obj, string filePath)
        {
            FileStream file = File.Create(filePath);
            try
            {
                BinaryFormatter binaryFormatter = new BinaryFormatter();
                binaryFormatter.Serialize(file, obj);
            }
            catch (Exception ex)
            {
                Debug.LogException(ex);
            }
        }

        public T Deserialize<T>(string filePath)
        {
            FileStream file = File.OpenRead(filePath);
            T obj = default(T);
            try
            {
                BinaryFormatter binaryFormatter = new BinaryFormatter();
                obj = (T)binaryFormatter.Deserialize(file);
            }
            catch (Exception ex)
            {
                Debug.LogException(ex);
            }
            return obj;
        }
    }

}
