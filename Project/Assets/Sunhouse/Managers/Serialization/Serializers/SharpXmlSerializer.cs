﻿using UnityEngine;
using Polenter.Serialization;
using System.IO;
using System;
using System.Collections.Generic;
//using System.Xml.Serialization;

namespace Sun.Serialization
{
    /// <summary>
    /// Remember This Serializer Reads only properties (I won't get any field)
    /// </summary>

    public class SharpXmlSerializer : Serializer.ISerializer
    {
        public string DefaultExtension { get { return ".xml"; } }

        //public string GetDefaultExtension() { return ".xml"; }

        public void Serialize(object obj, string filePath) // Serialize specified object to specified file path.
        {
            FileStream file = File.Create(filePath);
            try
            {
                var settings = new SharpSerializerXmlSettings(); // for xml mode

                // configure the type serialization
                settings.IncludeAssemblyVersionInTypeName = false;
                settings.IncludeCultureInTypeName = false;
                settings.IncludePublicKeyTokenInTypeName = false;
                //settings.AdvancedSettings.

                // change RootName
                settings.AdvancedSettings.RootName = "DATAS";
                //// instatiate sharpSerializer
                //var serializer = new SharpSerializer(settings);

                SharpSerializer binaryFormatter = new SharpSerializer(settings);

                binaryFormatter.PropertyProvider.AttributesToIgnore.Clear();
                binaryFormatter.PropertyProvider.AttributesToIgnore.Add(typeof(System.Xml.Serialization.XmlIgnoreAttribute));
                // use instead [ExcludeFromSerialization]
                //binaryFormatter.PropertyProvider.PropertiesToIgnore.Add(typeof(List<sVector3>), "Capacity");
                binaryFormatter.PropertyProvider.PropertiesToIgnore.Add(typeof(List<OneMoreClass>), "Capacity");
                binaryFormatter.PropertyProvider.PropertiesToIgnore.Add(typeof(List<DerivedData>), "Capacity");
                binaryFormatter.PropertyProvider.PropertiesToIgnore.Add(typeof(List<object>), "Capacity");

                binaryFormatter.Serialize(obj, file);
            }
            catch (Exception ex)
            {
                Debug.LogException(ex);
            }
            file.Close();
        }
    
        public T Deserialize<T>(string filePath)          // Deserialize object from the specified filePath.
        {
            if (!File.Exists(filePath))
                throw new ArgumentNullException("File not found to load");

            T obj = default(T);
            FileStream file = File.OpenRead(filePath);
            try
            {
                SharpSerializer serializer = new SharpSerializer();
                obj = (T)serializer.Deserialize(file);
            }
            catch (Exception ex)
            {
                Debug.LogException(ex);
            }
            file.Close();
            return obj;            
        }
    }
}
