﻿using System;
using UnityEngine;
using System.IO;


namespace Sun.Serialization
{
    public class XmlSerializer : Serializer.ISerializer
    {
        public string DefaultExtension
        {
            get { return ".xml"; }
        }

        public void Serialize(object obj, string filePath)
        {
            //XmlSerializer serializer = new XmlSerializer();
            //using (var stream = new StreamWriter(filePath, false, System.Text.Encoding.UTF8))
            //{
            //    serializer.Serialize(obj, stream.ToString());
            //    stream.Close();
            //}

            FileStream file = File.Create(filePath);
            try
            {
                XmlSerializer XmlFormatter = new XmlSerializer();
                XmlFormatter.Serialize( obj, file.ToString());
            }
            catch (Exception ex)
            {
                Debug.LogException(ex);
            }
        }

        public T Deserialize<T>(string filePath)
        {
            //if (File.Exists(filePath))
            //{
            //    XmlSerializer serializer = new XmlSerializer();
            //    using (FileStream stream = new FileStream(filePath, FileMode.Open))
            //    {
            //        return serializer.Deserialize<T>(stream.ToString());
            //    }
            //}
            //return default(T);

            FileStream file = File.OpenRead(filePath);
            T obj = default(T);
            try
            {
                XmlSerializer XmlFormatter = new XmlSerializer();
                obj = XmlFormatter.Deserialize<T>(file.ToString());
            }
            catch (Exception ex)
            {
                Debug.LogException(ex);
            }
            return obj;
        }
    }
}
