﻿using System.ComponentModel;
using System.IO;
using UnityEngine;

namespace Sun           
{

    /// Main Class API for Saving and Loading Game Data (a Global Wrapper for ANY Serializers).
    /// You can add more customized serializers and calling right here just adding in the enums section

    public class Serializer            //  Debug.Log(System.Environment.Version);
    {

        public enum Formats     // The Available Formats for Storing Data.
        {
            sharpXML,
            sharpBinary,
            Xml,
            Json,
            Binary
        }

        public enum PathType      // The Available Paths for Saving and Loading Data.  
        {
            StreamingAssetsPath,
            PersistentDataPath,
            DataPath
        }

        public interface ISerializer
        {
            string DefaultExtension { get; }
            void Serialize(object obj, string filePath);  // Serialize specified object to file path.      
            T Deserialize<T>(string filePath); // Deserialize object from the specified filePath.
        }

        //--------------------------------------------------------------------------------//  Properties

        /// Gets or sets the current serializer. You can override it by every save function.
        //public static ISerializer CurrentSerializer { get; set; } ;
        public static ISerializer CurrentSerializer = new Serialization.SharpXmlSerializer();

        /// Gets or sets the current format.
        [DefaultValue(Formats.sharpXML)]
        public static Formats Format { get; set; }              // Debug.Log(System.Environment.Version);

        /// Gets or sets the current save path.
        [DefaultValue(PathType.StreamingAssetsPath)]
        public static PathType SavePath { get; set; }


        /// Gets or sets the current (global) file extension.   // FileExtension { get; set; } = ".xml";
        static string fileExtension = CurrentSerializer.DefaultExtension;
        public static string FileExtension { get { return fileExtension; } set { fileExtension = value; } }

        //--------------------------------------------------------------------------------//  Variables

        public static readonly string PersistentDataPath     = Application.persistentDataPath;

		public static readonly string DataPath               = Application.dataPath + "/Application/Resources/Data/";

		public static readonly string StreamingAssetsPath    = Application.streamingAssetsPath;

        //--------------------------------------------------------------------------------//  Methods

        public static void Initialize ( Formats givenFormat = Formats.sharpXML,
                                        PathType savePath = PathType.StreamingAssetsPath,
                                        string prefferedExtension = "default")
        {
            Format = givenFormat;
            SavePath = savePath;

            switch ( Format )
			{
                case Formats.Xml:
                    CurrentSerializer = new Serialization.XmlSerializer();
                    break;
                case Formats.Binary:
                    CurrentSerializer = new Serialization.BinarySerializer();
                    break;
                case Formats.Json:
                    CurrentSerializer = new Serialization.JsonSerializer();
                    break;
                case Formats.sharpBinary:
					CurrentSerializer = new Serialization.SharpBinarySerializer ();
					break;
				case Formats.sharpXML:
					CurrentSerializer = new Serialization.SharpXmlSerializer();
					break;
			}

            if ( prefferedExtension == "default")
                FileExtension = CurrentSerializer.DefaultExtension;
            else
                FileExtension = prefferedExtension;
        }

        public static string GetCurrentPath() { return GetSavePath(SavePath); }

        /// Gets the save path for the given path type.
        public static string GetSavePath ( PathType savePath)
		{
            switch (savePath)
            {
                case PathType.DataPath:
                    return DataPath;
                case PathType.PersistentDataPath:
                    return PersistentDataPath;
                default:
                case PathType.StreamingAssetsPath:
                    return StreamingAssetsPath;
            }
        }

        /// Gets the file path with the given file name and save path type.
        public static string GetFilePath ( string fileName, PathType savePath )
		{
            //De.Log("Checking format " + Format);
            //De.Log("Checking path " + SavePath);
            //De.Log("Checking extension " + FileExtension);
            return Path.Combine ( GetSavePath ( savePath ), fileName + FileExtension );
		}

		/// Save the specified object with the given file name.
		public static void Save( object obj, string fileName )
		{
            //if (!Initialized)  Initialize();
            Save(obj, fileName, SavePath);
		}

		/// Save the specified object with the given file name in the specified save path.
		public static void Save( object obj, string fileName, PathType savePath )
		{
            string filePath = GetFilePath ( fileName, savePath );

            CurrentSerializer.Serialize ( obj, filePath );
			if ( OnSaved != null )
				OnSaved ( obj );

            De.Log("saved at filePath " + filePath);
		}

        /// Save the specified object in the name and path.
        public static void SaveAt(object obj, string fileNamePath)
        {
            CurrentSerializer.Serialize(obj, fileNamePath);
            if (OnSaved != null)
                OnSaved(obj);

            De.Log("saved at file Name and Path " + fileNamePath);
        }

        /// Load the specified file name.
        public static T Load<T> ( string fileName ) where T : new()
		{
            //if (!Initialized)  Initialize();
            return  Load<T>(fileName, SavePath);
        }

        /// Load the specified file name from the given save path.
        public static T Load<T> ( string fileName, PathType savePath ) where T : new()
		{
			string filePath = GetFilePath ( fileName, savePath );

            if ( !File.Exists ( filePath ) )
			{
                De.Log("file not found at Path: " + filePath + "\n Recreating a default one.");
                T replaceObj = new T();
                Save(replaceObj, fileName, savePath );
				return replaceObj;
			}

            T obj = CurrentSerializer.Deserialize<T> ( filePath );
			if ( obj == null )
                if (typeof(T).IsArray)
                {
                    System.Type elementType = typeof(T).GetElementType();
                    System.Array array = System.Array.CreateInstance(elementType, 0);
                    //inputs.CopyTo(array, 0);
                    obj = (T)(object)array;
                    return obj;
                }
                else
                    obj = new T ();

         

           if ( OnLoaded != null )
				OnLoaded ( obj );
            De.Log("Loaded at filePath " + filePath);

            return obj;
		}

        /// Load the specified file name with the given save path.
        public static T LoadAt<T>(string fileNamePath) where T : new()
        {
            if (!File.Exists(fileNamePath))
            {
                De.Log("file not found at Path: " + fileNamePath + "\n Recreating a default one.");
                T replaceObj = new T();
                SaveAt(replaceObj, fileNamePath);
                return replaceObj;
            }

            T obj = CurrentSerializer.Deserialize<T>(fileNamePath);
            if (obj == null)
                obj = new T();

            if (OnLoaded != null)
                OnLoaded(obj);
            De.Log("Loaded at filePath " + fileNamePath);

            return obj;
        }

        //--------------------------------------------------------------------------------//  Events

        /// Saved callback.
        public delegate void SavedCallback(object obj);

        /// Loaded callback.
        public delegate void LoadedCallback(object obj);

        /// Occurs when data saved.
        public static event SavedCallback OnSaved;

        /// Occurs when data loaded.
        public static event LoadedCallback OnLoaded;

    }

}

/// EXAMPLE!
//public class GameManager : MonoBehaviour
//{
//    public const string MY_GAME_DATA_FILE_NAME = "myGameData";
//    [Serializable]
//    public class MyGameData
//    {
//        public int score = 0;
//        public string playerName = "";
//    }

//    public MyGameData myGameData;
//    void Awake()
//    {
//        Saver.OnSaved += Saver_OnSaved;
//        Saver.OnLoaded += Saver_OnLoaded;
//        Saver.InitializeDefault();
//        myGameData = Saver.Load<MyGameData>(MY_GAME_DATA_FILE_NAME);
//    }

//    void Saver_OnLoaded(object obj)
//    {
//        MyGameData myGameDataLocal = obj as MyGameData;
//        Debug.LogFormat("Loaded Succescully: {0}", myGameDataLocal);
//    }

//    void Saver_OnSaved(object obj)
//    {
//        MyGameData myGameDataLocal = obj as MyGameData;
//        Debug.LogFormat("Saved Succescully: {0}", myGameDataLocal);
//    }

//}