﻿using UnityEngine;
//using System.Collections;
using System.Collections.Generic;
//using Polenter.Serialization;
//using System.Xml.Serialization;
//using System.IO;
using System;
//using System.Runtime.Serialization.Formatters.Binary;
//using System.Runtime.Serialization;
using Sun.Serialization;
using System.Reflection;
//using Sun.DataBase;



//  https://docs.unity3d.com/Manual/script-Serialization.html   UNITY DEFAULT SERIALIZATION
//  - Unity can only serialize fields, because properties are just methods.
//  - Unity serialize all public fields that are in the types it can serialize.It ignores fields flagged with[NonSerialized].
//  - Unity serialize private fields that are flagged with the[SerializeField] attribute, otherwise it ignores them.

//  is public, or has a SerializeField attribute(doesn’t need to be serialized for hot reloading)
//  is not static
//  is not const
//  is not property
//  No support for null for custom classes
//  is not readonly
//  has fieldtype that is of a type that can be serialized(See below.)
//  Fieldtypes that can be serialized

//  Custom non-abstract classes with[Serializable] attribute.
//  Custom structs with[Serializable] attribute(added in Unity 4.5).
//  References to objects that derive from UnityEngine.Object.
//  Primitive data types(int, float, double, bool, string, etc.).
//  Enum types.
//  Certain Unity built-in types: Vector2, Vector3, Vector4, Rect, Quaternion, Matrix4x4, Color, Color32, LayerMask, AnimationCurve, Gradient, RectOffset, GUIStyle.
//  Array of a fieldtype that can be serialized.
//  List<T> of a fieldtype that can be serialized.


namespace Sun
{
    //[Serializable]
    //public class Huevaa : Data
    //{
    //    public int publicInt;                           // YES - public base types will serialise
    //    public string publicString;                     // YES - public object references will serialise
    //    public string namer;                            // YES - public object references will serialise

    //    private int privateInt;                         // NO - private fields won't serialise...
    //    private string privateString;

    //    [SerializeField]
    //    private int privateSerializeFieldInt;           // YES - ... unless they are specifically labelled [SerializeField]
    //    [SerializeField]
    //    private string privateSerializeFieldString;
    //}

    [System.Serializable]
    public class Register : Singleton<Register> 
    {
      

        // Use this for initialization
        void Start()
        {
            Serializer.SavePath = Serializer.PathType.DataPath;

            // Loading all Data array
            Data[] d = Sun.DataBase.DataManager.Load("Data");
            //Data[] d = Sun.DataBase.DataManager.Load("Data/Data");

            // Parsing Data Array
            foreach (var info in d)
            {
                Debug.Log("info is " + info.name);
                if (info.name == "Checho")
                {
                    DerivedData oneData = (DerivedData)info;
                    oneData.testString = "changed!";
                    Debug.Log("checho is " + oneData.testString);
                }
            }

            // Direct Access
            OneMoreClass pepoData = DataBase.DataManager.GetDataByName<OneMoreClass>("Pepo", "Data");
                //DataBase.DataManager.GetDataByName<OneMoreClass>("Pepo", "Data/Data");

            Debug.Log(pepoData.name + " is " + pepoData.testNumber);
            pepoData.testNumber = 747;
            Debug.Log(pepoData.name + " now is " + pepoData.testNumber);


            // Write directly to Item List
            Data dItem = System.Array.Find(d, data => data.name == pepoData.name);
            dItem.CopyFromData(pepoData);

            //Data pepoItem = System.Array.Find(d, data => data.name == pepoData.name);
            //((OneMoreClass)pepoItem).testNumber = pepoData.testNumber;

            //int index = System.Array.FindIndex(d, data => data.name == pepoData.name);
            //((OneMoreClass)d[index]).testNumber = pepoData.testNumber;


            // Saving all Data array
            DataBase.DataManager.Save( d, "Data");
            //DataBase.DataManager.Save( d, "Data/Data");
        }

        // Update is called once per frame
        void Update()
        {

       
        }


      
    public int publicInt;                           // YES - public base types will serialise
    public string publicString;                     // YES - public object references will serialise
    public string namer;                            // YES - public object references will serialise

    private int privateInt;                         // NO - private fields won't serialise...
    private string privateString;

    [SerializeField]
    private int privateSerializeFieldInt;           // YES - ... unless they are specifically labelled [SerializeField]
    [SerializeField]
    private string privateSerializeFieldString;

    public int[] intArray;                          // YES - arrays of base types and objects will serialize
    public string[] stringArray;

    public int[,] multidimensionalArray;            // NO - multidimensional arrays won't serialize
    public int[][] arrayOfArrays;                   // NO - arrays of arrays won't serialize

    public List<int> listOfInt;                     // YES - lists of base types and objects will serialise
    public List<string> listOfString;

    public List<List<int>> listOfLists;             // NO - lists of lists won't serialize

    public Dictionary<int, string> dictionary;      // NO - dictionaries won't serialize

    [System.Serializable]
    public class UserClass                          // NO - user defined classes will not serialise... 
    {                                               // UNLESS MARKED THEN YES
        [SerializeField]
        public string _name = "Name";
        [SerializeField]
        public string Name { get { return _name; } set { _name = value; } }
        [SerializeField]
        public int x;
        public int X { get { return x; } set { x = value; } }

        //public UserClass(int x)
        public UserClass()
        {
            //this.x = i;
            x = 1;
            Name = "Name " + x;
        }

        override public string ToString()
        {
            return "" + x;
        }
    }

    [SerializeField]
    UserClass __userClass = new UserClass();
    public UserClass LuserClass
    {
        get { return __userClass; }
        set { __userClass = value; } }
    }


}
