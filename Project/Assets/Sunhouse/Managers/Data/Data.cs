﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Sun
{

    [System.Serializable]
    public class Data
    {

        public string name = "";
        //public string Name { get { return name; } set { name = value; } }

        [ShowOnly]
        public string uniqueId = "";
        //public string UniqueId { get { return uniqueId; } set { uniqueId = value; } }

        [ShowOnly]
        public int intUniqueId = 0;
        //public int IntUniqueId { get { return intUniqueId; } set { intUniqueId = value; } }

        public Data()
        {
            SetDefaultValues();
        }

        public Data(string json)
        {
            SetJson(json);
        }

        public virtual void SetDefaultValues()
        {
            uniqueId = Sun.Utils.GetUniqueString();
            intUniqueId = Utils.GetUniqueInteger();
        }

        public virtual void OnFinishedDeserializing()
        {
            if (string.IsNullOrEmpty(uniqueId))
                uniqueId = Utils.GetUniqueString();

            if (intUniqueId == 0)
                intUniqueId = Utils.GetUniqueInteger();
        }

        public virtual void OnWillSerialize()
        {

        }

        public void CopyFromData(Data sourceData)
        {
            if (sourceData == null)
                return;

            System.Type type = this.GetType();
            System.Reflection.FieldInfo[] fields = type.GetFields();
            System.Reflection.FieldInfo field = null;

            System.Type sourceType = sourceData.GetType();
            System.Reflection.FieldInfo sourceField = null;


            for (int i = fields.Length - 1; i >= 0; i--)
            {
                field = fields[i];
                if (field.IsPublic && !field.IsStatic)
                {
                    sourceField = sourceType.GetField(field.Name);
                    if (sourceField != null)
                    {

                        field.SetValue(this, sourceField.GetValue(sourceData));
                    }
                }
            }
        }

        public string GetJson()
        {
            return JsonUtility.ToJson(this);
        }

        public void SetJson(string json)
        {
            object result = JsonUtility.FromJson(json, this.GetType());

            //MotinData data = JsonReader.Deserialize<(json);
            CopyFromData((Data)result);
        }

        /// <summary>
        /// -------------------------------------------------------------
        /// </summary>
        /// 

        public object[] DataFields
        {
            get
            {
                 System.Reflection.FieldInfo[] dataFields = GetType().GetFields(   
                     System.Reflection.BindingFlags.Public | System.Reflection.BindingFlags.Instance);

                List<object> sFields = new List<object>();

                foreach (var dField in dataFields)
                    sFields.Add(
                        Sun.Serialization.Adapter.ToSerializableObject(dField.GetValue(this)));

                //De.Log("DataFields SERIALIZED");

                return sFields.ToArray();
            }
            set
            {
                object[] objects = (object[])value;

                System.Reflection.FieldInfo[] dataFields = GetType().GetFields(
                    System.Reflection.BindingFlags.Public | System.Reflection.BindingFlags.Instance);

                for (int i = 0, total = dataFields.Length; i < total; i++)
                {
                    dataFields[i].SetValue(this,
                        Sun.Serialization.Adapter.FromSerializableObject(objects[i]));
                }

                //De.Log("DataFields DESERIALED");
            }
        }


    }
}

/// Use this like this http://www.pixelplacement.com/site/2012/03/21/simple-state-saving/

/*
 [System.Serializable]
public class HeroStats
{
	public float health;
	public float magic;
	public float money;
	public bool talkedToWizard;
	public bool defeatedRedDragon;
	public bool hasMagicKey;
}
Next add an instance of this class to your GameObject as a replacement for anything specific to runtime operation/progress. I typically make this field public so I can monitor and observe things easier in the inspector and load and save to this field as needed. Here’s an example that leverages OnEnable and OnDisable to automatically load and save the state of a GameObject using the values in the HeroStats class from above:

using UnityEngine;
using System.Collections;

public class Hero : MonoBehaviour 
{
	public HeroStats stats;
	
	void OnEnable()
	{
		stats = StateStorage.LoadData("MainHeroStats");	
	}
	
	void OnDisable()
	{
		Save();
	}

	void OnApplicationPause()
	{
		Save();
	}

	void OnApplicationQuit()
	{
		Save();
	}

	void Save()
	{
		StateStorage.SaveData("MainHeroStats", stats);
	}
}
 */
