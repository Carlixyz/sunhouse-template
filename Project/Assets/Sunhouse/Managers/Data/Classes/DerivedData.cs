﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;



[System.Serializable]
public enum anEnum
{
    enumBlaBla1,
    enumBle2,
    enum3
}

[System.Serializable]
public class DerivedData : Sun.Data
{
    public DerivedData() { }

    public int testInt;
    [SerializeField, ShowOnly]
    public int testInt2;
    [SerializeField, ShowOnly]
    private int privateInt3;
    [  ShowOnly]
    public string InterestingValue = "Hola CUliau";
    public float testFloat;
    [SerializeField]
    private float testFloat2;
    public bool testBool;
    public string testString;
    public Vector3 testVector;
    [SerializeField]
    public anEnum testEnum;
    //[SerializeField]
    public OneMoreClass Me = new OneMoreClass();
    //public OneMoreClass[] testArrayClass = { new OneMoreClass(), new OneMoreClass() };
    //public anEnum[] testArrayEn = { anEnum.enumBlaBla1, anEnum.enumBle2 };
    public string[] testArraystr = { " ", "a" };
    //public int[] testArrayInt = new int[5];

    //[SerializeField]
    //[System.Xml.Serialization.XmlIgnoreAttribute]
    //public List<OneMoreClass> testListClass = new List<OneMoreClass>();
    //public List<OneMoreClass> TestListClass
    //{ get { return testListClass; } set { testListClass = value; } }


    public List<anEnum> testList = new List<anEnum>();

}
