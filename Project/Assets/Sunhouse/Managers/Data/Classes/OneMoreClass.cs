﻿using Sun.Serialization;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[System.Serializable]
public class OneMoreClass : Sun.Data
{
    public OneMoreClass() { }

    [SerializeField]
    public string testString;
    //public string TestString { get { return testString; } set { testString = value; } }
    [SerializeField]
    public Vector3 testVector;
    //public object TestVector
    //{ get { return testVector.ToSerializable(); } set { testVector = (Vector3)value.FromSerializable(); } }

    [SerializeField]
    public anEnum testEnum;
    //public anEnum TestEnum { get { return testEnum; } set { testEnum = value; } }

    public int testNumber;

}
