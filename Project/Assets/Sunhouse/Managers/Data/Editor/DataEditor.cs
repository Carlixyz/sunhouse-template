﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System.Linq;
using System;

namespace Sun.DataBase
{
    public class DataEditor : EditorWindow
    {

        private Rect leftPanel, rightPanel, resizer;

        const float WINDOW_MIN_WIDTH = 400.0f, WINDOW_MIN_HEIGHT = 200.0f;

        float LineHeight = EditorGUIUtility.singleLineHeight;
        float sizeRatio = 0.2f;
        float extra = 1.0f;

        private GUIStyle resizerStyle;
        bool isResizing;

        Vector2 _selectionScrollPos;                //Scroll Position for the Selection View
        Vector2 _editScrollPos;                     //Scroll Position for the Edit View

        int        _selectedIndex   = -1;           //Selected index of the data being editted 
        bool       _listChanged     = false;
        Data       _selectedData    = null;
        List<Data> datas            = new List<Data>();
        List<Data> filteredDatas    = new List<Data>();

        int selectedIndex;

        static string filePathAndName = "";
        static string searchFilter = "";
        static string newSearchFilter = "";
        string[] names;

        Type[] types;
        Type[] Types
        {
            get { return types; }
            set
            {
                types = value;
                names = types.Select(t => t.FullName).ToArray();
            }
        }

        [MenuItem("Tools/Data Editor")]
        public static void GetWindow()
        {
            //DataEditor window = GetWindow<DataEditor>(typeof(DataEditor).Name, true, typeof(SceneView));
            DataEditor window = GetWindow<DataEditor>("Data Editor", true, typeof(SceneView));
            window.minSize = new Vector2(WINDOW_MIN_WIDTH, WINDOW_MIN_HEIGHT);
            window.Show();
        }


        void DrawLeftPanel()
        {
            leftPanel = new Rect(0, LineHeight, position.width * sizeRatio, position.height - LineHeight);
            GUILayout.BeginArea(leftPanel);

            EditorGUILayout.BeginHorizontal(EditorStyles.toolbar);              /// List Up ToolBar
            if (GUILayout.Button("Create", EditorStyles.toolbarDropDown, GUILayout.MaxWidth(64)))    // Scratch all
                CreateData();
            EditorGUILayout.Separator();

            newSearchFilter = EditorUtils.DrawFilterField(newSearchFilter);
            EditorGUILayout.EndHorizontal();

            if ( datas != null )
                DrawList();                                                     /// List Drawing  

            EditorGUILayout.BeginHorizontal(EditorStyles.toolbar);              /// List Down ToolBar

            _listChanged = EditorUtils.DrawListToolbar( datas, ref _selectedIndex );

            EditorGUILayout.EndHorizontal();


            GUILayout.EndArea();
        }

        void DrawList()
        {
            GUILayout.BeginVertical( /*GUILayout.MaxWidth(leftPanel.width),*/ GUILayout.ExpandHeight(true));
            _selectionScrollPos = GUILayout.BeginScrollView(_selectionScrollPos, /*GUILayout.MaxWidth(leftPanel.width),*/ GUILayout.ExpandHeight(true));

            EditorGUILayout.LabelField("index " + _selectedIndex);

            FilterList();

            for (int index = 0, total = filteredDatas.Count; index < total; index++)
            {
                bool selected = _selectedData == filteredDatas[index];
                bool newSelected = DrawListItem(selected, filteredDatas[index]);
                if (newSelected != selected && newSelected == true)
                {
                    _selectedIndex = index;
                    _selectedData = filteredDatas[index];
                    GUIUtility.keyboardControl = 0;
                    HandleUtility.Repaint();
                }
            }

            //GUILayout.ExpandHeight(true);
            GUILayout.EndScrollView();
            GUILayout.EndVertical();
        }

        void FilterList()
        {
            _listChanged = _listChanged || datas.Count != filteredDatas.Count;

            if (_listChanged || searchFilter != newSearchFilter)
            {
                filteredDatas.Clear();
                searchFilter = newSearchFilter;

                if (newSearchFilter.Length > 0)
                {
                    filteredDatas.AddRange(datas.FindAll(d => d.name.Contains(searchFilter)));
                    _selectedIndex = Mathf.Clamp(_selectedIndex, 0, filteredDatas.Count - 1);
                }
                else
                {
                    //_selectedIndex = filteredDatas.FindIndex(d => d == _selectedData);
                    filteredDatas.AddRange(datas);
                    for (int i = 0; i < filteredDatas.Count; i++)
                        if (filteredDatas[i] == _selectedData)
                            _selectedIndex = i;
                }
   
                //HandleUtility.Repaint();
            }
        }

        protected virtual bool DrawListItem(bool selected, Data data)
        {
            //return GUILayout.Toggle(selected, data.name, GUILayout.ExpandWidth(true));
            //return GUILayout.Toggle(selected, data.name, (GUIStyle)"IN ThumbnailSelection", GUILayout.ExpandWidth(true));
            //return GUILayout.Toggle(selected, data.name, (GUIStyle)"MenuItemMixed", GUILayout.ExpandWidth(true));
            return GUILayout.Toggle(selected, data.name, (GUIStyle)"PR Label", GUILayout.ExpandWidth(true));
            //return GUILayout.Toggle(selected, data.name, (GUIStyle)"ObjectPickerResultsEven", GUILayout.ExpandWidth(true));

        }

        void DrawRightPanel()
        {
            rightPanel = new Rect(position.width * sizeRatio , LineHeight, position.width * (1- sizeRatio) , position.height - LineHeight);


            GUILayout.BeginArea(rightPanel);

            EditorGUILayout.BeginHorizontal(EditorStyles.toolbar, GUILayout.ExpandWidth(true));
            EditorGUILayout.LabelField("");
            EditorGUILayout.EndHorizontal();

            EditorGUILayout.LabelField("");
            EditorGUI.indentLevel++;

            EditorGUILayout.BeginVertical(/*"Box",*/ GUILayout.ExpandHeight(true), GUILayout.ExpandWidth(true));
            //if (_editMode)
            //{
            _editScrollPos = EditorGUILayout.BeginScrollView(_editScrollPos, GUILayout.ExpandWidth(true), GUILayout.ExpandHeight(true));

            DrawData(_selectedIndex);

            EditorGUILayout.EndScrollView();

            EditorGUILayout.EndVertical();
            EditorGUI.indentLevel--;

            GUILayout.EndArea();

        }

        void DrawData(int i)
        {
            if ((datas == null || /*datas.Count */filteredDatas.Count < 1) ||
                i < 0 || i >= filteredDatas.Count /*|| i >= datas.Count */)
            {
                EditorGUILayout.LabelField("Wrong ID or empty List");
                return;
            }

            if (_selectedData == null)
                return;

            //var d = _selectedData; 

            var d = filteredDatas[i];

            //GUILayout.BeginHorizontal(GUILayout.ExpandWidth(true), GUILayout.ExpandHeight(true));
            GUILayout.BeginVertical(GUILayout.ExpandWidth(true), GUILayout.ExpandHeight(true));

            if (EditorUtils.DrawFoldOut(d.ToString() + " " + d.name + ":	", d.uniqueId))
                EditorUtils.DrawClass(d);

            GUILayout.EndVertical();
            //GUILayout.EndHorizontal();
        }

        void DrawResizer()
        {
            resizer = new Rect((position.width * sizeRatio) - .5f, LineHeight, 10f, position.height);
            GUILayout.BeginArea(new Rect(resizer.position + (Vector2.right *.5f), new Vector2(2, position.height /*- LineHeight*2*/)), resizerStyle);
            GUILayout.EndArea();
            EditorGUIUtility.AddCursorRect(resizer, MouseCursor.ResizeHorizontal);
        }

        void OnGUI()
        {
            DrawFileLoadAndSaver();

            DrawLeftPanel();
            DrawResizer();
            DrawRightPanel();

            EditorGUILayout.Separator();

            ProcessEvents(Event.current);
            if (GUI.changed) Repaint();

        }

        void ProcessEvents(Event e)
        {
            switch (e.type)
            {
                case EventType.MouseDown:
                    if (e.button == 0 && resizer.Contains(e.mousePosition))
                        isResizing = true;
                        //Debug.Log("MOUSE IS DOWN");
                    break;
                case EventType.MouseUp:
                    isResizing = false;
                    break;
            }

            Resize(e);
        }

        void Resize(Event e)
        {
            if (isResizing)
            {
                sizeRatio = e.mousePosition.x / position.width;
                Repaint();
            }
        }

        void OnEnable()
        {
            //_tab = Tab.Actors;
            //Reset();
            resizerStyle = new GUIStyle();
            //resizerStyle.normal.background = EditorStyles.helpBox;
            //resizerStyle.normal.background = EditorGUIUtility.Load("icons/d_AvatarBlendBackground.png") as Texture2D;
            resizerStyle.normal.background = EditorGUIUtility.Load("BackgroundWithInnerShadow.png") as Texture2D;
            //resizerStyle.normal.background = EditorGUIUtility.Load("box.png") as Texture2D;
            LineHeight = EditorGUIUtility.singleLineHeight + extra;

            if (datas == null)
                datas = new List<Data>();

            UpdateDataMenu();
        }

        void DrawCreateMenu()
        {

        }


        void UpdateDataMenu()
        {
            var assembly = System.Reflection.Assembly.Load(
                                    new System.Reflection.AssemblyName("Assembly-CSharp"));

            // Get all classes derived from ScriptableObject
            var allDataObjects = (from t in assembly.GetTypes()
                                  where t.IsSubclassOf(typeof(Data))
                                  select t).ToArray();

            this.Types = allDataObjects;
        }

        void CreateData()
        {
            var menu = new GenericMenu();

            for (var i = 0; i < names.Length; i++)
            {
                var dataName = names[i];
                //Debug.Log("Created " + dataName);

                menu.AddItem(new GUIContent(dataName), false, () =>
                {
                    //var d1 = (Data)System.Activator.CreateInstance(types[i]);
                    //var d1 = Manager.InstanceDataFromName(types[i].FullName);
                    var d1 = DataManager.InstanceDataFromName(dataName);
                    d1.name = "New " + dataName + " " + d1.intUniqueId;
                    datas.Add(d1);
                    _listChanged = true;
                    FilterList();
                });
                //{ keyProp.stringValue = phraseName; keyProp.serializedObject.ApplyModifiedProperties(); });
            }

            if (menu.GetItemCount() > 0)
            {
                menu.ShowAsContext();
                //menu.DropDown(right);
            }
            else
            {
                Debug.LogWarning("Your scene doesn't contain any key phrases, so the phrase name list couldn't be created.");
            }


            //var d1 = new DerivedData();
            //d1.name = "New Data " + d1.intUniqueId;
            //datas.Add(d1);
        }

        void SaveChanges()
        {
            //Game.Save();
            Reset();

        }

        void Reset()
        {
            _selectionScrollPos = Vector2.zero;
            _editScrollPos = Vector2.zero;
            _selectedIndex = -1;
            GUI.FocusControl(null);                 //Removes the focus from text boxes and other UI controls (elements)
        }

        void DrawFileLoadAndSaver( )
        {
            //EditorGUILayout.Separator();
            EditorGUILayout.BeginHorizontal(GUI.skin.FindStyle("Toolbar"));


            //EditorGUILayout.PrefixLabel("Data List File");
            filePathAndName = EditorUtils.DrawFilterField(filePathAndName, "Data List File");

            if (GUILayout.Button("New File", EditorStyles.miniButton, GUILayout.Width(64)))    // Scratch all
            {
                _selectedIndex = 0;
                _selectedData = null;
                filteredDatas.Clear();
                datas.Clear();
                datas = new List<Data>();
            }

            GUILayout.Space(8);

            if (GUILayout.Button("Load Data List File", EditorStyles.miniButtonLeft, GUILayout.Width(128)))
            {

                if (string.IsNullOrEmpty(filePathAndName))          // Find where we want to Load the file
                    filePathAndName = UnityEditor.EditorUtility.OpenFilePanel(
                        "Choose a Data List File from where to Load Data", Serializer.GetCurrentPath(),
                       Serializer.CurrentSerializer.DefaultExtension.Trim('.') );   

                if (string.IsNullOrEmpty(filePathAndName))
                    return;

                //_selectedIndex = 0;
                //_selectedData = null;
                filteredDatas.Clear();
                datas.Clear();                          // <<<<<<<<<<<<<<<
                datas.AddRange(DataManager.LoadAt<Data>(filePathAndName));
            }

            if (GUILayout.Button("Save Data List File", EditorStyles.miniButtonRight, GUILayout.Width(128)))
            {

                if (string.IsNullOrEmpty(filePathAndName) || !System.IO.File.Exists(filePathAndName))
                {
                    //filePathAndName = UnityEditor.EditorUtility.SaveFilePanelInProject(
                    //    "Choose a Data List File name where to Save", Serializer.GetCurrentPath(),
                    //    Serializer.CurrentSerializer.DefaultExtension.Trim('.'), "Check Ur File!");

                    filePathAndName = UnityEditor.EditorUtility.SaveFilePanelInProject(
                        "Choose a Data List File name where to Save", "Data", "xml", "Set file name");
                    //filePathAndName = UnityEditor.EditorUtility.SaveFilePanelInProject(
                    //"Choose a Data List File name where to Save", "Data", "xml",
                    //"Chaque Tu Archivo!", Application.dataPath);
                }

                if (!string.IsNullOrEmpty(filePathAndName) && datas != null)
                    //DataManager.SaveAt(datas.ToArray(), filePathAndName);         
                    SaveData();

            }

            EditorGUILayout.EndHorizontal();
            EditorGUILayout.Separator();
        }


        void SaveData()
        {

            if (datas.Count == 0) return;
            //Debug.Log("Commiting Entity Definitions");
            // Handle duplicate names
            string dupNameString = "";
            HashSet<string> duplicateNames = new HashSet<string>();
            HashSet<string> nameList = new HashSet<string>();

            List<Data> DataList = datas.Cast<Data>().ToList();
            foreach (Data dataItem in DataList)
            {
                //if (data.Empty) continue;
                if (nameList.Contains(dataItem.name))
                {
                    duplicateNames.Add(dataItem.name);
                    dupNameString += dataItem.name + " ";
                    continue;
                }
                nameList.Add(dataItem.name);
            }

            if (duplicateNames.Count > 0)
            {
                int res = EditorUtility.DisplayDialogComplex("Commit",
                    "Duplicate names found in entity definition library. You won't be able to select duplicates in the interface.\n" +
                    "Duplicates: " + dupNameString,
                    "Auto-rename",
                    "Cancel",
                    "Force commit");

                if (res == 1) return; // cancel
                if (res == 0)
                {
                    // auto rename
                    HashSet<string> firstOccurances = new HashSet<string>();
                    foreach (Data dataItem in DataList)
                    {
                        //if (data.Empty) continue;
                        string name = dataItem.name;
                        if (duplicateNames.Contains(name))
                        {
                            if (!firstOccurances.Contains(name))
                            {
                                firstOccurances.Add(name);
                            }
                            else
                            {
                                // find suitable name
                                int i = 1;
                                string n = "";
                                do
                                {
                                    n = string.Format("{0} {1}", name, i++);
                                } while (nameList.Contains(n));
                                name = n;

                                nameList.Add(name);
                                dataItem.name = name;
                            }
                        }
                    }

                    Repaint();
                }
            }

            //EditorUtility.SetDirty(motinDatas);

            DataManager.SaveAt(DataList.ToArray(), filePathAndName);
            //DataManager.SaveAt(datas.ToArray(), filePathAndName);


            //loadedData = DataList;
            datas = DataList;
            if (/*this != null &&*/ datas != null)
                FilterList();


            //AssetDatabase.SaveAssets();
            //AssetDatabase.Refresh();




            ///-------------------------------------------------------///

            //DataManager.SaveAt(datas.ToArray(), filePathAndName);
        }

    }

}
