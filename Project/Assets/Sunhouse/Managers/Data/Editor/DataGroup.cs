﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System;

namespace Sun.DataBase
{
    [System.Serializable]
    public class DataGroup<T>  where T : Data
    {
        public System.Type GetDataType() { return typeof(T);  }

        [SerializeField]
        public List<T> elements { get; set; }                       //List to hold all the elements of the generic type T

        public int Length { get { return elements.Count; } }        //Number of elements in the "elements" list

        /// Default Constructor
        public DataGroup()
        {
            elements = new List<T>();
        }

        /// Adds the element to "elements" if it is not already in "elements"
        public void Add(T element)
        {
            if (!Contains(element))
            {
                elements.Add(element);
            }
        }

        public void Add(Data element)
        {
            T elemCast = (T)element;
            if (!Contains(elemCast))
                elements.Add(elemCast);
        }

        /// Determines whether or not the element exists in "elements"
        public bool Contains(T element)
        {
            return FindElement(element.name) >= 0;
        }

        /// Returns the index of the element if it exists in "elements"
        public int IndexOf(T element)
        {
            return IndexOf(element.name);
        }

        /// Returns the index of the element if it exists in "elements"
        public int IndexOf(string name)
        {
            return FindElement(name);
        }

        /// Returns the element if it exists in "elements"
        public T Get(string name)
        {
            int i = FindElement(name);

            if (i >= 0)
                return elements[i];

            return default(T);
        }

        /// Returns the element if it exists in "elements"
        public T GetAt(int index)
        {
            if (index >= 0 && index < elements.Count)
                return elements[index];

            return default(T);
        }

        /// Replaces one element with another element in "elements" if the index is within range, otherwise the new element is added
        public void Replace(int index, T element)
        {
            if (index >= 0 && index < elements.Count)
                elements[index] = element;
            else
                Add(element);
        }

        public void Replace(int index, Data element)
        {
            if (index >= 0 && index < elements.Count)
                elements[index] = (T)element;
            else
                Add(element);
        }

        /// Removes the element from "elements"
        public void Remove(T element)
        {
            elements.Remove(element);
        }

        /// Removes the element from "elements"
        public void Remove(string name)
        {
            int i = FindElement(name);

            if (i >= 0)
                RemoveAt(i);
        }

        /// Removes the element from "elements"
        public void RemoveAt(int index)
        {
            elements.RemoveAt(index);
        }

        /// Empties "elements" by removing all of the elements
        public void Clear()
        {
            elements.Clear();
        }

        /// Returns the index of the element if it exists in "elements"
        private int FindElement(string name)
        {
            for (int i = 0; i < elements.Count; i++)
            {
                if (elements[i].name == name)
                    return i;
            }

            return -1;
        }

        ///// Saves the Database to an XML file at the given path
        //public void Save<U>(string path) where U : Database<T>
        //{
        //    var serializer = new XmlSerializer(typeof(U));
        //    using (var stream = new FileStream(path, FileMode.Create))
        //    {
        //        serializer.Serialize(stream, this);
        //    }
        //}

        ///// Loads the Database from an XML file at the given path
        //public static U Load<U>(string path) where U : Database<T>, new()
        //{
        //    if (File.Exists(path))
        //    {
        //        var serializer = new XmlSerializer(typeof(U));
        //        using (var stream = new FileStream(path, FileMode.Open))
        //        {
        //            return serializer.Deserialize(stream) as U;
        //        }
        //    }
        //    return new U();
        //}


        //protected virtual void RaiseOnFieldValueChanged(object instance, System.Reflection.FieldInfo field) { isDirty = true; FieldValueChanged(instance, field); if (OnFieldValueChanged != null) OnFieldValueChanged(instance, field); }

        public void DrawAt(int i)
        {

            if(i < 0 || i >= elements.Count)
            {
                EditorGUILayout.LabelField("Wrong ID");
                return;
            }


            var d = elements[i];

            GUILayout.BeginVertical(GUILayout.ExpandWidth(true), GUILayout.ExpandHeight(true));


            if ( EditorUtils.DrawFoldOut(d.ToString() + " " + d.name + ":	", d.uniqueId))
                EditorUtils.DrawClass(d);

            GUILayout.EndVertical();

        }

//        Dictionary<string, bool> UnFoldings = new Dictionary<string, bool>();



//        public void DrawClass(object value)
//        {
//            //EditorGUILayout.LabelField("class type " + value.ToString() + ":	");
//            EditorGUI.indentLevel+= 2;
//            DrawFields(value);
//            EditorGUI.indentLevel-= 2;


//            EditorGUILayout.Separator();
//        }

//        protected void DrawFields(object value )
//        {
//            bool changed = false;
//            //GUILayout.BeginHorizontal();
//            GUILayout.BeginVertical(/*"PR Ping"*/);
//            //GUILayout.BeginVertical("Tooltip");

//            System.Type type = value.GetType();
//            System.Reflection.FieldInfo[] fields = type.GetFields();
//            System.Reflection.FieldInfo field = null;

//            for (int i = fields.Length - 1; i >= 0; i--)
//            {
//                field = fields[i];
////				Debug.Log("Draw Field "  + field.Name + " " + field.FieldType.ToString());
//                if (field.IsPublic && !field.IsStatic &&
//                    field.GetCustomAttributes(typeof(HideInInspector), false).Length == 0
//                    /*&& !hideFieldsList.Contains(field.Name)*/)
//                {
//                    if (DrawDefaultField(value, field))
//                        changed = true;
//                }
//            }

//            GUILayout.EndVertical();
//            //GUILayout.EndHorizontal();

//            //if (changed)
//            //    RaiseOnDataChanged();
//        }

//        protected bool DrawDefaultField(object value, System.Reflection.FieldInfo field)
//        {
//            object oldValue = field.GetValue(value);
//            object newValue = oldValue;
//            if (field.GetCustomAttributes(typeof(ShowOnly), false).Length == 0 /*&& !readOnlyFieldsList.Contains(field.Name)*/)
//            {
//                if (field.FieldType == typeof(int))
//                {
//                    if (field.GetCustomAttributes(typeof(SunDataEnumField), false).Length != 0)
//                    {
//                        //    int tmpInt = DrawMotinDataEnumIntField(field, (int)oldValue);
//                        //    if (tmpInt != (int)oldValue)
//                        //        newValue = tmpInt;
//                    }
//                    else
//                    {
//                        newValue = (object)EditorGUILayout.IntField(MakeLabel(field), (int)oldValue);
//                    }
//                }
//                if (field.FieldType == typeof(bool))
//                {
//                    newValue = (object)EditorGUILayout.Toggle(MakeLabel(field), (bool)oldValue);
//                }
//                else if (field.FieldType == typeof(float))
//                {
//                    newValue = (object)EditorGUILayout.FloatField(MakeLabel(field), (float)oldValue);
//                }
//                else if (field.FieldType == typeof(string))
//                {
//                    //if(field.GetCustomAttributes(typeof(MotinEditorSoundEnumField),false).Length!=0)
//                    //{
//                    //	SoundDefinitions.Sounds tmpSound = (SoundDefinitions.Sounds)EditorGUILayout.EnumPopup(field.Name,MotinUtils.StringToEnum<Sun.AudioLoad.Clips>((string)oldValue));
//                    //	if(tmpSound.ToString() != (string)oldValue)
//                    //	{
//                    //		newValue = tmpSound.ToString(); 
//                    //		//EditorUtility.SetDirty(stringComponent);
//                    //		//AssetDatabase.SaveAssets();
//                    //	}
//                    //}
//                    //else if(field.GetCustomAttributes(typeof(MotinEditorLocalizationEnumField),false).Length!=0)
//                    //{
//                    //	MotinStrings tmpString = (MotinStrings)EditorGUILayout.EnumPopup(field.Name,MotinUtils.StringToEnum<MotinStrings>((string)oldValue));
//                    //	if(tmpString.ToString() != (string)oldValue)
//                    //	{
//                    //		newValue = tmpString.ToString(); 
//                    //		//EditorUtility.SetDirty(stringComponent);
//                    //		//AssetDatabase.SaveAssets();
//                    //	}
//                    //}
//                    //else if (field.GetCustomAttributes(typeof(MotinEditorEnumField), false).Length != 0)
//                    //{
//                    //    MotinEditorEnumField motinEnumAttr = (MotinEditorEnumField)field.GetCustomAttributes(typeof(MotinEditorEnumField), false)[0];

//                    //    string tmpString = DrawEnumField(field.Name, motinEnumAttr.enumeration, (string)oldValue);
//                    //    if (tmpString != (string)oldValue)
//                    //    {
//                    //        newValue = tmpString;
//                    //        //EditorUtility.SetDirty(stringComponent);
//                    //        //AssetDatabase.SaveAssets();
//                    //    }
//                    //}
//                    //else if (field.GetCustomAttributes(typeof(MotinEditorMotinDataEnumField), false).Length != 0)
//                    //{

//                    //    string tmpString = DrawMotinDataEnumField(field, (string)oldValue);
//                    //    if (tmpString != (string)oldValue)
//                    //    {
//                    //        newValue = tmpString;
//                    //    }
//                    //}
//                    //else
//                    newValue = (object)EditorGUILayout.TextField(MakeLabel(field), (string)oldValue);

//                }
//                else if (field.FieldType == typeof(Vector2))
//                {
//                    newValue = (object)EditorGUILayout.Vector2Field(field.Name, (Vector2)oldValue);
//                }
//                else if (field.FieldType == typeof(Vector3))
//                {
//                    newValue = (object)EditorGUILayout.Vector3Field(field.Name, (Vector3)oldValue);
//                }
//                else if (field.FieldType == typeof(Vector4))
//                {
//                    newValue = (object)EditorGUILayout.Vector4Field(field.Name, (Vector4)oldValue);
//                }
//                else if (field.FieldType == typeof(Rect))
//                {
//                    newValue = (object)EditorGUILayout.RectField(field.Name, (Rect)oldValue);
//                }
//                else if (field.FieldType == typeof(Color))
//                {
//                    newValue = (object)EditorGUILayout.ColorField(field.Name, (Color)oldValue);
//                }
//                else if (field.FieldType.IsGenericType && field.FieldType.GetGenericTypeDefinition() == typeof(List<>))
//                {
//                    //Debug.Log("ES UNA LISTA " + field.FieldType.ToString() + " " + field.FieldType.GetGenericArguments()[0].ToString());
//                    EditorGUILayout.LabelField("ES UNA LISTA ");
//                    /*
//                    if (MotinUtils.IsTypeDerivedFrom(field.FieldType.GetGenericArguments()[0], typeof(MotinData)))
//                    {
//                        int editorIndex = GetEditorIndex(oldValue);
//                        MotinEditor tmpEditor = null;
//                        if (editorIndex == -1)
//                        {

//                            tmpEditor = CreateInitializedEditor(field, oldValue);

//                        }
//                        else
//                        {
//                            tmpEditor = motinEditors_[editorIndex];
//                        }
//                        tmpEditor.editorFieldName = field.Name;
//                        tmpEditor.editorName = field.Name;
//                        tmpEditor.target = oldValue;
//                        tmpEditor.Draw(tmpEditor.editorContentRect, false);
//                        newValue = oldValue;
//                    }
//                    */
//                }
//                else if (MotinUtils.IsTypeDerivedFrom(field.FieldType, typeof(UnityEngine.Object)))
//                {
//                    newValue = (object)EditorGUILayout.ObjectField(field.Name, (UnityEngine.Object)oldValue, field.FieldType, true);
//                }
//                else if (field.FieldType.IsEnum)
//                {
//                    newValue = EditorGUILayout.EnumPopup(field.Name, (System.Enum)System.Enum.ToObject(field.FieldType, oldValue));
//                }
//                else if (field.FieldType.IsClass)
//                {
//                    //EditorGUILayout.LabelField("class " + field.Name + ":	");
//                    var fold = DrawUnFold( oldValue.ToString() + " " + field.Name + ":	");

//                    if (fold)
//                    {
//                        DrawClass(oldValue);
//                        newValue = oldValue;
//                    }
          

//                    //DrawFields(oldValue);
//                    //newValue = oldValue;
//                    /*
//                    if (MotinUtils.IsTypeDerivedFrom(field.FieldType, typeof(MotinData)))
//                    {
//                        int editorIndex = GetEditorIndex(oldValue);
//                        MotinEditor tmpEditor = null;
//                        if (editorIndex == -1)
//                        {
//                            tmpEditor = CreateInitializedEditor(field, oldValue);

//                        }
//                        else
//                        {
//                            tmpEditor = motinEditors_[editorIndex];
//                        }
//                        tmpEditor.editorFieldName = field.Name;
//                        tmpEditor.target = oldValue;
//                        tmpEditor.editorName = field.Name;
//                        tmpEditor.Draw(tmpEditor.editorContentRect, false);
//                        newValue = oldValue;
//                    }
//                    */
//                }

//                if (newValue == null && oldValue != null)
//                {
//                    //Debug.Log("DEFAULT CHANGED  "+ field.Name + "  " + newValue.ToString()+ "  " + oldValue.ToString());
//                    field.SetValue(value, newValue);
//                    //FieldValueChanged(value,field);

//                    return true;
//                }
//                else if (newValue != null && !newValue.Equals(oldValue))
//                {
//                    field.SetValue(value, newValue);
//                    //FieldValueChanged(value,field);

//                    return true;
//                }
//                else
//                {
//                    return false;
//                }
//            }
//            else
//            {

//                //GUILayout.BeginHorizontal();
//                ////GUILayout.Space(10);
//                //GUILayout.Label(field.Name + ":	");
//                //GUILayout.FlexibleSpace();
//                //GUILayout.Label(field.GetValue(value).ToString());
//                //GUILayout.EndHorizontal();

//                EditorGUILayout.BeginHorizontal();
//                EditorGUILayout.LabelField(field.Name + ":	");
//                GUILayout.FlexibleSpace();
//                GUILayout.Label(field.GetValue(value).ToString());
//                EditorGUILayout.EndHorizontal();

//                return false;

//            }
//        }

//        bool DrawUnFold(string fieldName)
//        {
//            if (!UnFoldings.ContainsKey(fieldName))
//                UnFoldings.Add(fieldName, true);

//            var origFontStyle = EditorStyles.foldout.fontStyle;
//            EditorStyles.foldout.fontStyle = FontStyle.Bold;

//            UnFoldings[fieldName] =
//                EditorGUILayout.Foldout(UnFoldings[fieldName], fieldName, true);

//            EditorStyles.foldout.fontStyle = origFontStyle;

//            return UnFoldings[fieldName];
//        }

//        protected GUIContent MakeLabel(System.Reflection.FieldInfo field)
//        {
//            GUIContent guiContent = new GUIContent();
//            guiContent.text = field.Name /*.SplitCamelCase()*/;
//            object[] descriptions =
//               field.GetCustomAttributes(typeof(System.ComponentModel.DescriptionAttribute), true);

//            if (descriptions.Length > 0)
//            {
//                //just use the first one.
//                guiContent.tooltip =
//                   (descriptions[0] as System.ComponentModel.DescriptionAttribute).Description;
//            }

//            return guiContent;
//        }


        //////////////////////////////////////////////////////////////////////////////////////////////////////
        //////////////////////////////////////////////////////////////////////////////////////////////////////
    }

    public class SunDataEnumField : Attribute
    {

        public string filePath = "";
        public SunDataEnumField(string file)
        {
            filePath = file;
        }
    }

}
