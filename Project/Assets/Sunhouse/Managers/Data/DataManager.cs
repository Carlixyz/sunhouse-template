﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using UnityEngine;


namespace Sun.DataBase
{
    //public class Manager 
    public class DataManager
    {
        public static Serializer.PathType currentPath = Serializer.PathType.DataPath;


        ///---------------------------------------------------------------------------------///

        public static void Save(Data[] datas, string fileName = "DataBase")
        {
            Serializer.Save(datas, fileName);
        }

        public static T[] Load<T>(string fileName = "DataBase") /*where T : new() //*/ where T : Data
        {
            //Data[] loadedData = Load(filePath);
            //if (loadedData == null)
            //    return null;

            //T[] resultArray = new T[loadedData.Length];

            //for (int i = 0; i < loadedData.Length; i++)
            //    resultArray[i] = (T)loadedData[i];

            //return resultArray;

            return Serializer.Load<object>(fileName) as T[]; ;
        }

        public static Data[] Load(string fileName = "DataBase") 
        {
            return Serializer.Load<object>(fileName) as Data[]; ;
        }

        public static void SaveAt(Data[] datas, string fileNamePath)
        {
            Serializer.SaveAt(datas, fileNamePath);
        }

        public static T[] LoadAt<T>(string fileNamePath) where T : Data
        {
            return Serializer.LoadAt<object>(fileNamePath) as T[]; ;
        }

        public static System.Type GetSerializableDataType(string name)
        {
            return System.Reflection.Assembly.Load("UnityEditor.dll").GetType(name);
            //return Type.GetType(name);
            //System.Activator.CreateInstance(Type.GetType("name"));
        }

        public static Data InstanceDataFromName(string name)
        {
            Data instance = (Data)System.Activator.CreateInstance(Type.GetType(name));
            return instance;
        }

        public static object InstanceFromName(string name)
        {
            //object instance = System.Activator.CreateInstance(Type.GetType(name));
            //return instance;
            return System.Activator.CreateInstance(Type.GetType(name));
        }

        public static string[] GetDataNames(string filePath)
        {
            Data[] datas = Load(filePath);
            string[] dataNames = { "EMPTY" };

            if (datas == null)
                return dataNames;

            dataNames = new string[datas.Length + 1];

            for (int i = 0; i < datas.Length; i++)
                dataNames[i] = datas[i].name;

            dataNames[datas.Length] = "EMPTY";

            return dataNames;

        }

        public static int[] GetDataIntUniqueIds(string filePath)
        {
            Data[] datas = Load(filePath);
            int[] dataIds = { -1 };

            if (datas == null)
                return dataIds;

            dataIds = new int[datas.Length + 1];

            for (int i = 0; i < datas.Length; i++)
                dataIds[i] = datas[i].intUniqueId;

            dataIds[datas.Length] = -1;

            return dataIds;
        }

        public static T GetDataByName<T>(string name, string filePath) where T : Data
        {
            //T[] datas = Load<T>(filePath);

            /*
            foreach (T data in Load<T>(filePath))
                if (data.name == name)
                    return data;

            return null;
            */
            Data[] datas = Load(filePath);
            foreach (Data data in datas)
            {
                if (data.name == name)
                    return (T)data;
            }

            return null;
        }

        

        //-----------------------------------------------------------------------------//

        //public static void SaveFields(Data[] datas, string fileName = "DataBase")
        //{
        //    //Serializer.SaveAt(datas, )
        //    sDatas = null;
        //    sDatas = new List<object>();

        //    foreach (Data data in datas)
        //        sDatas.Add(Serialization.Adapter.ToSerializableObject(data));

        //    Serializer.Save(sDatas, fileName);
        //}

        //public static Data[] LoadFields(string fileName = "DataBase")
        //{
        //    //		Debug.Log("LOAD FILE " + filePath);
        //    sDatas = null;
        //    //sDatas = new List<object>();

        //    sDatas = Serializer.Load<List<object>>(fileName);
        //    List<Data> datas = new List<Data>();

        //    foreach (var sdata in sDatas)
        //        datas.Add((Data)Serialization.Adapter.FromSerializableObject(sdata));

        //    return datas.ToArray();
        //}


    }

    /*
    void Test()
    {

        // Loading all Data array
        Data[] d = DataManager.Load("Data/SerializableDemoDataFile.xml");

        // Parsing Data Array
        foreach (var info in d)
        {
            Debug.Log("info is " + info.name);
            if (info.name == "Checho")
            {
                MotinGames.DemoApp.Data.DemoSerializableData oneData = (MotinGames.DemoApp.Data.DemoSerializableData)info;
                oneData.testString = "changed!";
                Debug.Log("checho is " + oneData.testString);
            }
        }
        // Direct Access
        MotinGames.DemoApp.Data.DemoSerializableData pepoData = MotinDataManager.GetDataByName<MotinGames.DemoApp.Data.DemoSerializableData>("Pepo", "Data/SerializableDemoDataFile.xml");

        Debug.Log(pepoData.name + " is " + pepoData.testFloat);
        pepoData.testFloat = 747;
        Debug.Log(pepoData.name + " now is " + pepoData.testFloat);

        // Write to Item List
        MotinData pepoItem = System.Array.Find(d, data => data.name == pepoData.name);
        pepoItem.CopyFromData(pepoData);

        //MotinData pepoItem = System.Array.Find(d, data => data.name == pepoData.name);
        //((MotinGames.DemoApp.Data.DemoSerializableData)pepoItem).testFloat = pepoData.testFloat;

        //int index = System.Array.FindIndex(d, data => data.name == pepoData.name);
        //((MotinGames.DemoApp.Data.DemoSerializableData)d[index]).testFloat = pepoData.testFloat;

        // Saving all Data array
        DataManager.Save("Data/SerializableDemoDataFile.xml", d);
    }
    */

}


