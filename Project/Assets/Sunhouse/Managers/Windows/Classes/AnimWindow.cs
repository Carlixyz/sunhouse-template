using UnityEngine;
using System.Collections;

namespace Sun
{
    [ExecuteInEditMode]
    [RequireComponent(typeof(Animation))]
    public class AnimWindow : GenericWindow
    {
        public Animation Anim;

        public override void OnEnable()
        {
            base.OnEnable();
            Anim = GetComponent<Animation>();
        }

        //[HideInInspector]
        public string animOpen = "show";
        public string AnimOpen
        {
            get { return animOpen; }
            set { animOpen = value; }
        }
        //[HideInInspector]
        public string animIdle = "idle";
        public string AnimIdle
        {
            get { return animIdle; }
            set { animIdle = value; }
        }

        //[HideInInspector]
        public string animClose = "dismiss";
        public string AnimClose
        {
            get { return animClose; }
            set { animClose = value; }
        }


        public override void OpenAnim(Vector3 position, System.Action actionDelegate = null)
        {
            transform.localPosition = position;
            if (animOpen.Length == 0 || runningTransition)
            {
                RaiseActionDelegate(actionDelegate);
                return;
            }

            if (currentActionDelegate != null)
            {
                RaiseActionDelegate(actionDelegate);
                return;
            }

            runningTransition = true;
            currentActionDelegate = actionDelegate;

            ActivateChilds(true);

            if (Application.isPlaying)
                this.OnWillShow();

            if (Application.isPlaying)
            {
                SendEvent("WINDOW_OPEN");
                if (Anim)
                    Anim.Play(AnimOpen/*, OpenAnimComplete*/);
                //Anim.Play(AnimOpen/*, OpenAnimComplete*/);
            }
            else
            {
                runningTransition = false;
                RaiseCurrentActionDelegate();
            }
        }

        public virtual void OpenAnimComplete(string clipName)
        {

            runningTransition = false;

            SendEvent("WINDOW_OPENED");
            if (Anim)
                Anim.Play(clipName);

            if (Application.isPlaying)
                this.OnShowed();

            RaiseCurrentActionDelegate();
            raiseOnStateChanged((int)WindowStates.Opened);

        }

        public override void CloseAnim(System.Action actionDelegate)
        {
            if (animClose.Length == 0 || runningTransition)
            {
                RaiseActionDelegate(actionDelegate);
                return;
            }

            if (currentActionDelegate != null)
            {
                Debug.LogError("TRANSITION DIDNT FINISH " + this.name);
                RaiseActionDelegate(currentActionDelegate);
                return;
            }

            runningTransition = true;
            currentActionDelegate = actionDelegate;

            if (Application.isPlaying)
                this.OnWillDismiss();

            SendEvent("WINDOW_CLOSE");

            //if (TweenVisual)
            //    TweenVisual.startGroup(animClose);

            if (Anim)
                Anim.Play(animClose /*, CloseAnimComplete*/);
        }

        public virtual void CloseAnimComplete(string clipName)
        {
            runningTransition = false;
            SendEvent("WINDOW_CLOSE");

            if (Anim)
                Anim.Play(clipName);
            //Anim.Play(clipName);

            ActivateChilds(false);

            if (Application.isPlaying)
                this.OnDismissed();

            RaiseCurrentActionDelegate();
            raiseOnStateChanged((int)WindowStates.Closed);
        }

        public override void Pause()
        {
            if (Anim)
                Anim.Stop();
        }

        public override void Resume()
        {
            if (Anim)
                Anim.Play();
        }
    }

}