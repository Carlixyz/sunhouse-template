using UnityEngine;
using System.Collections;
using DentedPixel.LTEditor;

namespace Sun
{
    //[ExecuteInEditMode]
    [RequireComponent(typeof(LeanTweenVisual))] // To Use with Lean Tween
    public class TweenWindow : GenericWindow
    {

        public LeanTweenVisual TweenVisual;

        public override void OnEnable()
        {
            base.OnEnable();

            TweenVisual = GetComponent<LeanTweenVisual>();

            if (TweenVisual.groupList.Count < 1)
            {
                TweenVisual.playOnStart = false;

                var ltGroupOpen = new LeanTweenGroup();
                var ltGroupIdle = new LeanTweenGroup();
                var ltGroupClose = new LeanTweenGroup();

                ltGroupOpen.name = animOpen;
                ltGroupIdle.name = animIdle;
                ltGroupClose.name = animClose;

                TweenVisual.groupList.Add(ltGroupOpen);
                TweenVisual.groupList.Add(ltGroupIdle);
                TweenVisual.groupList.Add(ltGroupClose);
            }
        }

        //[HideInInspector]
        public string animOpen = "show";
        public string AnimOpen
        {
            get { return animOpen; }
            set { animOpen = value; }
        }
        //[HideInInspector]
        public string animIdle = "idle";
        public string AnimIdle
        {
            get { return animIdle; }
            set { animIdle = value; }
        }

        //[HideInInspector]
        public string animClose = "hide";
        public string AnimClose
        {
            get { return animClose; }
            set { animClose = value; }
        }


        public override void OpenAnim(Vector3 position, System.Action actionDelegate = null)
        {
            transform.localPosition = position;
            if (animOpen.Length == 0 || runningTransition)
            {
                RaiseActionDelegate(actionDelegate);
                return;
            }

            if (currentActionDelegate != null)
            {
                RaiseActionDelegate(actionDelegate);
                return;
            }

            runningTransition = true;
            currentActionDelegate = actionDelegate;

            ActivateChilds(true);

            if (Application.isPlaying)
                this.OnWillShow();

            if (Application.isPlaying)
            {
                //Debug.Log("OpenAnim " + animOpen);

                SendEvent("WINDOW_OPEN");
                if (TweenVisual)
                    // This only workif added CloseTweenComplete to the tween group's OnComplete Event 
                    //TweenVisual.startGroup(animOpen, gameObject);
                    TweenVisual.startGroup(animOpen, OpenTweenComplete() /*,gameObject*/);
            }
            else
            {
                runningTransition = false;
                RaiseCurrentActionDelegate();
            }
        }

        public UnityEngine.Events.UnityEvent OpenTweenComplete()  // This Works Automatically
        {
            runningTransition = false;
            //Debug.Log("OpenTweenComplete " + animIdle);
            SendEvent("WINDOW_OPENED");


            if (TweenVisual && !string.IsNullOrEmpty(animIdle))
                TweenVisual.startGroup(animIdle/*, gameObject*/);
            //TweenVisual.startGroup(animIdle, gameObject);

            if (Application.isPlaying)
                this.OnShowed();

            RaiseCurrentActionDelegate();
            raiseOnStateChanged((int)WindowStates.Opened);
            return null;
        }

        /*
        public virtual void OpenTweenComplete(string clipName)
        {
            runningTransition = false;

            Debug.Log("OpenTweenComplete " + clipName);

            SendEvent("WINDOW_OPENED");
            if (TweenVisual && !string.IsNullOrEmpty(clipName))
                TweenVisual.startGroup(clipName);
            //    TweenVisual.startGroup(animIdle);

            //// Jump up
            //var seq = LeanTween.sequence();
            //seq.append(LeanTween.scale(gameObject, new Vector3(0.85f, 1.2f, 1f), 0.6f)
            //                .setEase(LeanTweenType.easeOutElastic));
            //seq.append(LeanTween.scale(gameObject, new Vector3(1, 1, 1f), 0.5f)
            //        .setEase(LeanTweenType.easeOutBounce));

            if (Application.isPlaying)
                this.OnShowed();

            RaiseCurrentActionDelegate();
            raiseOnStateChanged((int)WindowStates.Opened);
        }
        */

        //--------------------------------.

        public override void CloseAnim(System.Action actionDelegate)
        {
            if (animClose.Length == 0 || runningTransition)
            {
                RaiseActionDelegate(actionDelegate);
                return;
            }

            if (currentActionDelegate != null)
            {
                Debug.LogError("TRANSITION DIDNT FINISH " + this.name);
                RaiseActionDelegate(currentActionDelegate);
                return;
            }

            runningTransition = true;
            currentActionDelegate = actionDelegate;

            if (Application.isPlaying)
                this.OnWillDismiss();

            SendEvent("WINDOW_CLOSE");

            if (TweenVisual && !string.IsNullOrEmpty(animClose))
            {
                // This only workif added CloseTweenComplete to the tween group's OnComplete Event 
                //TweenVisual.startGroupOnComplete(animClose);
                //TweenVisual.startGroupOnComplete(animClose, CloseTweenComplete());
                TweenVisual.startGroup(animClose, CloseTweenComplete());
                //Debug.Log("CloseAnim " + animClose);
            }
        }


        public UnityEngine.Events.UnityEvent CloseTweenComplete()  
        {
            runningTransition = false;
            //Debug.Log("CloseTweenComplete " + animClose);
            SendEvent("WINDOW_CLOSE");

            if (TweenVisual && !string.IsNullOrEmpty(animIdle))
                LeanTween.cancel(gameObject);
                //    TweenVisual.startGroup(animIdle, gameObject);

            ////ActivateChilds(false);

            if (Application.isPlaying)
                this.OnDismissed();

            RaiseCurrentActionDelegate();
            raiseOnStateChanged((int)WindowStates.Closed);

            //transform.localScale = Vector3.one;
            transform.rotation = Quaternion.identity;

            return null;
        }

        /*
        public virtual void CloseTweenComplete(string clipName)
        {
            runningTransition = false;
            SendEvent("WINDOW_CLOSE");

            Debug.Log("CloseTweenComplete " + clipName);

            if (TweenVisual && !string.IsNullOrEmpty(clipName))
                TweenVisual.startGroup(clipName, gameObject); //Anim.Play(clipName);

            ActivateChilds(false);

            if (Application.isPlaying)
                this.OnDismissed();

            RaiseCurrentActionDelegate();
            raiseOnStateChanged((int)WindowStates.Closed);
        }
        */

        public override void Pause()
        {
            if (TweenVisual)
                LeanTween.pause(gameObject);
            //if (Anim)
            //    Anim.Stop();
        }

        public override void Resume()
        {
            if (TweenVisual)
                LeanTween.resume(gameObject);
            //if (Anim)
            //    Anim.Play();
        }

    }


}