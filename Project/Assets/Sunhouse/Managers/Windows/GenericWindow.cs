using UnityEngine;
using System.Collections;

namespace Sun
{

    [ExecuteInEditMode]
    public class GenericWindow : MonoBehaviour
    {
        public enum WindowStates
        {
            Opened = 0,
            Closed = 1,
            Disabled = 2,
            Count = 3
        }

        public enum WindowAnchors
        {
            lower_left,
            lower_center,
            lower_right,
            middle_center,
            top_left,
            top_center,
            top_right
        }

        int windowState_ = (int)WindowStates.Closed;
        public int WindowState { get { return windowState_; } }

        private void Start()
        {
#if UNITY_EDITOR
            if (!Application.isPlaying)
                return;
#endif
            //Anim.PreviewValue("show", true, 0);
            WindowStart();
        }

        protected virtual void WindowStart() { }

        public virtual void OnEnable()
        {
#if UNITY_EDITOR
            if (!Application.isPlaying)
                return;
#endif
            //Anim = GetComponent<Animator>();

            ActivateChilds(false);
        }

        protected void ActivateChilds(bool value)
        {
            for (int i = 0; i < transform.childCount; i++)
            {
                transform.GetChild(i).gameObject.SetActive(value);
            }
        }

        protected virtual void SendEvent(string eventName)
        {
            /*
            //I use this function to sync the playmaker fsm
            if(windowFsm!=null && Application.isPlaying)
                windowFsm.SendEvent(eventName);
            */
        }


        ///  ------------------------------------------------------------------------ ///

        protected bool runningTransition = false;

        public System.Action<int> OnWindowStateChanged = null;

        [HideInInspector]
        public System.Action currentActionDelegate = null;

        protected void RaiseActionDelegate(System.Action actionDelegate)
        {
            if (actionDelegate != null)
                actionDelegate();
        }

        protected void RaiseCurrentActionDelegate()
        {
            if (currentActionDelegate != null)
            {
                System.Action auxDelegate = currentActionDelegate;

                runningTransition = false;
                currentActionDelegate = null;
                auxDelegate();
            }
        }

        public void raiseOnStateChanged(int state)
        {
            windowState_ = state;
            if (OnWindowStateChanged != null)
                OnWindowStateChanged(state);
        }

        ///  ------------------------------------------------------------------------ ///

        public virtual void Open() { this.Open(null); }   /// Events config in Editor 

        public virtual void Open(System.Action actionDelegate)
        {
            if (currentActionDelegate != null)
            {
                Debug.LogError("TRANSITION DIDNT FINISH " + this.name);
                RaiseActionDelegate(actionDelegate);
                return;
            }

            currentActionDelegate = actionDelegate;

            if (Application.isPlaying)
                this.OnWillShow();

            ActivateChilds(true);

            if (Application.isPlaying)
                SendEvent("WINDOW_OPENED");

            if (Application.isPlaying)
                this.OnShowed();

            RaiseCurrentActionDelegate();
            raiseOnStateChanged((int)WindowStates.Opened);
        }


        public void OpenAnim() { OpenAnim(null); }                                  /// Events config in Editor 

        public void OpenAnim(System.Action actionDelegate)
        {
            OpenAnim(this.transform.localPosition, actionDelegate);
        }

        public virtual void OpenAnim(Vector3 position, System.Action actionDelegate = null)
        {
            transform.localPosition = position;
            Open(actionDelegate);
        }

        /// - - - - -  - - -  - - - -  - - - -  - - - -  - - - -  - - - -  - -

        public void Close() { this.Close(null); }

        public void Close(System.Action actionDelegate)
        {
            if (Application.isPlaying)
                OnWillDismiss();

            SendEvent("START");

            ActivateChilds(false);

            if (Application.isPlaying)
                this.OnDismissed();

            raiseOnStateChanged((int)WindowStates.Closed);
            if (actionDelegate != null)
                actionDelegate();
        }


        public void CloseAnim() { CloseAnim(null); }

        public virtual void CloseAnim(System.Action actionDelegate)
        {
            Close();
        }

        /// - - - - -  - - -  - - - -  - - - -  - - - -  - - - -  - - - -  - -

        public virtual void Pause()
        {
            //if (Anim)
            //    Anim.Stop();
        }

        public virtual void Resume()
        {
            //if (Anim)
            //    Anim.Play();
        }

        protected void Update()
        {
            if (windowState_ == (int)WindowStates.Opened)
                WindowOpenUpdate();
        }

        protected void FixedUpdate()
        {
            if (windowState_ == (int)WindowStates.Opened)
                WindowOpenFixedUpdate();
        }

        protected void LateUpdate()
        {
            if (windowState_ == (int)WindowStates.Opened)
                WindowOpenLateUpdate();
        }

        /// 

        protected virtual void WindowOpenUpdate() { }
        protected virtual void WindowOpenFixedUpdate() { }
        protected virtual void WindowOpenLateUpdate() { }

        protected virtual void OnWillDismiss() { }
        protected virtual void OnDismissed() { }
        protected virtual void OnWillShow() { }
        protected virtual void OnShowed() { }


    }


}
