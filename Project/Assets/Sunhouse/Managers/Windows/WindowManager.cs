using UnityEngine;
using System.Collections.Generic;
//using HutongGames.PlayMaker;

namespace Sun
{
    [ExecuteInEditMode]
    public class WindowManager : Singleton<WindowManager>
    {
        [System.NonSerialized]
        public string definesNamesPath = "WindowsEnum.cs";

        void OnEnable()
        {
            //	Debug.Log("WINDOW MANAGER ON ENABLE");
            LoadWindows();
            EnableWindows();
        }

        void Start()
        {
            //if (Application.isPlaying && !string.IsNullOrEmpty(startWindowName))
            //    ShowWindow(startWindowName);
            //    //ShowWindow(startWindowName, true);

            if (Application.isPlaying && (WindowEnums)startWindowID != WindowEnums.None)
                OpenAnimWindow(startWindowID);
        }

        void OnDisable()
        {
            //isQuitting = true;
            //		Debug.Log("QUITTING");
        }

        [HideInInspector]
        public GenericWindow[] Windows;
        public WindowEnums currentWindowID;
        public WindowEnums startWindowID;

        public T GetWindow<T>(int id) where T : GenericWindow
        {
            return (T)GetWindow(id);
        }

        public GenericWindow GetWindow(int id)
        {
            //if (Windows == null || Windows.Length == 0 )
            if (id <= (int)WindowEnums.None || id > Windows.Length || Windows == null )
                return null;

            //return Windows[id];
            return Windows[(uint)(id-1)];
        }


        public void LoadWindows()
        {
            Windows = null;
            List<GenericWindow> foundWindows = new List<GenericWindow>();
            GenericWindow auxWindow;

            for (int i = 0; i < transform.childCount; i++)
            {
                auxWindow = transform.GetChild(i).gameObject.GetComponent<GenericWindow>();

                if (auxWindow != null)
                    foundWindows.Add(auxWindow);
            }

            Windows = foundWindows.ToArray();

#if UNITY_EDITOR
            if (!Application.isPlaying)
                UnityEditor.EditorUtility.SetDirty(this);
#endif
        }

        public void EnableWindows()
        {
            if (Windows == null || Windows.Length == 0)
                return;

            for (int i = 0; i < Windows.Length; i++)
            {
                if (Windows[i] != null)
                    Windows[i].gameObject.SetActive(true);
                else
                    Debug.LogWarning("WINDOW IS NULL");
            }

        }

        public void DisableWindows()
        {
            if (Windows == null || Windows.Length == 0)
                return;

            for (int i = 0; i < Windows.Length; i++)
            {
                Windows[i].gameObject.SetActive(false);
            }
        }


        /// ------------------------------------------------------------------- ///

        [EnumAction(typeof(WindowEnums))]   /// for Action configuration in Editor 
        public void OpenAnimWindow(int id)   { OpenAnimWindow((WindowEnums)id, null); }

        public void OpenAnimWindow(WindowEnums id, System.Action OnComplete = null)
        {
            GenericWindow sceneWindow = GetWindow((int)id);
            if (sceneWindow == null)
            {
                if (OnComplete != null)
                    OnComplete();
                return;
            }

            currentWindowID = id;

            sceneWindow.OpenAnim(OnComplete);
        }

        [EnumAction(typeof(WindowEnums))]   /// for Action configuration in Editor 
        public void OpenWindow(int id)
        {
            GenericWindow sceneWindow = GetWindow(id);
            if (sceneWindow == null)
                return;

            currentWindowID = (WindowEnums)id;

            sceneWindow.Open();
        }


        public void OpenAllWindows()
        {
            foreach (GenericWindow window in Windows)
                window.Open();
        }


        /// ------------------------------------------------------------------- ///


        [EnumAction(typeof(WindowEnums))]
        public void CloseAnimWindow(int id)
        {
            CloseAnimWindow((WindowEnums)id, null);
        }

        public void CloseAnimWindow(WindowEnums id, System.Action OnComplete/* = null*/)
        {
            GenericWindow sceneWindow = GetWindow((int)id);
            if (sceneWindow == null)
            {
                if (OnComplete != null)
                    OnComplete();
                return;
            }

            sceneWindow.CloseAnim(OnComplete);
        }

        [EnumAction(typeof(WindowEnums))]
        public void CloseWindow(int id)
        {
            GenericWindow sceneWindow = GetWindow(id);
            if (sceneWindow == null)
                return;

            sceneWindow.Close();
        }

        public void CloseAllWindows()
        {
            foreach (GenericWindow window in Windows)
                window.Close();
        }


        /// ------------------------------------------------------------------- ///


        public void ResumeWindow(int id)
        {
            GenericWindow sceneWindow = GetWindow(id);
            if (sceneWindow == null)
                return;

            sceneWindow.Resume();
        }

        public void PauseWindow(int id)
        {
            GenericWindow sceneWindow = GetWindow(id);
            if (sceneWindow == null)
                return;

            sceneWindow.Pause();
        }




    }


}