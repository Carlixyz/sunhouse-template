using UnityEngine;
using UnityEditor;
using System.Collections.Generic;

namespace Sun
{
    [CustomEditor(typeof(WindowManager))]
    public class WindowManagerInspector : Editor
    {

        private WindowManager winManager = null;
        private bool defaultInspectorFoldout = false;
   

        public override void OnInspectorGUI()
        {
            winManager = (WindowManager)target;
            GUILayout.BeginVertical(GUI.skin.box);

            if (GUILayout.Button("Update Windows Array"))
                winManager.LoadWindows();

            if (GUILayout.Button("Generate Window Enum"))
                GenerateWindowsEnum();

            if (GUILayout.Button("Close All Windows"))
                winManager.CloseAllWindows();

            if (GUILayout.Button("Open All Windows"))
                winManager.OpenAllWindows();

            if (winManager.Windows != null && winManager.Windows.Length > 0)
            {
                foreach (GenericWindow tmpWindow in winManager.Windows)
                {
                    if (tmpWindow == null)
                        continue;

                    GUILayout.BeginHorizontal(GUI.skin.box);
                    GUILayout.Label(tmpWindow.name);

                    //if (GUILayout.Button("Open"))
                        //tmpWindow.Open();

                    //if (GUILayout.Button("Close"))
                        //tmpWindow.Close();

                    if (GUILayout.Button("Open Anim"))
                        tmpWindow.OpenAnim();

                    if (GUILayout.Button("Close Anim"))
                        tmpWindow.CloseAnim();

                    GUILayout.EndHorizontal();
                }
            }

            EditorGUILayout.LabelField("Current Window ", winManager.currentWindowID.ToString());

            winManager.startWindowID = (WindowEnums)
                EditorGUILayout.EnumPopup("Default Start Window ", winManager.startWindowID);


            GUILayout.EndVertical();



            defaultInspectorFoldout = DrawDefaultEditorFoldout(defaultInspectorFoldout, this);

            if (GUI.changed)
                EditorUtility.SetDirty(winManager);
        }

        public static bool DrawDefaultEditorFoldout(bool open, Editor targetEditor)
        {
            EditorGUILayout.BeginVertical();
            open = EditorGUILayout.Foldout(open, "Default Inspector");
            if (open)
            {
                EditorGUI.indentLevel = 1;
                targetEditor.DrawDefaultInspector();
                EditorGUI.indentLevel = 0;
            }
            EditorGUILayout.EndVertical();
            return open;
        }


        /// --------------------------------------------------------------------------- ///

        void GenerateWindowsEnum()
        {
            List<string> windowNames = new List<string>();
            windowNames.Add("None");

            foreach (GenericWindow tmpWindow in winManager.Windows)
            {
                windowNames.Add(tmpWindow.name);
            }

            var script = MonoScript.FromMonoBehaviour( winManager );
            string path = AssetDatabase.GetAssetPath(script.GetInstanceID());
            path = path.Replace("WindowManager.cs", "") ;

            WriteDefinesFile("WindowEnums", "Sun", path + winManager.definesNamesPath, windowNames);
        }


        public static void WriteDefinesFile(string enumName, string Namespace, string filePath, List<string> defineNames)
        {
            Dictionary<string, int> definesDict = new Dictionary<string, int>();

            for (int i = 0; i < defineNames.Count; i++)
            {
                definesDict.Add(defineNames[i], i);
            }
            WriteDefinesFile(enumName, Namespace, filePath, definesDict);
        }

        public static void WriteDefinesFile(string enumName, string Namespace, string filePath, Dictionary<string, int> definesDict)
        {
            List<string> textLines = new List<string>();

            WriteHeader(textLines, Namespace);
            WriteDefines(textLines, enumName, definesDict);
            WriteFooter(textLines, Namespace);

            System.IO.File.WriteAllLines(
                Application.dataPath.Replace("Assets", "") + filePath, textLines.ToArray());

            //Debug.Log("Creating Enum at " + Application.dataPath.Replace("Assets", "") + filePath);

            AssetDatabase.Refresh();
        }


        protected static void WriteHeader(List<string> textLines, string Namespace)
        {
            textLines.Add("using UnityEngine;");
            if (Namespace != null && Namespace != "")
                textLines.Add("namespace " + Namespace + " {");


        }
        protected static void WriteDefines(List<string> textLines, string enumName, Dictionary<string, int> definesDict)
        {
            textLines.Add("	public enum " + enumName);
            textLines.Add("	{");

            //windowNames.Add("None");

            foreach (KeyValuePair<string, int> kv in definesDict)
            {
                textLines.Add("		" + kv.Key + "=" + kv.Value.ToString() + ",");
            }

            //textLines.Add("		COUNT=" + definesDict.Count.ToString());
            textLines.Add("	}");
        }

        protected static void WriteFooter(List<string> textLines, string Namespace)
        {
            if (Namespace != null && Namespace != "")
                textLines.Add("}");
        }

    }
}