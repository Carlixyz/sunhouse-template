﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Xml;
using System.IO;
using System;
using System.Text;
using System.Linq;
//using Ionic.Zlib;

/*
*   Only 1 (ONE) TileSet Per Layer
*   
*   Only one Size (Ex. 512x512) for ALL TileSets in one file (otherwise prepare for caos)
*   
*   Use the Same Tile Size (Ex. 32x32) for ALL TileSets in one Map
*   
*   All TileSets Images Sizes must be x2 (like 32x32, 64x64, 128x128, 256x256x 512x512, 1024x1024) 
*/

namespace Sun.Tools
{

    [RequireComponent(typeof(PrefabLoader))]
    [System.Serializable] [ExecuteInEditMode] 
    public class Tiler : Singleton<Tiler>              // Logic Controller
    {

        public bool showIngame = true, enableGizmo = true;            // Some Control things

        public enum TilerMode { None = 0, Paint, Bucket, Erase, Select, Prefab };

        TilerMode currentMode = TilerMode.None;

        [ShowOnly, SerializeField] uint currentTileID = 1;

        public uint currentTileSetIndex = 0;

        public uint CurrentTileID
        {
            get
            {
                return currentTileID;
            }
            set
            {
                if (value != currentTileID)
                {
                    currentTileID = value;
                    UpdateModeChanged();
                }
            }
        }

        public bool CombineMesh = false;

        [ShowOnly, SerializeField] int LastUsedMat = 0;

        public TilerMode CurrentMode
        {
            get
            {
                return currentMode;
            }
            set
            {
                if (currentMode != value)
                {
                    currentMode = value;
                    UpdateModeChanged();
                }
            }
        }

        public void UpdateModeChanged()
        {
            FlipX = FlipY = Diagonal = false;
            rotDeg = 0; 
            mirrorX = false; mirrorY = false;

            RefreshPreviewTarget(CurrentTileID);
        }

        [HideInInspector] public bool moduledGrids = true, inFocus = false;

        public float GridDistance
        {
            get
            { return gridMap.CurrentGridOffset; }
            set
            {
                if (value != gridMap.CurrentGridOffset)
                {
                    if(gridMap.KeepPlaneInbounds)
                        value = Mathf.Clamp(value, 0, gridMap.GetGridMaxOffsetCurrent());
                    gridMap.CurrentGridOffset = value;
                    gridMap.RefreshGridPlaneSettings(transform.position);
                }
            }
        }

        [HideInInspector] public Vector3 TilePosition = Vector3.zero;

        public TilerInterface tilerView = null;     // User's Input/Output 

        public GridMap gridMap;               // Model (Data Holding)

        [ShowOnly, SerializeField] Transform MapTransform;

        public GameObject previewPrefab;

        [ShowOnly] public GameObject previewObject;

        
        void OnEnable()
        {
            if (gridMap == null )
                NewMap();

            //gridMap.RefreshGridPlaneSettings();

            if (tilerView == null)
                tilerView = new TilerInterface(this);
            else
                tilerView.Init(this);
        }

        void Update()                   // ALL LOGIC HERE
        {
            tilerView.Update();         // Keyb && Mouse Input Update

            //if (gridMap != null)
            //{
                if (gridMap.origin != transform.position /*|| gridMap.CurrentGridOffset != GridDistance*/)
                gridMap.RefreshGridPlaneSettings(transform.position);

                if(CurrentMode == TilerMode.Prefab && Input.GetKey(KeyCode.LeftControl) )
                    TilePosition = gridMap.GetGridMousePositionFree(); // Unlock positon from grid
                else
                    TilePosition = gridMap.GetGridMousePosition();
            //}

            if (previewObject != null)
                if (CurrentMode == TilerMode.Prefab)
                    previewObject.transform.position = TilePosition + (previewObject.transform.localScale * 0.5f); // + gridMap.GetOrientationStep();
                else 
                    previewObject.transform.position = TilePosition;
        }

        void OnGUI()
        {
            tilerView.OnDrawGUI();

            GUI.Label( new Rect(0,0, Screen.width, 20), "Tile Pos = " + TilePosition);
        }

        void OnRenderObject()
        {
            if (!showIngame)
                return;

            //if (gridMap != null)
                gridMap.Draw();
        }

        void OnDrawGizmos()
        {
            if (!enableGizmo)
                return;
            gridMap.Draw();
        }

        /// ----- IN ORDER TO GET A HUMAN-REASONABLY MVC Allmost Logic GOES DOWN HERE -------

        /// --------------------------------------------------------------------------- /// EDIT
        public void SetNextGridOrientation()
        {
            GridDistance = gridMap.SetNextOrientation();
            RefreshPreviewTarget(CurrentTileID);
        }

        public void SetPreviousGridOrientation()
        {
            GridDistance = gridMap.SetPreviousOrientation();
            RefreshPreviewTarget(CurrentTileID);
        }

        public void IncreaseGridDistance()
        {
            if (moduledGrids)
                GridDistance++;                                 // scroll down
            else
                GridDistance += gridMap.GridUnlockedMove;                
        }

        public void DecreaseGridDistance()
        {
            if (moduledGrids)
                GridDistance--;                                 // scroll down
            else
                GridDistance -= gridMap.GridUnlockedMove;
        }

        void OnApplicationFocus(bool hasFocus)
        {
            inFocus = hasFocus;
        }

        public void CreatePrefab(Transform parent = null)
        {
            if (inFocus && previewObject)
            {
                GameObject go = Instantiate(previewObject,
                             TilePosition + previewObject.transform.localScale * 0.5f,
                             previewObject.transform.rotation, parent);
                go.name = previewPrefab.name;
                go.layer = previewPrefab.layer;
            }
        }

        bool mapVisibleInEditMode = true;

        public bool MapVisibleInEditMode
        {
            get
            {
                return mapVisibleInEditMode;
            }
            set
            {
                if (mapVisibleInEditMode != value)  //if (gridMap != null)     
                {
                    mapVisibleInEditMode = value;
                    if (mapVisibleInEditMode)
                        MapTransform.hideFlags = HideFlags.None;
                    else
                        MapTransform.hideFlags = HideFlags.HideInHierarchy;
                }
            }
        }

        public void RefreshPreviewTarget(uint tileId)
        {
#if UNITY_EDITOR

            GameObject.DestroyImmediate(previewObject);
#else
            GameObject.Destroy(previewObject);
#endif

            previewObject = null;

            if (CurrentMode == TilerMode.Paint || CurrentMode == Tiler.TilerMode.Bucket)
            {
                if (GridMap.TileSets.Count > 0)
                {

                    if (tileId == 0)
                    {
                        CurrentMode = TilerMode.None;
                        return;
                    }

                    uint modifs = (uint)(Diagonal ? 32 : 0);
                    modifs += (uint)(FlipY ? 64 : 0);
                    modifs += (uint)(FlipX ? 128 : 0);


                    uint ptID = (uint)(tileId | modifs << 24);   //((FlipX ? (uint)128 : 0) << 8) |
                                                                 //((FlipY ? (uint)64 : 0) << 16) |
                                                                 //((uint)(Diagonal ? 32 : 0) << 24));

                    //Debug.Log("Building Preview with id " + ptID);

                    currentTileID = tileId;

                    previewObject =
                        TileLayer.BuildPreviewTile(ptID, gridMap.step,
                                                    tilerView.CurrentTileSet,
                                                    gridMap.currentOrientation);

                    previewObject.hideFlags = HideFlags.HideInHierarchy;
                    //Debug.Log("Building preview Tile " + ptID);
                }
            }
            else if (CurrentMode == TilerMode.Prefab && previewPrefab != null)
            {
                previewObject = Instantiate(previewPrefab);
                previewObject.hideFlags = HideFlags.HideInHierarchy;
            }

            if (Application.isPlaying)
            {
                if (previewObject != null )
                    previewObject.layer = 2; //IgnoreRaycast
            }
        }

        public void RefreshPreviewObject(int objID)
        {
            previewPrefab = PrefabLoader.Get.GetPrefab( objID);

            previewPrefab.transform.rotation = Quaternion.identity;

            RefreshPreviewTarget(0);
        }

        public void PaintTile(uint tileID )
        {
            PaintTile(tileID, FlipX, FlipY, Diagonal);
        }

        public void PaintTile(uint tileID, bool flipX, bool flipY, bool Diag)
        {
            if (!inFocus)
                return;

            float layerOffset = gridMap.CurrentGridOffset;

            if (gridMap.CurrentGroup.layers.ContainsKey(layerOffset) == false)
                StartCoroutine(CreateNewLayer(layerOffset)); //CreateNewLayer(layerOffset);

            if (gridMap.CurrentGroup.layers[layerOffset].currentTileSetID != tilerView.CurrentTileSet)
                gridMap.CurrentGroup.layers[layerOffset].currentTileSetID = tilerView.CurrentTileSet;

            byte[] bytesModifs = System.BitConverter.GetBytes(tileID);

            byte fx = (byte)(FlipX ? 128 : 0);
            byte fy = (byte)(FlipY ? 64 : 0);
            byte dg = (byte)(Diagonal ? 32 : 0);
            bytesModifs[3] = (byte)(fx + fy + dg);

            tileID = System.BitConverter.ToUInt32(bytesModifs, 0);

            gridMap.CurrentGroup.layers[layerOffset].EditTileAt(TilePosition, tileID); //,
                                                                    //flipX, flipY, Diag);
        }

        public void PaintBucket(uint tileID) // //WARNING NOT FULLY READY, STILL BUGGY
        {
            if (!inFocus)
                return;

            float layerOffset = gridMap.CurrentGridOffset;

            if (gridMap.CurrentGroup.layers.ContainsKey(layerOffset) == false)
                StartCoroutine(CreateNewLayer(layerOffset));

            if (gridMap.CurrentGroup.layers[layerOffset].currentTileSetID != tilerView.CurrentTileSet)
                gridMap.CurrentGroup.layers[layerOffset].currentTileSetID = tilerView.CurrentTileSet;

            TileLayer currentLayer = gridMap.CurrentGroup.layers[layerOffset];
            //gridMap.CurrentGroup.layers[layerOffset]

            byte[] bytesAdjust = System.BitConverter.GetBytes(tileID);

            byte fx = (byte)(FlipX ? 128 : 0);
            byte fy = (byte)(FlipY ? 64 : 0);
            byte dg = (byte)(Diagonal ? 32 : 0);
            bytesAdjust[3] = (byte)(fx + fy + dg);

            tileID = System.BitConverter.ToUInt32(bytesAdjust, 0);

            currentLayer.FloodFill(TilePosition, tileID);  //WARNING NOT FULLY READY, STILL BUGGY

            /// -------------------------------------------------------------------------------------- ///
        }

        public void CopyTile()
        {
            if (!inFocus)
                return;

            var layerOffset = gridMap.CurrentGridOffset;

            if (gridMap.CurrentGroup.layers.ContainsKey(layerOffset) == false)
                return;

            uint tileId = gridMap.CurrentGroup.layers[layerOffset].GetTileAt(TilePosition);

            currentTileID = tileId & ~(FlippedHorizontallyFlag | FlippedVerticallyFlag | FlippedDiagonallyFlag);

            if (currentTileID == 0)
                return;

            mirrorX = FlipX = ((tileId & FlippedHorizontallyFlag) != 0);
            mirrorY = FlipY = ((tileId & FlippedVerticallyFlag) != 0);
            Diagonal = ((tileId & FlippedDiagonallyFlag) != 0);

            rotDeg = GetDegRotation(tileId);
            Rotation = rotDeg;

            RefreshPreviewTarget(currentTileID);
        }

        public void EraseTile()
        {
            if (!inFocus)
                return;

            var layerOffset = gridMap.CurrentGridOffset;

            if (gridMap.CurrentGroup.layers.ContainsKey(layerOffset) == false)
                return;

            gridMap.CurrentGroup.layers[layerOffset].EditTileAt(TilePosition, 0);

            if (gridMap.CurrentGroup.layers[layerOffset].totalTiles <= 0)
                RemoveLayer(layerOffset);
        }

        public bool mirrorX = false, mirrorY = false;

        [ShowOnly]
        public bool FlipX = false, FlipY = false, Diagonal = false;

        [ShowOnly, SerializeField]
        uint rotDeg = 0;/*, rotByte = 0;*/

        [ShowOnly, SerializeField]
        float AutoDepthIncrement = 0;

        public uint Rotation                    // Degrees to Bytes Rotation Conversor 
        {
            get
            { return rotDeg; }
            set
            {
                if (value != rotDeg)
                {
                    uint rotByte = 0;
                    rotDeg = value % 360;

                    if (!mirrorX && !mirrorY)
                    {
                        if (rotDeg == 0)
                            rotByte = 0x00;     // 0
                        else if (rotDeg == 90)
                            rotByte = 0xA0;     // 160
                        else if (rotDeg == 180)
                            rotByte = 0xC0;     // 192
                        else if (rotDeg == 270)
                            rotByte = 0x60;     // 96
                    }
                    else if (mirrorX && !mirrorY)
                    {
                        if (rotDeg == 0)
                            rotByte = 0x80;     // 128
                        else if (rotDeg == 90)
                            rotByte = 0xE0;     // 224
                        else if (rotDeg == 180)
                            rotByte = 0x40;     // 64
                        else if (rotDeg == 270)
                            rotByte = 0x20;     // 32
                    }
                    else if (!mirrorX && mirrorY)
                    {
                        if (rotDeg == 0)
                            rotByte = 0x40;     // 64
                        else if (rotDeg == 90)
                            rotByte = 0x20;     // 32
                        else if (rotDeg == 180)
                            rotByte = 0x80;     // 128
                        else if (rotDeg == 270)
                            rotByte = 0xE0;     // 224
                    }
                    else if (mirrorX && mirrorY)
                    {
                        if (rotDeg == 0)
                            rotByte = 0xC0;     // 192
                        else if (rotDeg == 90)
                            rotByte = 0x60;     // 96
                        else if (rotDeg == 180)
                            rotByte = 0x00;     // 0
                        else if (rotDeg == 270)
                            rotByte = 0xA0;     // 160
                    }

                    RotateTileClockWise((rotByte << 24));
                }
            }
        }

        public uint GetDegRotation(uint rotByte)
        {
            byte[] bytes = System.BitConverter.GetBytes(rotByte);
            rotByte = bytes[3];

            uint rotation = 0;

            if (!FlipX && !FlipY && (rotByte == 160 || rotByte == 32))
            {
                mirrorY = true;
                rotation = 90;     // 160
            }
            else if (FlipX && !FlipY && (rotByte == 224 || rotByte == 160))
            {
                mirrorX = false;
                rotation = 90;     // 224
            }
            else if (!FlipX && FlipY && (rotByte == 224 || rotByte == 96))
            {
                mirrorY = false;
                rotation = 270;     // 224
            }
            else if (FlipX && FlipY && (rotByte == 160 || rotByte == 224))
            {
                mirrorX = false;
                rotation = 270;     // 160
            }

            //Debug.Log(" rotDeg " + rotByte + " rotation is " + rotation);

            return rotation;
        }   // Bytes 2 Degs Recovery

        uint FlippedHorizontallyFlag = 0x80000000;

        uint FlippedVerticallyFlag = 0x40000000;

        uint FlippedDiagonallyFlag = 0x20000000;

        void RotateTileClockWise(uint rotation)
        {
            FlipX = FlipY = Diagonal = false;

            //if ((rotation & FlippedHorizontallyFlag) != 0 )
            //    FlipX = true;

            //if ((rotation & FlippedVerticallyFlag) != 0 )
            //  FlipY = true;

            //if ((rotation & FlippedDiagonallyFlag) != 0 )
            //    Diagonal = true;

            FlipX = ((rotation & FlippedHorizontallyFlag) != 0);
            FlipY = ((rotation & FlippedVerticallyFlag) != 0);
            Diagonal = ((rotation & FlippedDiagonallyFlag) != 0);


            RefreshPreviewTarget(CurrentTileID);
        }


        /// --------------------------------------------------------------------------- /// SAVE

        public void SaveMap(string filePathName)
        {
            Debug.Log("Saving " + filePathName);

            gridMap.RefreshGridPlaneSettingsAll();

            var xmlSettings = new XmlWriterSettings/* ()*///;
            {
                Indent = true,
                IndentChars = "\t",
                NewLineChars = Environment.NewLine,
                NewLineHandling = NewLineHandling.Replace,
                Encoding = new UTF8Encoding(true)
            };


            XmlWriter xmlWriter =
                //XmlWriter.Create(Serializer.GetCurrentPath() + "/testAlpha.tmx", xmlSettings);
                XmlWriter.Create(filePathName, xmlSettings);

            xmlWriter.WriteStartDocument();

            /// --------------------------------------------------------------- MAP
            SaveMapData(xmlWriter);

            /// --------------------------------------------------------------- tilesets

            // Get Image Path names Correctly
            foreach (var tileSet in GridMap.TileSets)
                SaveTileSet(xmlWriter, tileSet);         // Save special tile'sCollisions Properties too       

            /// --------------------------------------------------------------- Layers

            foreach (var group in gridMap.Groups)
                if (group.layers.Count > 0)
                    foreach (var layer in group.layers.Reverse())
                        if (layer.Value != null && layer.Value.totalTiles > 0)
                            SaveLayer(xmlWriter, layer.Value);

            /// --------------------------------------------------------------- Objects Prefabs

            foreach (Transform goT in MapTransform)
                if (!goT.GetComponent<TileLayer>() && !goT.GetComponent<Renderer>())
                    SaveGroup(xmlWriter, goT);

            /// --------------------------------------------------------------- CLOSE

            xmlWriter.WriteEndDocument();
            xmlWriter.Close();
        }

        void SaveMapData(XmlWriter xmlWriter)
        {
            xmlWriter.WriteStartElement("map");
            xmlWriter.WriteAttributeString("version", "1.0");
            xmlWriter.WriteAttributeString("tiledversion", "1.0.3");
            xmlWriter.WriteAttributeString("orientation", "orthogonal");
            xmlWriter.WriteAttributeString("renderorder", "right-up");

            xmlWriter.WriteAttributeString("width", gridMap.fileWidth.ToString());
            xmlWriter.WriteAttributeString("height", gridMap.fileHeight.ToString());
            xmlWriter.WriteAttributeString("tilewidth", gridMap.tileWidth.ToString());
            xmlWriter.WriteAttributeString("tileheight", gridMap.tileHeight.ToString());
            xmlWriter.WriteAttributeString("nextobjectid", "1");


            xmlWriter.WriteStartElement("properties");  // MAP PROPERTIES OPEN

            if (gridMap.fileWidth != (int)gridMap.countX)
            {
                xmlWriter.WriteStartElement("property");                    // MAP Width 
                xmlWriter.WriteAttributeString("name", "Width");
                xmlWriter.WriteAttributeString("type", "float");
                xmlWriter.WriteAttributeString("value", gridMap.countX.ToString());
                xmlWriter.WriteEndElement();
            }

            if (gridMap.fileHeight != (int)gridMap.countY)
            {
                xmlWriter.WriteStartElement("property");                    // MAP Height 
                xmlWriter.WriteAttributeString("name", "Height");
                xmlWriter.WriteAttributeString("type", "float");
                xmlWriter.WriteAttributeString("value", gridMap.countY.ToString());
                xmlWriter.WriteEndElement();
            }


            if ((int)gridMap.countZ != 1)
            {
                xmlWriter.WriteStartElement("property");                    // MAP Depth 
                xmlWriter.WriteAttributeString("name", "Depth");
                xmlWriter.WriteAttributeString("type", "float");
                xmlWriter.WriteAttributeString("value", gridMap.countZ.ToString());
                xmlWriter.WriteEndElement();
            }

            if ((int)gridMap.stepX != 1)
            {
                xmlWriter.WriteStartElement("property");                    // STEP W
                xmlWriter.WriteAttributeString("name", "stepWidth");
                xmlWriter.WriteAttributeString("type", "float");
                xmlWriter.WriteAttributeString("value", gridMap.stepX.ToString());
                xmlWriter.WriteEndElement();
            }

            if ((int)gridMap.stepY != 1)
            {
                xmlWriter.WriteStartElement("property");                    // STEP H
                xmlWriter.WriteAttributeString("name", "stepHeight");
                xmlWriter.WriteAttributeString("type", "float");
                xmlWriter.WriteAttributeString("value", gridMap.stepY.ToString());
                xmlWriter.WriteEndElement();
            }

            if ((int)gridMap.stepZ != 1)
            {
                xmlWriter.WriteStartElement("property");                    // STEP Z
                xmlWriter.WriteAttributeString("name", "stepDepth");
                xmlWriter.WriteAttributeString("type", "float");
                xmlWriter.WriteAttributeString("value", gridMap.stepZ.ToString());
                xmlWriter.WriteEndElement();
            }

            xmlWriter.WriteEndElement();                // PROPERTIES CLOSE
        }

        void SaveTileSet(XmlWriter xmlWriter, TileSet tileSet)
        {

            xmlWriter.WriteStartElement("tileset");

            xmlWriter.WriteAttributeString("firstgid", tileSet.FirstGid.ToString());
            xmlWriter.WriteAttributeString("name", tileSet.TileSetName);
            xmlWriter.WriteAttributeString("tilewidth", tileSet.tileWidth.ToString());
            xmlWriter.WriteAttributeString("tileheight", tileSet.tileHeight.ToString());
            xmlWriter.WriteAttributeString("tilecount", tileSet.TileCount.ToString());
            xmlWriter.WriteAttributeString("columns", tileSet.TileColumns.ToString());

            xmlWriter.WriteStartElement("image");
            xmlWriter.WriteAttributeString("source", tileSet.ImagePath);
            xmlWriter.WriteAttributeString("width", tileSet.ImageWidth.ToString());
            xmlWriter.WriteAttributeString("height", tileSet.ImageHeight.ToString());
            xmlWriter.WriteEndElement();

            foreach (var collision in tileSet.Collisions)
            {

                xmlWriter.WriteStartElement("tile");                        // OPEN TILE              
                xmlWriter.WriteAttributeString("id", (collision.Key -1).ToString());

                xmlWriter.WriteStartElement("properties");              // PROPERTIES OPEN

                xmlWriter.WriteStartElement("property");            // OPEN PROP
                xmlWriter.WriteAttributeString("name", "Collision");
                xmlWriter.WriteAttributeString("value", collision.Value.ToString());
                xmlWriter.WriteEndElement();                        // CLOSE PROP

                xmlWriter.WriteEndElement();                            // CLOSE PROPERTIES

                xmlWriter.WriteEndElement();                                // CLOSE TILE
            }

            xmlWriter.WriteEndElement();
        }

        void SaveLayer(XmlWriter xmlWriter, TileLayer tLayer)
        {

            xmlWriter.WriteStartElement("layer");

            xmlWriter.WriteAttributeString("name", tLayer.name);
            xmlWriter.WriteAttributeString("width", gridMap.fileWidth.ToString());
            xmlWriter.WriteAttributeString("height", gridMap.fileHeight.ToString());


            xmlWriter.WriteStartElement("properties");  // PROPERTIES OPEN

            xmlWriter.WriteStartElement("property");                    // OFFSET
            switch (tLayer.currentOrientation)
            {
                case GridMap.GridOrientation.Vertical:
                    xmlWriter.WriteAttributeString("name", "VerticalOffset");  
                    break;
                case GridMap.GridOrientation.Depth:
                    xmlWriter.WriteAttributeString("name", "DepthOffset");          
                    break;
                default:
                    xmlWriter.WriteAttributeString("name", "HorizontalOffset");  
                    break;
            }

            xmlWriter.WriteAttributeString("type", "float");
            xmlWriter.WriteAttributeString("value", tLayer.Offset.z.ToString());
            xmlWriter.WriteEndElement();

            if(tLayer.CollisionLayer != GridMap.CollisionType.None)
            {
                xmlWriter.WriteStartElement("property");
                xmlWriter.WriteAttributeString("name", "Collision");    // LAYER COLLISION
                xmlWriter.WriteAttributeString("type", "string");
                xmlWriter.WriteAttributeString("value",  tLayer.CollisionLayer.ToString() );
                xmlWriter.WriteEndElement();
            }

            xmlWriter.WriteEndElement();                // PROPERTIES CLOSE


            Vector2 realSize = new Vector2(tLayer.Width, tLayer.Height);
            tLayer.Width = gridMap.fileWidth;
            tLayer.Height = gridMap.fileHeight;
            tLayer.ReSize();

            xmlWriter.WriteStartElement("data");
            xmlWriter.WriteAttributeString("encoding", "base64");
            xmlWriter.WriteAttributeString("compression", "zlib");
            xmlWriter.WriteString( tLayer.GetTileDataCompressed() );
            xmlWriter.WriteEndElement();

            xmlWriter.WriteEndElement();


            tLayer.Width = (int)realSize.x;
            tLayer.Height = (int)realSize.y;
            tLayer.ReSize();


        }

        void SaveGroup(XmlWriter xmlWriter, Transform objGroup)
        {

            xmlWriter.WriteStartElement("objectgroup");
            xmlWriter.WriteAttributeString("name", objGroup.name);

            int i = 1;

            foreach (Transform goT in objGroup)
                if (goT.GetComponent<Renderer>())
                {
                    SaveObject(xmlWriter, goT, i);
                    i++;
                }

            xmlWriter.WriteEndElement();

            Debug.Log("Saving GROUP " + objGroup +
                " last Obj " + objGroup.GetChild(objGroup.childCount-1));

        }

        void SaveObject(XmlWriter xmlWriter, Transform obj, int i)
        {
            string name = obj.name.ToLower().Replace("(clone)", ""); 
            
            xmlWriter.WriteStartElement("object");
            xmlWriter.WriteAttributeString("id", i.ToString());
            xmlWriter.WriteAttributeString("name", name);

            xmlWriter.WriteAttributeString("x", 
                ((obj.position.x * gridMap.tileWidth) 
                - (obj.localScale.x * gridMap.tileWidth) * 0.5f).ToString());

            xmlWriter.WriteAttributeString("y", (((Mathf.CeilToInt(obj.position.y) 
                - (int)gridMap.countY) * gridMap.tileHeight) *-1 ).ToString());

            xmlWriter.WriteAttributeString("width", (obj.localScale.x * gridMap.tileWidth).ToString());

            xmlWriter.WriteAttributeString("height", (obj.localScale.y * gridMap.tileHeight).ToString());

            AddObjectProperties(xmlWriter, obj);

            xmlWriter.WriteEndElement();


        }

        void AddObjectProperties(XmlWriter xmlWriter, Transform obj)
        {
            if (Mathf.Approximately(obj.position.z, 0) && obj.rotation == Quaternion.identity)
                        return;

            xmlWriter.WriteStartElement("properties");  // PROPERTIES OPEN

            if (Mathf.Approximately(obj.position.z, 0) != true)
            {
                xmlWriter.WriteStartElement("property");  // PROPERTIES OPEN
                xmlWriter.WriteAttributeString("name", "depth");
                xmlWriter.WriteAttributeString("type", "float");
                xmlWriter.WriteAttributeString("value", obj.position.z.ToString());
               // xmlWriter.WriteAttributeString("value", (((Mathf.CeilToInt(obj.position.z)
               //- (int)gridMap.countZ) * gridMap.tileWidth) * -1).ToString());
                xmlWriter.WriteEndElement();
            }

            if (obj.rotation.x != 0.0f)
            {
                xmlWriter.WriteStartElement("property");  // PROPERTIES OPEN
                xmlWriter.WriteAttributeString("name", "rotationX");
                xmlWriter.WriteAttributeString("type", "float");
                xmlWriter.WriteAttributeString("value", obj.rotation.x.ToString());
                xmlWriter.WriteEndElement();
            }


            if (obj.rotation.y != 0.0f)
            {
                xmlWriter.WriteStartElement("property");  // PROPERTIES OPEN
                xmlWriter.WriteAttributeString("name", "rotationY");
                xmlWriter.WriteAttributeString("type", "float");
                xmlWriter.WriteAttributeString("value", obj.rotation.y.ToString());
                xmlWriter.WriteEndElement();
            }


            if (obj.rotation.z != 0.0f)
            {
                xmlWriter.WriteStartElement("property");  // PROPERTIES OPEN
                xmlWriter.WriteAttributeString("name", "rotationZ");
                xmlWriter.WriteAttributeString("type", "float");
                xmlWriter.WriteAttributeString("value", obj.rotation.z.ToString());
                xmlWriter.WriteEndElement();
            }

            xmlWriter.WriteEndElement();

        }

     
        /// --------------------------------------------------------------------------- /// LOAD

        public bool LoadFileMap(string filePath)
        {
            XmlDocument Doc;

#if (UNITY_ANDROID || UNITY_WEBGL)
            Doc = LoadFileAsResource(filePath);
#else
            Doc = LoadFileAsDefault(filePath);
#endif

            if (Doc.DocumentElement.Name != "map")                      // Access root Map		
            {
                Debug.LogError( "not a Tiled File format!, wrong load at: " + filePath);
                return false;
            }

            //Debug.Log("Loading from  " + filePath);
            LoadMap(Doc.DocumentElement, filePath);

            return true;
        }

        XmlDocument LoadFileAsDefault(string filePath)
        {
            FileInfo info = new FileInfo(filePath);
            if (info == null && info.Exists == false)   //if (!File.Exists(@filePath))
            {
                //ScreenLog.Get.ShowLog = true;
                Debug.LogWarning("Couldn't Load the TileMap, File Don't Exists! " + filePath);
                //Debug.LogWarning("Couldn't Load the TileMap, File Don't Exists! " + Application.dataPath + filePath);
                return null;
            }

            XmlDocument Doc = new XmlDocument();

            StreamReader sr = File.OpenText(filePath);   // Do Stream Read
            Doc.LoadXml(sr.ReadToEnd());                                        // and Read XML
            sr.Close();

            return Doc;
        }

        XmlDocument LoadFileAsResource(string filePath)
        {

            if (filePath[0] == '/') // Clean Up some old FilePaths '/'...
                filePath = filePath.Remove(0, 1);
                     
            XmlDocument Doc = new XmlDocument();

            // because Resource.Loader won't read it
            TextAsset textAsset = (TextAsset)Resources.Load(
                filePath.Remove(filePath.LastIndexOf(".")), typeof(TextAsset));    
            Doc.LoadXml(textAsset.text);

            return Doc;
        }


        public void NewMap(string name = "GridMap")
        {
            if (MapTransform  != null)
            {
                CurrentMode = Tiler.TilerMode.None;
                CurrentTileID = 0;
                tilerView.ResetInterface();

                RefreshPreviewTarget( CurrentTileID);


                PrefabLoader.Get.ClearAll();
                MapTransform.DestroyChildren();

#if UNITY_EDITOR
                GameObject.DestroyImmediate(MapTransform.gameObject);
#else
                GameObject.Destroy(MapTransform.gameObject);
#endif
            }


            //MapTransform = new GameObject(name, typeof(GridMap)).transform;           // Create inside the editor hierarchy & take map transform cached
            //gridMap = MapTransform.GetComponent<GridMap>();
            //gridMap.Init();

            MapTransform = new GameObject(name).transform;           // Create inside the editor hierarchy & take map transform cached
            gridMap = MapTransform.gameObject.AddComponent<GridMap>();
            gridMap.Init();



            //    transform.DestroyChildren();

            //    if (MapTransform != null)
            //        Destroy(MapTransform.gameObject);

            //    MapTransform = new GameObject("New Map").transform;           // Create inside the editor hierarchy & take map transform cached
            //    gridMap = new GridMap();

        }

        public void LoadMap(XmlElement DocElement, string filePath)
        {
            // trim folder path structure
            string fileName = filePath.Remove(0, filePath.LastIndexOf("\\") + 1);
            //string fileName = filePath.Remove(filePath.LastIndexOf("//") + 1);
            fileName = fileName.Remove(fileName.LastIndexOf("."));  // trim .tmx or .xml extension


            //Managers.Register.currentLevelFile = filePath;
            //Managers.Tiled.Setup(DocElement, fileName);

            NewMap(fileName);

            gridMap.LoadProperties(DocElement, filePath);

            //XmlNodeList XMLlayers = DocElement.GetElementsByTagName("layer");
            //for (int i = XMLlayers.Count - 1; i >= 0; i-- )
            //    StartCoroutine(LoadLayer(XMLlayers[i]));

            for (XmlNode Layer = DocElement.LastChild; Layer.Name != "tileset"; Layer = Layer.PreviousSibling)
            {
                switch (Layer.Name)
                {
                    case "layer":                                       // TagName: TileSet Layers
                        StartCoroutine(LoadLayer(Layer));
                        break;
                    case "group":                                       // TagName: TileSet Group Layers
                        XmlNodeList grouplayers = Layer.ChildNodes;
                        for (int i = grouplayers.Count - 1; i >= 0; i--)
                            StartCoroutine(LoadLayer(grouplayers[i]));
                        break;
                    case "imagelayer":                                  // TagName: Image Layers (for scrolling)
                        //StartCoroutine(Managers.Scroll.BuildScrollLayers(Layer));   //  Managers.Scroll.BuildScrollLayers(Layer);
                        break;
                    case "objectgroup":                                 // TagName: Object Group Layer
                        StartCoroutine(PrefabLoader.Get.BuildPrefabGroupXML(Layer, MapTransform));
                        break;
                }
            }


            LastUsedMat = GridMap.TileSets.Count-1;

            if (CombineMesh && LastUsedMat == 0)
            {
#if UNITY_EDITOR
                //meshCombiner mc = MapTransform.gameObject.AddComponent<meshCombiner>();
                //mc.CombineMeshes();                 ////    mc.OnEnabled();
                MapTransform.gameObject.AddComponent<meshCombiner>().CombineMeshes();
#else
                 MapTransform.gameObject.AddComponent<meshCombiner>();
#endif
            }

            //Managers.Scroll.SetupScroll();
        }

         /// --------------------------------------------------------------------------- /// LAYER EDIT

        public void RemoveLayer(float offset)
        {
            gridMap.CurrentGroup.layers[offset].DestroyAll();
            Destroy(gridMap.CurrentGroup.layers[offset].gameObject);
            gridMap.CurrentGroup.layers.Remove(offset);
        }

        public void ChangeLayer(float oldOffset, float newOffset)
        {
            TileLayer sourceLayer = gridMap.CurrentGroup.layers[oldOffset];

            ChangeLayer(sourceLayer, oldOffset, newOffset);
        }

        public void ChangeLayer(TileLayer srcLayer, float oldOffset, float newOffset)
        {
            if (gridMap.CurrentGroup.layers.ContainsKey(oldOffset) )
            {

                if (gridMap.CurrentGroup.layers.ContainsKey(newOffset)) // Delete old Destiny Layer
                    RemoveLayer(newOffset);

                gridMap.CurrentGroup.layers.Add(newOffset, srcLayer);
                gridMap.CurrentGroup.layers.Remove(oldOffset);

                gridMap.CurrentGroup.layers[newOffset].OffsetZ = newOffset;
                //Debug.Log("Changed Layer!");
            }
        }

        public IEnumerator CreateNewLayer(float offset, string name = "")
        {

            if (string.IsNullOrEmpty(name))
                name = "Capa " + gridMap.currentOrientation.ToString() + " " + offset.ToString();

            GameObject LayerGO = new GameObject(name); // add Layer Childs inside hierarchy.

            TileLayer layer = LayerGO.AddComponent<TileLayer>();

            layer.SetupProperties(gridMap);    // layerProps LOADING

            ///---------------------------------------------------------------------------///

            layer.offset.z = offset;

            gridMap.Groups[(int)layer.currentOrientation].layers.Add(layer.offset.z, layer);

            layer.LayerTransform.parent = MapTransform;

            yield return 0;
        }

        public IEnumerator LoadLayer(XmlNode LayerInfo)
        {
            GameObject LayerGO = new GameObject(LayerInfo.Attributes["name"].Value); // add Layer Childs inside hierarchy.

            TileLayer layer = LayerGO.AddComponent<TileLayer>();

            layer.LoadProperties(LayerInfo);    // layerProps LOADING

            //layer.Size = gridMap.GetOrientationSize(layer.currentOrientation);

            layer.Width = (int)gridMap.GetOrientationSize(layer.currentOrientation).x;

            layer.Height = (int)gridMap.GetOrientationSize(layer.currentOrientation).y;

            layer.step = gridMap.step;  // gridMap.GetOrientationSteps(layer.currentOrientation);

            layer.discrepance = gridMap.Discrepance;

            layer.eps = gridMap.Epsilon;

            layer.ReSize();

            // Check if there're multiple layers without depth properties [AUTO-DEPTH]
            if (layer.offset.z == 0 &&
                layer.currentOrientation == GridMap.GridOrientation.Horizontal &&
                gridMap.Groups[(int)layer.currentOrientation].layers.Count > 0)
                    AutoDepthIncrement = SetAutoDepth(layer, AutoDepthIncrement);
            // Check GridMap Depth & Bounds with current Layer (Only for Horizontal 2D Maps)
            

            while (gridMap.Groups[(int)layer.currentOrientation].layers.ContainsKey(layer.offset.z))
            {
                // There's already one Layer there let's just push our Layer's Depth position 
                // to aproximately match values until We found some free Slot
                var pushedLayer =
                    gridMap.Groups[(int)layer.currentOrientation].layers[layer.offset.z];

                Debug.LogWarning("ALREADY USED OFFSET " + layer.offset.z + " in layer " +
                    pushedLayer.name + " Clashing with new Layer " + layer.name);

                layer.offset.z = layer.offset.z + 0.1f; // - layer.discrepance;

                Debug.LogWarning(" Moving Layer " + layer.name + " To Offset " + layer.offset.z);
            }

            gridMap.Groups[(int)layer.currentOrientation].layers.Add(layer.offset.z, layer);

            layer.LayerTransform.parent = MapTransform;

            layer.BuildTiles();

            yield return 0;
        }

        float SetAutoDepth(TileLayer tLayer, float currentDepth = 0.0f)
        {
            tLayer.offset += Vector3.forward // * 0.5f
                   * gridMap.GetAxisStep(tLayer.currentOrientation) 
                   * gridMap.Groups[(int)tLayer.currentOrientation].layers.Count;

            tLayer.Locked = true;
            //Debug.Log("Doing Auto-Depth on default Tiled layer:" + tLayer.name + " " + tLayer.offset);

            // Check if some layer is out of map bounds size
            if (tLayer.offset.z > gridMap.GetGridMaxOffset(tLayer.currentOrientation))
            {
                // then adjust map size
                if (tLayer.currentOrientation == GridMap.GridOrientation.Horizontal)
                    gridMap.count.z = tLayer.offset.z;
                else if (tLayer.currentOrientation == GridMap.GridOrientation.Vertical)
                    gridMap.count.y = tLayer.offset.z;
                else
                    gridMap.count.x = tLayer.offset.z;

                Debug.Log("Out of Z Bounds, ReSizeing Map: " + tLayer.name);
            }

            //Debug.Log("Doing Auto-Depth :" + tLayer.name + " " + tLayer.offset.z);
            return tLayer.offset.z; // Return last depth increment
        }



    }

}
