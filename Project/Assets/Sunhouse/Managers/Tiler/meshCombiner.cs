﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

#if UNITY_EDITOR
using UnityEditor;
#endif

//[RequireComponent(typeof(MeshFilter))]
//[RequireComponent(typeof(MeshRenderer))]
[RequireComponent(typeof(MeshFilter), typeof(MeshRenderer))]
[DisallowMultipleComponent]
public class meshCombiner : MonoBehaviour
{

    public void OnEnabled()
    {
        CombineMeshes();
    }


    public void CombineMeshes()
    {
        //foreach (Transform child in transform)
        //    child.position += transform.position;
        //transform.position = Vector3.zero;
        //transform.rotation = Quaternion.identity;

        Quaternion oldRot = transform.rotation;
        Vector3 oldPos = transform.position;

        transform.rotation = Quaternion.identity;
        transform.position = Vector3.zero;


        MeshFilter[] meshFilters = GetComponentsInChildren<MeshFilter>();

        CombineInstance[] combine = new CombineInstance[meshFilters.Length - 1];

        int index = 0;
        for (int i = 0; i < meshFilters.Length; i++)
        {
            if (meshFilters[i].transform == transform || meshFilters[i].sharedMesh == null )
            //if (/*meshFilters[i].transform == transform ||*/ meshFilters[i].sharedMesh == null )
                continue;

            combine[index].mesh = meshFilters[i].sharedMesh;
            combine[index++].transform = meshFilters[i].transform.localToWorldMatrix;
            //meshFilters[i].GetComponent<Renderer>().enabled = false;    //meshFilters[i].renderer.enabled = false;
        }


    #if UNITY_EDITOR

            GetComponent<MeshFilter>().sharedMesh = new Mesh();
            GetComponent<MeshFilter>().sharedMesh.CombineMeshes(combine, true, true);
    #else

            GetComponent<MeshFilter>().mesh = new Mesh();
            GetComponent<MeshFilter>().mesh.CombineMeshes(combine, true, true);
    #endif

            GetComponent<Renderer>().materials = meshFilters[1].GetComponent<Renderer>().sharedMaterials;     // Si en algún momento Unity te manda por aqui 

        //  ------------------------------------------------------------------------------ //
        transform.rotation = oldRot;
        transform.position = oldPos;

        for (int i = transform.childCount - 1; i >= 0; --i)
        //for (int i = 0; i < transform.childCount; i++)
        {
                //if (transform.GetChild(i).GetComponent<BoxCollider>() == null)
                if (transform.GetChild(i).GetComponentInChildren<Collider>() == null)
                //transform.GetChild(i).GetComponent<MeshRenderer>().enabled = false;
    #if UNITY_EDITOR
                    DestroyImmediate(transform.GetChild(i).gameObject);
    #else
                    Destroy(transform.GetChild(i).gameObject);
    #endif
                else
                    {
                    //transform.GetChild(i).GetComponentInChildren<MeshRenderer>().enabled = false;
                        Destroy(transform.GetChild(i).GetComponentInChildren<MeshRenderer>());
                        Destroy(transform.GetChild(i).GetComponentInChildren<MeshFilter>());
                    ////Destroy(transform.GetChild(i).GetComponent<MeshRenderer>() );
                    ////Destroy(transform.GetChild(i).GetComponent<MeshFilter>() );

                    }

            }

        //  ------------------------------------------------------------------------------ //


        //for (int i = transform.childCount - 1; i >= 0; --i)
        //GameObject.Destroy(transform.GetChild(i).gameObject); // transform.GetChild(a).gameObject.SetActive(false);
        //transform.DetachChildren();
    }

    public void Combine(GameObject objMesh)
    {

        Quaternion oldRot = transform.rotation;
        Vector3 oldPos = transform.position;

        transform.rotation = Quaternion.identity;
        transform.position = Vector3.zero;


        MeshFilter[] meshFilters = GetComponentsInChildren<MeshFilter>();
        CombineInstance[] combine = new CombineInstance[meshFilters.Length];
        Destroy(this.gameObject.GetComponent<MeshCollider>());

        int i = 0;

        while(i < meshFilters.Length)
        {
            combine[i].mesh = meshFilters[i].sharedMesh;
            combine[i].transform = meshFilters[i].transform.localToWorldMatrix;
            meshFilters[i].gameObject.SetActive(false);
            i++;
        }
        transform.GetComponent<MeshFilter>().mesh = new Mesh();
        transform.GetComponent<MeshFilter>().mesh.CombineMeshes(combine, true);
        transform.GetComponent<MeshFilter>().mesh.RecalculateBounds();
        transform.GetComponent<MeshFilter>().mesh.RecalculateNormals();
        //transform.GetComponent<MeshFilter>().mesh.Optimize();

        GetComponent<Renderer>().materials = meshFilters[1].GetComponent<Renderer>().sharedMaterials;     // Si en algún momento Unity te manda por aqui 
        transform.rotation = oldRot;
        transform.position = oldPos;

        this.gameObject.AddComponent<MeshCollider>();
        transform.gameObject.SetActive(true);

        Destroy(objMesh);
    }

#if UNITY_EDITOR

    public void SaveAsset()
    {
        SaveAsset(gameObject.name);
    }

    public void SaveAsset(string fileName)
    {
        var mf = GetComponent<MeshFilter>();
        if (mf)
        {
            var savePath = "Assets/" + fileName + "Mesh.asset";
            Debug.Log("Saved Mesh to:" + savePath);
            AssetDatabase.CreateAsset(mf.mesh, savePath);
        }

        var mr = GetComponent<MeshRenderer>();
        if (mr && mr.materials.Length > 0)
        {
            var savePath = "Assets/" + fileName + "Material.mat";
            Debug.Log("Saved Mat to:" + savePath);
            foreach(var m in mr.materials)
                AssetDatabase.CreateAsset(m, savePath);
        }


    }

#endif


}
