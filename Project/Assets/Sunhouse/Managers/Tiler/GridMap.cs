﻿using UnityEngine;
using System.Collections.Generic;
using System.Xml;

namespace Sun.Tools
{
    [System.Serializable]
    public class GridMap : MonoBehaviour    
    {


        [SerializeField]
        public enum CollisionType { None = 0, Box3d, Plane3d, Slope3d, Mesh3d, Box2d, Poly2d };
        //public enum CollisionType { none = 0, box3d, plane3d, slope3d, mesh3d, box2d, poly2d };

        [SerializeField]
        public enum GridOrientation { Horizontal = 0/*XZ*/, Vertical /*XY*/, Depth /*YZ*/};

        // VARS
        [HideInInspector]
        public Vector3 origin = Vector3.zero;           // grid start origin position

        public Vector3 count = Vector3.one * 4;         // 3D grid counter size

        public float countX
        {
            get { return count.x; }
            set
            {
                if (value != count.x)
                {
                    count.x = value;
                    foreach (AxisGroup g in Groups)
                        foreach (TileLayer t in g.layers.Values)
                        {
                            t.Size = count;
                            t.ReBuildLayer();
                            RefreshGridPlaneSettingsAll();
                        }
                }
            }
        }                           // Helpers to keep all layers updated

        public float countY
        {
            get { return count.y; }
            set
            {
                if (value != count.y)
                {
                    count.y = value;
                    foreach (AxisGroup g in Groups)
                        foreach (TileLayer t in g.layers.Values)
                        {
                            t.Size = count;
                            t.ReBuildLayer();
                            RefreshGridPlaneSettingsAll();
                        }
                }
            }
        }

        public float countZ
        {
            get { return count.z; }
            set
            {
                if (value != count.z)
                {
                    count.z = value;
                    foreach (AxisGroup g in Groups)
                        foreach (TileLayer t in g.layers.Values)
                        {
                            t.Size = count;
                            t.ReBuildLayer();
                            RefreshGridPlaneSettingsAll();
                        }
                }
            }
        }


        public Vector3 step = Vector3.one;              // 3D grid subdivisions scale

        public float stepX
        {
            get { return step.x; }
            set
            {
                if (value != step.x)
                {
                    step.x = value;
                    foreach (AxisGroup g in Groups)
                        foreach (TileLayer t in g.layers.Values)
                        {
                            t.step = step;
                            t.ReBuildLayer();
                            RefreshGridPlaneSettingsAll();
                        }
                }
            }
        }

        public float stepY
        {
            get { return step.y; }
            set
            {
                if (value != step.y)
                {
                    step.y = value;
                    foreach (AxisGroup g in Groups)
                        foreach (TileLayer t in g.layers.Values)
                        {
                            t.step = step;
                            t.ReBuildLayer();
                            RefreshGridPlaneSettingsAll();
                        }
                }
            }
        }

        public float stepZ
        {
            get { return step.z; }
            set
            {
                if (value != step.z)
                {
                    step.z = value;
                    foreach (AxisGroup g in Groups)
                        foreach (TileLayer t in g.layers.Values)
                        {
                            t.step = step;
                            t.ReBuildLayer();
                            RefreshGridPlaneSettingsAll();
                        }
                }
            }
        }


        public Vector3 size                             // 3D grid volumetric size   
        {
            get
            {
                return new Vector3(count.x * step.x, count.y * step.y, count.z * step.z);
            }
            set
            {
                //if(value != count)
                count = new Vector3(value.x / step.x, value.y / step.y, value.z / step.z);
            }
        }

        public Vector3 sizeInt                          // 3D grid volumetric Integral size    
        {
            get
            {
                return new Vector3(Mathf.Floor(count.x * step.x), Mathf.Floor(count.y * step.y), Mathf.Floor(count.z * step.z));
            }
            set
            {
                count = new Vector3(value.x / step.x, value.y / step.y, value.z / step.z);
            }
        }

        public Vector3 sizeFixed                        // 3D grid volumetric Integral size    
        {
            get
            {

                //Vector3 MaxBound = new Vector3( size.x - step.x,
                //                                size.y - step.y,
                //                                size.z - step.z);

                //MaxBound += GetAxisStepVectorized();    // Add +1 on current axis'step bound

                //rayPos = Vector3.Min(Vector3.Max(rayPos, Vector3.zero)/*rayPos*/, MaxBound);

                return new Vector3(size.x  /*+ (step.x == .5f ? step.x : 0) */,
                                    size.y  /*- step.y*/,
                                    size.z /* - step.z*/);
            }
            set
            {
                count = new Vector3(value.x / step.x, value.y / step.y, value.z / step.z);
            }
        }


        public bool showPlane = true;

        public bool showMapBounds = true;               // 

        public bool KeepPlaneInbounds = true;

        public int fileWidth = 4, fileHeight = 4, tileWidth = 32, tileHeight = 32, nextObjectID = 0;

        const string tiledVersion = "1.0.2";

        public void Init()
        {
            Groups = new AxisGroup[] { new AxisGroup(), new AxisGroup(), new AxisGroup() };

            TileSets.Clear();

            TileSets = null;

            TileSets = new List<TileSet>();

            TileSetsChanged = true;

            RefreshGridPlaneSettingsAll();

            //RefreshGridPlaneSettings();
        }

        public void Init(Vector3 s)
        {
            size = s;

            fileWidth = (int)GetRealTiledMapSize().x;

            fileHeight = (int)GetRealTiledMapSize().y;

            Groups = new AxisGroup[] { new AxisGroup(), new AxisGroup(), new AxisGroup() };

            TileSets.Clear();

            TileSets = null;

            TileSets = new List<TileSet>();

            TileSetsChanged = true;

            RefreshGridPlaneSettingsAll();
        }





        ///------------------------------------------------------------------------///

        [ShowOnly]  // Use this for 64x Tiles else use 0 for 32x
        public Vector2 Epsilon = new Vector2(0.0025f, 0.0025f); // epsilon to fix some Texture bleeding	// 5e-06

        public float EpsilonX
        {
            get
            { return Epsilon.x; }
            set
            {
                if (Epsilon.x != value)
                {
                    Epsilon.x = value;
                    foreach (var group in Groups)
                        foreach (var layer in group.layers.Values)
                            layer.eps = Epsilon;
                }
            }
        }

        public float EpsilonY
        {
            get
            { return Epsilon.y; }
            set
            {
                if (Epsilon.y != value)
                {
                    Epsilon.y = value;
                    foreach (var group in Groups)
                        foreach (var layer in group.layers.Values)
                            layer.eps = Epsilon;
                }
            }
        }

        [ShowOnly]
        float tileDiscrepance = 0;   //0.001f

        public float Discrepance
        {
            get
            {
                return tileDiscrepance;
            }
            set
            {
                if (value != tileDiscrepance)
                {
                    tileDiscrepance = value;
                    foreach (var group in Groups)
                        foreach (var layer in group.layers.Values)
                            layer.discrepance = tileDiscrepance;
                }
            }
        }  //0.001f

        //[ShowOnly]
        public float GridUnlockedMove = 0.1f;
        ///------------------------------------------------------------------------///

        [SerializeField] public Color boundsColor = Color.yellow;   // gridMap volume color

        [HideInInspector]
        public GridOrientation currentOrientation = GridOrientation.Horizontal;

        [SerializeField]
        public Grid[] planes = {
            new Grid(Vector3.forward, Vector3.zero, new Color( .36f, .6f,    1, .8f)),
            new Grid(Vector3.up, Vector3.zero,      new Color( 0,       1, .26f, .6f)),
            new Grid(Vector3.right, Vector3.zero,   new Color( 1,    .26f,    0, .6f))
        };

        [SerializeField]
        static public List<TileSet> TileSets = new List<TileSet>();
        static public bool TileSetsChanged = false;


        //[SerializeField]
        [ShowOnly]
        public AxisGroup[] Groups = { new AxisGroup(), new AxisGroup(), new AxisGroup() };


        ///------------------------------------------------------------------------///


        // METHODS 
        public AxisGroup CurrentGroup                               // Get/Set Current Orientation Group 
        {
            get
            {
                return Groups[(int)currentOrientation];
            }
            set
            {
                Groups[(int)currentOrientation] = value;
            }
        }

        public Grid CurrentGrid                                 // Get actual grid
        {
            get { return planes[(int)currentOrientation]; }      // 
            //set { planes[(int)currentOrientation] = value; }   // and update
        }

        public float CurrentGridOffset                          // Get actual grid offset
        {
            get { return CurrentGrid.offset; }    // Check Previous Grid with current and
            set { planes[(int)currentOrientation].offset = value; }   // and update
        }

        public float GetGridMaxOffsetCurrent()
        {
            //switch (currentOrientation)
            //{
            //    case GridOrientation.Horizontal:
            //        return size.z;
            //    case GridOrientation.Vertical:
            //        return size.y;
            //    case GridOrientation.Depth:
            //        return size.x;
            //}
            //return 0;
            return GetGridMaxOffset(currentOrientation);
        }

        public float GetGridMaxOffset(GridOrientation orientation)
        {
            switch (orientation)
            {
                //case GridOrientation.Horizontal:
                //    return size.z;
                //case GridOrientation.Vertical:
                //    return size.y;
                //case GridOrientation.Depth:
                //    return size.x;

                case GridOrientation.Horizontal:
                    return count.z * step.z;
                case GridOrientation.Vertical:
                    return count.y * step.y;
                case GridOrientation.Depth:
                    return count.x * step.x;
            }
            return 0;
        }

        public float GetAxisStepCurrent()
        {
            return GetAxisStep(currentOrientation);
        }

        public float GetAxisStep(GridOrientation orientation)
        {
            switch (orientation)
            {
                case GridOrientation.Horizontal:
                    return step.z;
                case GridOrientation.Vertical:
                    return step.y;
                case GridOrientation.Depth:
                    return step.x;
            }
            return 0;
        }

        public Vector3 GetAxisStepVectorized()  // Returns a Vector3(0, 0, orientationStep);
        {
            switch (currentOrientation)
            {
                case GridOrientation.Vertical:
                    return Vector3.up * step.y;
                case GridOrientation.Depth:
                    return Vector3.right  * step.x;
                case GridOrientation.Horizontal:
                default:
                    return Vector3.forward* step.z;
            }
        }


        public Vector3 GetOrientationSize()                     //only for Tiled exporting 
        {
            return GetOrientationSize(currentOrientation);
        } 

        public Vector3 GetOrientationSize( GridOrientation orientation )
        {

            //Debug.Log("Orientation is " + orientation.ToString());
            switch (orientation) 
            {
                case GridOrientation.Horizontal:
                    return count;    //  new Vector3(size.x, size.y, size.z);
                case GridOrientation.Vertical:
                    //Debug.Log(new Vector3(count.x, count.z, count.y));
                    return new Vector3(count.x, count.z, count.y);
                case GridOrientation.Depth:
                    return new Vector3(count.z, count.y, count.x);
                default:
                return count;
            }
        }

        public Vector3 GetOrientationSteps()                     //only for Tiled exporting 
        {
            return GetOrientationSteps(currentOrientation);
        }

        public Vector3 GetOrientationSteps(GridOrientation orientation)
        {
            switch (orientation)
            {
                case GridOrientation.Horizontal:
                    return step;    //  new Vector3(size.x, size.y, size.z);
                case GridOrientation.Vertical:
                    return new Vector3(step.x, step.z, step.y);
                case GridOrientation.Depth:
                    return new Vector3(step.z, step.y, step.x);
                default:
                    return step;
            }
        }

        public Vector2 GetRealTiledMapSize()                    // This is for Tiled exporting only
        {
            int TiledMapWidth = (int)(count.x > GetOrientationSize(GridOrientation.Depth).x ?
                    count.x : GetOrientationSize(GridOrientation.Depth).x);

            int TiledMapHeight = (int)(count.y > GetOrientationSize(GridOrientation.Vertical).y ?
                            count.y : GetOrientationSize(GridOrientation.Vertical).y);

            return new Vector2(TiledMapWidth, TiledMapHeight);
        }


        public float SetNextOrientation()                       // Set Next Grid & return dist
        {
            int tempVal = ((int)currentOrientation + 1) % 3;
            currentOrientation = (GridOrientation)tempVal;
            RefreshGridPlaneSettings();
            return CurrentGridOffset;
        }

        public float SetPreviousOrientation()                   // Set Prev Grid & return dist
        {
            int tempVal = (int)currentOrientation;
            tempVal--;
            currentOrientation = (GridOrientation)(tempVal < 0 ? 2 : tempVal);
            RefreshGridPlaneSettings();
            return CurrentGridOffset;
        }

        public void RefreshGridPlaneSettingsAll()
        {
            planes[(int)GridOrientation.Horizontal].RefreshSettings(
                        new Vector3(count.x, count.y, 0),
                        planes[(int)GridOrientation.Horizontal].offset);

            planes[(int)GridOrientation.Vertical].RefreshSettings(
                        new Vector3(count.x, 0, count.z),
                        planes[(int)GridOrientation.Vertical].offset);

            planes[(int)GridOrientation.Depth].RefreshSettings(
                        new Vector3(0, count.y, count.z),
                        planes[(int)GridOrientation.Depth].offset);


            fileWidth = (int)GetRealTiledMapSize().x;
            fileHeight = (int)GetRealTiledMapSize().y;
        }

        public void RefreshGridPlaneSettings()
        {
            RefreshGridPlaneSettings(origin);
        }

        public void RefreshGridPlaneSettings(Vector3 newPosition)
        {
            origin = newPosition;

            switch (currentOrientation)
            {
                case GridOrientation.Horizontal:
                    planes[(int)currentOrientation].RefreshSettings(new Vector3(count.x, count.y, 0),
                        CurrentGridOffset);

                    break;
                case GridOrientation.Vertical:
                    planes[(int)currentOrientation].RefreshSettings(new Vector3(count.x, 0, count.z),
                        CurrentGridOffset);

                    break;
                case GridOrientation.Depth:
                    planes[(int)currentOrientation].RefreshSettings(new Vector3(0, count.y, count.z),
                       CurrentGridOffset); // GridDistance);
                    break;
            }

            fileWidth = (int)GetRealTiledMapSize().x;
            fileHeight = (int)GetRealTiledMapSize().y;
        }

        public Vector3 GetGridMousePosition()
        {
            Camera Cam = Camera.main;
            Vector2 mousePos = Input.mousePosition;
            Ray ray = Cam.ScreenPointToRay(mousePos);

            float distance;

            if (CurrentGrid.plane.Raycast(ray, out distance))
            {
                Vector3 rayPos = ray.GetPoint(distance);
                //Debug.DrawLine(rayPos, ray.direction, Color.magenta);


                rayPos.x = rayPos.x - rayPos.x % step.x /*+ step.x * 0.5f*/;
                if (currentOrientation == GridMap.GridOrientation.Depth) // Y Z
                    rayPos.x = CurrentGridOffset;
                //else
                //    rayPos.x = Mathf.Clamp(rayPos.x, 0, (count.x * step.x) - step.x);
                ////rayPos.x = Mathf.Clamp(rayPos.x, 0, count.x - step.x);
                ////rayPos.x = Mathf.Clamp(rayPos.x, step.x * 0.5f, size.x - step.x * 0.5f);

                rayPos.y = rayPos.y - rayPos.y % step.y /*+ step.y * 0.5f*/;
                if (currentOrientation == GridMap.GridOrientation.Vertical) // X Z
                    rayPos.y = CurrentGridOffset;
                //else
                //    rayPos.y = Mathf.Clamp(rayPos.y, 0, (count.y * step.y) - step.y);
                //    //rayPos.y = Mathf.Clamp(rayPos.y, 0, count.y - step.y);
                ////rayPos.y = Mathf.Clamp(rayPos.y, step.y * 0.5f, size.y - step.y * 0.5f);

                rayPos.z = rayPos.z - rayPos.z % step.z/* + step.z * 0.5f*/;
                if (currentOrientation == GridMap.GridOrientation.Horizontal) // X Y
                    rayPos.z = CurrentGridOffset;
                //else
                //    rayPos.z = Mathf.Clamp(rayPos.z, 0, (count.z * step.z) - step.z);
                //    //rayPos.z = Mathf.Clamp(rayPos.z, 0, count.z - step.z);
                ////rayPos.z = Mathf.Clamp(rayPos.z, step.z * 0.5f, size.z - step.z * 0.5f);

                //if(IsTile)
                //{

                    Vector3 MaxBound = new Vector3( (count.x * step.x) -step.x,
                                                    (count.y * step.y) -step.y, 
                                                    (count.z * step.z) -step.z);

                    MaxBound += GetAxisStepVectorized();    // Add +1 on current axis'step bound

                    rayPos = Vector3.Min(Vector3.Max(rayPos, Vector3.zero)/*rayPos*/, MaxBound);
                //}


                //rayPos = Vector3.Min(Vector3.Max(rayPos, Vector3.zero)/*rayPos*/, count);

                //Debug.DrawLine(mousePos, rayPos, Color.red);
                return rayPos;
            }

            return origin;    // else (When Ray is parallel to Grid Plane) return start position
        }

        public Vector3 GetGridMousePositionFree()
        {
            Camera Cam = Camera.main;
            Vector2 mousePos = Input.mousePosition;
            Ray ray = Cam.ScreenPointToRay(mousePos);

            float distance;

            if (CurrentGrid.plane.Raycast(ray, out distance))
            {
                Vector3 rayPos = ray.GetPoint(distance) -(step*0.5f);

                //rayPos.x = rayPos.x - rayPos.x % step.x ;
                if (currentOrientation == GridMap.GridOrientation.Depth) // Y Z
                    rayPos.x = CurrentGridOffset;

                //rayPos.y = rayPos.y - rayPos.y % step.y ;
                if (currentOrientation == GridMap.GridOrientation.Vertical) // X Z
                    rayPos.y = CurrentGridOffset;

                //rayPos.z = rayPos.z - rayPos.z % step.z;
                if (currentOrientation == GridMap.GridOrientation.Horizontal) // X Y
                    rayPos.z = CurrentGridOffset;
        

                Vector3 MaxBound = new Vector3((count.x * step.x) - step.x,
                                                (count.y * step.y) - step.y,
                                                (count.z * step.z) - step.z);

                MaxBound += GetAxisStepVectorized();    // Add +1 on current axis'step bound

                rayPos = Vector3.Min(Vector3.Max(rayPos, Vector3.zero)/*rayPos*/, MaxBound);


                //rayPos = Vector3.Min(Vector3.Max(rayPos, Vector3.zero)/*rayPos*/, count);

                return rayPos;
            }

            return origin;    // else (When Ray is parallel to Grid Plane) return start position
        }


        public void Draw()
        {

            if (showPlane)                              // Draw current grid plane reticle 
                {
         

                    if (!CurrentGroup.layers.ContainsKey(CurrentGrid.offset))
                        CurrentGrid.DrawCountColor(origin, step, Color.white * 0.5f);
                    else
                        CurrentGrid.DrawCount(origin, step);
                }

            if (showMapBounds)
                Utils.DrawGridArea(origin, size, size, boundsColor);
        }

        public void LoadProperties(XmlElement docElement, string filePath)
        {
            //////////////////////////////////////////////////////////	MAP PROPERTIES
            //TileSets.Clear();
            //currentOrientation = GridOrientation.Horizontal;

            Vector3 gridSize = new Vector3(int.Parse(docElement.Attributes["width"].Value),
                                        int.Parse(docElement.Attributes["height"].Value), 1);

            Vector3 gridStep = Vector3.one;

            if (docElement.FirstChild.Name == "properties")
                foreach (XmlNode MapProperty in docElement.FirstChild)
                {
                    if (MapProperty.Attributes["name"].Value.ToLower() == "width")
                    {
                        gridSize.x = float.Parse(MapProperty.Attributes["value"].Value);
                        //gridSize.x *= gridStep.x;
                    }

                    if (MapProperty.Attributes["name"].Value.ToLower() == "height")
                    {
                        gridSize.y = float.Parse(MapProperty.Attributes["value"].Value);
                        //gridSize.y *= gridStep.y;
                    }

                    if (MapProperty.Attributes["name"].Value.ToLower() == "depth")
                    {
                        gridSize.z = float.Parse(MapProperty.Attributes["value"].Value);
                        //gridSize.z *= gridStep.z;
                    }

                    if (MapProperty.Attributes["name"].Value.ToLower() == "stepheight")
                        gridStep.y = float.Parse(MapProperty.Attributes["value"].Value);

                    if (MapProperty.Attributes["name"].Value.ToLower() == "stepwidth")
                        gridStep.x = float.Parse(MapProperty.Attributes["value"].Value);

                    if (MapProperty.Attributes["name"].Value.ToLower() == "stepdepth")
                        gridStep.z = float.Parse(MapProperty.Attributes["value"].Value);

                    //if (MapProperty.Attributes["name"].Value.ToLower() == "music")
                    //Managers.Audio.PlayMusic((AudioClip)Resources.Load("Sound/" + MapProperty.Attributes["value"].Value, typeof(AudioClip)), .45f, 1);

                    //if (MapProperty.Attributes["name"].Value.ToLower() == "zoom" && Managers.Objects.Player != null)
                    //Managers.Objects.Player.GetComponent<CameraTargetAttributes>().distanceModifier = float.Parse(MapProperty.Attributes["value"].Value);
                }

            //gridMap = new GridMap(gridSize);

            count = gridSize;

            step = gridStep;

            fileWidth = (int)GetRealTiledMapSize().x;

            fileHeight = (int)GetRealTiledMapSize().y;

            tileWidth = int.Parse(docElement.Attributes["tilewidth"].Value);

            tileHeight = int.Parse(docElement.Attributes["tileheight"].Value);

            //gridMap.nextObjectID = int.Parse(docElement.Attributes["nextobjectid"].Value);

            //Groups = new Group[] { new Group(), new Group(), new Group() };
            TileSets.Clear();

            TileSets = null;

            TileSets = new List<TileSet>();

            RefreshGridPlaneSettingsAll();



            foreach (XmlNode TileSetInfo in docElement.GetElementsByTagName("tileset"))             // array of the level nodes.
                GridMap.TileSets.Add(new TileSet(TileSetInfo, filePath));

            TileSetsChanged = true;
        }

        public void ReBuildLayers()
        {
            foreach (var group in Groups)
                foreach (var layer in group.layers.Values)
                    layer.ReBuildLayer();
        }

        ///------------------------------------------------------------------------///

    }
}