﻿using UnityEngine;


namespace Sun.Tools
{

    [System.Serializable]
    public struct Grid          // A GridPlane with area, offset and color attributes
    {
        [SerializeField] public Color color;        // Grid color of reticle plane

        [Tooltip("Offset distance of Plane From the origin")] [SerializeField]
        public float offset;                        // Axial Distance from origin to gridPlane center

        [SerializeField] [HideInInspector]
        public Vector3 area;//, Center, Normal;     // = Vector3.one; // Area Size of GridPlane

        public Plane plane;

        public Grid(Vector3 normal, Vector3 position, Color c)
        {
            color = c;
            area = Vector3.one * 4;
            plane = new Plane(normal, position);
            offset = plane.distance;

        }


        public void RefreshSettings(Vector3? areaVolume = null, float distance = 0.0f)
        {
            this.area = areaVolume ?? Vector3.one * 4;
            offset = distance;
            plane.SetNormalAndPosition(plane.normal, plane.normal * offset);
            //plane.distance = offset;

        }

        public void RefreshSettings(Vector3 area, Vector3 position)
        {
            this.area = area;

            if (plane.normal == Vector3.up)
                offset = position.y;
            else if (plane.normal == Vector3.right)
                offset = position.x;
            else
                offset = position.z;

            plane.distance = offset;
        }

        public void Draw(Vector3? position = null, Vector3? step = null)
        {
            Sun.Utils.DrawGridArea((position ?? Vector3.zero) + plane.normal * offset,
                                area,
                                step ?? Vector3.one,
                                color);    // Draw With RGB on each Axis
        }


        public void DrawCount(Vector3? start = null, Vector3? step = null)
        {
            Sun.Utils.DrawGridTiled((start ?? Vector3.zero) + plane.normal * offset,
                                    area, step ?? Vector3.one, color);
        }

        public void DrawCountColor(Vector3? start = null, Vector3? step = null, Color? c = null)
        {
            Sun.Utils.DrawGridTiled((start ?? Vector3.zero) + plane.normal * offset,
                                    area, step ?? Vector3.one, c ?? color);
        }


        /*
         
        /// Draw a 3D volumetric TileGrid set start, tileCount size & color (step can be just Vector3.one )
        public static void DrawGridTiled(Vector3 start, Vector3 count, Vector3? step = null, Color? color = null)
        {
            Color[] colors =  {    color ??    new Color(1, .26f, 0, .78f),
                                    color ??    new Color(0, 1, .26f, .5f),
                                    color ??    new Color(.26f, .675f, 1, .78f) };

            DrawGridTiled(start, count, step ?? Vector3.one, colors);

        }

        /// Draw a 3D volumetric TileGrid set start, tileCount size & color (step can be just Vector3.one )
        public static void DrawGridTiled(Vector3 start, Vector3 count, Vector3 step, Color[] colors)
        {
            step = Vector3.Max(step, Vector3.one * 0.025f);        // Clamp to a minimun step for GPU 

            CreateLineMaterial();

            Vector3 realSize =
                new Vector3(count.x * step.x, count.y * step.y, count.z * step.z);

            lineMaterial.SetPass(0);                                // set the current material
            GL.Begin(GL.LINES);

            for (float j = 0; j <= (int)realSize.y; j += step.y)                        //Layers
            {

                for (float i = 0; i <= (int)realSize.z; i += step.z)                    //X axis lines      
                {
                    GL.Color(colors[0]);                   //GL.Color(Color.red);
                    GL.Vertex3(start.x, start.y + j, start.z + i);
                    GL.Vertex3(start.x + (int)realSize.x, start.y + j, start.z + i);
                }

                for (float i = 0; i <= (int)realSize.x; i += step.x)                    //Z axis lines
                {
                    GL.Color(colors[2]);                   //GL.Color(Color.blue);
                    GL.Vertex3(start.x + i, start.y + j, start.z);
                    GL.Vertex3(start.x + i, start.y + j, start.z + (int)realSize.z);
                }
            }

            for (float i = 0; i <= (int)realSize.z; i += step.z)                        //Y axis lines
            {
                GL.Color(colors[1]);                     //GL.Color(Color.green);
                for (float k = 0; k <= (int)realSize.x; k += step.x)
                {
                    GL.Vertex3(start.x + k, start.y, start.z + i);
                    GL.Vertex3(start.x + k, start.y + (int)realSize.y, start.z + i);
                }
            }

            GL.End();
        }
        */

    }
}
