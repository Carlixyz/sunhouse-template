﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Xml;
using System.IO;
using System;
using System.Text;
using Ionic.Zlib;

[Serializable]
public class TileSet  
{
    [SerializeField]
    public string TileSetName = "";
    [SerializeField]
    public string ImageName = "";                                   // Image Source data references
    [SerializeField]
    public string ImagePath = "";

    [SerializeField]
    public ShaderMaterial currentShader = ShaderMaterial.DoubleSided;

    public ShaderMaterial CurrentShader
    {
        get { return currentShader; }
        set
        {
            if (currentShader != value)
            {
                currentShader = value;
                SetupShaderMaterial();
            }
        }
    }

    [SerializeField]
    public int ImageWidth = 512;    // 
    [SerializeField]
    public int ImageHeight = 512;

    [SerializeField]
    public int tileWidth = 64;      //
    [SerializeField]
    public int tileHeight = 64;

    public int TileWidth
    {
        get { return tileWidth; }
        set
        {
            if (tileWidth != value)
            {
                tileWidth = value;
                SetupTileData();
                //TileColumns = ImageWidth / tileWidth;
                //this.ModuleWidth = 1.0f / TileColumns;
                //TileCount = TileColumns * TileRows;
            }
        }
    }      //

    public int TileHeight
    {
        get { return tileHeight; }
        set
        {
            if (tileHeight != value)
            {
                tileHeight = value;
                SetupTileData();

                //TileRows = ImageHeight / tileHeight;
                //this.ModuleHeight = 1.0f / TileRows;
                //TileCount = TileColumns * TileRows;
            }
        }
    }

    [SerializeField]
    public int TileColumns = 8;     
    [SerializeField]
    public int TileRows = 8;

    [SerializeField]
    public int TileCount = 64;      //
    [SerializeField]
    public int FirstGid = 0;

    [SerializeField]
    public float ModuleWidth = 1.0f;                                // Tiled image area % coords            
    [SerializeField]
    public float ModuleHeight = 1.0f;

    public Material mat;
    public Dictionary<int, string> Collisions = new Dictionary<int, string>();

    public TileSet(string name, string filePath)
    {
        tileWidth = 32; // scrollLayer width inside bitmap file  ( 64 )
        tileHeight = 32;// scrollLayer height inside bitmap file 
        //TileCount = 256;      //
        ImagePath = "";
        TileSetName = name;                         // Image Source data references
                                                    //FirstGid = 0;
        ImageWidth = 512; // File Resolution (512)
        ImageHeight = 512;

        //TileColumns = ImageWidth / tileWidth;
        //TileRows = ImageHeight / tileHeight;

        TileColumns =  Mathf.Clamp(ImageWidth / tileWidth, 1, int.MaxValue);
        TileRows = Mathf.Clamp(ImageHeight / tileHeight, 1, int.MaxValue);

        this.ModuleWidth = 1.0f / TileColumns;
        this.ModuleHeight = 1.0f / TileRows;

        TileCount = TileColumns * TileRows;

        FirstGid = ((int)Sun.Tools.GridMap.TileSets.Count * TileCount)+1/* % TileCount*/;

        //ImagePath = TileSetNode.Attributes["source"].Value; // Ex.: "textures/test.png"
        //ImageName = ImagePath.Remove(0, ImagePath.LastIndexOf("\\") + 1);
        //Texture2D tex = new Texture2D(2, 2);
        //Texture tex;
        SetupShaderMaterial();

        //SetImagePathName("Bitmap File Path Here", filePath);
        //if(this.mat != null)
        //SetImagePathName("", filePath);

        //Texture tex = new Texture();
        //this.mat.mainTexture = tex;

    }

    public TileSet(XmlNode TileSetXML, string FilePath)			// if ( TileSet.HasChildNodes ) {  var lTileSet : cTileSet = new cTileSet(); lTileSet.Load(
    {
        tileWidth = System.Convert.ToInt32(TileSetXML.Attributes["tilewidth"].Value); // scrollLayer width inside bitmap file  ( 64 )
        tileHeight = System.Convert.ToInt32(TileSetXML.Attributes["tileheight"].Value);// scrollLayer height inside bitmap file 
        TileSetName = TileSetXML.Attributes["name"].Value;                          // Image Source data references

  

        foreach (XmlNode TileSetNode in TileSetXML)
        {
            if (TileSetNode.Name == "image")
            {
                ImageWidth = System.Convert.ToInt32(TileSetNode.Attributes["width"].Value); // File Resolution (512)
                ImageHeight = System.Convert.ToInt32(TileSetNode.Attributes["height"].Value);

                TileColumns = Mathf.Clamp(ImageWidth / tileWidth, 1, int.MaxValue);
                TileRows = Mathf.Clamp(ImageHeight / tileHeight, 1, int.MaxValue);

                //TileColumns = ImageWidth / tileWidth;
                //TileRows = ImageHeight / tileHeight;

                TileCount = TileColumns * TileRows;

                this.ModuleWidth = 1.0f / TileColumns;
                this.ModuleHeight = 1.0f / TileRows;

                FirstGid = System.Convert.ToInt16(TileSetXML.Attributes["firstgid"].Value);

                ImagePath = TileSetNode.Attributes["source"].Value; // Ex.: "textures/test.png"
                ImageName = ImagePath.Remove(0, ImagePath.LastIndexOf("\\") + 1);						    // Image Source data references

            }
            else if (TileSetNode.Name == "tile")
            {
                if(TileSetNode.FirstChild.Name == "objectgroup")
                    GetTileCollisionProperty(TileSetNode.FirstChild.FirstChild.FirstChild,
                       int.Parse(TileSetNode.Attributes["id"].Value) + 1);

                if (TileSetNode.FirstChild.Name == "properties")
                    GetTileCollisionProperty(TileSetNode.FirstChild.FirstChild,
                       int.Parse(TileSetNode.Attributes["id"].Value) + 1);

            }


            //Debug.Log(" TileColumns " + TileColumns
            //                  + " TileRows " + TileRows);
            //Texture2D tex = null;

            /// ---------------------------------------------- ADD EVENT CALL THERE
        }

            ///
            SetupShaderMaterial();
            //////----------------------------------------------------------------//////////

            SetImagePathName(ImagePath, FilePath);

    }

    public void SetupTileData()
    {
        //tileWidth = 32; // scrollLayer width inside bitmap file  ( 64 )
        //tileHeight = 32;// scrollLayer height inside bitmap file 

        //TileRows = ImageHeight / tileHeight;
        //this.ModuleHeight = 1.0f / TileRows;
        //TileCount = TileColumns * TileRows;

        //return;

        if (this.mat != null && this.mat.mainTexture != null)
        {
            ImageWidth = this.mat.mainTexture.width; // File Resolution (512)
            ImageHeight = this.mat.mainTexture.height;
        }
        else
            ImageWidth = ImageHeight = 512; // Default File Resolution (512)

        TileColumns =  Mathf.Clamp(ImageWidth / tileWidth, 1, int.MaxValue);
        TileRows = Mathf.Clamp(ImageHeight / tileHeight, 1, int.MaxValue);

        //TileColumns = ImageWidth / tileWidth;
        //TileRows = ImageHeight / tileHeight;

        TileCount = TileColumns * TileRows;

        int index = 0;
        foreach (var ts in Sun.Tools.GridMap.TileSets)
        {
            if (ts == this)
                FirstGid = index * TileCount +1;
            index++;
        }

        //FirstGid = ((int)Sun.Tools.GridMap.TileSets.Count * TileCount) + 1/* % TileCount*/;
        ////FirstGid = 1 ;

        this.ModuleWidth = 1.0f / TileColumns;
        this.ModuleHeight = 1.0f / TileRows;


        //ImagePath = TileSetNode.Attributes["source"].Value; // Ex.: "textures/test.png"
        ImageName = ImagePath.Remove(0, ImagePath.LastIndexOf("\\") + 1);                           // Image Source data references

    }

    void SetupShaderMaterial()
    {

        Shader shade;

        switch (CurrentShader)
        {
            case ShaderMaterial.SingleSideSimpleLit:
                shade = Shader.Find("Custom/1SidedCutoutLit") ;      
                break;
            case ShaderMaterial.DoubleSideSimpleLit:
                shade = Shader.Find("Custom/2Sided/2SidedCutoutLit") ;         ///
                break;
            case ShaderMaterial.FullShadowCast:
                shade = Shader.Find("Custom/2Sided/2SidedCutout-ShadowCast"); ///
                break;
            case ShaderMaterial.SingleSided:
                shade = Shader.Find("Unlit/Transparent Cutout");        ///
                break;
            default:
            case ShaderMaterial.DoubleSided:
                shade = Shader.Find("Custom/2Sided/2SidedCutOutUnlit"); ///
                break;
        }

        if (this.mat == null)
            this.mat = new Material(shade);
        else
            this.mat.shader = shade;

        if (this.mat == null)
        {
            Debug.LogError(" Shader CutOut 2Sided mat isn't found or ready " + mat.name);
            this.mat = new Material(Shader.Find("Unlit/Transparent"));
        }

    }

    public void SetImagePathName(string imagePath,string filePath)
    {
        //if(imagePath != ImagePath)
        //{
            ImagePath = imagePath;
            string path = "";
        //Texture2D tex = new Texture2D(2, 2 );
        //Texture tex = new Texture();
        Texture tex;


        if (string.IsNullOrEmpty(imagePath))
            Debug.Log("file path IsNullOrEmpty ->" + imagePath + "<-");



        //    tex = new Texture2D(1024, 1024);
        //else 

        if (ImagePath.Contains("Resources"))        //#if (UNITY_ANDROID || UNITY_WEBGL)
            {
                if (ImagePath.Contains("."))                // Trim file'  ".extension"
                    path = ImagePath.Remove(ImagePath.LastIndexOf("."));

                path = path.Replace("/", "\\");             // Avoid /t, /r, /etc bullshit
                path = path.Remove(0, path.IndexOf("Resources\\") + 10);

                Debug.Log("Resource path is ->" + path + "<-");
                tex = Resources.Load<Texture2D>(path);

                    
                if(tex == null) // Maybe it's inside in Another Project's Resource folder...
                {
                    Debug.Log("Found Extern project Resource path ->" + path + "<-");

                    path = "file://" + AttachRelativePathString(filePath, ImagePath);

                    WWW www = new WWW(path);
                    tex = www.texture;
                }
                //else ImagePath = path;

            }
            else  
            {
                path = "file://" + AttachRelativePathString(filePath, ImagePath);

                //Debug.Log("Loading This BITMAP ->" + path + "<- \n with filePath "+ filePath + " y ImagePath "+ ImagePath);

                WWW www = new WWW(path);
                tex = www.texture;
            }    //#endif 


            if (tex == null)        // If still null it's because you suck it ! XD
            {
                Debug.LogError(path + " texture file not found, put it in the same Tiled map folder: ");
                return;   //	 this.close; 
            }
        //else Debug.Log("tex is ready " + tex.name);

            ImageWidth = tex.width;
            ImageHeight = tex.height;

            if(ImageWidth != ImageHeight)
                Debug.LogWarning(path + " texture size not self-matching: " + ImageWidth + " != " +ImageHeight);

            tex.filterMode = FilterMode.Point;
            tex.wrapMode = TextureWrapMode.Repeat;
            tex.anisoLevel = 0;
            //tex.mipMapBias = 3;

            this.mat.mainTexture = tex;

        //ImagePath = path;

        /*currentTileSet.*/SetupTileData();
        //}
    }

    // The best foo in the world EVER PERIOD (Replaces all relatives "../" ocurrences inside one absolut path)
    public string AttachRelativePathString(string basePath, string relativePath)
    {

        if (string.IsNullOrEmpty(basePath))
        {
            Debug.LogWarning("basePath is empty >" + basePath + "< using only imagePath");
            return relativePath;
        }   

        if (string.IsNullOrEmpty(relativePath))
            Debug.LogWarning("This relativePath is empty " + relativePath);

        if (basePath.Contains("."))
            basePath = basePath.Remove(basePath.LastIndexOf("\\") + 1);

        string match = "..";
//        https://stackoverflow.com/questions/541954/how-would-you-count-occurrences-of-a-string-within-a-string
        int totalMatches = (relativePath.Length - relativePath.Replace(match, "").Length) / match.Length;

        relativePath = relativePath.Replace("/", "\\");
        relativePath = relativePath.Replace("..\\", "");

        while (totalMatches >= 0) 
        {
            basePath = basePath.Remove(basePath.LastIndexOf("\\"));
            totalMatches--;
        }

        //Debug.Log("result " + System.IO.Path.Combine(basePath, relativePath));

        return System.IO.Path.Combine(basePath, relativePath);
    }


    public void GetTileCollisionProperty(XmlNode node, int id)
    {

        //if (node.Attributes["name"].Value.ToLower() == "box")
        //        Collisions.Add(id, "box");

        //if (node.Attributes["name"].Value.ToLower() == "plane")
        //        Collisions.Add(id, "plane");

        //if (node.Attributes["name"].Value.ToLower() == "collision")
        //        Collisions.Add(id, node.Attributes["value"].Value.ToLower());

        if (node.Attributes["name"].Value.ToLower() == "box")
            Collisions.Add(id, "box3d");

        if (node.Attributes["name"].Value.ToLower() == "plane")
            Collisions.Add(id, "plane");

        if (node.Attributes["name"].Value.ToLower() == "collision")
        {
            Collisions.Add(id, node.Attributes["value"].Value.ToLower());
            Debug.Log("Added collision as " + node.Attributes["value"].Value.ToLower());
        }

    }


    public enum ShaderMaterial
    {
        DoubleSided = 0,        ///
        DoubleSideSimpleLit,
        SingleSided,            
        SingleSideSimpleLit,
        FullShadowCast,         ///
        //Transparent             ///
    }

    /*
    Texture tex;
    public IEnumerator LoadSprite(string absoluteImagePath)
    {
        string finalPath;
        WWW localFile;
        //Texture texture;
        //Sprite sprite;

        finalPath = "file://" + absoluteImagePath;
        localFile = new WWW(finalPath);

        yield return localFile;
        //Texture tex;
        tex = localFile.texture;
        //sprite = Sprite.Create(texture as Texture2D, new Rect(0, 0, texture.width, texture.height), Vector2.zero);
    }
    */

}
