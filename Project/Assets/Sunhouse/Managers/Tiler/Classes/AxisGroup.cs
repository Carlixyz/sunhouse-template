﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Sun.Tools
{
    //[System.Serializable]
    public class AxisGroup // Simple Bind 4 fast access Tile Layers with right Orientation & Alignment
    {

        //public List<TileLayer> layers = new List<TileLayer>();

        public Dictionary<float, TileLayer> layers = new Dictionary<float, TileLayer>();

        // Use this when enabled
        public AxisGroup()
        {
            //size = Vector3.one;
        }



        Vector2 area;

        public int width
        {
            get
            {
                return (int)area.x;
            }
            set
            {
                area.x = value;
            }
        }

        public int height
        {
            get
            {
                return (int)area.y;
            }
            set
            {
                area.y = value;
            }
        }

    }
}
