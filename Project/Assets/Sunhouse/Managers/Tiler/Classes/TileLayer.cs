﻿using System.Collections;
using System.Collections.Generic;
using System.Xml;
using UnityEngine;
//using Ionic.Zlib;

namespace Sun.Tools
{
#if UNITY_EDITOR
    [UnityEditor.CanEditMultipleObjects]
#endif
    [System.Serializable]
    public class TileLayer : MonoBehaviour
    {
        [SerializeField]
        public Transform LayerTransform;

        [SerializeField]
        public bool visible = true, Locked = false;

        public bool Visible
        {
            get { return visible; }
            set
            {
                if (value != visible)
                {
                    visible = value;
                    gameObject.SetActive(visible);
                }
            }
        }

        [Range(0,1.0f)]
        public float Opacity = 1.0f;

        public GridMap.GridOrientation currentOrientation = GridMap.GridOrientation.Horizontal;

        [SerializeField]
        public GridMap.CollisionType CollisionLayer = GridMap.CollisionType.None;

        [SerializeField, ShowOnly]
        int realLayerWidth, realLayerHeight;

        [ShowOnly, SerializeField]
        bool reSized = false;

        public int Width
        {
            get { return (int)size.x; }
            set { size = new Vector3(value, size.y); }
            // Orientation Helpers
        }                               // Current Orientation 2D Helper                 

        public int Height 
        {
            get { return (int)size.y; }
            set { size = new Vector3(size.x, value); }
        }                                                   // These used to been "floats

        public int Area { get { return (Width* Height) *4; } }

        [SerializeField]
        public Vector3 size = Vector3.one;                  // Total Grid Count

        public Vector3 Size
        {
            get
            {
                switch (currentOrientation)
                {
                    case GridMap.GridOrientation.Vertical:
                        return new Vector3(size.x, size.z, size.y);
                    case GridMap.GridOrientation.Depth:
                        return new Vector3(size.z, size.y, size.x);
                    default: // case GridMap.GridOrientation.Horizontal:
                        return size;    //  new Vector3(size.x, size.y, size.z);
                }
            }
            set
            {
                switch (currentOrientation)
                {
                    case GridMap.GridOrientation.Horizontal:
                        size = value;    //  new Vector3(size.x, size.y, size.z);
                        break;
                    case GridMap.GridOrientation.Vertical:
                        size = new Vector3(value.x, value.z, value.y);
                        break;
                    case GridMap.GridOrientation.Depth:
                        size = new Vector3(value.z, value.y, value.x);
                        break;
                }
            }
        }

        [SerializeField]
        public Vector3 step = Vector3.one;                  // Each Grid' step

        [SerializeField]
        public Vector3 offset = Vector3.zero;               // Grid offset (Z is Depth)

        public Vector3 Offset
        {
            get { return offset; }
            set
            {
                if(value != offset)
                {
                    offset = value;
                    ReBuildLayer();
                }
            }
        }

        public float OffsetX
        {
            get { return offset.x; }
            set
            {
                if (value != offset.x)
                {
                    offset.x = value;
                    ReBuildLayer();
                }
            }
        }

        public float OffsetY
        {
            get { return offset.y; }
            set
            {
                if (value != offset.y)
                {
                    offset.y = value;
                    ReBuildLayer();
                }
            }
        }

        public float OffsetZ
        {
            get { return offset.z; }
            set
            {
                if (value != offset.z)
                {
                    offset.z = value;
                    ReBuildLayer();
                }
            }
        }

        public Vector3 OffsetConversion
        {
            get
            {
                switch (currentOrientation)
                {
                    case GridMap.GridOrientation.Vertical:
                        return new Vector3(offset.x, offset.z, offset.y);
                    case GridMap.GridOrientation.Depth:
                        return new Vector3(offset.z, offset.y, -offset.x);
                    default: // case GridMap.GridOrientation.Horizontal:
                        return offset;    //  new Vector3(size.x, size.y, size.z);
                }
            }
            set
            {
                switch (currentOrientation)
                {
                    case GridMap.GridOrientation.Horizontal:
                        offset = value;    //  new Vector3(size.x, size.y, size.z);
                        break;
                    case GridMap.GridOrientation.Vertical:
                        offset = new Vector3(value.x, value.z, value.y);
                        break;
                    case GridMap.GridOrientation.Depth:
                        offset = new Vector3(value.z, value.y, value.x);
                        break;
                }
            }
        }

        /// --------------------------------------------------------------------------- /// VARS


        [SerializeField] public byte[] tiles;

        uint FlippedHorizontallyFlag = 0x80000000;

        uint FlippedVerticallyFlag = 0x40000000;

        uint FlippedDiagonallyFlag = 0x20000000;

        [ShowOnly] public Vector2 eps = new Vector2(0, 0); // epsilon to fix some Texture bleeding	// 5e-06
        //public Vector2 eps = new Vector2(0.0025f, 0.0025f); // epsilon to fix some Texture bleeding	// 5e-06
        [ShowOnly] public float discrepance = 0.001f;    // Small dist for better tile fit during edition

        public bool CombineMesh = true;

        bool isBaking = false;

        [ShowOnly] public int totalTiles = 0;

        [ShowOnly] public int currentTileSetID = -1;

        //[ShowOnly, SerializeField] int LastUsedMat = 0;

        /// --------------------------------------------------------------------> FUNCTIONS

        void OnEnable()
        {
            LayerTransform = transform;
        }

        public int GetRealTilesWidth { get { return realLayerWidth; } }

        public void SetupProperties(GridMap gMap)
        {
            currentOrientation = gMap.currentOrientation;

            Width = (int)gMap.GetOrientationSize(currentOrientation).x;

            Height = (int)gMap.GetOrientationSize(currentOrientation).y;

            step = gMap.step;

            eps = gMap.Epsilon;             //eps = eps;

            discrepance = gMap.Discrepance;

            LayerTransform = transform;

            realLayerWidth = (int)Width;

            realLayerHeight = (int)Height;

            int newLength = (int)(Width * Height) * 4;

            tiles = new byte[newLength];    // Set all bytes to 0 before starting

            CollisionLayer = GridMap.CollisionType.None;

        }

        public void LoadProperties(XmlNode layerInfo)
        {
            LayerTransform = transform;

            //int ColIndex = 0;

            //int RowIndex = int.Parse(layerInfo.Attributes["height"].Value) - 1;

            //bool visible = int.Parse(layerInfo.Attributes["visible"].Value) != 0;

            realLayerWidth = int.Parse(layerInfo.Attributes["width"].Value);

            realLayerHeight = int.Parse(layerInfo.Attributes["height"].Value);

            CollisionLayer = GridMap.CollisionType.None;
            
            currentOrientation = GridMap.GridOrientation.Horizontal;


            Vector3 displacemnt = Vector3.zero;

            XmlElement Data = (XmlElement)layerInfo.FirstChild;
            while (Data.Name != "data")
            {
                foreach (XmlElement property in Data)
                {
                    if (property.GetAttribute("name").ToLower() == "collision")
                        if (property.GetAttribute("value").ToLower() != "plane")
                            CollisionLayer = GridMap.CollisionType.Box3d;                                         // check if it's a boxed collision and setup
                        else
                            CollisionLayer = GridMap.CollisionType.Plane3d;                                         // else it's a plane type collsion Layer..

                    if (property.GetAttribute("name").ToLower() == "depth")
                        displacemnt.z = float.Parse(property.GetAttribute("value"));

                    //if (property.GetAttribute("name").ToLower() == "offsetx")
                    //    Offset.x = float.Parse(property.GetAttribute("value")) / tileWidth;

                    if (property.GetAttribute("name").ToLower() == "horizontaloffset")
                    {
                        displacemnt.z = float.Parse(property.GetAttribute("value"));
                        currentOrientation = GridMap.GridOrientation.Horizontal;
                    }

                    if (property.GetAttribute("name").ToLower() == "verticaloffset")
                    {
                        displacemnt.z = float.Parse(property.GetAttribute("value"));
                        currentOrientation = GridMap.GridOrientation.Vertical;
                    }

                    if (property.GetAttribute("name").ToLower() == "depthoffset")
                    {
                        displacemnt.z = float.Parse(property.GetAttribute("value"));
                        currentOrientation = GridMap.GridOrientation.Depth;
                    }

                }
                Data = (XmlElement)Data.NextSibling;
            }

            offset = displacemnt;
            //Offset = displacemnt;
            //Debug.Log("Layer "+ gameObject.name + " offset is " + Offset);

            /// We should not do this here because We didn't load up the REAL Volumetric size yet
            LoadTileData(Data); /// But We need to because it's the only place to get the Data

            //if (LoadData(Data) == true)
            //    StartCoroutine(BuildTiles(Vector3.one));

            // REMEMBER TO DON't DO THIS BECAUSE WE'RE USING DIFERENT ORIENTATION FOR EVERY LAYER
            //LayerTransform.position = new Vector3(
            //            LayerTransform.position.x, LayerTransform.position.y, float.Parse(property.GetAttribute("value")));

            /// -------------------------------------------------------------------------- ///


            //Debug.LogError(" Format Error: Save Tiled File in XML style or Compressed mode(Gzip + Base64)");

        }

        public void LoadTileData(XmlElement layerData)
        {

            if (layerData.HasAttribute("compression") )
            {
                if (layerData.Attributes["compression"].Value == "zlib")
                {
                    // Decode Base64 and then Uncompress zlib scrollLayer Information
                    tiles = Ionic.Zlib.ZlibStream.UncompressBuffer(
                        System.Convert.FromBase64String(layerData.InnerText)); 

                    return;
                }

                //XmlElement Data = (XmlElement)layerData; // <-- YOU NEED THIS
                // & CHECK IF DATA IS GZIP COMPRESSED AND CREATE OR BUILD ALL TILES INSIDE EACH LAYER			
                if (layerData.Attributes["compression"].Value == "gzip")
                {
                    // Decode Base64 and then Uncompress Gzip scrollLayer Information
                    //byte[] decodedBytes = Ionic.Zlib.GZipStream.UncompressBuffer(System.Convert.FromBase64String(layerData.InnerText));
                    tiles = Ionic.Zlib.GZipStream.UncompressBuffer(
                        System.Convert.FromBase64String(layerData.InnerText));

                    return;
                }
            }

            // https://github.com/carlixyz/niangapiry/commit/e648910303d349db07e01f7b2652c61a762bd704#diff-b5befe894ab9f77eff61282f74c5727f
            /*
            else if (Data.HasChildNodes)    // Else if not a zlib Compression then try as XML data
            {
                foreach (XmlNode TileInfo in Data.GetElementsByTagName("tile"))
                {
                    var TileRefXml = BuildTile(System.Convert.ToUInt32(TileInfo.Attributes["gid"].Value));
                    if (TileRefXml != null)
                        TileRefXml.transform.position = new Vector3(ColIndex * TileOutputSize.x,
                            RowIndex * TileOutputSize.y,LayerTransform.position.z);
                        TileRefXml.transform.parent = LayerTransform;
                        if (CollisionLayer)
                        (TileRefXml.AddComponent("BoxCollider") as BoxCollider).size = Vector3.one;
                }
                ColIndex++;
                RowIndex -= System.Convert.ToByte(ColIndex >= int.Parse(LayerInfo.Attributes["width"].Value));
                ColIndex = ColIndex % int.Parse(LayerInfo.Attributes["width"].Value);
            }//end of each Tile XML Info 
            */

            Debug.LogError(" Format Error: Save Tiled File in XML style or Compressed mode(Gzip + Base64)");
        }

        public string GetTileDataCompressed()
        {
            //tiles = Ionic.Zlib.ZlibStream.UncompressBuffer(
            //           System.Convert.FromBase64String(layerData.InnerText));

            //Ionic.Zlib.CompressionLevel = Ionic.Zlib.CompressionLevel.BestCompression;
            //string compression =
            return    System.Convert.ToBase64String( Ionic.Zlib.ZlibStream.CompressBuffer(tiles)) ;
        }

        public void SquashTileData(int biggerWidth)
        {

            byte[] tempBytes = new byte[tiles.Length];
            tiles.CopyTo(tempBytes, 0);
            //Debug.Log("Squash TILED FORMAT WIDTH " + name + " Orientation: " + currentOrientation);

            int trim = Mathf.Abs(biggerWidth - (int)Width) * 4;
            int i = 0;
            int fileCol = 0, acum = 0;
            int newLength = (int)(Width * Height) * 4;

            tiles = new byte[newLength];    // Set all bytes to 0 before starting

            while (i < newLength)
            {
                int count = fileCol + (acum /** fileWidth * 4*/);

                if (count >= tempBytes.Length || i >= tempBytes.Length) break;

                tiles[i] = tempBytes[count];

                i++;
                fileCol++;

                if (fileCol >= ((int)Width * 4))
                {
                    acum += fileCol + trim;
                    fileCol = 0;
                }
            }
        }

        public void ExpandTileData()
        {
            byte[] oldBytes = new byte[tiles.Length];
            tiles.CopyTo(oldBytes, 0);      // Get all previous bytes

            int trim = Mathf.Abs((int)Width - realLayerWidth) * 4;
            int i = 0;
            int fileCol = 0, acum = 0;
            int newLength = (int)(Width * Height) * 4;

            tiles = new byte[newLength];    // Set all bytes to 0 before starting

            while (i < newLength)
            {
                int count = fileCol + acum;

                if (count >= newLength || i >= oldBytes.Length) break;

                tiles[count] = oldBytes[i];

                i++;
                fileCol++;

                if (fileCol >= ((int)realLayerWidth * 4))
                {
                    acum += fileCol + trim;
                    fileCol = 0;
                }
            }
        }

        public void ReSize()
        {
            if (realLayerWidth != (int)Width || realLayerHeight != (Height))
                reSized = true;
            //if (realLayerWidth * realLayerHeight != (int)(Width * Height))

            if (reSized)
            {
                //if (Width != GetRealTilesWidth)            // Checkup & Fix File Width Mis-match
                if (GetRealTilesWidth <= Width)
                    ExpandTileData();
                else
                    SquashTileData(GetRealTilesWidth);

                realLayerWidth = (int)Width;
                realLayerHeight = (int)Height;
                reSized = false;
                //Debug.Log("ReSized!");
            }
        }

        public void ReBuildLayer()
        {
            DestroyAll();

            ReSize();

            BuildTiles();
        }

        public void DestroyAll()
        {
            Component[] comps = GetComponents<Component>();
            for (int i = comps.Length - 1; i >= 0; --i)
                if (comps[i] != this && typeof(Transform) != comps[i].GetType())
                    Destroy(comps[i]);

            //Destroy(GetComponent<meshCombiner>());
            //Destroy(GetComponent<MeshFilter>());
            //Destroy(GetComponent<MeshRenderer>());

            for (int i = LayerTransform.childCount - 1; i >= 0; --i)
                GameObject.Destroy(LayerTransform.GetChild(i).gameObject);
            LayerTransform.DetachChildren();

        }

        public void BuildTiles()
        {
            //BuildTiles(Step);
            BuildTiles(step);

            if (CombineMesh && !isBaking && totalTiles > 0)
                Invoke("BakeTiles", 0);                //   BakeTiles(); //  StartCoroutine(BakeTilesDelayed());

        }

        public void BuildTiles(Vector3 step) 
        {
            totalTiles = 0;

            int ColIndex = 0;

            int RowIndex = (int)Height - 1; //  (int)Size.y - 1;
            //int RowIndex = int.Parse(layerInfo.Attributes["height"].Value) - 1;

            LayerTransform.position = OffsetConversion;

            for (int tile_index = 0; tile_index < tiles.Length; tile_index += 4)
            {
                //GameObject TileRef = new GameObject();
                GameObject TileRef = BuildTile((uint)(tiles[tile_index] |
                                                     tiles[tile_index + 1] << 8 |
                                                  tiles[tile_index + 2] << 16 |
                                                     tiles[tile_index + 3] << 24)); // BuildTile( uint global_tile_id ); // >> Build a Tile with that


                if (TileRef != null)
                {

                    switch(currentOrientation)      // DON'T TOUCH THESE, THEY WORK
                    {
                        case GridMap.GridOrientation.Vertical:

                            TileRef.transform.position = LayerTransform.position +
                            new Vector3(ColIndex * step.x, 0, RowIndex * step.z);

                            break;
                        case GridMap.GridOrientation.Depth:
                            TileRef.transform.position = LayerTransform.position +
                            new Vector3(0, RowIndex * step.y, ((Width - ColIndex) - 1) * step.z);
                 
                            break;
                        default:
                            TileRef.transform.position = LayerTransform.position +
                                new Vector3(ColIndex * step.x, RowIndex * step.y, 0);

                            break;
                    }

                    TileRef.transform.parent = LayerTransform;

                    if (CollisionLayer > 0)
                        if (CollisionLayer == GridMap.CollisionType.Box3d && TileRef.GetComponent<BoxCollider>() == null)
                            (TileRef.AddComponent<BoxCollider>() as BoxCollider).size = Vector3.one; // simple boxed collision
                        else if (TileRef.GetComponent<MeshCollider>() == null)
                            (TileRef.AddComponent<MeshCollider>() as MeshCollider).sharedMesh =
                             (Mesh)Resources.Load("Prefabs/Collision/1_0 ColPlane", typeof(Mesh));   // One-Way Plane Collision 
                }

                ColIndex++;
                RowIndex -= System.Convert.ToByte(ColIndex >= (int)Width);
                ColIndex = ColIndex % (int)(Width);      // ColIndex % TotalColumns 

                //RowIndex -= System.Convert.ToByte(ColIndex >= (int)Width / Step.x);
                //ColIndex = ColIndex % (int)(Width / Step.x);      // ColIndex % TotalColumns 

                //yield return 0;
            }
            //yield break;
        }

        public void GetTileCoords(Vector3 pos, 
                                    out int posX, 
                                    out int posY)   // Set a Vector & Gets 'x' + 'y' Coords output 
        {
            // Because We use a bytefield inline a 3D space world we must do a space conversion
            switch (currentOrientation)
            {
                case GridMap.GridOrientation.Vertical:
                    posX = (int)((pos.x / step.x) + (step.x < 1 ? step.x : 0));
                    posY = (int)(Height - (pos.z / step.z) - Mathf.Min(step.z, 1));   /// USE HEIGHT BECAUSE only time
                    break;

                case GridMap.GridOrientation.Depth:
                    posX = (int)(Width - (pos.z / step.z) - Mathf.Min(step.z, 1));
                    posY = (int)(Height - (pos.y / step.y) - Mathf.Min(step.y, 1));   /// USE HEIGHT BECAUSE only time
                    break;
                default: // case GridMap.GridOrientation.Horizontal:
                    posX = (int)((pos.x / step.x) + (step.x < 1 ? step.x : 0));
                    posY = (int)(Height - (pos.y / step.y) - Mathf.Min(step.y, 1));
                    break;
            }
        }

        public int GetTileIndex(Vector3 pos)        // Get Tile Index by a Vector input  
        {
            int posX, posY = 0;

            GetTileCoords(pos, out posX, out posY);

            // Basic index algorythm for fake "2D" linear arrays
            return ((posY * Width) + posX) * 4;

            // All tiles IDs in Tiled Files are packed in a set of 4 bytes 
            // { uint8, uint8, uint8, uint8 *} 
            // * Where the last set keeps the flips & rotation modificactions)
        }

        public void EditTileAt(Vector3 pos, uint tileID) 
        {
            int Index = GetTileIndex(pos); //int Index = (RowIndex * (int)Width) + ColIndex ;
            //Index *= 4;

            byte[] bytes = System.BitConverter.GetBytes(tileID);
            tiles[Index] = bytes[0];
            tiles[Index + 1] = bytes[1];
            tiles[Index + 2] = bytes[2];
            tiles[Index + 3] = bytes[3];

            //byte fx = (byte)(flipX ? 128 : 0);
            //byte fy = (byte)(flipY ? 64 : 0);
            //byte dg = (byte)(Rot ? 32 : 0);
            //tiles[Index + 3] = (byte)(fx+fy+dg);                 //Index += 3;

            //Debug.Log("Tile list index is " + Index + " with value " + (byte)tileID);
            totalTiles -= (tiles[Index] == 0 ? 1 : 0);

            //if (Refresh)
            ReBuildLayer();
            //Invoke("ReBuildLayer", 0);
        }

        public uint GetTileAt(Vector3 pos)          // It Get's The FULL Byted Value with modifs
        {
            //int Index = GetTileIndex(pos);    //  Index *= 4;
            //byte[] bytes = { tiles[Index], tiles[Index + 1], tiles[Index + 2], tiles[Index + 3] };
            //uint realTileID = System.BitConverter.ToUInt32(bytes, 0);

            uint fullTileID = System.BitConverter.ToUInt32(tiles, GetTileIndex(pos));

            //Debug.Log("Tile INDEX is " + Index + " *4 is " + Index * 4);

            return fullTileID;
        }

        public void FloodFill(Vector3 pos, uint newTileID)  //WARNING NOT FULLY READY, STILL BUGGY
        {
            uint originalTile = GetTileAt(pos);
            int x = 0, y = 0;

            GetTileCoords(pos, out x, out y);

            if (floodFillScanline(x, y, newTileID, originalTile) == true)
                ReBuildLayer();
        }
 
        public bool floodFillScanline(int x , int y , uint newTileID, uint originalTile)
        {
            // Algorythm based on http://lodev.org/cgtutor/floodfill.html
            // & some of https://unitycoder.com/blog/2012/10/10/flood-fill-algorithm/

            if (originalTile == newTileID)
                    return true;

            //int i = (y * (int)Width + x) * 4 ;
            //if ( GetTileIndex(x,y) <= Area - 3)
                if ( CheckEqualTiles( x, y, originalTile) == false) 
                    return true;           //if (tiles[Index] != originalTile)

            int x1;

            //draw current scanline from start position to the right
            x1 = x;
            while (x1 < (int)Width && CheckEqualTiles(x1, y, originalTile))
            {
                //tiles[(y * (int)Width + x1) * 4] = System.BitConverter.GetBytes(newTileID)[0];

                int Index = (y * (int)Width + x1) * 4;
                byte[] bytes = System.BitConverter.GetBytes(newTileID);
                tiles[Index] = bytes[0];
                tiles[Index + 1] = bytes[1];
                tiles[Index + 2] = bytes[2];
                tiles[Index + 3] = bytes[3];
                totalTiles++;

                x1++;
            }

            //draw current scanline from start position to the left
            x1 = x - 1;
            while (x1 >= 0 && CheckEqualTiles(x1, y, originalTile))
             {
                //tiles[(y * (int)Width + x1) * 4] = System.BitConverter.GetBytes(newTileID)[0];

                int Index = (y * (int)Width + x1) * 4;
                byte[] bytes = System.BitConverter.GetBytes(newTileID);
                tiles[Index] = bytes[0];
                tiles[Index + 1] = bytes[1];
                tiles[Index + 2] = bytes[2];
                tiles[Index + 3] = bytes[3];
                totalTiles++;

                x1--;
            }

            //test for new scanlines above
            x1 = x;
            while (x1 < (int)Width && CheckEqualTiles(x1, y, newTileID))
            {
                if (y > 0  && CheckEqualTiles(x1, y-1, originalTile))
                    floodFillScanline( x1, y-1, newTileID, originalTile);

                x1++;
            }

            x1 = x - 1;
            while (x1 >= 0 && CheckEqualTiles(x1, y, newTileID))
            {
                if (y > 0 && CheckEqualTiles(x1, y-1, originalTile))
                    floodFillScanline( x1, y-1, newTileID, originalTile);

                x1--;
            }

            //test for new scanlines below
            x1 = x;
            while (x1 < (int)Width && CheckEqualTiles(x1, y, newTileID))
            {
                if (y < (int)Height - 1 && CheckEqualTiles(x1, y +1, originalTile))
                    floodFillScanline( x1, y+1, newTileID, originalTile);

                x1++;
            }
            x1 = x - 1;
            while (x1 >= 0 && CheckEqualTiles(x1, y, newTileID))
            {
                if (y < (int)Height - 1 && CheckEqualTiles(x1, y+1, originalTile))
                    floodFillScanline( x1, y+1, newTileID, originalTile);

                x1--;
            }

            return true;
        }

        bool CheckEqualTiles(int x, int y, uint newTileID)
        {
            // Basic index algorythm for fake "2D" linear arrays [(posY * Width) + posX] 
            uint oldTileID = System.BitConverter.ToUInt32(tiles, ((y * Width) + x) * 4);
            // All tiles IDs in Tiled Files are packed in a set of 4 bytes 
            // { uint8, uint8, uint8, uint8 *} 
            // * Where the last set keeps the flips & rotation modificactions)

            return (oldTileID == newTileID);

            //byte[] newBytes = System.BitConverter.GetBytes(newTileID);
            //return (tiles[index] == newBytes[0]
            //    && tiles[index + 1] == newBytes[1]
            //    && tiles[index + 2] == newBytes[2]
            //    && tiles[index + 3] == newBytes[3]);
        }

        public GameObject BuildTile(uint tileId)
        {
            bool flipX = false, flipY = false, rotated = false;

            if (tileId != 0)    //	 if ( FirstGid => TileId && TileId <= TotalTiles)	// Si es mayor que 0!				 	 
            {
                uint Index = tileId & ~(FlippedHorizontallyFlag | FlippedVerticallyFlag | FlippedDiagonallyFlag);
                string TileName = "Tile_" + Index;

                if ((tileId & FlippedHorizontallyFlag) != 0)
                { flipX = true; TileName += "_H"; }

                if ((tileId & FlippedVerticallyFlag) != 0)
                { flipY = true; TileName += "_V"; }

                if ((tileId & FlippedDiagonallyFlag) != 0)
                { rotated = true; TileName += "_VH"; }

                for (int i = GridMap.TileSets.Count - 1; i >=  0; i--)       // Recorrer en reversa la lista y chequear el TileSet firstGid
                {

                    if (Index >= GridMap.TileSets[i].FirstGid)
                    {
                        if (currentTileSetID == -1)
                            currentTileSetID = i;

                        int localIndex = (int)(Index - GridMap.TileSets[i].FirstGid) + 1;

                        GameObject Tile = new GameObject();                                             // Build new scrollLayer inside layer

                        Tile.name = TileName;
                        Tile.transform.position = Vector3.zero;

                        MeshFilter meshFilter = (MeshFilter)Tile.AddComponent<MeshFilter>();            // Add mesh Filter & Renderer
                        MeshRenderer meshRenderer = (MeshRenderer)Tile.AddComponent<MeshRenderer>();

                        meshRenderer.sharedMaterial = GridMap.TileSets[currentTileSetID].mat;                                  // Set His own Material w/ texture
                        //LastUsedMat = i;

                        Mesh m = new Mesh();                                                            // Prepare mesh UVs offsets
                        m.name = TileName + "_mesh";

                        switch(currentOrientation)
                        {
                            case GridMap.GridOrientation.Horizontal:
                                m.vertices = new Vector3[4] {
                                    new Vector3( step.x, 0,      discrepance),
                                    new Vector3( 0,      0,      discrepance),
                                    new Vector3( 0,      step.y, discrepance),
                                    new Vector3( step.x, step.y, discrepance) };
                                break;

                            case GridMap.GridOrientation.Vertical:
                                m.vertices = new Vector3[4] {
                                new Vector3( step.x, -discrepance,      0),
                                new Vector3( 0,      -discrepance,      0),
                                new Vector3( 0,      -discrepance,     step.z),
                                new Vector3( step.x, -discrepance,     step.z) };
                                break;

                            case GridMap.GridOrientation.Depth:
                                m.vertices = new Vector3[4] {
                                new Vector3( -discrepance,     0,      0),
                                new Vector3( -discrepance,     0,      step.z),
                                new Vector3( -discrepance,     step.y, step.z),
                                new Vector3( -discrepance,     step.y, 0) };
                                break;
                        }

                        //Debug.Log(" localIndex " + localIndex 
                            //+ " / GridMap.TileSets[i].TileColumns "+GridMap.TileSets[i].TileColumns);

                        int offset_x = localIndex % GridMap.TileSets[i].TileColumns;
                        int offset_y = GridMap.TileSets[i].TileRows -
                            (Mathf.FloorToInt(localIndex / GridMap.TileSets[i].TileColumns) +
                            System.Convert.ToByte((localIndex % GridMap.TileSets[i].TileRows) != 0));


                        Vector2 vt1 = new Vector2((offset_x - System.Convert.ToByte(flipX)) * GridMap.TileSets[i].ModuleWidth,
                                                 ((offset_y + System.Convert.ToByte(flipY)) * GridMap.TileSets[i].ModuleHeight));
                        Vector2 vt2 = new Vector2((offset_x - System.Convert.ToByte(!flipX)) * GridMap.TileSets[i].ModuleWidth,
                                                 ((offset_y + System.Convert.ToByte(flipY)) * GridMap.TileSets[i].ModuleHeight));
                        Vector2 vt3 = new Vector2((offset_x - System.Convert.ToByte(!flipX)) * GridMap.TileSets[i].ModuleWidth,
                                                 ((offset_y + System.Convert.ToByte(!flipY)) * GridMap.TileSets[i].ModuleHeight));
                        Vector2 vt4 = new Vector2((offset_x - System.Convert.ToByte(flipX)) * GridMap.TileSets[i].ModuleWidth,
                                                 ((offset_y + System.Convert.ToByte(!flipY)) * GridMap.TileSets[i].ModuleHeight));


                        int Xsign = (flipX ? -1 : 1);                                           // if ( Flipped_X ) Sign = -1;
                        int Ysign = (flipY ? -1 : 1);

                        vt1.x += -eps.x * Xsign; vt1.y += eps.y * Ysign;
                        vt2.x += eps.x * Xsign; vt2.y += eps.y * Ysign;
                        vt3.x += eps.x * Xsign; vt3.y += -eps.y * Ysign;
                        vt4.x += -eps.x * Xsign; vt4.y += -eps.y * Ysign;

                        if (rotated)                                                                // This is some Math for flip 
                        {
                            if (flipX && flipY || !flipX && !flipY)                 // & I don't want to explain!
                            {
                                Vector2 vtAux1 = vt2;
                                vt2 = vt4;
                                vt4 = vtAux1;
                            }
                            else
                            {
                                Vector2 vtAux2 = vt3;
                                vt3 = vt1;
                                vt1 = vtAux2;
                            }
                        }

                        m.uv = new Vector2[4] { vt1, vt2, vt3, vt4 };
                        m.triangles = new int[6] { 0, 1, 2, 0, 2, 3 };
                        m.RecalculateNormals();
                        ;
                        meshFilter.sharedMesh = m;
                        m.RecalculateBounds();

                        /////////////////////////////////////////////////////////////////////////////////////////////////////

                        if (GridMap.TileSets[i].Collisions != null)
                            if (GridMap.TileSets[i].Collisions.ContainsKey(localIndex))                         // If there's a Collision property,
                            {
                                //Debug.Log("Found tile colliding " + localIndex + " " + (GridMap.TileSets[i].Collisions[localIndex]));
                                switch ((string)(GridMap.TileSets[i].Collisions[localIndex]).ToLower())                   // Check type and Setup:
                                {
                                    case "plane":
                                        Debug.Log("Set Property plane");
                                        Tile.AddComponent<MeshCollider>().sharedMesh =
                                            (Mesh)Resources.Load("Prefabs/Collision/1_0 ColPlane", typeof(Mesh));   // One-Way Plane Collision 
                                        break;

                                    case "slope":
                                        Tile.layer = 8;
                                        if (flipX && !flipY || !flipX && flipY && rotated || !flipX && flipY && !rotated)
                                        {
                                            Tile.AddComponent<MeshCollider>().sharedMesh =
                                                (Mesh)Resources.Load("Prefabs/Collision/SlopeLeft", typeof(Mesh));  // Left Slope Collision
                                        }
                                        else
                                        {
                                            Tile.AddComponent<MeshCollider>().sharedMesh =
                                                (Mesh)Resources.Load("Prefabs/Collision/SlopeRight", typeof(Mesh)); // Right Slope Collision
                                        }
                                        break;

                                    default:
                                        (Tile.AddComponent<BoxCollider>() as BoxCollider).size = Vector3.one;       // or default's Box Collision 
                                        //Debug.Log("Set Property box");
                                        //GridMap.TileSets[i].Collisions[localIndex] = "Box3D";
                                        break;
                                }
                            }

                        totalTiles++;

                        return Tile;
                    }   //break;
                }
            }
            return null;
        }

        public IEnumerator BakeTilesDelayed()       // Not in Use Actually (Should Be Deleted!?)
        {
            isBaking = true;

            //float delay = 0.1f;
            //while (delay > 0)   // We Need This Delay to Hold unity's end of Frame update
            //{
            //    delay -= Time.fixedUnscaledDeltaTime;
                yield return 0;
            //}

            //yield WaitForSeconds
            gameObject.AddComponent<meshCombiner>();

#if UNITY_EDITOR
            GetComponent<meshCombiner>().CombineMeshes();                 ////    mc.OnEnabled();
#endif
            isBaking = false;

            yield break;
        }

        public void BakeTiles()
        {
            //if (GetComponent<meshCombiner>())
            //    return;

            isBaking = true;
            gameObject.AddComponent<meshCombiner>();

#if UNITY_EDITOR
            GetComponent<meshCombiner>().CombineMeshes();                 ////    mc.OnEnabled();
#endif

            isBaking = false;
        }

        static public GameObject BuildPreviewTile(  uint tileId, 
                    Vector3 step, int currentTileSet = -1,
                    GridMap.GridOrientation orientation = GridMap.GridOrientation.Horizontal  )
        {
            bool flipX = false;
            bool flipY = false;
            bool rotated = false;

            //step = step ?? Vector3.one;
            uint fHorizontallyFlag =    0x80000000;
            uint fVerticallyFlag =      0x40000000;
            uint fDiagonallyFlag =      0x20000000;

            float overSet = 0.01f * step.magnitude;

            if (tileId != 0)    //	 if ( FirstGid => TileId && TileId <= TotalTiles)	// Si es mayor que 0!				 	 
            {
                uint Index = tileId & ~(fHorizontallyFlag | fVerticallyFlag | fDiagonallyFlag);
                string TileName = "Tile_" + Index;

                if ((tileId & fHorizontallyFlag) != 0)
                { flipX = true; TileName += "_H"; }

                if ((tileId & fVerticallyFlag) != 0)
                { flipY = true; TileName += "_V"; }

                if ((tileId & fDiagonallyFlag) != 0)
                { rotated = true; TileName += "_VH"; }

                for (int i = GridMap.TileSets.Count - 1; i >= 0; i--)       // Recorrer en reversa la lista y chequear el TileSet firstGid
                {
                    if (Index >= GridMap.TileSets[i].FirstGid)
                    {
                        if (currentTileSet == -1)
                            currentTileSet = i;

                        int localIndex = (int)(Index - GridMap.TileSets[i].FirstGid) + 1;

                        GameObject Tile = new GameObject();                                             // Build new scrollLayer inside layer

                        Tile.name = TileName;
                        Tile.transform.position = Vector3.zero;
                        //Tile.transform.localPosition = Vector3.zero;

                        MeshFilter meshFilter = (MeshFilter)Tile.AddComponent<MeshFilter>();            // Add mesh Filter & Renderer
                        MeshRenderer meshRenderer = (MeshRenderer)Tile.AddComponent<MeshRenderer>();

                        //meshRenderer.sharedMaterial = GridMap.TileSets[currentTileSet].mat;                                  // Set His own Material w/ texture
                        meshRenderer.material = GridMap.TileSets[currentTileSet].mat;   // Set His own Material w/ texture
                        //meshRenderer.material.shader = Shader.Find("Legacy Shaders/Transparent/Diffuse");
                        //meshRenderer.material.color =  new Color(1,1,1,0.5f);
                        //meshRenderer.material.shader = Shader.Find("Particles/Alpha Blended");
                        //meshRenderer.material.color = new Color(1, 1, 1, 0.25f);
                        meshRenderer.material.shader = Shader.Find("Particles/Additive (Soft)");

                        Mesh m = new Mesh();                                                            // Prepare mesh UVs offsets
                        m.name = TileName + "_mesh";

                        switch (orientation)
                        {
                            case GridMap.GridOrientation.Horizontal:
                                m.vertices = new Vector3[4] {
                                    new Vector3( step.x, 0,      -overSet*step.z),
                                    new Vector3( 0,      0,      -overSet*step.z),
                                    new Vector3( 0,      step.y, -overSet*step.z),
                                    new Vector3( step.x, step.y, -overSet*step.z) };
                                break;

                            case GridMap.GridOrientation.Vertical:
                                m.vertices = new Vector3[4] {
                                new Vector3( step.x, overSet*step.y,      0),
                                new Vector3( 0,      overSet*step.y,      0),
                                new Vector3( 0,      overSet*step.y,     step.z),
                                new Vector3( step.x, overSet*step.y,     step.z) };
                                break;

                            case GridMap.GridOrientation.Depth:
                                m.vertices = new Vector3[4] {
                                new Vector3( -overSet*step.x,     0,        0),
                                new Vector3( -overSet*step.x,     0,        step.z),
                                new Vector3( -overSet*step.x,     step.y,   step.z),
                                new Vector3( -overSet*step.x,     step.y,   0) };
                                break;
                        }
                        int offset_x = localIndex % GridMap.TileSets[i].TileColumns;
                        int offset_y = GridMap.TileSets[i].TileRows -
                            (Mathf.FloorToInt(localIndex / GridMap.TileSets[i].TileColumns) +
                            System.Convert.ToByte((localIndex % GridMap.TileSets[i].TileRows) != 0));


                        Vector2 vt1 = new Vector2((offset_x - System.Convert.ToByte(flipX)) * GridMap.TileSets[i].ModuleWidth,
                                                 ((offset_y + System.Convert.ToByte(flipY)) * GridMap.TileSets[i].ModuleHeight));
                        Vector2 vt2 = new Vector2((offset_x - System.Convert.ToByte(!flipX)) * GridMap.TileSets[i].ModuleWidth,
                                                 ((offset_y + System.Convert.ToByte(flipY)) * GridMap.TileSets[i].ModuleHeight));
                        Vector2 vt3 = new Vector2((offset_x - System.Convert.ToByte(!flipX)) * GridMap.TileSets[i].ModuleWidth,
                                                 ((offset_y + System.Convert.ToByte(!flipY)) * GridMap.TileSets[i].ModuleHeight));
                        Vector2 vt4 = new Vector2((offset_x - System.Convert.ToByte(flipX)) * GridMap.TileSets[i].ModuleWidth,
                                                 ((offset_y + System.Convert.ToByte(!flipY)) * GridMap.TileSets[i].ModuleHeight));

                        //Debug.Log("Module PREVIEW is " + GridMap.TileSets[i].ModuleWidth +
                        //                    " GridMap.TileSets[i] " + GridMap.TileSets[i].TileSetName);

                        int Xsign = (flipX ? -1 : 1);                                           // if ( Flipped_X ) Sign = -1;
                        int Ysign = (flipY ? -1 : 1);

                        vt1.x += Xsign; vt1.y += Ysign;
                        vt2.x += Xsign; vt2.y += Ysign;
                        vt3.x += Xsign; vt3.y += Ysign;
                        vt4.x += Xsign; vt4.y += Ysign;

                        if (rotated)                                                                // This is some Math for flip 
                        {
                            if (flipX && flipY || !flipX && !flipY)                 // & I don't want to explain!
                            {
                                Vector2 vtAux1 = vt2;
                                vt2 = vt4;
                                vt4 = vtAux1;
                            }
                            else
                            {
                                Vector2 vtAux2 = vt3;
                                vt3 = vt1;
                                vt1 = vtAux2;
                            }
                        }

                        m.uv = new Vector2[4] { vt1, vt2, vt3, vt4 };
                        m.triangles = new int[6] { 0, 1, 2, 0, 2, 3 };
                        m.RecalculateNormals();
                        meshFilter.sharedMesh = m;
                        m.RecalculateBounds();

                        /////////////////////////////////////////////////////////////////////////////////////////////////////

                        return Tile;
                    }   //break;
                }
            }
            return null;
        }


    }
}