﻿using UnityEngine;
using System.Collections;

namespace Sun.Tools
{
    [System.Serializable]
    public class TilerInterface                      // ALL User's Input/Output 
    {

        public bool Enabled = true;
        public Tiler tilerRef { get; set; }     // View (Data Hold)

        public KeyCode toggleKey = KeyCode.Tab;
        [SerializeField, ShowOnly]
        bool keyControl = false, keyShift = false, keyTab = false, keyAlt = false;

        float margin = 2;
        float ButtonHeight = 22;                // GUI.skin.button.CalcSize(new GUIContent("▲")).y;

        [SerializeField] string filePathAndName = "File Name + Path";
        Rect LeftBoxPanel, GridMapPanel, LeftBoxArea;
        float FileNameFieldWidth = 16;

        bool LeftPanelFoldOut = true, RightPanelFoldOut = true;

        public bool LinkPlanesWithCamera = false;

        Rect RightBoxPanel, ToolsPanel, RightBoxArea;
        ComboBox OrientationComboBox;// = new ComboBox();


        /// ------------------------------------------------------------------------ ///

        public TilerInterface(Sun.Tools.Tiler tiler)
        {
            //tilerRef = tiler;
            Init(tiler);
        }

        public void Init(Tiler t)
        {
            tilerRef = t;
            //Sun.ScreenLog.Get.OnEnable();
            //Sun.Audio.Get.OnEnable();

        }

        public void Update()
        {

            keyTab = Input.GetKeyDown(KeyCode.Tab); //Input.GetButtonDown("EditorTab");

            keyControl = Input.GetKey(KeyCode.LeftControl) || Input.GetKey(KeyCode.Mouse1);

            keyShift = Input.GetKey(KeyCode.LeftShift);

            keyAlt = Input.GetKey(KeyCode.LeftAlt);


            if (keyTab && keyControl)
            {
                if (keyShift)
                    tilerRef.SetNextGridOrientation();
                else
                    tilerRef.SetPreviousGridOrientation();

                if (LinkPlanesWithCamera)
                    SetupCameraOrientation();
            }

            if (keyControl && !keyShift && !BottomArea.Contains(Input.mousePosition))
            {
                var d = Input.GetAxis("Mouse ScrollWheel");
                if (d > 0f)
                    tilerRef.IncreaseGridDistance();
                else if (d < 0f)
                    tilerRef.DecreaseGridDistance();

                if (Input.GetKeyDown(KeyCode.G))
                    tilerRef.gridMap.showPlane = !tilerRef.gridMap.showPlane;
            }


            if (!keyControl && (Input.GetKeyDown(KeyCode.E)) )
            {
                tilerRef.CurrentMode = Tiler.TilerMode.Erase;
                onErase = SetMeOnly();
            }

            if (!keyControl && (Input.GetKeyDown(KeyCode.G)))
            {
                tilerRef.CurrentMode = Tiler.TilerMode.Bucket;
                onBucket = SetMeOnly();
            }

            if (!keyControl && (Input.GetKeyDown(KeyCode.B)))
            {
                tilerRef.CurrentMode = Tiler.TilerMode.Paint;
                onPaint = SetMeOnly();
            }

            if (!keyAlt && !LeftBoxArea.Contains(Input.mousePosition)
                                && !RightBoxArea.Contains(Input.mousePosition))
                ProcessCurrentMode();

            if (Camera.main.GetComponent<UnityCameraIngame>())
                Camera.main.GetComponent<UnityCameraIngame>().enabled =
                    !BottomArea.Contains(Input.mousePosition);

            if (Input.GetKeyDown(KeyCode.Escape))
            {
                if (tilerRef.CurrentMode == Tiler.TilerMode.Prefab)
                {
                    selectedObjId = -1;
                    tilerRef.previewPrefab = null;
                    tilerRef.RefreshPreviewTarget(0);
                }
                else
                {
                    tilerRef.CurrentMode = Tiler.TilerMode.None;
                    SetMeOnly();
                }
            }


        }

        public void ResetInterface()
        {
            SelectedObjId = -1;
            tilerRef.previewPrefab = null;

            CurrentObjGroup = 0;
            CurrentTileSet = 0;
            enableLayerSetup = enableTileSetSetup = enableObjSetup = false;

            if (!Application.isPlaying)
                tileSetComboBox = tileSetShaderComboBox = tileCollisionComboBox = null;

            //SetMeOnly();
            ////SelectedTileCollision = GridMap.CollisionType.None;
        }

        void ProcessCurrentMode()
        {
            if (Input.GetMouseButtonDown(1) && !BottomArea.Contains(Input.mousePosition)
                   && tileSetComboBox != null && !tileSetComboBox.isClickedButton
                   && tileCollisionComboBox != null && !tileCollisionComboBox.isClickedButton
                   && tilerRef.CurrentMode != Tiler.TilerMode.Prefab)
            {
                onPaint = SetMeOnly();
                tilerRef.CurrentMode = Tiler.TilerMode.Paint;


                tilerRef.CopyTile();

                if (tilerRef.CurrentTileID == 0)
                {
                    onPaint = false;
                    tilerRef.CurrentMode = Tiler.TilerMode.None;
                }
            }

            switch (tilerRef.CurrentMode)
            {
                case Tiler.TilerMode.Paint:

                    if (Input.GetKeyDown(KeyCode.X))
                    {
                        tilerRef.FlipX = !tilerRef.FlipX;
                        tilerRef.mirrorX = !tilerRef.mirrorX;

                        if (tilerRef.Rotation != 0 && tilerRef.Rotation != 180)
                            tilerRef.Rotation += 180;

                        tilerRef.RefreshPreviewTarget(tilerRef.CurrentTileID);
                    }

                    if (Input.GetKeyDown(KeyCode.Y))
                    {
                        tilerRef.FlipY = !tilerRef.FlipY;
                        tilerRef.mirrorY = !tilerRef.mirrorY;

                        if (tilerRef.Rotation != 0 && tilerRef.Rotation != 180)
                            tilerRef.Rotation += 180;

                        tilerRef.RefreshPreviewTarget(tilerRef.CurrentTileID);
                    }

                    if (Input.GetKeyDown(KeyCode.Z))
                    {
                        if (keyShift) // keyControl
                            tilerRef.Rotation += (uint)(tilerRef.Rotation == 0 ? 360 : 0) - 90;
                        else
                            tilerRef.Rotation += 90;
                        tilerRef.RefreshPreviewTarget(tilerRef.CurrentTileID);
                    }

                    if (Input.GetMouseButton(0) && !BottomArea.Contains(Input.mousePosition)
                            && tileSetComboBox != null && !tileSetComboBox.isClickedButton
                            && tileCollisionComboBox != null && !tileCollisionComboBox.isClickedButton)
                    {
                        tilerRef.PaintTile(tilerRef.CurrentTileID);
                        //Debug.Log(" Drawing tileIndex is " + tilerRef.CurrentTileID  );
                    }

                    //if (Input.GetMouseButtonDown(1) && !BottomArea.Contains(Input.mousePosition)
                    //    && tileSetComboBox != null && !tileSetComboBox.isClickedButton
                    //    && tileCollisionComboBox != null && !tileCollisionComboBox.isClickedButton)
                    //        {
                    //            tilerRef.CopyTile();
                    //        }

                    if (!keyControl && keyShift)
                    {

                        var d = Input.GetAxis("Mouse ScrollWheel");

                        if (!BottomArea.Contains(Input.mousePosition)
                        && GridMap.TileSets.Count > 0 && GridMap.TileSets[CurrentTileSet] != null)
                        {

                            if (d > 0f &&
                                tilerRef.CurrentTileID < GridMap.TileSets[CurrentTileSet].TileCount)
                            {
                                tilerRef.CurrentTileID++;
                                tilerRef.RefreshPreviewTarget(tilerRef.CurrentTileID);
                            }
                            else if (d < 0f && tilerRef.CurrentTileID > 1)
                            {
                                tilerRef.CurrentTileID--;
                                tilerRef.RefreshPreviewTarget(tilerRef.CurrentTileID);
                            }

                        }
                        //else if(BottomArea.Contains(Input.mousePosition))
                        //{
                        //    //Camera.main.GetComponent<UnityCameraIngame>().enabled = false;
                        //    if (d > 0f && ZoomFactor < 100)
                        //        ZoomFactor += 0.1f;
                        //    else if (d < 0f && ZoomFactor > 0)
                        //        ZoomFactor -= 0.1f;
                        //}

                    }

                    if (!keyControl && BottomArea.Contains(Input.mousePosition))
                    {
                        var d = Input.GetAxis("Mouse ScrollWheel");

                        if (d > 0f && ZoomFactor < 100)
                            ZoomFactor += 0.1f;
                        else if (d < 0f && ZoomFactor > 0)
                            ZoomFactor -= 0.1f;
                    }

                    break;
                case Tiler.TilerMode.Erase:
                    if (Input.GetMouseButton(0) && !BottomArea.Contains(Input.mousePosition)
                            && tileSetComboBox != null && !tileSetComboBox.isClickedButton
                            && tileCollisionComboBox != null && !tileCollisionComboBox.isClickedButton)
                        tilerRef.EraseTile();
                    break;
                case Tiler.TilerMode.Bucket:

                    if (Input.GetKeyDown(KeyCode.X))
                    {
                        tilerRef.FlipX = !tilerRef.FlipX;
                        tilerRef.mirrorX = !tilerRef.mirrorX;

                        if (tilerRef.Rotation != 0 && tilerRef.Rotation != 180)
                            tilerRef.Rotation += 180;

                        tilerRef.RefreshPreviewTarget(tilerRef.CurrentTileID);
                    }

                    if (Input.GetKeyDown(KeyCode.Y))
                    {
                        tilerRef.FlipY = !tilerRef.FlipY;
                        tilerRef.mirrorY = !tilerRef.mirrorY;

                        if (tilerRef.Rotation != 0 && tilerRef.Rotation != 180)
                            tilerRef.Rotation += 180;

                        tilerRef.RefreshPreviewTarget(tilerRef.CurrentTileID);
                    }

                    if (Input.GetKeyDown(KeyCode.Z))
                    {
                        if (keyShift) // keyControl
                            tilerRef.Rotation += (uint)(tilerRef.Rotation == 0 ? 360 : 0) - 90;
                        else
                            tilerRef.Rotation += 90;
                        tilerRef.RefreshPreviewTarget(tilerRef.CurrentTileID);
                    }


                    if (Input.GetMouseButton(0) && !BottomArea.Contains(Input.mousePosition)
                   && tileSetComboBox != null && !tileSetComboBox.isClickedButton
                   && tileCollisionComboBox != null && !tileCollisionComboBox.isClickedButton)
                    {
                        tilerRef.PaintBucket(tilerRef.CurrentTileID);
                    }

                    break;
                case Tiler.TilerMode.Select:
                    break;
                case Tiler.TilerMode.Prefab:

     
                    if(SelectedObjId <0)
                        break;

                    if (Input.GetKeyDown(KeyCode.X))
                    {
                        if (keyShift)
                            tilerRef.previewPrefab.transform.Rotate(Vector3.up, +45, Space.World);
                        else
                            tilerRef.previewPrefab.transform.Rotate(Vector3.up, -45, Space.World);
                    }

                    if (Input.GetKeyDown(KeyCode.Y))
                    {
                        if (keyShift)
                            tilerRef.previewPrefab.transform.Rotate(Vector3.right, +45, Space.World);
                        else
                            tilerRef.previewPrefab.transform.Rotate(Vector3.right, -45, Space.World);
                    }

                    if (Input.GetKeyDown(KeyCode.Z))
                    {
                        if (keyShift)
                            tilerRef.previewPrefab.transform.Rotate(Vector3.forward, +45, Space.World);
                        else
                            tilerRef.previewPrefab.transform.Rotate(Vector3.forward, -45, Space.World);
                    }

                    if ( Input.GetMouseButtonDown(0)
                            && (!enableObjSetup || !ObjSetupPanel.Contains(Input.mousePosition))
                            && !BottomArea.Contains(Input.mousePosition) )
                    {
                        tilerRef.CreatePrefab(PrefabLoader.Get.ObjGroups[CurrentObjGroup].transform);
                        Debug.Log(" Created a Prefab " );

                    }

                    break;
                case Tiler.TilerMode.None:
                default:
                    break;
            }
        }

        void SetupCameraOrientation()
        {
            //if (!LinkPlanesWithCamera) return;

            if (tilerRef.gridMap.currentOrientation == GridMap.GridOrientation.Horizontal)
                Camera.main.gameObject.SendMessage("FrontCamera", SendMessageOptions.DontRequireReceiver);
            else if (tilerRef.gridMap.currentOrientation == GridMap.GridOrientation.Vertical)
                Camera.main.gameObject.SendMessage("TopCamera", SendMessageOptions.DontRequireReceiver);
            else
                Camera.main.gameObject.SendMessage("LeftCamera", SendMessageOptions.DontRequireReceiver);
        }

        void InitGUI()
        {

            if (OrientationComboBox == null)
            {
                OrientationComboBox = new ComboBox(
                            new Rect(ToolsPanel.xMin, ToolsPanel.yMin + 178, ToolsPanel.width, 22),
                                    new string[3] { "Horizontal", "Vertical", "Depth" });

                OrientationComboBox.SelectedItemIndex = (int)tilerRef.gridMap.currentOrientation;
            }


            margin = 2;
            ButtonHeight = GUI.skin.button.CalcSize(new GUIContent("▲")).y;

            LeftBoxArea = new Rect(0, 0, 64, 272);  // <<< Global Menu Size (Some Mouse Contact Area)

            LeftBoxPanel = new Rect(0, Screen.height - LeftBoxArea.height, LeftBoxArea.width, LeftBoxArea.height);
            GridMapPanel = new Rect(LeftBoxPanel.xMin + margin, Screen.height + margin - LeftBoxPanel.height,
                                        LeftBoxPanel.width - margin * 2, LeftBoxPanel.height - margin);

            RightBoxPanel =
                new Rect(Screen.width - RightBoxPanel.width,
                            Screen.height - RightBoxPanel.height, 64, 272);
            ToolsPanel =
                new Rect(RightBoxPanel.xMin + margin,
                            Screen.height + margin - RightBoxPanel.height,
                                        RightBoxPanel.width - margin * 2,
                                        RightBoxPanel.height - margin);

            RightBoxArea = new Rect(Screen.width - RightBoxPanel.width - 50, 0, 64 + 50, 272);


            LayerSetupPanel = // Always Put with (int) because it Blurs everything inside the window
                new Rect((int)(Screen.width * 0.5f) - 160, (int)(Screen.height * 0.5f) - 160, 320, 320);

            MapSetupPanel =
                new Rect((int)(Screen.width * 0.5f) - 160, (int)(Screen.height * 0.5f) - 160, 320, 320);


            BottomPanel = new Rect((int)(Screen.width * 0.5f) - 256, (int)(Screen.height * 0.8f),
                512, (int)(Screen.height * 0.2f));

            TileSetPanel = new Rect((int)(Screen.width * 0.5f) - 160, (int)(Screen.height * 0.5f) - 120, 320, 260);

            ObjSetupPanel = new Rect((int)(Screen.width * 0.5f) - 160, (int)(Screen.height * 0.5f) - 120, 320, 260);


            BottomArea = new Rect((int)(Screen.width * 0.5f) - 256, 0, 512, 160);

            if (tileSetComboBox == null)
            {
                tileSetComboBox = new ComboBox();
                CheckTileSetList();
            }

            if (collisionComboBox == null)
                collisionComboBox = new ComboBox(
                             new Rect(LayerSetupPanel.width * 0.25f, LayerSetupPanel.height * .30f, LayerSetupPanel.width * 0.7f, 22),
                             new string[7] { "No Collision", "Box 3D", "Plane 3D", "Slope 3D", "Mesh 3D", "Box 2D", "Poly 2D" });

            if (tileSetShaderComboBox == null)
                tileSetShaderComboBox = new ComboBox(
                         new Rect(TileSetPanel.width * 0.25f, TileSetPanel.height * .375f, TileSetPanel.width * 0.7f, 22),
                             new string[5] { "DoubleSided", "DoubleSideSimpleLit", "SingleSided", "SingleSideSimpleLit", "FullShadowCast" });

            if (tileCollisionComboBox == null)
                tileCollisionComboBox = new ComboBox(
                             new Rect(TileSetPanel.x + TileSetPanel.width * 0.6f, TileSetPanel.height * .375f, TileSetPanel.width * 0.625f, 22),
                             new string[7] { "No Collision", "Box 3D", "Plane 3D", "Slope 3D", "Mesh 3D", "Box 2D", "Poly 2D" });

            if (objGroupComboBox == null)
            {
                objGroupComboBox = new ComboBox();
                CheckObjGroupList();
                selectedObjId = -1;
                tilerRef.previewPrefab = null;
            }


            ZoomFactor = 1;
        }

        public void OnDrawGUI()
        {
            if (!Enabled)
                return;

            if (RightBoxPanel.y != Screen.height - RightBoxPanel.height ||
                RightBoxPanel.x != Screen.width - RightBoxPanel.width)
                InitGUI();

            if (enableLayerSetup)
                LayerSetupPanel = GUI.Window(0, LayerSetupPanel, DrawLayerSetupWindow, "Current Layer Setup");

            if (EnabledMapSetup)
                MapSetupPanel = GUI.Window(0, MapSetupPanel, DrawMapSetupWindow, "Grid Map Setup");



            DrawLeftPanel();
            DrawRightPanel();


            if (ViewBottomPanel)
                DrawBottomPanel();
            else if (GUI.Button(
                new Rect(BottomPanel.xMax - ButtonHeight, Screen.height - ButtonHeight,
                                ButtonHeight, ButtonHeight), "▲"))
                ViewBottomPanel = true;

        }


        /// -------------------------- File GridMap Management  --------------------- ///


        void OpenFileCheck()
        {
            if (string.IsNullOrEmpty(filePathAndName) || !System.IO.File.Exists(filePathAndName))
            {
                var extFilters = new[] {
                //new ExtensionFilter("Binary", "bin"),
                new SFB.ExtensionFilter("Tiled map", "tmx"),
                new SFB.ExtensionFilter("TextAsset file", "xml") };

                var fileStrings = SFB.StandaloneFileBrowser.OpenFilePanel("Open File",
                    Serializer.GetCurrentPath(),/* "xml"*/extFilters, false);

                if (fileStrings.Length > 0)
                    filePathAndName = fileStrings[0];
            }

            if (string.IsNullOrEmpty(filePathAndName))
                return;

            if (!tilerRef.LoadFileMap(filePathAndName))
                filePathAndName = "";
        }

        void SaveFileCheck()
        {
            if (string.IsNullOrEmpty(filePathAndName) /*|| !System.IO.File.Exists(filePathAndName)*/)
            {
                filePathAndName = SFB.StandaloneFileBrowser.SaveFilePanel(
                    "Save File", Serializer.GetCurrentPath(), "gridMap_01", "Tmx");
            }

            if (!string.IsNullOrEmpty(filePathAndName) && tilerRef.gridMap != null)
                Debug.Log("Saving to " + filePathAndName);

            if (string.IsNullOrEmpty(filePathAndName))
                return;

            tilerRef.SaveMap(filePathAndName);
        }

        string OpenImageCheck(string imgFile = "")
        {
            if (string.IsNullOrEmpty(imgFile) || !System.IO.File.Exists(imgFile))
            {
                var extFilters = new[] {
                new SFB.ExtensionFilter("PNG", "png"),
                new SFB.ExtensionFilter("PhotoShop", "psd"),
                new SFB.ExtensionFilter("GIF", "gif"),
                new SFB.ExtensionFilter("Jpeg", "jpg") };

                var fileStrings = SFB.StandaloneFileBrowser.OpenFilePanel("Open Image File",
                    Serializer.GetCurrentPath(),/* "xml"*/extFilters, false);

                if (fileStrings.Length > 0)
                    imgFile = fileStrings[0];
            }

            if (string.IsNullOrEmpty(imgFile))
                return "";

            return imgFile;
        }

        /// -------------------------- Tiler Creation ToolBars  --------------------- ///


        bool onPaint = false, onBucket, onErase, onPrefab;// = onSelect
        bool SetMeOnly()
        {
            onPaint = onBucket = onErase = onPrefab = false; // = onSelect
            return true;
        }

        [ShowOnly, SerializeField]
        bool enableLayerSetup = false, EnabledMapSetup = false;
        Rect LayerSetupPanel = new Rect(512, 320, 320, 240);
        Rect MapSetupPanel = new Rect(512, 320, 320, 240);

        ComboBox collisionComboBox;

        ///   -----------------------------------------------------------------------  ///


        void DrawLayerSetupWindow(int windowID)
        {

            if (tilerRef.gridMap.CurrentGroup.layers.ContainsKey(tilerRef.gridMap.CurrentGridOffset))
            {
                TileLayer currentLayer =
                    tilerRef.gridMap.CurrentGroup.layers[tilerRef.gridMap.CurrentGridOffset];

                GUILayout.BeginHorizontal();
                GUILayout.Label("Name", GUILayout.MaxWidth(50));
                currentLayer.name = GUILayout.TextField(currentLayer.name);
                currentLayer.Visible =
                    GUILayout.Toggle(currentLayer.Visible, "Visible", "Button", GUILayout.MaxWidth(70));
                GUILayout.EndHorizontal();

                GUILayout.BeginHorizontal();
                GUILayout.Label("Opacity", GUILayout.MaxWidth(50));
                currentLayer.Opacity = GUILayout.HorizontalSlider(currentLayer.Opacity, 0, 1.0f);
                GUILayout.EndHorizontal();

                GUILayout.BeginHorizontal();
                GUILayout.Label("Offset", GUILayout.MaxWidth(50));
                GUILayout.Space(10);
                GUILayout.Label("X:", GUILayout.MaxWidth(20));
                currentLayer.OffsetX =
                    float.Parse(GUILayout.TextField(currentLayer.OffsetX.ToString(), 5));
                GUILayout.Space(10);
                GUILayout.Label("Y:", GUILayout.MaxWidth(20));
                currentLayer.OffsetY =
                    float.Parse(GUILayout.TextField(currentLayer.OffsetY.ToString(), 5));
                GUILayout.Space(10);
                GUILayout.Label("Z:", GUILayout.MaxWidth(20));
                GUI.SetNextControlName("DepthChanger");

                GUI.color = (GUI.GetNameOfFocusedControl() == "DepthChanger" ? Color.red : Color.white);

                float tempOffset =
                    float.Parse(GUILayout.TextField(currentLayer.OffsetZ.ToString(), 5));
                if (tempOffset != currentLayer.OffsetZ)
                {

                    tilerRef.ChangeLayer(currentLayer.OffsetZ, tempOffset);
                    tilerRef.GridDistance = tempOffset;
                    return;
                }
                GUI.color = Color.white;

                //currentLayer.OffsetZ =
                GUILayout.EndHorizontal();

                GUILayout.BeginHorizontal();
                GUILayout.Label("Collision", GUILayout.MaxWidth(50));

                if (collisionComboBox != null)
                {
                    if (collisionComboBox.SelectedItemIndex != (int)currentLayer.CollisionLayer)
                        collisionComboBox.SelectedItemIndex = (int)currentLayer.CollisionLayer;

                    currentLayer.CollisionLayer = (GridMap.CollisionType)collisionComboBox.Show();

                    if (collisionComboBox.isChanged)
                        currentLayer.ReBuildLayer();
                }
                GUILayout.EndHorizontal();


                GUILayout.BeginHorizontal();
                if (GUILayout.Button("Delete", GUILayout.MaxWidth(50)))
                {
                    currentLayer.DestroyAll();
                    GameObject.Destroy(currentLayer.gameObject);
                    tilerRef.gridMap.CurrentGroup.layers.Remove(tilerRef.GridDistance);
                    return;

                }
                GUILayout.FlexibleSpace();
                GUILayout.EndHorizontal();

                GUILayout.Label(
                    "Layer TileSet: " +
                    GridMap.TileSets[currentLayer.currentTileSetID].TileSetName +
                    " ID:" + currentLayer.currentTileSetID);
                GUILayout.Label("Drawn Tiles: " + currentLayer.totalTiles);

                GUILayout.Toggle(false, "Hide Upper Layers");

                currentLayer.CombineMesh = GUILayout.Toggle(currentLayer.CombineMesh, "Combine Meshes"/*, "Button"*/);

            }
            else
                GUI.Label(new Rect(LayerSetupPanel.width * 0.5f - 30, ButtonHeight * 2, 100, 20), "empty layer");



            GUILayout.FlexibleSpace();

            if (GUILayout.Button("Close") || KeyCode.Escape == Event.current.keyCode)
                enableLayerSetup = false;

            //GUI.DragWindow(new Rect(0, 0, 10000, 10000));
            GUI.DragWindow(new Rect(0, 0, Screen.width, Screen.height));    //GUI.DragWindow(new Rect(0, 0, 10000, 10000));
        }

        void DrawMapSetupWindow(int windowID)
        {


            GUILayout.BeginHorizontal();
            GUILayout.Label("Size", GUILayout.MaxWidth(50));
            GUILayout.Space(10);
            GUILayout.Label("X", GUILayout.Width(10));
            tilerRef.gridMap.countX = float.Parse(GUILayout.TextField(tilerRef.gridMap.count.x.ToString(), 5));
            GUILayout.Space(10);
            GUILayout.Label("Y", GUILayout.Width(10));
            tilerRef.gridMap.countY = float.Parse(GUILayout.TextField(tilerRef.gridMap.count.y.ToString(), 5));
            GUILayout.Space(10);
            GUILayout.Label("Z", GUILayout.Width(10));
            tilerRef.gridMap.countZ = float.Parse(GUILayout.TextField(tilerRef.gridMap.count.z.ToString(), 5));
            GUILayout.EndHorizontal();

            ///----

            GUILayout.BeginHorizontal();
            GUILayout.Label("Step", GUILayout.MaxWidth(50));
            GUILayout.Space(10);
            GUILayout.Label("X", GUILayout.Width(10));
            tilerRef.gridMap.stepX = float.Parse(GUILayout.TextField(tilerRef.gridMap.stepX.ToString(), 5));
            GUILayout.Space(10);
            GUILayout.Label("Y", GUILayout.Width(10));
            tilerRef.gridMap.stepY = float.Parse(GUILayout.TextField(tilerRef.gridMap.stepY.ToString(), 5));
            GUILayout.Space(10);
            GUILayout.Label("Z", GUILayout.Width(10));
            tilerRef.gridMap.stepZ = float.Parse(GUILayout.TextField(tilerRef.gridMap.stepZ.ToString(), 5));
            GUILayout.EndHorizontal();

            ///

            GUILayout.BeginHorizontal();
            GUILayout.Label("Tiles", GUILayout.MaxWidth(50));
            GUILayout.Space(10);
            GUILayout.Label("Width", GUILayout.MaxWidth(50));
            tilerRef.gridMap.tileWidth =
                int.Parse(GUILayout.TextField(tilerRef.gridMap.tileWidth.ToString(), 6));
            GUILayout.Space(10);
            GUILayout.Label("Height", GUILayout.MaxWidth(50));
            tilerRef.gridMap.tileHeight =
                int.Parse(GUILayout.TextField(tilerRef.gridMap.tileHeight.ToString(), 6));
            GUILayout.EndHorizontal();
            ///

            //GUILayout.FlexibleSpace();
            GUILayout.Space(ButtonHeight);

            GUILayout.BeginHorizontal();
            GUILayout.Space(10);
            tilerRef.showIngame = GUILayout.Toggle(tilerRef.showIngame, "Show ALL", "Button");
            GUILayout.Space(10);
            tilerRef.gridMap.showPlane =
                GUILayout.Toggle(tilerRef.gridMap.showPlane, "Show Plane", "Button");
            GUILayout.Space(10);
            tilerRef.gridMap.showMapBounds =
                GUILayout.Toggle(tilerRef.gridMap.showMapBounds, "Show Bounds", "Button");
            GUILayout.Space(10);

            GUILayout.EndHorizontal();


            ///

            GUILayout.BeginHorizontal();
            GUILayout.Label("Tile Discrepance: ");
            tilerRef.gridMap.Discrepance =
                float.Parse(GUILayout.TextField(tilerRef.gridMap.Discrepance.ToString(), 9));
            GUILayout.EndHorizontal();

            GUILayout.BeginHorizontal();
            GUILayout.Label("Grid Unlocked Displacement: ");
            tilerRef.gridMap.GridUnlockedMove =
                float.Parse(GUILayout.TextField(tilerRef.gridMap.GridUnlockedMove.ToString(), 9));
            GUILayout.EndHorizontal();

            /// 
            GUILayout.BeginHorizontal();
            GUILayout.Label("Epsilon", GUILayout.MaxWidth(50));
            GUILayout.Space(10);
            GUILayout.Label("X:", GUILayout.MaxWidth(20));
            tilerRef.gridMap.EpsilonX =
                float.Parse(GUILayout.TextField(tilerRef.gridMap.EpsilonX.ToString(), 9, GUILayout.MaxWidth(50)));
            GUILayout.Space(10);
            GUILayout.Label("Y:", GUILayout.MaxWidth(20));
            tilerRef.gridMap.EpsilonY =
                float.Parse(GUILayout.TextField(tilerRef.gridMap.EpsilonY.ToString(), 9, GUILayout.MaxWidth(50)));
            GUILayout.Space(20);
            if (GUILayout.Button("Reset", GUILayout.MaxWidth(50)))
                tilerRef.gridMap.Epsilon = new Vector2(0.0025f, 0.0025f);
            GUILayout.EndHorizontal();

            GUILayout.Label("Experimental");
            GUILayout.BeginHorizontal();
            tilerRef.gridMap.KeepPlaneInbounds =
                GUILayout.Toggle(tilerRef.gridMap.KeepPlaneInbounds, "Keep Plane Inbound", "Button");

            GUILayout.Space(10);

            tilerRef.CombineMesh =
                GUILayout.Toggle(tilerRef.CombineMesh, "Combine ALL Mesh", "Button");

            GUILayout.EndHorizontal();

            /// 

            if (GUILayout.Button("ReBuild Layers"))
                tilerRef.gridMap.ReBuildLayers();
            /// 

            GUILayout.FlexibleSpace();

            if (GUILayout.Button("Close") || KeyCode.Escape == Event.current.keyCode)
                EnabledMapSetup = false;
            GUI.DragWindow(new Rect(0, 0, Screen.width, Screen.height));    //GUI.DragWindow(new Rect(0, 0, 10000, 10000));
        }

        void DrawLeftPanel()
        {

            if (LeftPanelFoldOut)  // Change this If you want permanent minimization
            {
                Rect rLeftClose =
                    new Rect(margin, GridMapPanel.yMax - ButtonHeight - margin,
                    GridMapPanel.width, ButtonHeight);

                LeftPanelFoldOut = GUI.Toggle(rLeftClose, LeftPanelFoldOut, "▲", "Button");
                return;
                //GUIUtility.ExitGUI();
            }


            GUI.Box(LeftBoxPanel, "");


            if (OrientationComboBox != null)
            {
                if (OrientationComboBox.SelectedItemIndex != (int)tilerRef.gridMap.currentOrientation)
                {
                    tilerRef.gridMap.RefreshGridPlaneSettingsAll();                      //tilerRef.ProcessGridUpdates();
                    OrientationComboBox.SelectedItemIndex = (int)tilerRef.gridMap.currentOrientation;
                    tilerRef.RefreshPreviewTarget(tilerRef.CurrentTileID);
                }

                int tempSize = GUI.skin.button.fontSize;    // 13
                GUI.skin.button.fontSize = 10;


                OrientationComboBox.GetRect =
                    new Rect(GridMapPanel.xMin, GridMapPanel.yMin + ButtonHeight * 7 /*- margin*/,
                            GridMapPanel.width, ButtonHeight);

                tilerRef.gridMap.currentOrientation = (GridMap.GridOrientation)
                OrientationComboBox.ShowIn(
                        OrientationComboBox.GetRect.xMin + LeftBoxPanel.width + margin * 8,
                                    OrientationComboBox.GetRect.yMin - ButtonHeight);

                if (OrientationComboBox.isChanged && LinkPlanesWithCamera)
                    SetupCameraOrientation();

                GUI.skin.button.fontSize = tempSize;
            }


            GUI.SetNextControlName("FileNameField");
            filePathAndName = GUI.TextField(
                new Rect(margin, GridMapPanel.yMin + ButtonHeight + margin,
                                               FileNameFieldWidth, ButtonHeight), filePathAndName);

            if (GUI.GetNameOfFocusedControl() == "FileNameField")
            {
                FileNameFieldWidth =
                    GUI.skin.textField.CalcSize(new GUIContent(filePathAndName + "             ")).x;
            }
            else
                FileNameFieldWidth = GridMapPanel.width;



            GUILayout.BeginArea(GridMapPanel);

            if (GUILayout.Button("New File"))
            {
                filePathAndName = "";
                tilerRef.NewMap();
            }
            GUILayout.Space(ButtonHeight + margin);


            if (GUILayout.Button("Load"))
                OpenFileCheck();

            if (GUILayout.Button("Save"))
                SaveFileCheck();

            EnabledMapSetup =
                GUILayout.Toggle(EnabledMapSetup, "Setup", "Button");

            // ----------------------------------------------------------------------//

            if (LeftBoxArea.Contains(Input.mousePosition))
            {
                char chr = Event.current.character;
                if ((chr < '0' || chr > '9') && chr != '.' && chr != '-')
                    Event.current.character = '\0';
            }

            // ----------------------------------------------------------------------//

            GUILayout.BeginHorizontal();

            if (RightBoxArea.Contains(Input.mousePosition))
            {
                char chr = Event.current.character;
                if (!char.IsNumber(chr) && chr != '.' && chr != '-')
                    Event.current.character = '\0';
            }

            string tempString =
                GUILayout.TextField(
                tilerRef.GridDistance.ToString(), 4, GUILayout.Width(ToolsPanel.width * .5f));

            if (tempString == "." || tempString.Split('.').Length > 2)
                tempString = "0.1";

            tilerRef.GridDistance = float.Parse(tempString);


            tilerRef.moduledGrids =
                          GUILayout.Toggle(tilerRef.moduledGrids, "↕♯", "Button"/*, GUILayout.Width(24)*/); // "Grip"
            GUILayout.EndHorizontal();

            // Here Should Go Orientation ComboBox but it's outside the area
            // It cannot display the popup window properly embbed inside Rect area
            GUILayout.Space(ButtonHeight + margin);

            GUILayout.Button("Layers");

            enableLayerSetup =
                    GUILayout.Toggle(enableLayerSetup, "Options", "Button"); // "enableLayerSetup"


            // ----------------------------------------------------------------------//

            GUILayout.Button("Close");
            LeftPanelFoldOut = GUILayout.Toggle(LeftPanelFoldOut, "▼", "Button");

            GUILayout.EndArea();

            if (tilerRef.moduledGrids)
                tilerRef.GridDistance =
                    (int)GUI.VerticalSlider(
                        new Rect(LeftBoxPanel.xMax + margin, GridMapPanel.yMin + margin,
                        GridMapPanel.width * .5f, GridMapPanel.height - margin * 2),
                       tilerRef.GridDistance,
                       tilerRef.gridMap.GetGridMaxOffsetCurrent(), 0);
            else
            {
                tilerRef.GridDistance =
                GUI.VerticalSlider(
                    new Rect(LeftBoxPanel.xMax + margin, GridMapPanel.yMin + margin,
                    GridMapPanel.width * .5f, GridMapPanel.height - margin * 2),
                    tilerRef.GridDistance,
                    tilerRef.gridMap.GetGridMaxOffsetCurrent(), 0);
            }
        }


        void DrawRightPanel()
        {

            if (RightPanelFoldOut //) // Change this If you want permanent minimization
               && !RightBoxArea.Contains(Input.mousePosition))
            {
                Rect rRightClose = new Rect(RightBoxPanel.xMin + margin,
                                            Screen.height - ButtonHeight - margin,
                                            ToolsPanel.width, ButtonHeight);

                RightPanelFoldOut = GUI.Toggle(rRightClose, RightPanelFoldOut, "▲", "Button");
                return;
            }

            GUI.Box(RightBoxPanel, "");

            GUILayout.BeginArea(ToolsPanel);

            onPaint = GUILayout.Toggle(onPaint, "Paint", "Button");
            if (onPaint)
            {
                if (tilerRef.CurrentMode != Tiler.TilerMode.Paint)
                {
                    tilerRef.CurrentMode = Tiler.TilerMode.Paint;
                    tilerRef.RefreshPreviewTarget(tilerRef.CurrentTileID);
                }
                onPaint = SetMeOnly();
            }

            onErase = GUILayout.Toggle(onErase, "Erase", "Button");
            if (onErase)
            {
                if (tilerRef.CurrentMode != Tiler.TilerMode.Erase)
                    tilerRef.CurrentMode = Tiler.TilerMode.Erase;
                onErase = SetMeOnly();
            }

            onBucket = GUILayout.Toggle(onBucket, "Bucket", "Button");
            if (onBucket)
            {
                tilerRef.CurrentMode = Tiler.TilerMode.Bucket;
                onBucket = SetMeOnly();
            }

            ////GUILayout.Button("Prefabs");
            onPrefab = GUILayout.Toggle(onPrefab, "Prefabs", "Button");
            if (onPrefab)
            {
                tilerRef.CurrentMode = Tiler.TilerMode.Prefab;
                onPrefab = SetMeOnly();
            }

            //onSelect = GUILayout.Toggle(onSelect, "Select", "Button");
            //if (onSelect)
            //{
            //    tilerRef.CurrentMode = Tiler.TilerMode.Select;
            //    onSelect = SetMeOnly();
            //}

            if (!onBucket && !onPaint && !onPrefab && !onErase /*&& !onSelect */ )
                tilerRef.CurrentMode = Tiler.TilerMode.None;

            GUILayout.Space(ButtonHeight*2 +margin);


            GUILayout.Label("Rotate 90");
            GUILayout.BeginHorizontal();

            if (GUILayout.Button("-"))
                if (tilerRef.Rotation == 0)
                    tilerRef.Rotation = 270;
                else
                    tilerRef.Rotation -= 90;

            if (GUILayout.Button("+"))
                tilerRef.Rotation += 90;
            GUILayout.EndHorizontal();

            GUILayout.Label("Flip Tile");
            GUILayout.BeginHorizontal();

            GUI.skin.button.fontStyle = (tilerRef.FlipX ? FontStyle.BoldAndItalic : FontStyle.Normal);
            if (GUILayout.Button("X"))
            {
                tilerRef.FlipX = !tilerRef.FlipX;
                tilerRef.mirrorX = !tilerRef.mirrorX;

                tilerRef.RefreshPreviewTarget(tilerRef.CurrentTileID);
            }
            GUI.skin.button.fontStyle = FontStyle.Normal;

            GUI.skin.button.fontStyle = (tilerRef.FlipY ? FontStyle.BoldAndItalic : FontStyle.Normal);
            if (GUILayout.Button("Y"))
            {
                tilerRef.FlipY = !tilerRef.FlipY;
                tilerRef.mirrorY = !tilerRef.mirrorY;

                tilerRef.RefreshPreviewTarget(tilerRef.CurrentTileID);
            }

            GUI.skin.button.fontStyle = FontStyle.Normal;

            GUILayout.EndHorizontal();
      
            // ----------------------------------------------------------------------//

            //GUILayout.Button("▼");
            RightPanelFoldOut = GUILayout.Toggle(RightPanelFoldOut, "▼", "Button");

            GUILayout.EndArea();
        }


        /// -------------------------- Tiles Preview Grid     --------------------- ///
        // CLEAN UP ALL THOSE STUPIDS  "...tileSetComboBox != null..." "..etc.." iterative checks!

        void DrawBottomPanel()
        {
            GUI.Box(BottomPanel, "");

            if (!onPrefab)
                TileSetPalette();
            else
                PrefabPalette();
        }

        void PrefabPalette()
        {
            if (tilerRef.gridMap == null)
                return;

            if (objGroupComboBox != null && PrefabLoader.Get.ObjGroups != null
                && PrefabLoader.Get.ObjGroups.Count > 0)
            {

                if (objGroupComboBox.SelectedItemIndex != (int)CurrentObjGroup)   // ComboBox checkUp
                {
                    objGroupComboBox.SelectedItemIndex = (int)CurrentObjGroup;
                    if (onPrefab)
                        tilerRef.RefreshPreviewTarget(tilerRef.CurrentTileID);
                }

                if (PrefabLoader.Get.ObjectsChanged)
                {
                    //Debug.Log("Changed OBJECTS");
                    CheckObjGroupList();
                }

                //////////////////////
                objGroupComboBox.GetRect =
                    new Rect(BottomPanel.x + ButtonHeight + margin, BottomPanel.yMin /*+margin*/,
                                objGroupComboBox.GetRect.width, ButtonHeight);


                //if(!tileSetComboBox.initialized)
                CurrentObjGroup =
                    objGroupComboBox.ShowIn(BottomPanel.x + ButtonHeight + margin,
                                BottomPanel.yMin - 4 - ButtonHeight * PrefabLoader.Get.ObjGroups.Count);
            }

            //////////////////////
            GUILayout.BeginArea(BottomPanel);

            GUILayout.BeginHorizontal();

            // BAR
            if (GUILayout.Button("+", GUILayout.Width(ButtonHeight)))
            {
                PrefabLoader.Get.BuildPrefabGroup(tilerRef.gridMap);
                setLastObjGrpIndex = true;

                return;
            }

            //if (GUILayout.Button("-", GUILayout.Width(ButtonHeight)))
            //{ }   //enableTileSetSetup = !enableTileSetSetup;

            if (objGroupComboBox == null || !objGroupComboBox.initialized
                || PrefabLoader.Get.ObjGroups.Count == 0)
            {
                GUI.enabled = false;
                GUILayout.Button("empty object Group", GUILayout.MaxWidth(360));
                GUI.enabled = true;
            }
            else
                GUILayout.Space(360 + margin * 2);


            enableObjSetup = GUILayout.Toggle(enableObjSetup, "Obj Cfg", "Button");
            //if (GUILayout.Button("Obj Cfg", GUILayout.MaxWidth(60)))
            //    enableObjSetup = !enableObjSetup;

            if (enableObjSetup)
                ObjSetupPanel = GUI.Window(5, ObjSetupPanel, DrawObjSetupWindow, "Object Configuration");

            if (GUILayout.Button("▼", GUILayout.MaxWidth(ButtonHeight)))
                ViewBottomPanel = false;

            GUILayout.EndHorizontal();
            GUILayout.EndArea();

            if (objGroupComboBox != null && objGroupComboBox.initialized
              && PrefabLoader.Get.ObjGroups.Count > 0)
            {
                /// TileSet' All Selectable Tiles Panel
                //if (PrefabLoader.Get.PrefabCache == null ||
                //    tileSetTiles.Length != GridMap.TileSets[CurrentTileSet].TileCount)
                //{
                //    //Debug.Log("Current TileCount " + GridMap.TileSets[CurrentTileSet].TileCount);
                //    tileSetTiles = new string[GridMap.TileSets[CurrentTileSet].TileCount];
                //}

                ObjScrollView = GUI.BeginScrollView(
                    new Rect(BottomPanel.x + margin + ButtonHeight, BottomPanel.y + ButtonHeight + margin,
                    BottomPanel.width - margin - ButtonHeight, BottomPanel.height - ButtonHeight - margin),// Position
                    ObjScrollView,
                    new Rect(BottomPanel.x, BottomPanel.y + ButtonHeight + margin,
                                ZoomFactor * BottomPanel.width - (ButtonHeight * 2),
                                ZoomFactor * BottomPanel.width - (ButtonHeight * 2))/*,*/// ViewRect *[size]
                                                                                         /*true, true*/);
                //// Texture
                //GUI.DrawTextureWithTexCoords(
                //        new Rect(BottomPanel.x, BottomPanel.y + ButtonHeight,
                //        ZoomFactor * (BottomPanel.width - (ButtonHeight * 2)),
                //        ZoomFactor * (BottomPanel.width - (ButtonHeight * 2))),
                //        GridMap.TileSets[CurrentTileSet].mat.mainTexture,
                //        new Rect(0, 0, 1, 1));

                // Tile Selection Button Grid
                SelectedObjId = GUI.SelectionGrid(
                        new Rect(BottomPanel.x, BottomPanel.y + ButtonHeight,
                         (BottomPanel.width - (ButtonHeight * 2)),
                         (BottomPanel.height - (ButtonHeight * 2))),
                        SelectedObjId, ResourceLoad.PrefabNames, 6);



                GUI.EndScrollView();

            }

        }


        void TileSetPalette()
        {
            //if (!Application.isPlaying)
            //    tileSetComboBox = tileSetShaderComboBox = tileCollisionComboBox = null;

            if (tileSetComboBox != null && GridMap.TileSets != null && GridMap.TileSets.Count > 0)
            {

                if (tileSetComboBox.SelectedItemIndex != (int)CurrentTileSet)   // ComboBox checkUp
                {
                    tileSetComboBox.SelectedItemIndex = (int)CurrentTileSet;
                    tileCollisionComboBox.SelectedItemIndex = (int)SelectedTileCollision;
                    if (onPaint)
                        tilerRef.RefreshPreviewTarget(tilerRef.CurrentTileID);
                }

                if (GridMap.TileSetsChanged)
                    CheckTileSetList();

                //////////////////////
                tileSetComboBox.GetRect =
                            new Rect( BottomPanel.xMin + ButtonHeight + margin, BottomPanel.yMin, 
                             tileSetComboBox.GetRect.width, ButtonHeight);

                CurrentTileSet =
                    tileSetComboBox.ShowIn(BottomPanel.xMin + ButtonHeight + margin,
                                        BottomPanel.yMin - 4 - ButtonHeight * GridMap.TileSets.Count);

                //////////////////////

                if (tileCollisionComboBox.SelectedItemIndex != (int)SelectedTileCollision)
                    tileCollisionComboBox.SelectedItemIndex = (int)SelectedTileCollision;

                tileCollisionComboBox.GetRect =
                            new Rect(BottomPanel.xMin + ButtonHeight + tileSetComboBox.GetRect.width + 60 + margin * 3, BottomPanel.yMin,
                                        tileCollisionComboBox.GetRect.width, ButtonHeight);

                SelectedTileCollision = (GridMap.CollisionType)
                    tileCollisionComboBox.ShowIn( BottomPanel.xMin + ButtonHeight + tileSetComboBox.GetRect.width + 60 + margin * 2,
                            BottomPanel.yMin - 16 - ButtonHeight * tileCollisionComboBox.TotalItems);

                if (tileCollisionComboBox.isChanged &&
                    tilerRef.gridMap.CurrentGroup.layers.ContainsKey(tilerRef.gridMap.CurrentGridOffset))
                {
                    tilerRef.gridMap.CurrentGroup.layers[tilerRef.gridMap.CurrentGridOffset].ReBuildLayer();

                    //Debug.Log("ReBUILDED Layer Because tileID Has Collider");
                }
            }

            GUILayout.BeginArea(BottomPanel);

            GUILayout.BeginHorizontal();
            if (GUILayout.Button("+", GUILayout.MaxWidth(ButtonHeight)))
            {
                //enableTileSetSetup = false;
                TileSet tileSet = new TileSet("new TileSet " + ((int)GridMap.TileSets.Count), filePathAndName);
                tileSet.TileWidth = tilerRef.gridMap.tileWidth;
                tileSet.TileHeight = tilerRef.gridMap.tileHeight;
                GridMap.TileSets.Add(tileSet);
                GridMap.TileSetsChanged = true;
                setLastTileSetIndex = true;
                return;
            }

            if (tileSetComboBox == null || !tileSetComboBox.initialized || GridMap.TileSets.Count == 0)
            {
                GUI.enabled = false;
                GUILayout.Button("empty tileset", GUILayout.MaxWidth(196));
                GUI.enabled = true;
            }
            else
                GUILayout.Space(200);

            if (GUILayout.Button("Attribs", GUILayout.MaxWidth(60)))
                enableTileSetSetup = !enableTileSetSetup;


            //if (tileSetComboBox != null && GridMap.TileSets != null && GridMap.TileSets.Count > 0)
            if (tileSetComboBox == null || !tileSetComboBox.initialized || GridMap.TileSets.Count == 0)
            {
                //GUILayout.Button("no Collision", GUILayout.MaxWidth(196));
                //tileCollisionComboBox.Show()
                GUI.enabled = false;
                GUILayout.Button("no Collision", GUILayout.MaxWidth(196));
                GUI.enabled = true;
            }
            else
                GUILayout.Space(200);

            if (GUILayout.Button("▼", GUILayout.MaxWidth(ButtonHeight)))
                ViewBottomPanel = false;

            GUILayout.EndHorizontal();


            if (enableTileSetSetup)
                TileSetPanel = GUI.Window(1, TileSetPanel, DrawTileSetupWindow, "TileSet Attributes");


            GUILayout.EndArea();


            if (tileSetComboBox != null && tileSetComboBox.initialized
                && GridMap.TileSets.Count > 0
                && GridMap.TileSets[CurrentTileSet].mat != null
                && GridMap.TileSets[CurrentTileSet].mat.mainTexture != null)
            {

                /// TileSet' All Selectable Tiles Panel
                if (tileSetTiles == null || tileSetTiles.Length != GridMap.TileSets[CurrentTileSet].TileCount)
                {
                    //Debug.Log("Current TileCount " + GridMap.TileSets[CurrentTileSet].TileCount);
                    tileSetTiles = new string[GridMap.TileSets[CurrentTileSet].TileCount];

                    if (style == null)
                    {
                        style = new GUIStyle(GUI.skin.button);
                        style.margin = new RectOffset();
                        Texture2D texture = new Texture2D(1, 1);
                        style.normal.background = texture;
                        texture.SetPixel(1, 1, Color.clear);
                        texture.Apply();
                    }
                }

                scrollPosition = GUI.BeginScrollView(
                    new Rect(BottomPanel.x + margin + ButtonHeight, BottomPanel.y + ButtonHeight + margin,
                    BottomPanel.width - margin - ButtonHeight,
                    BottomPanel.height - ButtonHeight - margin),// Position
                    scrollPosition,
                    new Rect(BottomPanel.x, BottomPanel.y + ButtonHeight + margin,
                                ZoomFactor * BottomPanel.width - (ButtonHeight * 2),
                                ZoomFactor * BottomPanel.width - (ButtonHeight * 2))/*,*/// ViewRect *[size]
                                                                                         /*true, true*/);
                // Texture
                GUI.DrawTextureWithTexCoords(
                        new Rect(BottomPanel.x, BottomPanel.y + ButtonHeight,
                        ZoomFactor * (BottomPanel.width - (ButtonHeight * 2)),
                        ZoomFactor * (BottomPanel.width - (ButtonHeight * 2))),
                        GridMap.TileSets[CurrentTileSet].mat.mainTexture,
                        new Rect(0, 0, 1, 1));

                // Tile Selection Button Grid
                SelectedTileId = GUI.SelectionGrid(
                        new Rect(BottomPanel.x, BottomPanel.y + ButtonHeight,
                        ZoomFactor * (BottomPanel.width - (ButtonHeight * 2)),
                        ZoomFactor * (BottomPanel.width - (ButtonHeight * 2))),
                        SelectedTileId,
                            tileSetTiles, GridMap.TileSets[CurrentTileSet].TileColumns, style);


                GUI.EndScrollView();

                ZoomFactor = GUI.VerticalSlider(
                     new Rect(BottomPanel.x + margin /*- ButtonHeight*/,
                                //Screen.height - BottomPanel.height - ButtonHeight,
                                BottomPanel.y + ButtonHeight + margin,
                                ButtonHeight, BottomPanel.height - ButtonHeight - margin),
                                ZoomFactor, 0, 4);
            }

        }

        void CheckTileSetList()
        {
            if (GridMap.TileSets.Count == 0)  //if(!tileSetComboBox.initialized)
            {
                //Debug.Log("Empty lists");
                tileSetComboBox = null;
                tileSetComboBox = new ComboBox();

                tileSetComboBox.GetRect =
                    new Rect(BottomPanel.xMin + ButtonHeight + margin, BottomPanel.yMin,
                        tileSetComboBox.GetRect.width, ButtonHeight);
                return;
            }

            string[] matchs = new string[GridMap.TileSets.Count];
            int i = 0;
            foreach (TileSet tSets in GridMap.TileSets)
                matchs[i++] = tSets.TileSetName;


            if (tileSetComboBox.CheckListChanged(matchs))
            {
                tileSetComboBox = null;
                tileSetComboBox = new ComboBox(new Rect(ButtonHeight + 2, 0, 200, ButtonHeight), matchs);
                tileSetComboBox.GetRect =
                    new Rect(BottomPanel.xMin + ButtonHeight + margin, BottomPanel.yMin,
                                tileSetComboBox.GetRect.width, ButtonHeight);

                if (setLastTileSetIndex)
                {
                    tileSetComboBox.SelectedItemIndex = CurrentTileSet = GridMap.TileSets.Count - 1;
                    enableTileSetSetup = true;
                    setLastTileSetIndex = false;
                }
            }
            GridMap.TileSetsChanged = false;

            //CurrentTileTexture = new Texture[3];
            //CurrentTileTexture[0] = new GUIContent()
        }

        void DrawTileSetupWindow(int windowID)
        {

            if (GridMap.TileSets.Count > 0 && GridMap.TileSets[CurrentTileSet] != null)
            {
                /////////////////////////////////////////////////////////////////////////

                TileSet currentTileSet = GridMap.TileSets[CurrentTileSet];

                GUILayout.BeginHorizontal();
                GUILayout.Label("Name", GUILayout.MaxWidth(50));
                currentTileSet.TileSetName = GUILayout.TextField(currentTileSet.TileSetName);
                GUILayout.EndHorizontal();

                GUILayout.BeginHorizontal();
                GUILayout.Label("Image Path", GUILayout.MaxWidth(100));

                currentTileSet.ImagePath = GUILayout.TextField(currentTileSet.ImagePath, GUILayout.MaxWidth(160));

                if (GUILayout.Button("Search", GUILayout.MaxWidth(64)))
                {
                    string newImagePath = OpenImageCheck();

                    if (!string.IsNullOrEmpty(newImagePath) && newImagePath != currentTileSet.ImagePath)
                    {
                        //Debug.Log(" Searching path " + newImagePath + " in File" + filePathAndName);

                        currentTileSet.SetImagePathName(newImagePath, filePathAndName); //currentTileSet.ImagePath = newImagePath;
                        //currentTileSet.SetupTileData();
                    }
                }
                GUILayout.EndHorizontal();


                GUILayout.BeginHorizontal();
                GUILayout.Label("Tile Width", GUILayout.MaxWidth(64));
                GUILayout.Space(10);
                currentTileSet.TileWidth = int.Parse(GUILayout.TextField(currentTileSet.TileWidth.ToString(), GUILayout.MaxWidth(32)));
                GUILayout.Space(20);
                GUILayout.Label("Tile Height", GUILayout.MaxWidth(64));
                currentTileSet.TileHeight = int.Parse(GUILayout.TextField(currentTileSet.TileHeight.ToString(), GUILayout.MaxWidth(32)));
                GUILayout.EndHorizontal();

                GUILayout.BeginHorizontal();
                GUILayout.Label("Shader ", GUILayout.MaxWidth(64));
                if (tileSetShaderComboBox != null)
                {
                    if (tileSetShaderComboBox.SelectedItemIndex != (int)currentTileSet.CurrentShader)
                        tileSetShaderComboBox.SelectedItemIndex = (int)currentTileSet.CurrentShader;

                    currentTileSet.CurrentShader = (TileSet.ShaderMaterial)tileSetShaderComboBox.Show();
                }
                GUILayout.EndHorizontal();

                GUI.enabled = false;
                GUILayout.BeginHorizontal();
                GUILayout.Label("Tile Columns " + int.Parse(currentTileSet.TileColumns.ToString())
                    + " \t Rows " + int.Parse(currentTileSet.TileRows.ToString()));
                GUILayout.EndHorizontal();


                GUILayout.BeginHorizontal();
                GUILayout.Label("Tile Count " + int.Parse(currentTileSet.TileCount.ToString())
                    + " \t First Gid " + int.Parse(currentTileSet.FirstGid.ToString()));
                GUILayout.EndHorizontal();


                GUILayout.BeginHorizontal();
                GUILayout.Label("Image Width " + int.Parse(currentTileSet.ImageWidth.ToString())
                    + " \t Height " + int.Parse(currentTileSet.ImageHeight.ToString()));
                GUILayout.EndHorizontal();
                GUI.enabled = true;

                if (GUILayout.Button("Delete", GUILayout.MaxWidth(64)))
                {
                    TileSet current = GridMap.TileSets[CurrentTileSet];
                    GridMap.TileSets.Remove(current);
                    CurrentTileSet--;
                    CurrentTileSet = Mathf.Clamp(CurrentTileSet, 0, GridMap.TileSets.Count);
                    current = null;
                    GridMap.TileSetsChanged = true;
                }

                //////////////////////////////////////////////////////////////////////////
            }

            GUILayout.FlexibleSpace();

            if (GUILayout.Button("Close") || KeyCode.Escape == Event.current.keyCode)
                enableTileSetSetup = false;
            GUI.DragWindow(new Rect(0, 0, Screen.width, Screen.height));    //GUI.DragWindow(new Rect(0, 0, 10000, 10000));
        }

        GridMap.CollisionType SelectedTileCollision     // Setup Propierty for single Tile Collision         
        {
            get
            {
                if (GridMap.TileSets[CurrentTileSet].Collisions.ContainsKey((int)tilerRef.CurrentTileID))
                {
                    char[] a = GridMap.TileSets[CurrentTileSet].Collisions[(int)tilerRef.CurrentTileID].ToCharArray();
                    a[0] = char.ToUpper(a[0]);
                    return (GridMap.CollisionType)System.Enum.Parse(typeof(GridMap.CollisionType),
                        new string(a));
                }
                else return GridMap.CollisionType.None;
            }
            set
            {

                if (GridMap.TileSets[CurrentTileSet].Collisions.ContainsKey((int)tilerRef.CurrentTileID))
                {
                    // Already Included in Dictionary Check & Change it (or delete if equal to zero)
                    if (GridMap.TileSets[CurrentTileSet].Collisions[(int)tilerRef.CurrentTileID] != value.ToString())
                        if (value.ToString() != "None")
                            GridMap.TileSets[CurrentTileSet].Collisions[(int)tilerRef.CurrentTileID] = value.ToString();
                        else
                            //Debug.Log("Removed " + value.ToString() + " Off " + (int)tilerRef.CurrentTileID);
                            GridMap.TileSets[CurrentTileSet].Collisions.Remove((int)tilerRef.CurrentTileID);
                }
                else    // Never Seen it, Check it and include it (If diferent than zero)
                    if (value.ToString() != "None") /// CARGA Y DIBUJA LISTA COLLISION BIEN
                {
                    //Debug.Log("Added " + value.ToString() + " To " + (tilerRef.CurrentTileID));
                    GridMap.TileSets[CurrentTileSet].Collisions.Add(
                        (int)tilerRef.CurrentTileID, value.ToString());
                }
            }
        }

        int SelectedTileId  // A converter between the REAL tiler.CurrentTileId and the GUI Selected
        {
            get
            {
                return (int)(tilerRef.CurrentTileID - 1);
            }
            set
            {
                if (tilerRef.CurrentTileID != value + 1)
                {
                    if(tilerRef.CurrentMode != Tiler.TilerMode.Paint
                        && tilerRef.CurrentMode != Tiler.TilerMode.Bucket)
                    {
                        tilerRef.CurrentMode = Tiler.TilerMode.Paint;
                        onPaint = SetMeOnly();
                    }

                    tilerRef.CurrentTileID = (uint)value + 1;
                    tilerRef.RefreshPreviewTarget(tilerRef.CurrentTileID);
                }
            }
        }

        ///   -----------------------------------------------------------------------  ///

        GUIStyle style;
        Vector2 scrollPosition = new Vector2();

        [ShowOnly, SerializeField]
        bool enableTileSetSetup = false, setLastTileSetIndex = false, ViewBottomPanel = true;
        Rect BottomArea = new Rect(512, 512, 512, 256);
        Rect BottomPanel = new Rect(512, 512, 512, 256); /// This should be modifiable 
        Rect TileSetPanel = new Rect(512, 512, 320, 256);

        ComboBox tileSetComboBox, tileSetShaderComboBox, tileCollisionComboBox;
        public int CurrentTileSet = 0, TotalTileSets = 0;
        public float ZoomFactor = 1;
        string[] tileSetTiles;

        ///   -----------------------------------------------------------------------  ///

        ComboBox objGroupComboBox;

        public int CurrentObjGroup = 0;

        int selectedObjId = -1;

        public int SelectedObjId
        {
            get
            {
                return selectedObjId;
            }

            set
            {
                if (value == Mathf.Clamp(value, 0, (int)ResourceLoad.Prefabs.COUNT))
                {
                    if (selectedObjId != value)
                    {
                        selectedObjId = value;
                        tilerRef.RefreshPreviewObject(selectedObjId);
                    }
                }
            }
        }

        bool enableObjSetup = false, setLastObjGrpIndex = false;
        Rect ObjSetupPanel = new Rect(512, 512, 360, 256);
        Vector2 ObjScrollView = new Vector2();

        void DrawObjSetupWindow(int windowID)
        {
            if (PrefabLoader.Get.ObjGroups.Count > 0
                && PrefabLoader.Get.ObjGroups[CurrentObjGroup] != null)
            {
                /////////////////////////////////////////////////////////////////////////

                GameObject currentGroup = PrefabLoader.Get.ObjGroups[CurrentObjGroup];

                GUILayout.BeginHorizontal();
                GUILayout.Label("Name", GUILayout.MaxWidth(50));
                currentGroup.name = GUILayout.TextField(currentGroup.name);
                currentGroup.SetActive(
                    GUILayout.Toggle(currentGroup.activeSelf, "Active", "Button", GUILayout.MaxWidth(70)));
                GUILayout.EndHorizontal();

                GUILayout.BeginHorizontal();
                GUILayout.Label("Prefab Center", GUILayout.MaxWidth(50));
                GUILayout.Space(10);
                GUILayout.Label("X:", GUILayout.MaxWidth(20));
                //currentLayer.OffsetX =
                //    float.Parse(GUILayout.TextField(currentLayer.OffsetX.ToString(), 5));
                GUILayout.Space(10);
                GUILayout.Label("Y:", GUILayout.MaxWidth(20));
                //currentLayer.OffsetY =
                //    float.Parse(GUILayout.TextField(currentLayer.OffsetY.ToString(), 5));
                GUILayout.Space(10);
                GUILayout.Label("Z:", GUILayout.MaxWidth(20));
                //currentLayer.OffsetX =
                //    float.Parse(GUILayout.TextField(currentLayer.OffsetX.ToString(), 5));
                GUILayout.Space(10);
                GUILayout.EndHorizontal();

                GUILayout.FlexibleSpace();

                if (GUILayout.Button("Prefabs Disabled", GUILayout.MaxWidth(128)))
                {
                    Debug.Log("Create Disabled Prefabs");
                }


                if (GUILayout.Button("Delete Group", GUILayout.MaxWidth(128)))
                {
                    //GameObject currentGroup = PrefabLoader.Get.ObjGroups[CurrentObjGroup];
                    PrefabLoader.Get.ObjGroups.Remove(currentGroup);
                    GameObject.Destroy(currentGroup);
                    CurrentObjGroup--;
                    CurrentObjGroup =
                        Mathf.Clamp(CurrentObjGroup, 0, PrefabLoader.Get.ObjGroups.Count);
                    currentGroup = null;
                    PrefabLoader.Get.ObjectsChanged = true;
                }


            }
            GUILayout.FlexibleSpace();

            if(GUILayout.Button("Close") || (enableObjSetup&& KeyCode.Escape ==Event.current.keyCode) )
            {
                enableObjSetup = false;
                PrefabLoader.Get.ObjectsChanged = true;
            }
                GUI.DragWindow(new Rect(0, 0, Screen.width, Screen.height));    //GUI.DragWindow(new Rect(0, 0, 10000, 10000));
        }

        void CheckObjGroupList()
        {
            //if (tilerRef.gridMap == null)
            //    return;

            //if (objGroupComboBox == null)
            //    objGroupComboBox = new ComboBox();

            if ( PrefabLoader.Get.ObjGroups.Count == 0)  //if(!tileSetComboBox.initialized)
            {
                //Debug.Log("Empty lists");
                objGroupComboBox = null;
                objGroupComboBox = new ComboBox();

                objGroupComboBox.GetRect =
                    new Rect(BottomPanel.xMin + ( ButtonHeight + margin) * 2, BottomPanel.yMin,
                        objGroupComboBox.GetRect.width , ButtonHeight);
                return;
            }

            string[] matchs = new string[PrefabLoader.Get.ObjGroups.Count];
            int i = 0;
            foreach (GameObject objGroup in PrefabLoader.Get.ObjGroups)
                if(objGroup)
                    matchs[i++] = objGroup.name;

            if (objGroupComboBox.CheckListChanged(matchs))
            {
                objGroupComboBox = null;
                objGroupComboBox = 
                    new ComboBox(new Rect((ButtonHeight + margin ) * 2, 0, 360, ButtonHeight), matchs);

                objGroupComboBox.GetRect =
                    new Rect(BottomPanel.xMin + ButtonHeight + margin, BottomPanel.yMin,
                                objGroupComboBox.GetRect.width, ButtonHeight);

                if (setLastObjGrpIndex)
                {
                    objGroupComboBox.SelectedItemIndex = CurrentObjGroup =
                                                PrefabLoader.Get.ObjGroups.Count - 1;
                    enableTileSetSetup = true;
                    setLastObjGrpIndex = false;
                }
            }
            //GridMap.TileSetsChanged = false;
            PrefabLoader.Get.ObjectsChanged = false;

        }

    }
}
