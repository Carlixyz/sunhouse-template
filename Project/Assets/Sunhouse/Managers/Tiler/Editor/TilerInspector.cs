using UnityEngine;
using UnityEditor;
using System.Collections;
using System.Collections.Generic;

namespace Sun.Tools
{
    [CustomEditor(typeof(Tiler))]
    public class TilerInspector : Editor
    {
        //SerializeField GridDistanceValue;
        private SerializedObject lbserialized = null;
        private Tiler tilerRef = null;

        public GridMap.GridOrientation prevOrientation = GridMap.GridOrientation.Horizontal;
        public Vector3 prevSize = Vector3.one * 4, prevStep = Vector3.one * 4;
        bool  planesDataFolding = false;

        public void Initialize()
        {
            if (tilerRef == null)
                tilerRef = (Tiler)target;

            if (lbserialized == null)
                lbserialized = serializedObject;
        }

        public override void OnInspectorGUI()
        {
            Initialize();

            ShowTilerControls();

            base.DrawDefaultInspector();

            EditorGUILayout.Separator();

            //gridMapDataFolding = EditorGUILayout.Foldout(gridMapDataFolding, "Grid Map Data");
            //if (gridMapDataFolding)
            //    ShowGridControls();


            if (tilerRef.GridDistance != tilerRef.gridMap.CurrentGridOffset 
                || tilerRef.gridMap.count != prevSize || tilerRef.gridMap.step != prevStep )
            {
                tilerRef.gridMap.RefreshGridPlaneSettings(tilerRef.transform.position);
                tilerRef.gridMap.CurrentGridOffset = tilerRef.GridDistance;

                prevSize = tilerRef.gridMap.count ;
                prevStep = tilerRef.gridMap.step ;
            }

            //base.Repaint();

            GUILayout.BeginHorizontal();
            if (GUILayout.Button("Load File") )
                OpenFileCheck();

            //if (GUILayout.Button("Save Mesh"))
            //    //foreach (var comp in tilerRef.GetComponentsInChildren<meshCombiner>())
            //    foreach (var group in tilerRef.gridMap.Groups)
            //        foreach(var layer in group.layers)
            //            layer.
            //        foreach (var comp in tilerRef.gridMap.Groups.l.GetComponentsInChildren<meshCombiner>())
            //        comp.SaveAsset();

            //if (GUILayout.Button("PreLoadTest"))
            //   tilerRef.PreLoadTest();

            if (GUILayout.Button("new GridMap"))
                tilerRef.NewMap();

            tilerRef.MapVisibleInEditMode =
                GUILayout.Toggle(tilerRef.MapVisibleInEditMode, "Show Map in Hierarchy", "Button");

            GUILayout.EndHorizontal();

        }

        void OpenFileCheck()
        {
            string filePathAndName = "";
            if (string.IsNullOrEmpty(filePathAndName) || !System.IO.File.Exists(filePathAndName))
            {
                var extFilters = new[] {
                //new ExtensionFilter("Binary", "bin"),
                new SFB.ExtensionFilter("Tiled map", "tmx"),
                new SFB.ExtensionFilter("TextAsset file", "xml") };

                var fileStrings = SFB.StandaloneFileBrowser.OpenFilePanel("Open File",
                    Serializer.GetCurrentPath(),/* "xml"*/extFilters, false);

                if (fileStrings.Length > 0)
                    filePathAndName = fileStrings[0];
            }

            if (string.IsNullOrEmpty(filePathAndName))
                return;


            if (!tilerRef.LoadFileMap(filePathAndName))
                filePathAndName = "";
        }

        void ShowTilerControls()
        {
            tilerRef.moduledGrids = EditorGUILayout.Toggle(
                  new GUIContent("Module Grids", "Set Size values moduled to integers"),
                  tilerRef.moduledGrids);

            if (tilerRef.moduledGrids)
            {
                tilerRef.GridDistance =
                   EditorGUILayout.IntSlider(new GUIContent("Grid Normal Distance",
                                               "Set Current Grid Normal Axis offset"),
                                               (int)tilerRef.GridDistance, 0,
                                               (int)tilerRef.gridMap.GetGridMaxOffsetCurrent());


                ShowCurrentPlaneControl();


                //EditorGUILayout.BeginHorizontal();
                //EditorGUILayout.PrefixLabel(
                //    new GUIContent("Grid Map Size ", "Volumetric Size of GridMap"));

                //float oldValue = EditorGUIUtility.labelWidth;
                //EditorGUIUtility.labelWidth = 16;

                //tilerRef.gridMap.size.x = EditorGUILayout.IntField("X ", (int)tilerRef.gridMap.size.x);
                //tilerRef.gridMap.size.y = EditorGUILayout.IntField("Y ", (int)tilerRef.gridMap.size.y);
                //tilerRef.gridMap.size.z = EditorGUILayout.IntField("Z ", (int)tilerRef.gridMap.size.z);
                //EditorGUILayout.EndHorizontal();

                //EditorGUIUtility.labelWidth = oldValue;



                //EditorGUILayout.BeginHorizontal();
                //EditorGUILayout.PrefixLabel(new GUIContent("Grid Map Step ",
                //    "Each subdivision size (default:1)"));

                //EditorGUIUtility.labelWidth = 16;

                //tilerRef.gridMap.step.x = EditorGUILayout.IntField("X ", (int)tilerRef.gridMap.step.x);
                //tilerRef.gridMap.step.y = EditorGUILayout.IntField("Y ", (int)tilerRef.gridMap.step.y);
                //tilerRef.gridMap.step.z = EditorGUILayout.IntField("Z ", (int)tilerRef.gridMap.step.z);
                //EditorGUILayout.EndHorizontal();

                //EditorGUIUtility.labelWidth = oldValue;


            }
            else
            {
                tilerRef.GridDistance = EditorGUILayout.Slider(
                    new GUIContent("Grid Normal Distance", "Set Current Grid Normal Axis offset"),
                                        tilerRef.GridDistance, 0,
                                        tilerRef.gridMap.GetGridMaxOffsetCurrent());


                ShowCurrentPlaneControl();


                //tilerRef.gridMap.size =
                //    EditorGUILayout.Vector3Field(
                //        new GUIContent("Grid Map Size ", "Volumetric Size of GridMap"),
                //            tilerRef.gridMap.size);

                //tilerRef.gridMap.step =
                //    EditorGUILayout.Vector3Field(
                //        new GUIContent("Grid Map Step ", "Each subdivision size (default:1)"),
                //            tilerRef.gridMap.step);

            }


            //EditorGUI.indentLevel++;

            ////planesDataFolding = EditorGUILayout.Foldout(planesDataFolding, "Grid Planes Data");
            ////if (planesDataFolding)
            ////    ShowPlanesControls();

            //EditorGUI.indentLevel--;
        }


        void ShowGridControls()
        {
  

            tilerRef.moduledGrids = EditorGUILayout.Toggle(
                new GUIContent("Module Grids", "Set Size values moduled to integers"),
                tilerRef.moduledGrids);


            if (tilerRef.moduledGrids)
            {
                tilerRef.GridDistance =
                   EditorGUILayout.IntSlider(new GUIContent("Grid Normal Distance",
                                               "Set Current Grid Normal Axis offset"),
                                               (int)tilerRef.GridDistance, 0,
                                               (int)tilerRef.gridMap.GetGridMaxOffsetCurrent());


                ShowCurrentPlaneControl();


                EditorGUILayout.BeginHorizontal();
                EditorGUILayout.PrefixLabel(
                    new GUIContent("Grid Map Size ", "Volumetric Size of GridMap"));

                float oldValue = EditorGUIUtility.labelWidth;
                EditorGUIUtility.labelWidth =  16 ;

                tilerRef.gridMap.count.x = EditorGUILayout.IntField("X ", (int)tilerRef.gridMap.count.x);
                tilerRef.gridMap.count.y = EditorGUILayout.IntField("Y ", (int)tilerRef.gridMap.count.y);
                tilerRef.gridMap.count.z = EditorGUILayout.IntField("Z ", (int)tilerRef.gridMap.count.z);
                EditorGUILayout.EndHorizontal();

                EditorGUIUtility.labelWidth = oldValue;



                EditorGUILayout.BeginHorizontal();
                EditorGUILayout.PrefixLabel( new GUIContent("Grid Map Step ",
                    "Each subdivision size (default:1)") );

                EditorGUIUtility.labelWidth = 16;

                tilerRef.gridMap.step.x = EditorGUILayout.IntField("X ", (int)tilerRef.gridMap.step.x);
                tilerRef.gridMap.step.y = EditorGUILayout.IntField("Y ", (int)tilerRef.gridMap.step.y);
                tilerRef.gridMap.step.z = EditorGUILayout.IntField("Z ", (int)tilerRef.gridMap.step.z);
                EditorGUILayout.EndHorizontal();

                EditorGUIUtility.labelWidth = oldValue;

            
            }
            else
            {
                tilerRef.GridDistance = EditorGUILayout.Slider(
                    new GUIContent("Grid Normal Distance", "Set Current Grid Normal Axis offset"),
                                        tilerRef.GridDistance, 0,
                                        tilerRef.gridMap.GetGridMaxOffsetCurrent());


                ShowCurrentPlaneControl();


                tilerRef.gridMap.count =
                    EditorGUILayout.Vector3Field(
                        new GUIContent("Grid Map Size ", "Volumetric Size of GridMap"),
                            tilerRef.gridMap.count);

                tilerRef.gridMap.step =
                    EditorGUILayout.Vector3Field(
                        new GUIContent("Grid Map Step ", "Each subdivision size (default:1)"),
                            tilerRef.gridMap.step);

            }


            EditorGUI.indentLevel++;

            planesDataFolding = EditorGUILayout.Foldout(planesDataFolding, "Grid Planes Data");
            if (planesDataFolding)
                ShowPlanesControls();

            EditorGUI.indentLevel--;
        }

        void ShowCurrentPlaneControl()
        {
            tilerRef.gridMap.currentOrientation = (GridMap.GridOrientation)
                 EditorGUILayout.EnumPopup("Grid Plane Dimension ", tilerRef.gridMap.currentOrientation);

            if (prevOrientation != tilerRef.gridMap.currentOrientation)
            {
                prevOrientation = tilerRef.gridMap.currentOrientation;
                tilerRef.GridDistance = tilerRef.gridMap.CurrentGridOffset;
                tilerRef.gridMap.RefreshGridPlaneSettings(tilerRef.transform.position);
            }
        }

        void ShowPlanesControls()
        {

            EditorGUILayout.BeginHorizontal();
            EditorGUILayout.PrefixLabel(
                new GUIContent("Grids Colors ", "Horizontal, Vertical & Depth Grid Planes Colors"));

            float oldValue = EditorGUIUtility.labelWidth;
            EditorGUIUtility.labelWidth = 14;

            tilerRef.gridMap.planes[0].color =
                EditorGUILayout.ColorField( tilerRef.gridMap.planes[0].color);
            tilerRef.gridMap.planes[1].color =
                EditorGUILayout.ColorField( tilerRef.gridMap.planes[1].color);
            tilerRef.gridMap.planes[2].color =
                EditorGUILayout.ColorField( tilerRef.gridMap.planes[2].color);
            EditorGUIUtility.labelWidth = oldValue;
            EditorGUILayout.EndHorizontal();




            Vector3 tempOffsets = new Vector3(  tilerRef.gridMap.planes[0].offset,
                                                tilerRef.gridMap.planes[1].offset,
                                                tilerRef.gridMap.planes[2].offset );

            tempOffsets = EditorGUILayout.Vector3Field(
                new GUIContent("Grids Offsets ", "Horizontal, Vertical & Depth Grid Planes Offsets"),
                tempOffsets);

            tilerRef.gridMap.planes[0].offset = tempOffsets.x;
            tilerRef.gridMap.planes[1].offset = tempOffsets.y;
            tilerRef.gridMap.planes[2].offset = tempOffsets.z;

        }

        private void OnSceneGUI()
        {
        }
    }

}
