﻿using UnityEngine;
using System.Collections.Generic;
using System.Collections;
using System.Linq;

namespace Sun
{
    /// <summary>
    /// Simple MultiPool System 
    /// </summary>

    [System.Serializable, AddComponentMenu("Sun/Pool")]
    public class Pool : MonoBehaviour
    {
        // All the currently active pools in the scene
        public static List<Pool> AllPools = new List<Pool>();

        // The reference between a spawned GameObject and its pool
        public static Dictionary<GameObject, Pool> AllLinks = new Dictionary<GameObject, Pool>();

        [Tooltip("The prefab the clones will be based on")]
        public GameObject Prefab;

        [Tooltip("Should this pool preload some clones?")]
        public int Preload;

        [Tooltip("Should this pool have a maximum amount of spawnable clones?")]
        public int Capacity;
                                           
        [SerializeField]                        // All the currently cached prefab instances
        public Stack<GameObject> cache = new Stack<GameObject>();

        //[System.Serializable()]
        //public class Delays : Collections.DictionarySerialBase<GameObject, float> { }

        [SerializeField()]
        public Delays delayedDestructions = new Delays();

        [ShowOnly, SerializeField]
        private int total;                              // The total amount of created prefabs

        [ShowOnly, SerializeField]
        private int cached = 0;                             // The total amount of created prefabs

        private bool isDelaying = false;
        //////////////////////////////////////////////////////////////////////////////////////////////

        IEnumerator UpdateDelays()              // Process all going to be destroyed Objects
        {
            isDelaying = true;
            while (delayedDestructions.Any())                                                // start knock out time counter
            {
                foreach (GameObject markedObj in delayedDestructions.Keys.ToList())
                {
                    if (markedObj != null)                          // Is it still valid?
                    {
                        delayedDestructions[markedObj] -= Time.deltaTime;   // var delay -= Time.deltaTime;

                        if (delayedDestructions[markedObj] <= 0.0f) // Dead?
                        {
                            FastDespawn(markedObj);                 // Despawn it              < * * * * * *
                            delayedDestructions.Remove(markedObj);  //RemoveDelayedDestruction(i);   * * * *
                        }
                    }
                    else
                        delayedDestructions.Remove(markedObj);      //RemoveDelayedDestruction(i);
                }
                yield return 0;
            }

            isDelaying = false;
            yield break;
        }

        public delegate void OnSpawnDelegate(GameObject clone); // These are a Couple of delegates to Inits clones
        public static event OnSpawnDelegate CallSpawn;

        public delegate void OnDespawnDelegate(GameObject clone);// Usage: Sun.Pool.OnSpawnDelegate += OnInit;
        public static event OnDespawnDelegate CallDespawn;

        public static Pool Get(string instanceName)             //public Pool GetPool(GameObject instance)
        {
            //Ok, Isn't faster than checking against some Prefab's Id but simpler & the name's meanfully
            return AllPools.FirstOrDefault(p => instanceName.Contains(p.Prefab.name) == true);
        }

        public static Pool Get(GameObject clone)                //public Pool GetPool(GameObject instance)
        {
            var pool = default(Pool);
            if (AllLinks.TryGetValue(clone, out pool) != true)
                Debug.LogError("Attempting to find pool for " + clone.name + " but failed! Make sure it still exists!");
            return pool;
        }

        public static Pool Create(GameObject prefab, int preload = 0, int capacity = 0, bool permanent = false)
        {
            var pool = AllPools.Find(p => p.Prefab == prefab);// Find a pool that handles this prefab
            if (pool == null)                                 // Create a new pool for this prefab?
            {
                pool = new GameObject(prefab.name + " Pool").AddComponent<Pool>();
                pool.Prefab = prefab;
                pool.Preload = preload;
                pool.Capacity = capacity;
                pool.UpdatePreload();
                if (permanent)
                    DontDestroyOnLoad(pool);
            }
            return pool;
        }

        public static void Destroy(Pool p = null)               // Clear & Destroy  one or ALL Pools + Instances
        {
            if (p != null && AllPools.Contains(p))              // Destroy only one Selected pool (and childs)...
            {
                foreach (var obj in (p.delayedDestructions.Keys).ToList())
                    GameObject.Destroy(obj);

                p.delayedDestructions.Clear();

                foreach (var item in AllLinks.Where(kvp => kvp.Value == p).ToList())
                {
                    GameObject.Destroy(item.Key);
                    AllLinks.Remove(item.Key);
                }

                AllPools.Remove(p);
                GameObject.Destroy(p.gameObject);
                return;
            }

            for (int i = AllPools.Count - 1; i >= 0; --i)       // or else Destroy ALL by Default
            {
                foreach (var obj in (AllPools[i].delayedDestructions.Keys).ToList())
                    GameObject.Destroy(obj);

                AllPools[i].delayedDestructions.Clear();
                GameObject.Destroy(AllPools[i].gameObject);
            }
            foreach (var key in AllLinks.Keys)
            {
                //AllLinks[key].FastDespawn(key);
                GameObject.Destroy(key);
            }
            AllPools.Clear();
            AllLinks.Clear();
        }

        public static void Reset(float delay = 0.0f)            // Move all to the Cache
        {
            foreach (var key in AllLinks.Keys)
            {
                AllLinks[key].FastDespawn(key, delay);
            }
            AllLinks.Clear();                                   // Remove the association
        }

        public static void Reset(GameObject singleObj, float delay = 0.0f)  // Move one single obj to Cache
        {
            AllLinks[singleObj].FastDespawn(singleObj, delay);
            AllLinks.Remove(singleObj);                               // Remove the association
        }

        //public static GameObject Spawn(GameObject prefab, Vector3 position, Quaternion rotation, Transform parent = null)
        public static GameObject Spawn(GameObject prefab, Vector3 position = new Vector3(), 
                                                    Quaternion rotation = new Quaternion(), Transform parent = null)
        {
            if (prefab != null)
            {
                var pool = AllPools.Find(p => p.Prefab == prefab);// Find a pool that handles this prefab
                if (pool == null)                                 // Create a new pool for this prefab?
                {
                    pool = new GameObject(prefab.name + " Pool").AddComponent<Pool>();
                    pool.Prefab = prefab;
                }

                var clone = pool.FastSpawn(position, rotation, parent); // Spawn a clone from this pool
                if (clone != null)// Was a clone created? (will be null if pool's total has been reached)
                {
                    AllLinks.Add(clone, pool);                  // Associate this clone with this pool
                    return clone.gameObject;                    // Return the clone
                }
            }
            else
            {
                Debug.LogError("Attempting to spawn a null prefab");
            }

            return null;
        }

        // These methods allows you to spawn prefabs via Component with varying levels of transform data
        public static T Spawn<T>(T prefab, Vector3 position, Quaternion rotation, Transform parent = null) where T : Component
        {
            // Clone this prefabs's GameObject
            var gameObject = prefab != null ? prefab.gameObject : null;
            var clone = Spawn(gameObject, position, rotation, parent);

            // Return the same component from the clone
            return clone != null ? clone.GetComponent<T>() : null;
        }

        // This allows you to despawn a clone via Component, with optional delay
        public static void Despawn(Component clone, float delay = 0.0f)
        {
            if (clone != null) Despawn(clone.gameObject);
        }

        public static void Despawn(GameObject clone, float delay = 0.0f)
        {
            if (clone != null)
            {
                var pool = default(Pool);

                // Try and find the pool associated with this clone
                if (AllLinks.TryGetValue(clone, out pool) == true)
                {
                    AllLinks.Remove(clone);                                 // Remove the association
                    pool.FastDespawn(clone, delay);                         // Despawn it
                }
                else
                {
                    Debug.LogError("Attempting to despawn " + clone.name +
                        ", but failed to find his pool! Make sure you created it using SunPool.Spawn!");
                    // Fall back to normal destroying
                    Destroy(clone);
                }
            }
            else
            {
                //Debug.LogError("Attempting to despawn a null clone");
            }
        }


        //////////////////////////////////////////////////////////////////////////////////////////////

        protected virtual void OnEnable()               // Adds pool to list
        {
            AllPools.Add(this);
            //this.transform.SetParent(Sun.Pools.Get.transform, false);
        }

        protected virtual void OnDisable()              // Remove pool from list
        {
            AllPools.Remove(this);
        }

        protected virtual void Awake()                  // Update preloaded count
        {
            UpdatePreload();
        }

        public void UpdatePreload()
        {
            if (Prefab != null)                         // private void UpdatePreload()
            {
                for (var i = total; i < Preload; i++)
                    FastPreload();

                if(cached != cache.Count)
                cached = cache.Count;
            }

        }

        // This allows you to make another clone and add it to the cache
        public void FastPreload()
        {
            if (Prefab != null)
            {
                //var clone = FastClone(Vector3.zero, Quaternion.identity, null); // Create clone
                var clone = FastClone(Vector3.zero, Quaternion.identity, transform); // Create clone
                cache.Push(clone);
                clone.SetActive(false);                                 // Deactivate it     
                //cached = cache.Count;
                //clone.transform.SetParent(transform, false);          // Move it under this GO
            }
        }

        // This will return a clone from the cache, or create a new instance
        public GameObject FastSpawn(Vector3 position, Quaternion rotation, Transform parent = null)
        {
            if (Prefab != null)
            {
                while (cache.Any())                         // Attempt to spawn from the cache
                {
                    var clone = cache.Pop();
                    cached = cache.Count;

                    if (clone != null)
                    {
                        var cloneTransform = clone.transform;   // Update transform of clone
                        cloneTransform.localPosition = position;
                        cloneTransform.localRotation = rotation;
                        cloneTransform.SetParent(parent, false);
                        clone.SetActive(true);                  // Activate clone
                        if (CallSpawn != null)
                            CallSpawn(clone);//SendNotification(clone, "OnSpawn");   // Messages?

                        return clone;
                    }
                    else
                    {
                        Debug.LogError("The " + name + " pool contained a null cache entry");
                    }
                }

                if (Capacity <= 0 || total < Capacity)      // Make a new clone?
                {
                    var clone = FastClone(position, rotation, parent);
                    if (CallSpawn != null)
                        CallSpawn(clone);                            // Messages?
                    return clone;
                }
            }
            else
            {
                Debug.LogError("Attempting to spawn null");
            }

            return null;
        }

        // This will despawn a clone and add it to the cache
        public void FastDespawn(GameObject clone, float delay = 0.0f)
        {
            if (clone != null)
            {
                if (delay > 0.0f)                               // Delay the despawn?       
                {
                    // Make sure we only add it to the marked object list once
                    if (!delayedDestructions.ContainsKey(clone))
                        delayedDestructions.Add(clone, delay);                          /// * * * * *

                    if (!isDelaying)
                        StartCoroutine(UpdateDelays());
                }
                else                                            // Despawn now?
                {
                    cache.Push(clone);                          // Add it to the cache  < * * * * * *
                    cached = cache.Count;
                    if (CallDespawn != null)
                        CallDespawn(clone);    //SendNotification(clone, "OnDespawn");     // Messages?
                    Sun.Events.Get.Raise(new GameEvent());

                    clone.SetActive(false);                     // Deactivate it
                    clone.transform.SetParent(transform, false);// Move it under this GO
                }
            }
            else
            {
                Debug.LogWarning("Attempting to despawn a null clone");
            }
        }

        // Returns a clone of the prefab and increments the total (Prefab is assumed to exist!)
        private GameObject FastClone(Vector3 position, Quaternion rotation, Transform parent)
        {
            var clone = (GameObject)Instantiate(Prefab, position, rotation);
            total += 1;
            clone.name = Prefab.name + " " + total;
            clone.transform.SetParent(parent, false);
            return clone;
        }

#if UNITY_EDITOR
        [UnityEditor.MenuItem("GameObject/Sun/Pool", false, 1)]
        public static void CreatePool()
        {
            var gameObject = UnityEditor.Selection.activeGameObject;
            if (gameObject == null)
            {
                gameObject = new GameObject(typeof(Sun.Pool).Name);
                UnityEditor.Selection.activeGameObject = gameObject;
            }
            UnityEditor.Undo.RegisterCreatedObjectUndo(gameObject, "Create Pool");
            gameObject.AddComponent<Sun.Pool>();
        }
#endif
    }



    //////////////////////////////////////////////////////////////////////////////////////////////

    // This class allows you to pool normal C# classes, for example:
    // var foo = Sun.ClassPool<Foo>.Spawn() ?? new Foo();
    // Sun.ClassPool<Foo>.Despawn(foo);
    public static class ClassPool<T> where T : class
    {
        private static List<T> cache = new List<T>();

        public static T Spawn()
        {
            return Spawn(null, null);
        }

        public static T Spawn(System.Action<T> onSpawn)
        {
            return Spawn(null, onSpawn);
        }

        public static T Spawn(System.Predicate<T> match)
        {
            return Spawn(match, null);
        }

        // This will either return a pooled class instance, or null
        // You can also specify a match for the exact class instance you're looking for
        // You can also specify an action to run on the class instance (e.g. if you need to reset it)
        // NOTE: Because it can return null, you should use it like this: Sun.ClassPool<Whatever>.Spawn(...) ?? new Whatever(...)
        public static T Spawn(System.Predicate<T> match, System.Action<T> onSpawn)
        {
            // Get the matched index, or the last index
            var index = match != null ? cache.FindIndex(match) : cache.Count - 1;

            // Was one found?
            if (index >= 0)
            {
                // Get instance and remove it from cache
                var instance = cache[index];

                cache.RemoveAt(index);

                // Run action?
                if (onSpawn != null)
                {
                    onSpawn(instance);
                }

                return instance;
            }

            // Return null?
            return null;
        }

        public static void Despawn(T instance)
        {
            Despawn(instance, null);
        }

        // This allows you to desapwn a class instance
        // You can also specify an action to run on the class instance (e.g. if you need to reset it)
        public static void Despawn(T instance, System.Action<T> onDespawn)
        {
            // Does it exist?
            if (instance != null)
            {
                // Run action on it?
                if (onDespawn != null)
                {
                    onDespawn(instance);
                }

                // Add to cache
                cache.Add(instance);
            }
        }
    }
}