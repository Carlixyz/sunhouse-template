// ------------------------------------------------------------------------------
// A Simple Prefab Resource PreConfig/Loader For Tiler
// ------------------------------------------------------------------------------

using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Xml;
using System.IO;

namespace Sun.Tools
{
    /* OBJECTMANAGER - RULES OF CONSIDERATIONS:
     * 
     * - Load Files From Assets Datafolder as: "/SomeFolder/SomeFile.SomeExtension" ( Don't use '�' letter)
     *   (Always before every Build Add again the "Levels" Folder with maps inside the 'ProjectName/ProjectName_Data/'Folder)
     * 
     * - If you add new Prefabs Remember to Update This with The 'GenerateData' & Then Preload Cache
     * 
     * - Use Tiler Editor and U can rename file extensions from '.TMX' to '.XML' (And save in Gzip + Base64 Compression)
     * 
     * - WIP
     *  
     * */

    [DisallowMultipleComponent, System.Serializable]                  //, AddComponentMenu("Sun/AudioManager")]
    //[ExecuteInEditMode]
    //[PrefabAttribute("Prefabs/PrefabLoader")]
    public class PrefabLoader : Singleton<PrefabLoader>
    {

        //public List<GameObject> ObjList = new List<GameObject>();

        //public int ObjTotal = 100;

        public List<GameObject> ObjGroups = new List<GameObject>();

        public int height = 100, tilewidth = 32, tileheight = 32;
        //public GameObject ObjGroup;
        //public Transform GrpTransform;
        public bool ObjectsChanged = false;

        public GameObject Player;
        public Transform PlayerTransform;

        public void OnEnable()
        {
            LoadAllPrefabs();
        }

        public void OnDisable()
        {

        }

        public void ClearAll()
        {
            //if (ObjTotal <= 0)
            //    return;

            //ObjTotal = 0;
            //ObjList.Clear();
            ObjGroups.Clear();
            ObjectsChanged = true;

            if (PlayerTransform != null)
            {
                if (Player)
                    Destroy(Player);
                PlayerTransform = null;
                Player = null;
            }
        }

        public void BuildPrefabGroup(GridMap map = null)
        {
            height = map.fileHeight;
            tilewidth = map.tileWidth;
            tileheight = map.tileHeight;

            GameObject ObjGroup = new GameObject("new Group " + ((int)ObjGroups.Count));
            ObjGroups.Add(ObjGroup);

            Transform GrpTransform = ObjGroup.transform;
            GrpTransform.parent = map.transform;                                   // Set Map Transform Here 


            ObjectsChanged = true;
        }


        public IEnumerator BuildPrefabGroupXML(XmlNode ObjectsGroup, Transform map = null)
        {
            height = int.Parse(ObjectsGroup.ParentNode.Attributes["height"].Value);
            tilewidth = int.Parse(ObjectsGroup.ParentNode.Attributes["tilewidth"].Value);
            tileheight = int.Parse(ObjectsGroup.ParentNode.Attributes["tileheight"].Value);

            GameObject ObjGroup = new GameObject(ObjectsGroup.Attributes["name"].Value);
            ObjGroups.Add(ObjGroup);

            Transform GrpTransform = ObjGroup.transform;
            GrpTransform.parent = map;                                   // Set Map Transform Here 


            //Debug.Log("Group " + GrpTransform.name);

            foreach (XmlNode ObjInfo in ObjectsGroup.ChildNodes)
            {
                string ObjName = "";

                if (ObjInfo.Attributes["type"] != null)
                    ObjName = ObjInfo.Attributes["type"].Value.ToLower();   // Check type match
                else if (ObjInfo.Attributes["name"] != null)
                    ObjName = ObjInfo.Attributes["name"].Value.ToLower();   // else take it's name as type
                else
                    continue;                                               // else discard object

                ObjName = ObjName.Remove(0, ObjName.LastIndexOf("/") + 1);

                string ObjNameUpcast = ObjName.ToUpper().Replace(" ", "_");

                if (System.Enum.IsDefined(typeof(ResourceLoad.Prefabs), ObjNameUpcast))
                //ObjName.ToUpper().Replace(" ", "_")) == true)
                {
                    ResourceLoad.Prefabs objEnum = (ResourceLoad.Prefabs)System.Enum.Parse(
                                typeof(ResourceLoad.Prefabs), ObjNameUpcast);

                    BuildPrefabXML(objEnum, ObjInfo, GrpTransform);
                    //Debug.Log(" Building ObjName " + objEnum + " in group " + GrpTransform);
                }
                else
                {
                    Debug.LogWarning("Object >" + ObjNameUpcast + "< not found at: Resources/");
                    continue;
                }


                yield return 0;
            }

            ObjectsChanged = true;
        }

        public IEnumerator BuildPrefabs(XmlNode ObjectsGroup, Transform parent = null)
        {
            foreach (XmlNode ObjInfo in ObjectsGroup.ChildNodes)
            {
                string ObjName = "";

                if (ObjInfo.Attributes["type"] != null)
                    ObjName = ObjInfo.Attributes["type"].Value.ToLower();   // Check type match
                else if (ObjInfo.Attributes["name"] != null)
                    ObjName = ObjInfo.Attributes["name"].Value.ToLower();   // else take it's name as type
                else
                    continue;                                               // else discard object

                ObjName = ObjName.Remove(0, ObjName.LastIndexOf("/") + 1);


                string ObjNameUpcast = ObjName.ToUpper().Replace(" ", "_");

                if (System.Enum.IsDefined(typeof(ResourceLoad.Prefabs), ObjNameUpcast))
                //ObjName.ToUpper().Replace(" ", "_")) == true)
                {
                    ResourceLoad.Prefabs objEnum = (ResourceLoad.Prefabs)System.Enum.Parse(
                                typeof(ResourceLoad.Prefabs), ObjNameUpcast);

                    BuildPrefabXML(objEnum, ObjInfo, parent);
                    Debug.Log(" Building ObjName " + objEnum + " in transform " + parent);
                }
                else
                {
                    Debug.LogWarning("Object >" + ObjNameUpcast + "< not found at: Resources/");
                }


                yield return 0;
            }

            ObjectsChanged = true;
        }

        void BuildPrefabXML(ResourceLoad.Prefabs objId, XmlNode objInfo, Transform parent = null)
        {

            Transform ObjTransform = ((GameObject)BuildPrefab(objId)).transform;

            ObjTransform.position = new Vector3(
                (float.Parse(objInfo.Attributes["x"].Value) / tilewidth)
                + (ObjTransform.localScale.x * .5f),                    // X
                height - (float.Parse(objInfo.Attributes["y"].Value) /
                tileheight - ObjTransform.localScale.y * .5f),          // Y		 		     
                    0f /*Managers.Tiled.MapTransform.position.z*/ );    // Z

            if (objInfo.Attributes["gid"] == null)  //  If not a gid it's a Trigger Volume (Great)
            {
                //ObjTransform.localScale =
                ObjTransform.localScale =
                    new Vector3(float.Parse(objInfo.Attributes["width"].Value) / tilewidth,
                                float.Parse(objInfo.Attributes["height"].Value) / tileheight, 1);

                ObjTransform.position += new Vector3((ObjTransform.localScale.x * .5f) - .5f,
                                                     -(ObjTransform.localScale.y * .5f + .5f),
                                                     0.0f /*Managers.Tiled.MapTransform.position.z*/);                // Model your own space!
            }

            //ObjTransform.name = objId.ToString().ToLower();
            ObjTransform.name = ResourceLoad.PrefabNames[(int)objId];

            ObjTransform.parent = parent;

            PrefabConfig(ObjTransform, objInfo);

            //ObjectsChanged = true;
        }

        void PrefabConfig(Transform objTransform, XmlNode objConfig)
        {
            //switch (objName.ToLower())
            switch (objTransform.name.ToLower())
            {
                case "pombero":
                    {
                        //Managers.Objects.Player = ObjTransform.gameObject;
                        //PlayerTransform = objTransform;
                        //Managers.Display.cameraScroll.SetTarget(
                        //    PlayerTransformManagers.Objects.PlayerTransform, false);
                        //Managers.Register.SetPlayerPos();

                        //foreach (XmlNode ObjProp in ((XmlElement)ObjInfo).GetElementsByTagName("property"))
                        //{
                        //    if (ObjProp.Attributes["name"].Value.ToLower() == "zoom")
                        //        ((CameraTargetAttributes)ObjTransform.GetComponent<CameraTargetAttributes>()).distanceModifier =
                        //            float.Parse(ObjProp.Attributes["value"].Value.ToLower());

                        //    if (ObjProp.Attributes["name"].Value.ToLower() == "offset")
                        //        ((CameraTargetAttributes)ObjTransform.GetComponent<CameraTargetAttributes>()).Offset =
                        //            ReadVector(ObjProp.Attributes["value"].Value.ToLower(), 0);
                        //}
                    }
                    break;
                case "door":
                    goto case "warp";
                case "warp":
                    {
                        //    Portal portal = (Portal)ObjTransform.GetComponent<Portal>();
                        //    portal.SetType((Portal.type)Enum.Parse(typeof(Portal.type), ObjName));

                        //    if (((XmlElement)ObjInfo).GetElementsByTagName("property").Item(0) != null)
                        //        portal.SetTarget(((XmlElement)ObjInfo).GetElementsByTagName("property").Item(0).Attributes["value"].Value);

                        //    portal.SetId((ObjInfo.Attributes["name"] != null ? ObjInfo.Attributes["name"].Value : ObjName));
                    }
                    break;
                case "camerabound":
                    {
                        //foreach (XmlNode ObjProp in ((XmlElement)ObjInfo).GetElementsByTagName("property"))
                        //{
                        //    if (ObjProp.Attributes["name"].Value.ToLower() == "zoom")
                        //        ((CameraBounds)ObjTransform.GetComponent<CameraBounds>()).ZoomFactor = float.Parse(ObjProp.Attributes["value"].Value);

                        //    if (ObjProp.Attributes["name"].Value.ToLower() == "offset")
                        //        ((CameraBounds)ObjTransform.GetComponent<CameraBounds>()).Offset = ReadVector(ObjProp.Attributes["value"].Value, 0);
                        //}
                    }
                    break;

                case "zonetag":
                    {
                        //if (ObjInfo.Attributes["name"] != null)
                        //    ObjTransform.name = ObjInfo.Attributes["name"].Value;
                    }
                    break;
                default:
                    foreach (XmlNode objProp in ((XmlElement)objConfig).GetElementsByTagName("property"))
                    {
                        if (objProp.Attributes["name"].Value.ToLower() == "depth")
                            objTransform.position += Vector3.forward *
                                float.Parse(objProp.Attributes["value"].Value);

                        if (objProp.Attributes["name"].Value.ToLower() == "rotation")
                            objTransform.localRotation = Quaternion.Euler(new Vector3(0, 0, float.Parse(objProp.Attributes["value"].Value)));

                        if (objProp.Attributes["name"].Value.ToLower() == "scale")
                        {
                            objTransform.localScale = ReadVector(objProp.Attributes["value"].Value);
                            objTransform.localScale += Vector3.forward;
                        }
                    }
                    break;
            }
        }


 

        public GameObject BuildPrefab( ResourceLoad.Prefabs prefabId, 
                                        Vector3 position = new Vector3(), 
                                        Quaternion rotation = new Quaternion(), 
                                        Transform parent = null)
        {
            return Instantiate(GetPrefab((int)prefabId), position, rotation, parent);
        }

     


        ///----------------------------------------------------------------------------------///

        //public string basePath = "Prefabs/";

        public List<GameObject> PrefabCache = new List<GameObject>();

        public  GameObject GetPrefab(int prefabId)
        {
            GameObject tmpPrefab = null;

            if (TryGetValue(prefabId, out tmpPrefab))
                return tmpPrefab;
            else
                return LoadPrefab(prefabId);
        }

        public bool TryGetValue(int id, out GameObject value)
        {
            if (Mathf.Clamp(id, 0, (PrefabCache.Count - 1)) == id)
            {
                //Debug.Log("return id " + id + " prefab ");
                value = PrefabCache[id];
                return true;
            }

            //Debug.Log("Loading default " + id + " prefab ");
            value = default(GameObject);
            return false;
        }

        public GameObject LoadPrefab(int prefabId)
        {
            if (prefabId >= (int)ResourceLoad.Prefabs.COUNT)
                return null;

            if (prefabId >= 0 && prefabId < PrefabCache.Count && PrefabCache[prefabId] != null)
                if (PrefabCache[prefabId] != null)
                    return PrefabCache[prefabId];                       // Enum.IsDefined

            GameObject tmpPrefab = null;
            tmpPrefab = (GameObject)Resources.Load(/*basePath +*/ ResourceLoad.Paths[prefabId]);

            if (tmpPrefab != null)
                PrefabCache.Add(tmpPrefab);
            else
                Debug.LogWarning("Serious Error while Resource Loading " + prefabId + "not loaded");

            return tmpPrefab;
        }

        public void LoadAllPrefabs()
        {
            if (PrefabCache != null && PrefabCache.Count != 0)
                PrefabCache.Clear();

            Debug.Log("Loading All Prefabs");
            for (int i = 0; i < (int)ResourceLoad.Prefabs.COUNT; i++)
                LoadPrefab(i);
        }

        private Vector3 ReadVector(string input)
        {                                                                                   // seek float values inside string
            if (input.Contains(","))                                                        // if there's a comma, separate things
            {
                //return new Vector3(float.Parse(input.Remove(input.IndexOf(","))),
                //                   float.Parse(input.Remove(0, input.IndexOf(",") + 1)));

                return new Vector3(float.Parse(input.Substring(0, input.IndexOf(","))),
                        float.Parse(input.Substring(input.IndexOf(",") + 1, input.LastIndexOf(","))),
                        float.Parse(input.Substring(input.LastIndexOf(",") + 1)));
            }

            // else set just the X Axis 
            return new Vector3(float.Parse(input), 0, 0);            // or both if 'AxisY' is enabled    
        }

        ///----------------------------------------------------------------------------------///

        List<string> textLines = new List<string>();
        List<string> defineNames = new List<string>();
        List<string> fileNames = new List<string>();
        List<string> prefabNames = new List<string>();
        List<string> extensionNames = new List<string>();

        void UpdateResourceLoaded()
        {
            if (PrefabCache == null)
                PrefabCache = new List<GameObject>();

            if (PrefabCache.Count != (int)ResourceLoad.Prefabs.COUNT)
            {
                PrefabCache.Clear();

                LoadAllPrefabs();
            }
        }


        void ScanFolderData(DirectoryInfo info, string path = "")
        {
            System.IO.FileInfo[] fileInfo = info.GetFiles();
            int i = 0;
            foreach (System.IO.FileInfo file in fileInfo)
            {
                if (file.Extension == ".prefab")
                {
                    string def = file.Name.Replace(file.Extension, "").ToUpper();
                    def = def.Replace("(", "").Replace(")", "");  // Trim "(",")" and spaces

                    //defineNames.Add(path.Replace("/", "_").ToUpper() + def.Replace(" ", "_"));
                    defineNames.Add(def.Replace(" ", "_"));

                    fileNames.Add(path + file.Name.Replace(file.Extension, ""));
                    prefabNames.Add(file.Name.Replace(file.Extension, ""));
                    extensionNames.Add(file.Extension);
                    //Debug.Log("Asset " + file.FullName + " extension " + file.Extension);
                    i++;
                }
            }


            foreach (var folder in info.GetDirectories())
            {
                if (folder != info)
                {
                    //Debug.Log("folder " + folder.FullName + " folderPath " + folderPath);
                    string folderPath = folder.FullName.Replace("\\", "/");

                    folderPath = folderPath.Substring(folderPath.IndexOf("Resources") + 10);
                    //folderPath = folderPath.Substring(folderPath.IndexOf(basePath) + basePath.Length);

                    Debug.Log("folder " + folder.FullName + " folderPath " + folderPath);

                    ScanFolderData(folder, folderPath + "/");
                }
            }
        }

        public void GenerateData()
        {
            defineNames.Clear();
            fileNames.Clear();
            prefabNames.Clear();
            textLines.Clear();
            extensionNames.Clear();

            DirectoryInfo info = new DirectoryInfo("Assets/Application/Resources/"/* + basePath*/);
            ScanFolderData(info);

            WriteHeader();
            WriteDefines();
            WritePaths();

            WriteFooter();

            System.IO.File.WriteAllLines(
            Application.dataPath + "/Sunhouse/Managers/ResourceLoad.cs", textLines.ToArray());

#if UNITY_EDITOR
            UnityEditor.AssetDatabase.Refresh();
#endif

        }

        void WriteHeader()
        {
            textLines.Add("using UnityEngine;");
            textLines.Add("public class ResourceLoad");
            textLines.Add("{");

        }

        void WriteDefines()
        {
            textLines.Add("	public enum Prefabs");
            textLines.Add("	{");


            for (int i = 0; i < defineNames.Count; i++)
            {
                textLines.Add("		" + defineNames[i] + "=" + i.ToString() + ",");

            }
            textLines.Add("		COUNT=" + defineNames.Count.ToString());
            textLines.Add("	}");
        }

        void WritePaths()
        {
            textLines.Add("	public static readonly string[] Paths = ");
            textLines.Add("	{");

            for (int i = 0; i < defineNames.Count; i++)
            {
                textLines.Add("		\"" + fileNames[i] + "\",");
            }
            textLines.Add("	};");


            textLines.Add("	public static readonly string[] Extensions = ");
            textLines.Add("	{");

            for (int i = 0; i < defineNames.Count; i++)
            {
                textLines.Add("		\"" + extensionNames[i] + "\",");
            }
            textLines.Add("	};");


            textLines.Add("	public static readonly string[] PrefabNames = ");
            textLines.Add("	{");

            for (int i = 0; i < defineNames.Count; i++)
            {
                textLines.Add("		\"" + prefabNames[i] + "\",");
            }
            textLines.Add("	};");
        }

        void WriteFooter()
        {
            textLines.Add("}");
        }

        ///----------------------------------------------------------------------------------///


        //void SetGroupTransform(Transform grpTransform)
        //{
        //    isTransforming = grpTransform;
        //}
    }

}


/*
using UnityEngine;
public class ResourceLoad
{
    public enum Prefabs  { COUNT };
    public static readonly string[] Paths;
    public static readonly string[] Extensions;
}
*/

