
using UnityEngine;
using UnityEditor;
using System.Collections;
using System.Collections.Generic;

namespace Sun.Tools
{
    [CustomEditor(typeof(PrefabLoader))]
    public class PrefabLoaderEditor : Editor
    {

        //SerializedObject lbserialized = null;
        PrefabLoader prefabloadRef = null;

        void Initialize()
        {
            if (prefabloadRef == null)
                prefabloadRef = (PrefabLoader)target;
        }

        public void OnEnable()
        {
            Initialize();
        }

        public override void OnInspectorGUI()
        {
            GUI.enabled = false;
            DrawDefaultInspector();
            GUI.enabled = true;

            GUILayout.BeginHorizontal();
            //if (GUILayout.Button("Update Now"))
            //{
            //    prefabloadRef.UpdateDB(true);
            //}
            //prefabloadRef.UpdateAutomatically = GUILayout.Toggle(m_Target.UpdateAutomatically, "AutoUpdate", "Button");
            //if (GUI.changed)
            //{
            //    EditorUtility.SetDirty(prefabloadRef);
            //    AssetDatabase.SaveAssets();
            //}

            if (GUILayout.Button("Generate Data Enums"))
                prefabloadRef.GenerateData();


            if (GUILayout.Button("PreLoad Resources"))
                prefabloadRef.LoadAllPrefabs();

            if (GUILayout.Button("Clear Cache"))
                prefabloadRef.PrefabCache.Clear();

            GUILayout.EndHorizontal();
            //EditorGUILayout.LabelField("Folders:", prefabloadRef.FolderCount.ToString());
            //EditorGUILayout.LabelField("Files:", prefabloadRef.FileCount.ToString());



        }

      
    }
}