using UnityEngine;
public class AudioLoad
{
	public enum Clips
	{
		EXPLOSION=0,
		FX_LAZER=1,
		LAS_RANAS_CAMPO_DEL_CIELO=2,
		LASER_1=3,
		MUSIC1=4,
		MUSIC2=5,
		MUSIC3=6,
		MUSIC4=7,
		MUSIC5=8,
		NOTIFICATION=9,
		OPENING=10,
		PAMPERO=11,
		PAZ_Y_ALEGRIA=12,
		COUNT=13
	}
	public static readonly string[] Paths = 
	{
		"Explosion",
		"fx_Lazer",
		"LAS RANAS (campo del cielo)",
		"Laser 1",
		"Music1",
		"Music2",
		"Music3",
		"Music4",
		"Music5",
		"Notification",
		"Opening",
		"Pampero",
		"PAZ Y ALEGRIA",
	};
	public static readonly string[] Extensions = 
	{
		".wav",
		".wav",
		".ogg",
		".wav",
		".ogg",
		".ogg",
		".ogg",
		".ogg",
		".ogg",
		".wav",
		".ogg",
		".ogg",
		".ogg",
	};
}
