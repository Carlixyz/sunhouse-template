﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEditor;
using UnityEngine;
//using System.Linq;

namespace Sun.Sound
{
    //[CustomEditor(typeof(LocalizedBehaviour), true)]
    [CustomEditor(typeof(Audio))]
    public class AudioEditor : Editor
    {
        string CurrentMusicName = "SoundTrack Name";    // Current Music Source Index playing now

        public Audio AudioManager = null;
        private SerializedObject lbserialized = null;
        private static bool LoadedClipsFoldout = false;

        List<string> textLines = new List<string>();
        List<string> defineNames = new List<string>();
        List<string> fileNames = new List<string>();
        List<string> extensionNames = new List<string>();

        public override void OnInspectorGUI()
        {
            //base.DrawDefaultInspector();

            Initialize();

            lbserialized.Update();

            AudioManager.Persistent = EditorGUILayout.Toggle("Persistent ", AudioManager.Persistent);

            DrawMusicControls();

            //EditorGUILayout.LabelField("", GUI.skin.FindStyle("RL DragHandle") /*GUI.skin.horizontalSlider*/);
            //EditorGUILayout.LabelField("", GUI.skin.FindStyle("ShurikenLine") /*GUI.skin.horizontalSlider*/);
            EditorGUILayout.LabelField("", GUI.skin.horizontalSlider);

            DrawSoundControls();

            EditorGUILayout.LabelField("", GUI.skin.horizontalSlider);

            DrawAudioLoaded();

            EditorGUILayout.Separator();
            EditorGUILayout.Separator();

            //EditorGUILayout.LabelField("", GUI.skin.horizontalSlider);

            lbserialized.ApplyModifiedProperties();
            //EditorUtility.SetDirty(target);
        }

        void DrawMusicControls()
        {
            //Handles.BeginGUI();
            //Handles.color = Color.red;
            //Handles.DrawLine(new Vector3(0, 0), new Vector3(300, 300));
            //Handles.EndGUI();

            //GUILayout.BeginVertical("GroupBox"/*EditorStyles.helpBox*/);   //  OPEN A
            EditorGUILayout.Separator();

            // ------------------------------------------------------------------- Enable Music + Volumen

            GUILayout.BeginHorizontal();

                EditorGUILayout.PropertyField(
                    lbserialized.FindProperty("_musicEnable"), GUIContent.none, GUILayout.MaxWidth(16));

                EditorGUILayout.PrefixLabel("Music Enable"/*, GUILayout.MaxWidth(128) */);

                EditorGUILayout.PropertyField( 
                    lbserialized.FindProperty("_musicVolume"), GUIContent.none );


            GUILayout.EndHorizontal();

            // ------------------------------------------------------------------- Current track name

            GUILayout.BeginHorizontal();

                int musicIndex = lbserialized.FindProperty("_current").intValue;

                EditorGUILayout.PrefixLabel("Current Track index " + musicIndex);

                if (AudioManager.MusicSources[musicIndex] == null )
                {
                    GUILayout.EndHorizontal();
                //GUILayout.EndVertical();                                   //  CLOSE A 1
                return;
                }

                GUILayout.FlexibleSpace();

                if (AudioManager.MusicSources[musicIndex].clip != null)
                    CurrentMusicName = AudioManager.MusicSources[musicIndex].clip.name;

                EditorGUILayout.LabelField(CurrentMusicName, EditorStyles.boldLabel );

            GUILayout.EndHorizontal();

            // ------------------------------------------------------------------- The MusicBuffer Duo

            EditorGUI.indentLevel += 1;

            EditorGUILayout.PropertyField(lbserialized.FindProperty("MusicSources"), true);

            EditorGUI.indentLevel -= 1;

            //GUILayout.EndVertical();                                  //  CLOSE A 2
        }

        void DrawSoundControls()
        {


            // ------------------------------------------------------------------- Enable + Volumen

            GUILayout.BeginHorizontal();

                EditorGUILayout.PropertyField(
                    lbserialized.FindProperty("_soundEnable"), GUIContent.none, GUILayout.MaxWidth(16));

                EditorGUILayout.PrefixLabel("Sounds Enable"/*, GUILayout.MaxWidth(128)*/);

                EditorGUILayout.PropertyField( lbserialized.FindProperty("_soundVolume"), GUIContent.none);

            GUILayout.EndHorizontal();

            // ------------------------------------------------------------------- SoundFXSources[SrcIndex]

            GUILayout.BeginHorizontal();

                int sfxIndex = lbserialized.FindProperty("_srcIndex").intValue;

                EditorGUILayout.PrefixLabel("Current SoundFX index " + sfxIndex);

                if (AudioManager.SoundFXSources[sfxIndex] == null)
                {
                    GUILayout.EndHorizontal();
                    return;
                }

                GUILayout.FlexibleSpace();

                CurrentMusicName = AudioManager.SoundFXSources[sfxIndex].ClipName;

                EditorGUILayout.LabelField(CurrentMusicName, EditorStyles.boldLabel);

            GUILayout.EndHorizontal();

            // ------------------------------------------------------------------- Serialized List


            EditorGUI.indentLevel += 1;

            EditorGUILayout.PropertyField( lbserialized.FindProperty("SoundFXSources"), true);

            EditorGUI.indentLevel -= 1;


            // ------------------------------------------------------------------- Busy Sources List

            EditorGUI.indentLevel += 1;

            EditorGUILayout.PropertyField(lbserialized.FindProperty("delayedStops"), true);

            EditorGUI.indentLevel -= 1;

            EditorGUILayout.Separator();

        }

        void DrawAudioLoaded()
        {
            EditorGUI.indentLevel += 1;
            
            GUILayout.BeginHorizontal();
            LoadedClipsFoldout = EditorGUILayout.Foldout(LoadedClipsFoldout, "Preloaded AudioClips", true);

            GUILayout.FlexibleSpace();
            if (GUILayout.Button("Generate Audio PreLoad Data", EditorStyles.miniButton/*, GUILayout.MaxWidth(180)*/))
            {
                GenerateData();
                EditorUtility.SetDirty(AudioManager);
                AssetDatabase.SaveAssets();
            }
            EditorGUILayout.Space();
            GUILayout.EndHorizontal();

            //EditorGUILayout.PropertyField(lbserialized.FindProperty("LoadedSounds"), true);

            if (LoadedClipsFoldout)
            {
                EditorGUILayout.Separator();
                GUILayout.BeginVertical();      // OPEN B                    

                //foreach (KeyValuePair<int, AudioClip> lSound in AudioManager.LoadedSounds)
                for (int i = 0; i < AudioManager.LoadedSounds.Count; i++)
                {

                    GUILayout.BeginHorizontal();
                    EditorGUI.indentLevel += 1;

                    EditorGUILayout.SelectableLabel(
                     ((AudioLoad.Clips)i).ToString(), "PreLabel" , GUILayout.Height(15)); /* "SC ViewLabel" */


                    //    ((AudioLoaded.Sounds)i).ToString(),
                    //    //((AudioLoaded.SoundIDs)lSound.Key).ToString()
                    //      EditorStyles.objectFieldMiniThumb,  "ProfilerSelectedLabel"  );

                    EditorGUILayout.ObjectField(
                    AudioManager.LoadedSounds[i], typeof(AudioClip), true, GUILayout.ExpandWidth(true));
                    //lSound.Value, typeof(AudioClip), true, GUILayout.ExpandWidth(true) );

                    EditorGUILayout.Separator();
                    EditorGUI.indentLevel -= 1;
                    GUILayout.EndHorizontal();
                    EditorGUILayout.Separator();
                }

                GUILayout.EndVertical();        // CLOSE B 

            }
            EditorGUI.indentLevel -= 1;
            EditorGUILayout.Separator();

        }

        public void Initialize()
        {
            if (AudioManager == null)
                AudioManager = (Audio)target;
            //AudioManager = Object.FindObjectOfType<Audio>();

            if (lbserialized == null)
                lbserialized = serializedObject;
                //lbserialized = new SerializedObject(target);

            UpdateAudioLoaded();
        }

        void UpdateAudioLoaded()
        {
            if (AudioManager.LoadedSounds == null)
                AudioManager.LoadedSounds = new List<AudioClip>();

            if (AudioManager.LoadedSounds.Count != (int)AudioLoad.Clips.COUNT)
            {
                AudioManager.LoadedSounds.Clear();

                AudioManager.LoadAllClips();

                //foreach (KeyValuePair<int, AudioClip> lSound in AudioManager.LoadedSounds)
                //    LoadedClips.Add(lSound.Value);
            }
        }

        void ScanFolderData(DirectoryInfo info, string path = "")
        {

            System.IO.FileInfo[] fileInfo = info.GetFiles();
            int i = 0;
            foreach (System.IO.FileInfo file in fileInfo)
            {

                //{
                if (file.Extension == ".wav" || file.Extension == ".mp3" || file.Extension == ".ogg")
                {
                        string def = file.Name.Replace(file.Extension, "").ToUpper();
                        def = def.Replace("(", "").Replace(")", "");  // Trim "(",")" and spaces

                    //defineNames.Add(path.Replace("/","_").ToUpper() + def.Replace(" ", "_"));
                    defineNames.Add(def.Replace(" ", "_"));

                    //fileNames.Add("Audio/" + path  + file.Name.Replace(file.Extension, "") );
                    fileNames.Add( path  + file.Name.Replace(file.Extension, "") );
                    extensionNames.Add(file.Extension);
                    //Debug.Log("Asset " + file.FullName + " extension " + file.Extension);
                    i++;
                }
                //}
            }


            foreach (var folder in info.GetDirectories())
            {
                //Debug.Log("folder " + folder.FullName + " as " + folder.Name);

                if (folder != info )
                {
                    //Debug.Log("folder " + folder.FullName + " folderPath " + folderPath);
                    string folderPath = folder.FullName.Replace("\\", "/");

                    //folderPath = folderPath.Substring(folderPath.IndexOf("Audio") );
                    //folderPath = folderPath.Substring(folderPath.IndexOf("Audio") + 6);
                    folderPath = folderPath.Substring(
                        folderPath.IndexOf(AudioManager.basePath) + AudioManager.basePath.Length );

                    Debug.Log("folder " + folder.FullName + " folderPath " + folderPath);

                    ScanFolderData(folder, folderPath + "/");
                }
            }
        }

        void GenerateData()
        {
            //Object[] loadedAssets = AssetDatabase.LoadAllAssetsAtPath("Assets/Resources/Audio");
            defineNames.Clear();
            fileNames.Clear();
            textLines.Clear();
            extensionNames.Clear();

            DirectoryInfo info = new DirectoryInfo("Assets/Application/Resources/Audio");
            ScanFolderData(info);
            //System.IO.FileInfo[] fileInfo = info.GetFiles();
            //int i = 0;
            //foreach (System.IO.FileInfo file in fileInfo)
            //{
            //    {
            //        if (file.Extension == ".wav" || file.Extension == ".mp3" || file.Extension == ".ogg")
            //        {
            //            string def = file.Name.Replace(file.Extension, "").ToUpper() ;
            //            def = def.Replace( "(","").Replace(")", "");  // Trim "(",")" and spaces
            //            defineNames.Add(def.Replace(" ", "_"));
            //            //defineNames.Add(file.Name.Replace(file.Extension, "").ToUpper());
            //            fileNames.Add(file.Name.Replace(file.Extension, ""));
            //            extensionNames.Add(file.Extension);
            //            //Debug.Log("Asset " + file.FullName +  " extension " + file.Extension);
            //            i++;
            //        }
            //    }
            //}

            WriteHeader();
            WriteDefines();
            WritePaths();
            WriteFooter();

            System.IO.File.WriteAllLines(
            Application.dataPath + "/Sunhouse/Managers/Audio/AudioLoad.cs", textLines.ToArray() );

            AssetDatabase.Refresh();

            //AudioManager.UpdateSoundProps();
        }

        void WriteHeader()
        {
            textLines.Add("using UnityEngine;");
            textLines.Add("public class AudioLoad");
            textLines.Add("{");

        }

        void WriteDefines()
        {
            textLines.Add("	public enum Clips");
            textLines.Add("	{");


            for (int i = 0; i < defineNames.Count; i++)
            {
                textLines.Add("		" + defineNames[i] + "=" + i.ToString() + ",");

            }
            textLines.Add("		COUNT=" + defineNames.Count.ToString());
            textLines.Add("	}");
        }

        void WritePaths()
        {
            textLines.Add("	public static readonly string[] Paths = ");
            textLines.Add("	{");

            for (int i = 0; i < defineNames.Count; i++)
            {
                textLines.Add("		\"" + fileNames[i] + "\",");
            }
            textLines.Add("	};");


            textLines.Add("	public static readonly string[] Extensions = ");
            textLines.Add("	{");

            for (int i = 0; i < defineNames.Count; i++)
            {
                textLines.Add("		\"" + extensionNames[i] + "\",");
            }
            textLines.Add("	};");
        }

        void WriteFooter()
        {
            textLines.Add("}");
        }

        [MenuItem("GameObject/Sun/AudioManager", false, 1)]
        public static void CreateAudioManager()
        {
            if (GameObject.FindObjectOfType<Audio>() == null)
            {
                var gameObject = UnityEditor.Selection.activeGameObject;
                if (gameObject == null)
                {
                    gameObject = new GameObject(typeof(Sun.Audio).Name); // typeof(Sun.Audio).Name
                    UnityEditor.Selection.activeGameObject = gameObject;
                }
                UnityEditor.Undo.RegisterCreatedObjectUndo(gameObject, "Create Audio Manager");
                gameObject.AddComponent<Sun.Audio>().OnEnable();
            }
            else
            {
                Debug.LogError("Audio already added to the scene.");
            }
        }
        //    public class AudioLoad
        //    {
        //        public enum Sounds
        //        {
        //            FX_LAZER = 0,
        //            COUNT = 1
        //        }
        //        public static readonly string[] Paths =
        //        {
        //              "fx_Lazer",
        //        };
        //        public static readonly string[] Extensions =
        //        {
        //          ".wav",
        //        };
        //    }

    }
}