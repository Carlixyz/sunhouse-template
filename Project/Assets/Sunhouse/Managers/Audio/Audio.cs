using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace Sun
{

    /// <summary>
    /// Simple Audio System with Music CrossFade & SoundFx Sources f
    /// </summary>
    //[PrefabAttribute("Prefabs/Audio")]
    [DisallowMultipleComponent, System.Serializable]                  //, AddComponentMenu("Sun/AudioManager")]
    public class Audio : Singleton<Audio>
    {
        public bool _musicEnable = true, _isCrossFading = false;

        [SerializeField, Range(0.0f, 1.0f)]
        float _musicVolume  = 1.0f;                                  // Music Multiplier WITH EDITOR PRIORITY

        [SerializeField]
        int _current = 0, _musicTotal = 2;      // Current Music Source Index Actually playing + total Mixing Buffers

        public AudioSource[] MusicSources;                          // Circular Array for a double Music Playback Sources

        Coroutine[] _crossFades = new Coroutine[2];                 // Coroutines Mixers Refs for an easy Start/Stop

        //--------------------------------------------------------------------------------// MUSIC			

        public bool _soundEnable = true, _isDelaying = false;

        [SerializeField]
        int _srcIndex = 0; int _srcTotal = 10;                       // Next Sound FX Source Index to be used

        [SerializeField, Range(0.0f, 1.0f)]
        float _soundVolume              = 1.0f;

        public SoundFX[] SoundFXSources  ;                          // Circular Array of all sound FXs Sources

        [SerializeField]
        List<SoundFX> delayedStops = new List<SoundFX>();           // Sound FXs currently playing & following

        //--------------------------------------------------------------------------------// SOUND FX			

        public string basePath = "Audio/";

        [SerializeField]
        public List<AudioClip> LoadedSounds = new List<AudioClip>();

        public AudioClip GetAudioClip(int soundId)
        {
            AudioClip tmpClip = null;

            if ( TryGetValue(soundId, out tmpClip) )
                return tmpClip;   //return LoadedSounds.ElementAtOrDefault(soundId);
            else
                return LoadAudioClip(soundId);
        }

        public bool TryGetValue(int id, out AudioClip value)
        {
            if (Mathf.Clamp(id, 0, (LoadedSounds.Count - 1)) == id) //if (0 <= id && id < LoadedSounds.Count)
            {
                value = LoadedSounds[id] ;
                return true;
            }

            value = default(AudioClip);                             //value = null;
            return false;
        }

        public AudioClip LoadAudioClip(int soundId)
        {

            if (soundId >= (int)AudioLoad.Clips.COUNT)
                return null;

            //AudioClip tmpClip = null;
            //if (TryGetValue(soundId, out tmpClip))
            //    return tmpClip;   //return LoadedSounds.ElementAtOrDefault(soundId);

            if (soundId >= 0 && soundId < LoadedSounds.Count && LoadedSounds[soundId] != null )
                if (LoadedSounds[soundId] != null)
                return LoadedSounds[soundId];                       // Enum.IsDefined


            //if (LoadedSounds.ContainsKey(soundId))                // Enum.IsDefined
            //return LoadedSounds[soundId];

            AudioClip tmpClip = null;
            tmpClip = (AudioClip)Resources.Load(basePath + AudioLoad.Paths[soundId]);

            if (tmpClip != null)
                LoadedSounds.Add(tmpClip);
            //LoadedSounds.Add(soundId, tmpClip);

            //De.Log("TryGetValue");

            return tmpClip;
        }

        public void LoadAllClips()
        {
            if (LoadedSounds != null && LoadedSounds.Count != 0)
                LoadedSounds.Clear();

            Debug.Log("Loading All Clips");
            for(int i = 0; i < (int)AudioLoad.Clips.COUNT; i++)
            {
                LoadAudioClip(i);
            }
            //if (downloadSounds)
            //    StartCoroutine(DownloadingSoundsCoroutine());
        }

        public void OnEnable()
        {
            //InitPlayerPrefs()
            //LoadPlayerPrefs();
            Init();
        }

        public void OnDisable()
        {
            //SavePlayerPrefs();
        }

        public void Init()
        {
            MusicEnabled = _musicEnable;
            SoundEnabled = _soundEnable;
            
            if (MusicSources == null || MusicSources.Length == 0)
            {
                MusicSources = new AudioSource[_musicTotal];
                for (int i = 0; i < _musicTotal; i++)
                {
                    MusicSources[i] = gameObject.AddComponent<AudioSource>();
                    MusicSources[i].playOnAwake = false;
                    MusicSources[i].volume = 0;
                    MusicSources[i].loop = true;
                    MusicSources[i].spatialBlend = 0;   // This Allows to Listen Music Anywhere in Scene
                }
            }

            if (SoundFXSources == null || SoundFXSources.Length == 0)
            {
                SoundFXSources = new SoundFX[_srcTotal];
                for (var i = 0; i < _srcTotal; i++)
                    SoundFXSources[i] = new SoundFX(i); 

            }

            _srcTotal = SoundFXSources.Length;

            LoadAllClips();
        }

        public bool MusicEnabled                                    // Music Sources Pause/ PlayBack
        {
            set
            {
                if (value == this._musicEnable)
                    return;
                this._musicEnable = value;

                if (_musicEnable)
                    ResumeMusic();                                  // Try a soft music Resume
                else
                    PauseMusic();
            }
            get
            {
                if (!this) // return false if this Manager don't exists or something weird happen!;
                {
                    OnEnable();
                    return false;
                }
                return this._musicEnable;
            }
        }

        public bool SoundEnabled                                    // Sound FX Source Enabler
        {
            set
            {
                if (value == this._soundEnable)
                    return;

                this._soundEnable = value;
                CheckAllSound();
            }
            get
            {
                if (!this) // return false;
                {
                    OnEnable();
                    return false;
                }
                return this._soundEnable;
            }
        }

        public float MusicVolumen
        {
            set
            {
                if (value == this._musicVolume)
                    return;
                this._musicVolume = value;

                foreach (AudioSource audioSrc in MusicSources)
                    audioSrc.volume = _musicVolume;                 // Ok, This is really nasty whatever
            }
            get
            {
                return _musicVolume;
            }
        }

        public float SoundVolumen
        {
            set
            {
                if (value == this._soundVolume)
                    return;
                this._soundVolume = value;
                CheckAllSound();
            }
            get
            {
                return _soundVolume;
            }
        }

        void InitPlayerPrefs()
        {
            if (!PlayerPrefs.HasKey(KEY_VOLUME_FX))
                PlayerPrefs.SetFloat(KEY_VOLUME_FX, 1.0f);
            if (!PlayerPrefs.HasKey(KEY_VOLUME_MUSIC))
                PlayerPrefs.SetFloat(KEY_VOLUME_MUSIC, 1.0f);
        }

        const string KEY_ENABLE_MUSIC   = "AudioManager_Enabled_Music";
        const string KEY_VOLUME_MUSIC   = "AudioManager_Volume_Music";
        const string KEY_ENABLE_FX      = "AudioManager_Enabled_Fx";
        const string KEY_VOLUME_FX      = "AudioManager_Volume_Fx";
        void LoadPlayerPrefs()
        {
            if (PlayerPrefs.HasKey(KEY_VOLUME_FX))
                _soundVolume = PlayerPrefs.GetFloat(KEY_VOLUME_FX);
            if (PlayerPrefs.HasKey(KEY_ENABLE_FX))
                _soundEnable = (PlayerPrefs.GetInt(KEY_ENABLE_FX) == 1 ? true : false);
            if (PlayerPrefs.HasKey(KEY_VOLUME_MUSIC))
                _musicVolume = PlayerPrefs.GetFloat(KEY_VOLUME_MUSIC);
            if (PlayerPrefs.HasKey(KEY_ENABLE_MUSIC))
                _soundEnable = (PlayerPrefs.GetInt(KEY_ENABLE_MUSIC) == 1 ? true : false);
        }
        void SavePlayerPrefs()
        {
            PlayerPrefs.SetInt(KEY_ENABLE_FX, (_soundEnable == true ? 1 : 0) );
            PlayerPrefs.SetFloat(KEY_VOLUME_FX, _soundVolume);
            PlayerPrefs.SetFloat(KEY_ENABLE_MUSIC, (_musicEnable == true ? 1 : 0));
            PlayerPrefs.SetFloat(KEY_VOLUME_MUSIC, _soundVolume);
            PlayerPrefs.Save();
        }

        //----------------------------------------------------------------------------------------//			

        /// <summary>
        /// Plays a sound with an AudioSource in some empty game object attached to the given Transforms
        /// (moving along with it during playback). and return it after end playing.
        /// </summary>
        public AudioSource Play(AudioLoad.Clips clip, Transform emitter, float volume = 1.0f, float pitch = 1.0f)
        {
            return Play(GetAudioClip((int)clip), emitter, volume, pitch);
        }

        public AudioSource Play(AudioClip clip, Transform emitter = null, float volume = 1.0f, float pitch = 1.0f)
        {
            if (!SoundEnabled)  // BECAREFULL WHEN YOU USE THIS WITH SOME PARENT ABOUT TO BE DESTROYED...
                return null;

            SoundFX src = TryGetFreeSource();
            if (src == null)
                return null;

            //src.transform.parent = emitter;
            //src.transform.localPosition = Vector3.zero;       //if (emitter)
            //src.AudioSrcFX.name = CurrentSoundName;

            src.ClipName = clip.name;

            src.AudioSrcFX.clip     = clip;
            src.AudioSrcFX.volume   = volume * _soundVolume; // NEVER DO THIS (volume == -1.0f ? _SoundVolumen : volume);
            src.AudioSrcFX.pitch    = pitch;

            src.AudioSrcFX.transform.localPosition = Vector3.zero;
            src.Target              = emitter ;
            src.TimeDelay           = clip.length;
            src.AudioSrcFX.Play();

            delayedStops.Add(src);

            if (!_isDelaying)
                StartCoroutine(UpdateDelays());

            return src.AudioSrcFX;                              // Try get a valid Source else return null
        }

        /// <summary>
        /// Plays a sound at the given point in space with an AudioSource
        /// in that place and return it after it finished playing.
        /// </summary>
        public AudioSource Play(AudioLoad.Clips clip, Vector3 point, float volume = 1.0f, float pitch = 1.0f)
        {
            return Play(GetAudioClip((int)clip), point, volume, pitch);
        }

        public AudioSource Play(AudioClip clip, Vector3 point, float volume = 1.0f, float pitch = 1.0f)
        {
            if (!SoundEnabled)
                return null;

            SoundFX src = TryGetFreeSource();
            if (src == null)
                return null;

            //src.AudioSrcFX.Stop();
            //src.AudioSrcFX.name = CurrentSoundName;
            src.ClipName = clip.name;

            src.AudioSrcFX.clip     = clip;
            src.AudioSrcFX.volume   = volume * _soundVolume; // NEVER DO THIS (volume == -1.0f ? _SoundVolumen : volume);
            src.AudioSrcFX.pitch    = pitch;

            src.AudioSrcFX.transform.localPosition = point;
            src.Target              = null;
            src.TimeDelay           = clip.length;
            src.AudioSrcFX.Play();

            delayedStops.Add(src);              // delayedStops.Add(src.gameObject, clip.length);

            if (!_isDelaying)
                StartCoroutine(UpdateDelays());

            return src.AudioSrcFX;                      // Try get a valid Source else return null
        }

        public AudioSource PlayRaw(AudioClip clip, Vector3 point, float volume = 1.0f, float pitch = 1.0f)
        {
            if (!SoundEnabled)
                return null;

            _srcIndex = (_srcIndex + 1) % _srcTotal; // Traverse Global SrcIndex
            SoundFXSources[_srcIndex].AudioSrcFX.transform.position = point;
            SoundFXSources[_srcIndex].AudioSrcFX.clip = clip;
            SoundFXSources[_srcIndex].AudioSrcFX.volume = volume * _soundVolume;
            SoundFXSources[_srcIndex].AudioSrcFX.pitch = pitch;
            SoundFXSources[_srcIndex].AudioSrcFX.Play();
            return SoundFXSources[_srcIndex].AudioSrcFX;            //  get a valid Source else return null
        }

        SoundFX TryGetFreeSource()                  // Traverse around a fixed circular array of AudioSources
        {
            for (int i = 0; i < _srcTotal; _srcIndex = (_srcIndex + ++i) % _srcTotal) // Traverse Global SrcIndex
                if (SoundFXSources[_srcIndex].TimeDelay <= 0)
                    return SoundFXSources[_srcIndex];                                // Get It! 8045ms
            return null;
         }

        public void CheckAllSound()                 // This should be called from options and Pause ingame
        {
            foreach (AudioSource audioSrc in Object.FindObjectsOfType(typeof(AudioSource)))
            {
                if (audioSrc.spatialBlend != 0)     // If this audioSource is music then stop
                    if (_soundEnable)
                    {
                        audioSrc.volume = _soundVolume;    // Ok, This is really nasty whatever
                        audioSrc.UnPause();                 // Try a soft sound Resume
                        //if (!audioSrc.isPlaying)
                        //    audioSrc.Play();
                    }
                    else
                        audioSrc.Pause();
            }
        }

        //----------------------------------------------------------------------------------------//			

        /// <summary>
        /// Plays a looping Music with a soft delayed fade in/out in volume
        /// you can even cancel it with another new call of Pause or Play with a diferent clip.
        /// </summary>
        public AudioSource PlayMusic(AudioLoad.Clips clip, float delay = 0f, float volume = 1.0f, float pitch = 1.0f)
        {
            return PlayMusic(GetAudioClip((int)clip), delay, volume, pitch);
        }

        public AudioSource PlayMusic(AudioClip clip, float delay = 0f, float volume = 1.0f, float pitch = 1.0f)
        {
            if (!MusicEnabled || clip.loadState == AudioDataLoadState.Loading /*|| clip.loadState != AudioDataLoadState.Loaded*/)
                return null;

            float Vol = volume * _musicVolume;

            if ( MusicSources[_current].clip == clip)// && MusicSources[Current].isPlaying)
            {
                if (!Mathf.Approximately(MusicSources[_current].volume, Vol))
                {
                    StopCoroutine(_crossFades[_current]);
                    _crossFades[_current] = StartCoroutine(
                        AudioCrossFade(MusicSources[_current], MusicSources[_current].volume, Vol, delay));
                }
                return MusicSources[_current];
            }
            // If you are here it's because we had a new clip PlayBack... 

            if (_isCrossFading)                     // We are in the middle of a Transition Stop out & Restart!
                foreach (Coroutine r in _crossFades)
                    StopCoroutine(r);

            int next = (_current + 1) % _musicTotal;  // Whe are Going to use the other Buffer and CrossFade to it...
            MusicSources[next].clip = clip;
            MusicSources[next].pitch = pitch;
            //MusicSources[next].volume = volume;   // This is already done in the Awake and then in Coroutine.

            _crossFades[_current] = StartCoroutine(
                AudioCrossFade(MusicSources[_current], MusicSources[_current].volume, 0f, delay));
            _crossFades[next] = StartCoroutine(
                AudioCrossFade(MusicSources[next], MusicSources[next].volume, Vol, delay));

            _current = next;
            //CurrentMusicName = MusicSources[_current].clip.name;

            return MusicSources[_current];
        }

        public void PauseMusic(float delay = 0f)
        {
            if (!MusicEnabled || !MusicSources[_current].isPlaying)
                return;

            //if (_IsCrossFading)             // We are in the middle of a Transition take the next ting & Bail out!
            if (_isCrossFading && _crossFades[_current] != null)
                StopCoroutine(_crossFades[_current]);

            _crossFades[_current] = StartCoroutine(
                AudioCrossFade(MusicSources[_current], MusicSources[_current].volume, 0.0f, delay));
            //MusicSources[Current].Pause();
        }

        public void ResumeMusic(float delay = 0f)
        {
            if (MusicEnabled || MusicSources[_current].isPlaying)
                return;

            if (_isCrossFading && _crossFades[_current] != null)
                StopCoroutine(_crossFades[_current]);

            var Vol = 1 * _musicVolume;

            _crossFades[_current] = StartCoroutine(
                AudioCrossFade(MusicSources[_current], MusicSources[_current].volume, Vol, delay));
            //AudioCrossFade(MusicSources[Current], 0.0f, Vol, delay));
            //MusicSources[Current].UnPause();
        }

        public AudioSource StopMusic()
        {
            if (!MusicSources[_current].isPlaying)
                return MusicSources[_current];

            if (_isCrossFading)             // We are in the middle of a Transition take the thing & Bail out!
                foreach (Coroutine r in _crossFades)
                    StopCoroutine(r);

            foreach (var s in MusicSources)
            {
                s.Stop();
                s.volume = 0;
            }

            return MusicSources[_current];
        }

        IEnumerator AudioCrossFade(AudioSource src, float StartVol, float EndVol, float duration = 0)
        {
            _isCrossFading = true;

            float startTime = Time.unscaledTime;
            float elapsed = 0f;

            src.UnPause();
            if (!src.isPlaying)
                src.Play(); 

            while ((Time.unscaledTime - startTime) < duration + .1f)
            {
                elapsed = (Time.unscaledTime - startTime) / duration;
                src.volume = Mathf.Lerp(StartVol, EndVol, elapsed);
                yield return 0;
            }

            if (Mathf.Approximately(src.volume, EndVol) && EndVol <= 0.1f)
                src.Pause();                    //src1.Stop();  //  else src.UnPause();        

            _isCrossFading = false;

            yield break;
        }

        IEnumerator UpdateDelays()                  // Process all going to be destroyed AudioSource's Objects
        {
            _isDelaying = true;

            while (delayedStops.Any())                              // start knock out time counter
            {
                foreach (SoundFX markedObj in delayedStops.ToList())
                {

                    if (markedObj.Target )
                        markedObj.AudioSrcFX.transform.position = markedObj.Target.position;

                    if (markedObj.TimeDelay <= 0.0f)            // Dead?
                        delayedStops.Remove(markedObj);         //RemoveDelayedDestruction(i);   * * * *

                    markedObj.TimeDelay -= Time.deltaTime;      // var delay -= Time.deltaTime;
                }

                yield return 0;
            }

            _isDelaying = false;
            yield break;
        }


        //----------------------------------------------------------------------------------------//			

        [System.Serializable]
        public class SoundFX            // A basic AudioSource's Helper to follow Targets without parenting 
        {
            [HideInInspector]
            public string ClipName;
            [SerializeField]
            //[HideInInspector]
            public AudioSource AudioSrcFX;                          // The AudioSource of this sound FX
            [SerializeField]
            public Transform Target;    // to avoid Destroyed Objects in scene change
            public float TimeDelay;                                 // Total time following some target

            public SoundFX(int index)
            {
                AudioSrcFX = new GameObject(ClipName).AddComponent<AudioSource>();
                AudioSrcFX.playOnAwake = false;
                AudioSrcFX.spatialBlend = 1;    // This Allows to Listen the Sound Fx Somewhere in Scene
                if (Application.isPlaying)
                    GameObject.DontDestroyOnLoad(AudioSrcFX.gameObject);
                AudioSrcFX.transform.parent = Audio.Get.transform;
                ClipName = AudioSrcFX.name = AudioSrcFX.gameObject.name = "AudioSource " + index;
            }
        }

        //----------------------------------------------------------------------------------------//			

        //public class AudioLoad
        //{
        //    public enum Clips { COUNT };
        //    public static readonly string[] Paths;
        //    public static readonly string[] Extensions;
        //}

    }
}