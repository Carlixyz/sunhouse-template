﻿//#define DISABLE_DEBUG_LOGGING
// Don't edit first line above as this is auto-edited by the DebugPreferencesConfig.cs editor script to enable/disable logging.

#if !DEVELOPMENT_BUILD && !UNITY_EDITOR
#define DISABLE_DEBUG_LOGGING
#endif

using UnityEngine;
using System.Collections.Generic;
using System.Diagnostics;
using System;



namespace Sun
{

    /// <summary>
    /// Simple Debug.Log calls Removable  System 
    /// Use as Sun.DevLog.Log("Bla Bla"); 
    /// And then set OFF the Development Build or #define DISABLE_DEBUG_LOGGING to remove all 
    /// </summary>

    //---------------------------------------------------------------------------------// 

    /// <summary>
    /// Usefull mobile ScreenLog Console call it with Sun.ScreenLog.Get.ShowLog = true;
    /// To Display FPS Use Sun.ScreenLog.Get.ShowFPS = true;
    /// To Do Benchmark Test just call StartBenchmark() & StopBenchmark("Test 01");
    /// To Save all the Log during playMode call Save("FileLog.xml")
    /// </summary>

    [DisallowMultipleComponent, System.Serializable]                  //, AddComponentMenu("Sun/AudioManager")]
    [ExecuteInEditMode]
    [PrefabAttribute("Prefabs/ScreenLog")]
    //[PrefabAttribute("ScreenLog")]    
    public class ScreenLog : Singleton<ScreenLog>
    {
        class LogMessage
        {
            public string Message;
            public LogType Type;

            public LogMessage(string msg, LogType type)
            {
                Message = msg;
                Type = type;
            }
        }

        public enum LogAnchor
        {
            TopLeft,
            TopRight,
            BottomLeft,
            BottomRight
        }

        public bool ShowLog = true;
        public bool ShowInEditor = true;
        public bool ShowFPS = false;

        [Tooltip("Height of the log area as a percentage of the screen height")]
        [Range(0.2f, 1.0f)]
        public float Height = 0.2f;

        [Tooltip("Width of the log area as a percentage of the screen width")]
        [Range(0.3f, 1.0f)]
        public float Width = 0.5f;

        public int Margin = 20;

        public LogAnchor AnchorPosition = LogAnchor.BottomLeft;

        public int FontSize = 14;

        [Range(0f, 01f)]
        public float BackgroundOpacity = 0.5f;
        public Color BackgroundColor = Color.black;

        public bool LogMessages = true;
        public bool LogWarnings = true;
        public bool LogErrors = true;

        public Color MessageColor = Color.white;
        public Color WarningColor = Color.yellow;
        public Color ErrorColor = new Color(1, 0.5f, 0.5f);

        public bool StackTraceMessages = false;
        public bool StackTraceWarnings = false;
        public bool StackTraceErrors = true;

        static Queue<LogMessage> queue = new Queue<LogMessage>();
 
        GUIStyle styleContainer, styleText;
        int padding = 5;
        float deltaTime = 0.0f;

        protected override void Awake()
        {
            base.Awake();

            InitStyles();
        }

        private void InitStyles()
        {
            Texture2D back = new Texture2D(1, 1);
            BackgroundColor.a = BackgroundOpacity;
            back.SetPixel(0, 0, BackgroundColor);
            back.Apply();

            styleContainer = new GUIStyle();
            styleContainer.normal.background = back;
            styleContainer.wordWrap = false;
            styleContainer.padding = new RectOffset(padding, padding, padding, padding);

            styleText = new GUIStyle();
            styleText.fontSize = FontSize;
        }

        public void OnEnable()
        {
            if (!ShowInEditor && Application.isEditor) return;

            queue = new Queue<LogMessage>();

#if UNITY_4_5 || UNITY_4_6 || UNITY_4_7
            Application.RegisterLogCallback(HandleLog);
#else
            Application.logMessageReceived += HandleLog;
#endif
        }

        void OnDisable()
        {
            // If destroyed because already exists, don't need to de-register callback
            if (Destroyed) return;

#if UNITY_4_5 || UNITY_4_6 || UNITY_4_7
            Application.RegisterLogCallback(null);
#else
            Application.logMessageReceived -= HandleLog;
#endif
        }

        void Update()
        {
            //if (Input.GetKeyDown(KeyCode.F9))
                //Sun.ScreenLog.Get.LoadFromStreamingAssets();

            //if (Input.GetKeyDown(KeyCode.F10))
            //    Sun.ScreenLog.Get.Save();

            if (ShowFPS)
                deltaTime += (Time.deltaTime - deltaTime) * 0.1f;

            if (!ShowInEditor && Application.isEditor) return;

            float InnerHeight = (Screen.height - 2 * Margin) * Height - 2 * padding;
            int TotalRows = (int) (InnerHeight / styleText.lineHeight);

            // Remove overflowing rows
            while (queue.Count > TotalRows)
                queue.Dequeue();
        }

        void OnGUI()
        {
            if (ShowFPS)
            {
                float msec = deltaTime * 1000.0f;
                float fps = 1.0f / deltaTime;
                string text = string.Format("{0:0.0} ms ({1:0.} fps)", msec, fps);
                GUI.Label(new Rect(Margin, Margin, Screen.width - Margin, Screen.height - Margin), text);
               // http://wiki.unity3d.com/index.php?title=FramesPerSecond
            }

            if (!ShowLog) return;
            if (!ShowInEditor && Application.isEditor) return;

            float w = (Screen.width - 2 * Margin) * Width;
            float h = (Screen.height - 2 * Margin) * Height;
            float x = 1, y = 1;

            switch (AnchorPosition)
            {
                case LogAnchor.BottomLeft:
                    x = Margin;
                    y = Margin + (Screen.height - 2 * Margin) * (1 - Height);
                    break;

                case LogAnchor.BottomRight:
                    x = Margin + (Screen.width - 2 * Margin) * (1 - Width);
                    y = Margin + (Screen.height - 2 * Margin) * (1 - Height);
                    break;

                case LogAnchor.TopLeft:
                    x = Margin;
                    y = Margin;
                    break;

                case LogAnchor.TopRight:
                    x = Margin + (Screen.width - 2 * Margin) * (1 - Width);
                    y = Margin;
                    break;
            }

            GUILayout.BeginArea(new Rect(x, y, w, h), styleContainer);

            foreach (LogMessage m in queue)
            {
                switch (m.Type)
                {
                    case LogType.Warning:
                        styleText.normal.textColor = WarningColor;
                        break;

                    case LogType.Log:
                        styleText.normal.textColor = MessageColor;
                        break;

                    case LogType.Assert:
                    case LogType.Exception:
                    case LogType.Error:
                        styleText.normal.textColor = ErrorColor;
                        break;

                    default:
                        styleText.normal.textColor = MessageColor;
                        break;
                }

                GUILayout.Label(m.Message, styleText);
            }

            GUILayout.EndArea();

        }

        void HandleLog(string message, string stackTrace, LogType type)
        {
            if (type == LogType.Assert && !LogErrors) return;
            if (type == LogType.Error && !LogErrors) return;
            if (type == LogType.Exception && !LogErrors) return;
            if (type == LogType.Log && !LogMessages) return;
            if (type == LogType.Warning && !LogWarnings) return;

            string[] lines = message.Split(new char[] { '\n' });

            //TextLog.AddRange(lines);

            foreach (string l in lines)
            {
                queue.Enqueue(new LogMessage(l, type));
                TextLog.Add(l);
            }

            if (type == LogType.Assert && !StackTraceErrors) return;
            if (type == LogType.Error && !StackTraceErrors) return;
            if (type == LogType.Exception && !StackTraceErrors) return;
            if (type == LogType.Log && !StackTraceMessages) return;
            if (type == LogType.Warning && !StackTraceWarnings) return;

            string[] trace = stackTrace.Split(new char[] { '\n' });

            //TextLog.AddRange(trace);

            foreach (string t in trace)
                if (t.Length != 0)
                {
                    queue.Enqueue(new LogMessage("  " + t, type));
                    TextLog.Add(t);
                }
        }

        public void InspectorGUIUpdated()
        {
            InitStyles();
        }

        //---------------------------------------------------------------------------------// 

        //protected override object PreSerialization()
        //{
        //    De.Log(" Saved FullLog " + TextLog);
        //    return TextLog as object;
        //}

        //protected override void PostDeserialization(object DeSerializedData)
        //{
        //    _textLog.Clear();
        //    _textLog = (List<string>)DeSerializedData ;
        //}

        //---------------------------------------------------------------------------------// 

        [SerializeField]
        List<string> _textLog = new List<string>();
        List<string> TextLog { get { return _textLog; } set { _textLog = value; } }

        System.Diagnostics.Stopwatch benchmark = new System.Diagnostics.Stopwatch();

        public void StartBenchmark()
        {
            benchmark.Reset();
            benchmark.Start();
        }

        public void EndBenchmark(string title)
        {
            benchmark.Stop();

            De.Log(title + " took " + benchmark.ElapsedMilliseconds + "ms");
        }

#if UNITY_EDITOR
        [UnityEditor.MenuItem("GameObject/Sun/Screen Logger", false, 1)]
        static void AddScreenLogger()
        {
            if (GameObject.FindObjectOfType<ScreenLog>() == null)
            {
                var gameObject = UnityEditor.Selection.activeGameObject;
                if (gameObject == null)
                {
                    gameObject = new GameObject(typeof(ScreenLog).Name); // typeof(Sun.Audio).Name
                    UnityEditor.Selection.activeGameObject = gameObject;
                }
                UnityEditor.Undo.RegisterCreatedObjectUndo(gameObject, "Create Screen Logger");
                gameObject.AddComponent<Sun.ScreenLog>().OnEnable();
            }
            else
            {
                De.LogError("ScreenLogger already added to the scene.");
            }
        }
#endif


        //---------------------------------------------------------------------------------// 
    }



    public static class De   // DevLog Custom Class for Easy Logging remoavable in builds
    {
#if DISABLE_DEBUG_LOGGING
    	    [Conditional("__NEVER_DEFINED__")]
#endif
        public static void Log(string message)
        {
            UnityEngine.Debug.Log(message);
        }
#if DISABLE_DEBUG_LOGGING
    	    [Conditional("__NEVER_DEFINED__")]
#endif
        public static void Log(string message, UnityEngine.Object context)
        {
            UnityEngine.Debug.Log(message, context);
        }
#if DISABLE_DEBUG_LOGGING
    	    [Conditional("__NEVER_DEFINED__")]
#endif
        public static void LogWarning(string message)
        {
            UnityEngine.Debug.LogWarning(message);
        }
#if DISABLE_DEBUG_LOGGING
    	    [Conditional("__NEVER_DEFINED__")]
#endif
        public static void LogWarning(string message, UnityEngine.Object context)
        {
            UnityEngine.Debug.LogWarning(message, context);
        }
#if DISABLE_DEBUG_LOGGING
    	    [Conditional("__NEVER_DEFINED__")]
#endif
        public static void LogError(string message)
        {
            UnityEngine.Debug.LogError(message);
        }
#if DISABLE_DEBUG_LOGGING
    	    [Conditional("__NEVER_DEFINED__")]
#endif
        public static void LogError(string message, UnityEngine.Object context)
        {
            UnityEngine.Debug.LogError(message, context);
        }
#if DISABLE_DEBUG_LOGGING
    	    [Conditional("__NEVER_DEFINED__")]
#endif
        public static void Assert(bool condition)
        {
            if (!condition) throw new Exception();
        }
    }




}


