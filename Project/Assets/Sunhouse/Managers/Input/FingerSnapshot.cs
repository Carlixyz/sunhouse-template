﻿using UnityEngine;
using System.Collections.Generic;


namespace Sun.Control
{
    public class FingerSnapshot
    {

        // The age of the finger when this snapshot was created
        public float Age;

        // The screen position of the finger when this snapshot was created
        public Vector2 ScreenPosition;

        // This will return the world position of this snapshot based on the distance from the camera
        public Vector3 GetWorldPosition(float distance, Camera camera = null)
        {
            if (camera == null) camera = Camera.main;

            if (camera != null)
            {
                var point = new Vector3(ScreenPosition.x, ScreenPosition.y, distance);

                return camera.ScreenToWorldPoint(point);
            }

            return default(Vector3);
        }

        public static List<FingerSnapshot> InactiveSnapshots = new List<FingerSnapshot>(1000);

        // Return the last inactive snapshot, or allocate a new one
        public static FingerSnapshot Pop()
        {
            if (InactiveSnapshots.Count > 0)
            {
                var index = InactiveSnapshots.Count - 1;
                var snapshot = InactiveSnapshots[index];

                InactiveSnapshots.RemoveAt(index);

                return snapshot;
            }

            return new FingerSnapshot();
        }
    }

}
