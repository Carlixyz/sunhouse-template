﻿using UnityEngine;
using UnityEditor;
using System.Collections.Generic;

namespace Sun.Control
{
    [CustomEditor(typeof(Touch))]
    public class TouchEditor : Editor
    {
        private static List<Finger> allFingers = new List<Finger>();

        private static GUIStyle fadingLabel;

        public static GUIStyle GetFadingLabel(bool active, float progress)
        {
            if (fadingLabel == null)
            {
                fadingLabel = new GUIStyle(EditorStyles.label);
            }

            var a = EditorStyles.label.normal.textColor;
            var b = a; b.a = active == true ? 0.5f : 0.0f;

            fadingLabel.normal.textColor = Color.Lerp(a, b, progress);

            return fadingLabel;
        }

        public override void OnInspectorGUI()                               // Draw the whole inspector
        {
            var touch = (Touch)target;

            if (touch != Touch.Get)
            {
                EditorGUILayout.Separator();

                EditorGUILayout.HelpBox("There is more than one active and enabled LeanThread...", MessageType.Warning);
            }

            touch.Persistent = EditorGUILayout.Toggle("Persistent ", touch.Persistent);

            Separator();

            DrawSettings(touch);

            Separator();

            DrawGestures();

            Separator();

            DrawFingers(touch);

            Separator();

            Repaint();
        } 

        private void DrawSettings(Touch touch)
        {
            DrawTitle("Settings");
            DrawDefault("TapThreshold");
            DrawDefault("SwipeThreshold");
            DrawDefault("HeldThreshold");
            DrawDefault("ReferenceDpi");

            Separator();

            DrawDefault("RecordFingers");

            if (touch.RecordFingers == true)
            {
                BeginIndent();
                DrawDefault("RecordThreshold");
                DrawDefault("RecordLimit");
                EndIndent();
            }

            Separator();

            DrawDefault("SimulateMultiFingers");

            if (touch.SimulateMultiFingers == true)
            {
                BeginIndent();
                DrawDefault("PinchTwistKey");
                DrawDefault("MultiDragKey");
                DrawDefault("FingerTexture");
                EndIndent();
            }
        }

        private void DrawFingers(Touch touch)
        {
            DrawTitle("Fingers");

            allFingers.Clear();
            allFingers.AddRange(Touch.Fingers);
            allFingers.AddRange(Touch.InactiveFingers);
            allFingers.Sort((a, b) => a.Index.CompareTo(b.Index));

            for (var i = 0; i < allFingers.Count; i++)
            {
                var finger = allFingers[i];
                var progress = touch.TapThreshold > 0.0f ? finger.Age / touch.TapThreshold : 0.0f;
                var style = GetFadingLabel(finger.Set, progress);

                if (style.normal.textColor.a > 0.0f)
                {
                    var screenPosition = finger.ScreenPosition;

                    EditorGUILayout.LabelField("#" + finger.Index + " x " + finger.TapCount + " (" + Mathf.FloorToInt(screenPosition.x) + ", " + Mathf.FloorToInt(screenPosition.y) + ") - " + finger.Age.ToString("0.0"), style);
                }
            }
        }

        private void DrawGestures()
        {
            DrawTitle("Gestures");

            EditorGUI.BeginDisabledGroup(true);
            {
                DrawVector2("Drag Delta", Touch.DragDelta);

                DrawVector2("Solo Drag Delta", Touch.SoloDragDelta);

                DrawVector2("Multi Drag Delta", Touch.MultiDragDelta);

                EditorGUILayout.FloatField("Twist Degrees", Touch.TwistDegrees);

                EditorGUILayout.FloatField("Twist Radians", Touch.TwistRadians);

                EditorGUILayout.FloatField("Pinch Scale", Touch.PinchScale);
            }
            EditorGUI.EndDisabledGroup();
        }

        private static void DrawVector2(string name, Vector2 xy)
        {
            var left = Reserve();
            var middle = Reserve(ref left);
            var right = Reserve(ref middle, middle.width / 2);

            EditorGUI.LabelField(left, name);
            EditorGUI.FloatField(middle, xy.x);
            EditorGUI.FloatField(right, xy.y);
        }

        private void Separator()
        {
            EditorGUILayout.Separator();
        }

        private void DrawTitle(string name)
        {
            EditorGUI.LabelField(Reserve(), name, EditorStyles.boldLabel);
        }

        private void DrawDefault(string name)
        {
            EditorGUI.BeginChangeCheck();
            {
                EditorGUILayout.PropertyField(serializedObject.FindProperty(name));
            }
            if (EditorGUI.EndChangeCheck() == true)
            {
                serializedObject.ApplyModifiedProperties();
            }
        }

        private void BeginIndent()
        {
            EditorGUI.indentLevel += 1;
        }

        private void EndIndent()
        {
            EditorGUI.indentLevel -= 1;
        }

        private static Rect Reserve(ref Rect rect, float rightWidth = 0.0f, float padding = 2.0f)
        {
            if (rightWidth == 0.0f)
            {
                rightWidth = rect.width - EditorGUIUtility.labelWidth;
            }

            var left = rect; left.xMax -= rightWidth;
            var right = rect; right.xMin = left.xMax;

            left.xMax -= padding;

            rect = left;

            return right;
        }

        private static Rect Reserve(float height = 16.0f)
        {
            var rect = EditorGUILayout.BeginVertical();
            {
                EditorGUILayout.LabelField("", GUILayout.Height(height));
            }
            EditorGUILayout.EndVertical();

            return rect;
        }

        [MenuItem("GameObject/Sun/Touch", false, 1)]
        public static void CreateTouch()
        {
            //var gameObject = new GameObject(typeof(Touch).Name);

            //UnityEditor.Undo.RegisterCreatedObjectUndo(gameObject, "Create Touch");

            //gameObject.AddComponent<Touch>();

            //Selection.activeGameObject = gameObject;
            if (GameObject.FindObjectOfType<Touch>() == null)
            {
                var gameObject = UnityEditor.Selection.activeGameObject;
                if (gameObject == null)
                {
                    gameObject = new GameObject(typeof(Touch).Name); // typeof(Sun.Audio).Name
                    UnityEditor.Selection.activeGameObject = gameObject;
                }
                UnityEditor.Undo.RegisterCreatedObjectUndo(gameObject, "Create Touch Manager");
                gameObject.AddComponent<Touch>();//  .OnEnable();
            }
            else
            {
                Debug.LogError("Touch Manager already added to the scene.");
            }
        }
    }
}
