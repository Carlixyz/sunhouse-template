using UnityEngine;
using System.Collections.Generic;

// http://www.willrmiller.com/?p=87

namespace Sun
{
    public class GameEvent
    {
        //void GameEventInherited(GameObject objRef)    // use Example!
        //{
        //    ObjRef = objRef;
        //    NumRef = ObjRef.GetInstanceID();
        //    TextRef = ObjRef.name;
        //}
        //public int NumRef;
        //public string TextRef;
        //public GameObject ObjRef;
    }

    public class Events 
    {
        // This Could be a classic Singleton but Why?
        static Events instanceInternal = null;
        public static Events Get    // Get Instance Singleton
        {
            get
            {
                if (instanceInternal == null)
                    instanceInternal = new Events();

                return instanceInternal;
            }
        }

        public delegate void EventDelegate<T>(T e) where T : GameEvent;
        private delegate void EventDelegate(GameEvent e);

        private Dictionary<System.Type, EventDelegate> delegates = new Dictionary<System.Type, EventDelegate>();
        private Dictionary<System.Delegate, EventDelegate> delegateLookup = new Dictionary<System.Delegate, EventDelegate>();

        public void AddListener<T>(EventDelegate<T> del) where T : GameEvent
        {
            // Early-out if we've already registered this delegate
            if (delegateLookup.ContainsKey(del))
                return;

            // Create a new non-generic delegate which calls our generic one.
            // This is the delegate we actually invoke.
            EventDelegate internalDelegate = (e) => del((T)e);
            delegateLookup[del] = internalDelegate;

            EventDelegate tempDel;
            if (delegates.TryGetValue(typeof(T), out tempDel))
            {
                delegates[typeof(T)] = tempDel += internalDelegate;
            }
            else
            {
                delegates[typeof(T)] = internalDelegate;
            }
        }

        public void RemoveListener<T>(EventDelegate<T> del) where T : GameEvent
        {
            EventDelegate internalDelegate;
            if (delegateLookup.TryGetValue(del, out internalDelegate))
            {
                EventDelegate tempDel;
                if (delegates.TryGetValue(typeof(T), out tempDel))
                {
                    tempDel -= internalDelegate;
                    if (tempDel == null)
                    {
                        delegates.Remove(typeof(T));
                    }
                    else
                    {
                        delegates[typeof(T)] = tempDel;
                    }
                }

                delegateLookup.Remove(del);
            }
        }

        public void Raise(GameEvent e)
        {
            EventDelegate del;
            if (delegates.TryGetValue(e.GetType(), out del))
            {
                del.Invoke(e);
            }
        }

/// ------------------------------------------------------------------------- /// EVENTS:

        // USAGE: INSERT EVENTS DOWN HERE AS...
        /*
        public class SomethingHappenedEvent : GameEvent //objA <->> objB, objC Comunication
        {
            // Add event parameters here
            public Vector3 targetPosition;
            public bool isReady = true;

            public SomethingHappenedEvent(
                Vector3? tPos = null, bool ready = true)
            {
                isReady = ready;
                isWorking = work;
                targetPosition = tPos ?? Vector3.zero;
            }
        }
        */
    }
}

/// ------------------------------------------------------------------------- /// HELP

/*
// Registering to listen for the event looks like this:
public class SomeObject : MonoBehaviour
{
	void OnEnable ()
	{
		Sun.Events.Get.AddListener<SomethingHappenedEvent>(OnSomethingHappened);
	}
 
	void OnDisable ()
	{
		Sun.Events.Get.RemoveListener<SomethingHappenedEvent>(OnSomethingHappened);
	}
 
	void OnSomethingHappened (SomethingHappenedEvent e)
	{
		// Handle event here (can be used even as a state )
	}
}

// And finally, to raise the event, do this somewhere:
...
Sun.Events.Get.Raise(new SomethingHappenedEvent());

*/


