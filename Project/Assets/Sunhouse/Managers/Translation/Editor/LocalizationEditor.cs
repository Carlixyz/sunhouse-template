﻿using UnityEngine;
using UnityEditor;
using System.Collections.Generic;

namespace Sun
{
    [CustomEditor(typeof(Localization))]
    public class LocalizationEditor : Editor
    {
        class PresetLanguage
        {
            public string Name;

            public string[] CultureName;
        }

        // Translations File for save/loading path
        private string filePathAndName;  //System.IO.Path.Combine(Application.dataPath, "Resources/Languages.txt");

        // Languages
        private static List<PresetLanguage> presetLanguages = new List<PresetLanguage>();

        // Currently expanded language
        private int languageIndex = -1;

        // Currently expanded language phrase key
        private int reverseIndex = -1;

        // Currently expanded culture
        private int cultureIndex = -1;

        // Currently expanded phrase key
        private int phraseIndex = -1;

        // Currently expanded translation
        private int translationIndex = -1;

        private bool dirty;

        public static bool LangFolded = false, CultFolded = false, KeysFolded = false;

        private List<string> existingLanguages = new List<string>();

        private List<string> existingKeys = new List<string>();

        private GUIStyle labelStyle;

        private GUIStyle LabelStyle
        {
            get
            {
                if (labelStyle == null)
                {
                    labelStyle = new GUIStyle(EditorStyles.label);
                    labelStyle.clipping = TextClipping.Overflow;
                }

                return labelStyle;
            }
        }

        static LocalizationEditor()
        {
            AddPresetLanguage("Chinese", "zh", "zh-TW", "zh-CN", "zh-HK", "zh-SG", "zh-MO");
            AddPresetLanguage("English", "en", "en-GB", "en-US", "en-AU", "en-CA", "en-NZ", "en-IE", "en-ZA", "en-JM", "en-en029", "en-BZ", "en-BZ", "en-TT", "en-ZW", "en-PH");
            AddPresetLanguage("Spanish", "es", "es-ES", "es-MX", "es-GT", "es-CR", "es-PA", "es-DO", "es-VE", "es-CO", "es-PE", "es-AR", "es-EC", "es-CL", "es-UY", "es-PY", "es-BO", "es-SV", "es-SV", "es-HN", "es-NI", "es-PR");
            AddPresetLanguage("Arabic", "ar", "ar-SA", "ar-IQ", "ar-EG", "ar-LY", "ar-DZ", "ar-MA", "ar-TN", "ar-OM", "ar-YE", "ar-SY", "ar-JO", "ar-LB", "ar-KW", "ar-AE", "ar-BH", "ar-QA");
            AddPresetLanguage("German", "de", "de-DE", "de-CH", "de-AT", "de-LU", "de-LI");
            AddPresetLanguage("Korean", "ko", "ko-KR");
            AddPresetLanguage("French", "fr", "fr-FR", "fr-BE", "fr-CA", "fr-CH", "fr-LU", "fr-MC");
            AddPresetLanguage("Russian", "ru", "RU-ru");
            AddPresetLanguage("Japanese", "jp", "JP-jp", "ru", "RU-ru");
            AddPresetLanguage("Italian", "it", "it-IT", "it-CH");
            AddPresetLanguage("Other...");
        }

        // Draw the whole inspector
        public override void OnInspectorGUI()
        {
            var localization = (Localization)target;

            Validate(localization);

#if !UNITY_4 && !UNITY_5_0 && !UNITY_5_1 && !UNITY_5_2
            Undo.RecordObject(localization, "Localization settings");
#endif
            //EditorGUILayout.Separator();
            localization.Persistent = EditorGUILayout.Toggle( "Persistent ", localization.Persistent);

            //GUILayout.BeginVertical("GroupBox"/*EditorStyles.helpBox*/);
            //EditorGUI.indentLevel += 1;
            //BeginSingleton(localization);

            EditorGUILayout.Separator();

            DrawCurrentLanguage(localization);

            EditorGUILayout.Separator();

            EditorGUILayout.PropertyField(serializedObject.FindProperty("DefaultLanguage") );

            EditorGUILayout.Separator();

            DrawLanguages(localization);

            EditorGUILayout.Separator();
            EditorGUILayout.Separator();

            DrawCultures(localization);

            EditorGUILayout.Separator();
            EditorGUILayout.Separator();

            DrawKeys(localization);

            EditorGUILayout.Separator();

            DrawFileLoadAndSaver(localization);

            EditorGUILayout.Separator();

            //EditorGUI.indentLevel -= 1;
            //GUILayout.EndVertical();
            //EndSingleton(localization);


            // Update if dirty?
            if (serializedObject != null && serializedObject.ApplyModifiedProperties() == true || dirty == true)
            {
                dirty = false;

                localization.UpdateTranslations();

                SetDirty(localization);
            }
        }

        private void DrawFileLoadAndSaver(Localization localization)
        {
            filePathAndName = EditorGUILayout.TextField("Translations File", filePathAndName);
            EditorGUILayout.BeginHorizontal(GUILayout.ExpandWidth(true));

            if (GUILayout.Button("Import.XLSX"))
            {

                string excelPath = EditorUtility.OpenFilePanel(
                    "Choose a .xlsx Excel File from where read",
                        Application.dataPath, "xlsx");   // Find where we want to Load the file

                if ( string.IsNullOrEmpty(excelPath) ||  localization.LoadXLSX(excelPath) != true)
                    Debug.LogWarning("Excel Language File at: " + excelPath + " not Found!");

            }

            if (GUILayout.Button("Load TXT"))
            {
                //var path = System.IO.Path.Combine(Application.dataPath, "Resources/Languages.txt");
                if (string.IsNullOrEmpty(filePathAndName) || !System.IO.File.Exists(filePathAndName))
                {
                    filePathAndName = UnityEditor.EditorUtility.OpenFilePanel(
                        "Choose a txt Translation File from where read",
                        Application.dataPath, "txt");   // Find where we want to Load the file
                }

                if (localization.LoadTXT(filePathAndName) != true)
                    Debug.LogWarning("Language File at: " + filePathAndName + " not Found!");
            }
            if (GUILayout.Button("Save TXT"))
            {
                //var path = UnityEditor.EditorUtility.SaveFilePanelInProject("Export Text Asset", SourceLanguage, "txt", "");
                //var path = System.IO.Path.Combine(Application.dataPath, "Resources/Languages.txt");
                if (string.IsNullOrEmpty(filePathAndName) || !System.IO.File.Exists(filePathAndName))
                {
                    filePathAndName = UnityEditor.EditorUtility.SaveFilePanelInProject(
                        "Choose a txt Translation File from where to Save", "Languages",
                        "txt", "Chaque Tu Archivo!", Application.dataPath);

                }

                if (localization.SaveTXT(filePathAndName) != true)
                    Debug.LogWarning("Language File at: " + filePathAndName + " not Saved!");
            }
            EditorGUILayout.EndHorizontal();
        }

        private void DrawCurrentLanguage(Localization localization)
        {
            var labelA = Reserve();
            var valueA = Reserve(ref labelA, 38.0f);

            localization.CurrentLanguage = EditorGUI.TextField(labelA, "Current Language", localization.CurrentLanguage);

            if (GUI.Button(valueA, "List") == true)
            {
                var menu = new GenericMenu();
                //Undo.RecordObject(localization._currentLanguage, "Current Language");
                localization.UpdateTranslations();

                for (var i = 0; i < localization.CurrentLanguages.Count; i++)
                {
                    var language = localization.CurrentLanguages[i];

                    menu.AddItem(new GUIContent(language), localization.CurrentLanguage == language, () => { localization.CurrentLanguage = language; MarkAsModified(); });
                }

                if (menu.GetItemCount() > 0)
                {
                    menu.DropDown(valueA);
                }
                else
                {
                    Debug.LogWarning("Your scene doesn't contain any languages, so the language name list couldn't be created.");
                }
            }
        }

        private void DrawLanguages(Localization localization)
        {
            var labelA = Reserve();
            var valueA = Reserve(ref labelA, 35.0f);

            LangFolded = DrawCustomFold(labelA, LangFolded, "Languages", localization.Languages.Count);

            // Add a new language?
            if (GUI.Button(valueA, "Add") == true)
            {
                MarkAsModified();

                var menu = new GenericMenu();

                for (var i = 0; i < presetLanguages.Count; i++)
                {
                    var presetLanguage = presetLanguages[i];

                    menu.AddItem(new GUIContent(presetLanguage.Name), false, () => localization.AddLanguage(presetLanguage.Name, presetLanguage.CultureName));
                }

                menu.DropDown(valueA);
            }

            if (!LangFolded)
                return;

            EditorGUI.indentLevel += 1;

            existingLanguages.Clear();

            // Draw all added languages
            for (var i = 0; i < localization.Languages.Count; i++)
            {
                var language = localization.Languages[i];
                var labelB = Reserve();
                var valueB = Reserve(ref labelB, 20.0f);

                // Edit language name or remove
                if (languageIndex == i)
                {
                    BeginModifications();
                    {
                        localization.Languages[i] = EditorGUI.TextField(labelB, language);
                    }
                    EndModifications();

                    if (GUI.Button(valueB, "X") == true)
                    {
                        MarkAsModified();

                        localization.Languages.RemoveAt(i); languageIndex = -1;
                    }
                }

                // Expand language?
                if (EditorGUI.Foldout(labelB, languageIndex == i, languageIndex == i ? "" : language) == true)
                {
                    if (languageIndex != i)
                    {
                        languageIndex = i;
                        reverseIndex = -1;
                    }

                    EditorGUI.indentLevel += 1;
                    {
                        DrawReverse(localization, language);
                    }
                    EditorGUI.indentLevel -= 1;

                    EditorGUILayout.Separator();
                }
                else if (languageIndex == i)
                {
                    languageIndex = -1;
                    reverseIndex = -1;
                }

                // Already added?
                if (existingLanguages.Contains(language) == true)
                {
                    EditorGUILayout.HelpBox("This language already exists in the list!", MessageType.Warning);
                }
                else
                {
                    existingLanguages.Add(language);
                }
            }

            EditorGUI.indentLevel -= 1;
        }

        // Reverse lookup the phrases for this language
        private void DrawReverse(Localization localization, string language)
        {
            for (var i = 0; i < localization.Keys.Count; i++)
            {
                var phrase = localization.Keys[i];
                var labelA = Reserve();
                var translation = phrase.Translations.Find(t => t.Language == language);

                BeginModifications();
                {
                    EditorGUI.LabelField(labelA, phrase.Name);
                }
                EndModifications();

                if (translation != null)
                {
                    if (reverseIndex == i)
                    {
                        BeginModifications();
                        {
                            phrase.Name = EditorGUI.TextField(labelA, "", phrase.Name);
                        }
                        EndModifications();
                    }

                    if (EditorGUI.Foldout(labelA, reverseIndex == i, reverseIndex == i ? "" : phrase.Name) == true)
                    {
                        reverseIndex = i;

                        EditorGUI.indentLevel += 1;
                        {
                            DrawTranslation(translation);
                        }
                        EditorGUI.indentLevel -= 1;

                        EditorGUILayout.Separator();
                    }
                    else if (reverseIndex == i)
                    {
                        reverseIndex = -1;
                    }
                }
                else
                {
                    var valueA = Reserve(ref labelA, 120.0f);

                    if (GUI.Button(valueA, "Create Translation") == true)
                    {
                        MarkAsModified();

                        var newTranslation = new Translation(language);

                        //newTranslation.Language = language;
                        newTranslation.Text = phrase.Name;

                        phrase.Translations.Add(newTranslation);
                    }
                }
            }
        }

        private void DrawCultures(Localization localization)
        {
            var labelA = Reserve();
            var valueA = Reserve(ref labelA, 35.0f);

            CultFolded = DrawCustomFold(labelA, CultFolded, "Cultures", localization.Cultures.Count);

            // Add a new culture?
            if (localization.Languages.Count > 0)
            {
                if (GUI.Button(valueA, "Add") == true)
                {
                    MarkAsModified();

                    var menu = new GenericMenu();

                    for (var i = 0; i < localization.Languages.Count; i++)
                    {
                        var language = localization.Languages[i];

                        menu.AddItem(new GUIContent(language), false, () => { var culture = localization.AddCulture(language, ""); cultureIndex = localization.Cultures.IndexOf(culture); });
                    }

                    menu.DropDown(valueA);
                }
            }


            if (!CultFolded)
                return;
            EditorGUI.indentLevel += 1;

            // Draw all cultures
            for (var i = 0; i < localization.Languages.Count; i++)
            {
                var language = localization.Languages[i];

                if (localization.Cultures.Exists(c => c.Language == language) == true)
                {
                    var labelB = Reserve();
                    var valueB = Reserve(ref labelB, 35.0f);    // Super CultureKiller Button 

                    if (EditorGUI.Foldout(labelB, cultureIndex == i, language ) == true)
                    {
                        if (GUI.Button(valueB, "X") == true)
                        {
                            MarkAsModified();
                            //localization.Cultures.Clear();
                            localization.Cultures.RemoveAll(c => c.Language == language);
                            cultureIndex = 0;
                            break;
                        }

                        if (cultureIndex != i)
                        {
                            cultureIndex = i;
                        }

                        EditorGUI.indentLevel += 1;
                        {


                            for (var j = 0; j < localization.Cultures.Count; j++)
                            {
                                var culture = localization.Cultures[j];

                                if (culture.Language == language)
                                {
                                    DrawCulture(localization, culture, false);
                                }
                            }
                        }
                        EditorGUI.indentLevel -= 1;
                    }
                    else if (cultureIndex == i)
                    {
                        cultureIndex = -1;
                    }
                }
            }


            for (var i = 0; i < localization.Cultures.Count; i++)
            {
                var culture = localization.Cultures[i];
                var cultureLanguage = culture.Language;

                if (localization.Languages.Contains(cultureLanguage) == false)
                {
                    DrawCulture(localization, culture, true);
                }
            }

            EditorGUI.indentLevel -= 1;
        }

        private void DrawCulture(Localization localization, Localization.Culture culture, bool full)
        {
            var labelA = Reserve();
            var valueA = Reserve(ref labelA, 20.0f);

            BeginModifications();
            {
                //culture.Language = EditorGUI.TextField(labelA, "", culture.Name);
                culture.Alias = EditorGUI.TextField(labelA, "", culture.Alias);

                if (GUI.Button(valueA, "X") == true)
                {
                    MarkAsModified();

                    localization.Cultures.Remove(culture);

                    if (localization.Cultures.Exists(c => c.Language == culture.Language) == false)
                    {
                        cultureIndex = 0;
                    }
                }
            }
            EndModifications();
        }
        
        private void DrawKeys(Localization localization)
        {
            var labelA = Reserve();
            var valueA = Reserve(ref labelA, 35.0f);

            KeysFolded = DrawCustomFold(labelA, KeysFolded, "Keys", localization.Keys.Count);

            EditorGUI.LabelField(labelA, "Keys", EditorStyles.boldLabel);

            if (GUI.Button(valueA, "Add") == true)
            {
                MarkAsModified();

                var newPhrase = localization.AddKey("New Key");

                phraseIndex = localization.Keys.IndexOf(newPhrase);
            }

            if (!KeysFolded)
                return;

            EditorGUI.indentLevel += 1;

            existingKeys.Clear();

            for (var i = 0; i < localization.Keys.Count; i++)
            {
                var key = localization.Keys[i];
                var labelB = Reserve();
                var valueB = Reserve(ref labelB, 20.0f);

                if (phraseIndex == i)
                {
                    BeginModifications();
                    {
                        key.Name = EditorGUI.TextField(labelB, "", key.Name);
                    }
                    EndModifications();

                    if (GUI.Button(valueB, "X") == true)
                    {
                        MarkAsModified();

                        localization.Keys.RemoveAt(i); phraseIndex = -1;
                    }
                }

                if (EditorGUI.Foldout(labelB, phraseIndex == i, phraseIndex == i ? "" : key.Name) == true)
                {
                    if (phraseIndex != i)
                    {
                        phraseIndex = i;
                        translationIndex = -1;
                    }

                    EditorGUI.indentLevel += 1;
                    {
                        DrawTranslations(localization, key);
                    }
                    EditorGUI.indentLevel -= 1;

                    EditorGUILayout.Separator();
                }
                else if (phraseIndex == i)
                {
                    phraseIndex = -1;
                    translationIndex = -1;
                }

                if (existingKeys.Contains(key.Name) == true)
                {
                    EditorGUILayout.HelpBox("This phrase already exists in the list!", MessageType.Warning);
                }
                else
                {
                    existingKeys.Add(key.Name);
                }
            }

            EditorGUI.indentLevel -= 1;
        }

        private void DrawTranslations(Localization localization, Localization.phraseKey phrase)
        {
            existingLanguages.Clear();

            for (var i = 0; i < phrase.Translations.Count; i++)
            {
                var labelA = Reserve();
                var valueA = Reserve(ref labelA, 20.0f);
                var translation = phrase.Translations[i];

                if (translationIndex == i)
                {
                    BeginModifications();
                    {
                        translation.Language = EditorGUI.TextField(labelA, "", translation.Language);
                    }
                    EndModifications();

                    if (GUI.Button(valueA, "X") == true)
                    {
                        MarkAsModified();

                        phrase.Translations.RemoveAt(i); translationIndex = -1;
                    }
                }

                if (EditorGUI.Foldout(labelA, translationIndex == i, translationIndex == i ? "" : translation.Language) == true)
                {
                    translationIndex = i;

                    EditorGUI.indentLevel += 1;
                    {
                        DrawTranslation(translation);
                    }
                    EditorGUI.indentLevel -= 1;

                    EditorGUILayout.Separator();
                }
                else if (translationIndex == i)
                {
                    translationIndex = -1;
                }

                if (existingLanguages.Contains(translation.Language) == true)
                {
                    EditorGUILayout.HelpBox("This phrase has already been translated to this language!", MessageType.Warning);
                }
                else
                {
                    existingLanguages.Add(translation.Language);
                }

                if (localization.Languages.Contains(translation.Language) == false)
                {
                    EditorGUILayout.HelpBox("This translation uses a language that hasn't been set in the localization!", MessageType.Warning);
                }
            }

            for (var i = 0; i < localization.Languages.Count; i++)
            {
                var language = localization.Languages[i];

                if (phrase.Translations.Exists(t => t.Language == language) == false)
                {
                    var labelA = Reserve();
                    var valueA = Reserve(ref labelA, 120.0f);

                    EditorGUI.LabelField(labelA, language);

                    if (GUI.Button(valueA, "Create Translation") == true)
                    {
                        MarkAsModified();

                        var newTranslation = new Translation(language);

                        //newTranslation.Language = language;
                        newTranslation.Text = phrase.Name;

                        translationIndex = phrase.Translations.Count;

                        phrase.Translations.Add(newTranslation);
                    }
                }
            }
        }

        private void DrawTranslation(Translation translation)
        {
            BeginModifications();
            {
                EditorGUILayout.BeginHorizontal();
                {
                    EditorGUILayout.LabelField("Text", LabelStyle, GUILayout.Width(50.0f));

                    translation.Text = EditorGUILayout.TextArea(translation.Text);
                }
                EditorGUILayout.EndHorizontal();

                EditorGUILayout.BeginHorizontal();
                {
                    EditorGUILayout.LabelField("Object", LabelStyle, GUILayout.Width(50.0f));

                    translation.Object = EditorGUILayout.ObjectField(translation.Object, typeof(Object), true);
                }
                EditorGUILayout.EndHorizontal();
            }
            EndModifications();
        }

        private bool DrawCustomFold(Rect pos, bool foldState, string label, int totalItems)
        {
            var tempFold = foldState;       
            GUIStyle style = EditorStyles.foldout;
            FontStyle previousStyle = style.fontStyle;
            style.fontStyle = FontStyle.Bold;

            if (totalItems > 0)         // If We have 1 item at least Draw fold Arrow else just only a Label               
                tempFold = EditorGUI.Foldout(pos, foldState, label, true, style);
            else
                EditorGUI.LabelField(pos, label, EditorStyles.boldLabel);
            style.fontStyle = previousStyle;

            return tempFold;
        }

        private static Rect Reserve(ref Rect rect, float rightWidth = 0.0f, float padding = 2.0f)
        {
            if (rightWidth == 0.0f)
            {
                rightWidth = rect.width - EditorGUIUtility.labelWidth;
            }

            var left = rect; left.xMax -= rightWidth;
            var right = rect; right.xMin = left.xMax;

            left.xMax -= padding;

            rect = left;

            return right;
        }

        private static Rect Reserve(float height = 16.0f)
        {
            var rect = EditorGUILayout.BeginVertical();
            {
                EditorGUILayout.LabelField("", GUILayout.Height(height));
            }
            EditorGUILayout.EndVertical();

            return rect;
        }

        private static void SetDirty(Object target)
        {
            EditorUtility.SetDirty(target);

#if UNITY_4 || UNITY_5_0 || UNITY_5_1 || UNITY_5_2
			EditorApplication.MarkSceneDirty();
#elif UNITY_5_5
            //UnityEditor.SceneManagement.EditorSceneManager.MarkSceneDirty(UnityEditor.SceneManagement.EditorSceneManager.GetActiveScene());
#else
            UnityEditor.SceneManagement.EditorSceneManager.MarkAllScenesDirty();
#endif
        }

        private void MarkAsModified()
        {
            // Uneeded from UNITY_5_4 on...
            dirty = true;
        }

        private void BeginModifications()
        {
            EditorGUI.BeginChangeCheck();
        }

        private void EndModifications()
        {
            if (EditorGUI.EndChangeCheck() == true)
            {
                dirty = true;
            }
        }

        void OnEnable()
        {
            filePathAndName = System.IO.Path.Combine(Application.dataPath, "Resources\\Languages.txt");
            //filePathAndName = Application.dataPath + "Resources\\Languages.txt";
        }

        private static void AddPresetLanguage(string name, params string[] cultureNames)
        {
            var presetLanguage = new PresetLanguage();

            presetLanguage.Name = name;
            presetLanguage.CultureName = cultureNames;

            presetLanguages.Add(presetLanguage);
        }

        private static void Validate(Localization localization)
        {
            if (localization.Languages == null) localization.Languages = new List<string>();
            if (localization.Cultures == null) localization.Cultures = new List<Localization.Culture>();
            if (localization.Keys == null) localization.Keys = new List<Localization.phraseKey>();

            for (var i = localization.Cultures.Count - 1; i >= 0; i--)
            {
                var culture = localization.Cultures[i];

                if (culture == null)
                {
                    culture = new Localization.Culture();

                    culture.Language = "New Language";
                    culture.Alias = "new-Alias";

                    localization.Cultures[i] = culture;
                }
            }

            for (var i = localization.Keys.Count - 1; i >= 0; i--)
            {
                var phrase = localization.Keys[i];

                if (phrase.Translations == null)
                {
                    phrase.Translations = new List<Translation>();
                }
            }
        }
        
        [UnityEditor.MenuItem("GameObject/Sun/Localization", false, 1)]
        public static void CreateLocalization()
        {
            if (GameObject.FindObjectOfType<Localization>() == null)
            {
                var gameObject = UnityEditor.Selection.activeGameObject;
                if (gameObject == null)
                {
                    gameObject = new GameObject(typeof(Localization).Name); // typeof(Sun.Audio).Name
                    UnityEditor.Selection.activeGameObject = gameObject;
                }
                UnityEditor.Undo.RegisterCreatedObjectUndo(gameObject, "Create Localization Manager");
                gameObject.AddComponent<Localization>();//  .OnEnable();
            }
            else
            {
                Debug.LogError("Localization already added to the scene.");
            }
        }

        //public void BeginSingleton(Localization localization)
        //{
        //    EditorGUILayout.Separator();

        //    GUILayout.BeginHorizontal();

        //    EditorGUILayout.LabelField("Persistent ", GUILayout.MaxWidth(128));

        //    localization.Persistent = EditorGUILayout.Toggle(localization.Persistent);

        //    GUILayout.EndHorizontal();

        //    GUILayout.BeginVertical("GroupBox");

        //    EditorGUI.indentLevel += 1;
        //}

        //public void EndSingleton(Localization localization)
        //{
        //    EditorGUI.indentLevel -= 1;

        //    GUILayout.EndVertical();

        //    GUILayout.BeginHorizontal();

        //    EditorGUILayout.LabelField("Default File Name ", GUILayout.MaxWidth(128));

        //    localization.DefaultFileName =
        //        EditorGUILayout.TextField(localization.DefaultFileName);

        //    if (GUILayout.Button("Load", EditorStyles.miniButton, GUILayout.MaxWidth(64)))
        //        localization.LoadFromStreamingAssets();

        //    if (GUILayout.Button("Save", EditorStyles.miniButton, GUILayout.MaxWidth(64)))
        //        localization.Save();

        //    GUILayout.EndHorizontal();

        //    EditorGUILayout.Separator();
        //}
    }
}