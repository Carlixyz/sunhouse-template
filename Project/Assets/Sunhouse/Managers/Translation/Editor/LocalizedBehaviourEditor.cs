﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using System.Linq;

namespace Sun.Local
{
    //[CustomEditor(typeof(LocalizedBehaviour))]
    [CustomEditor(typeof(LocalizedBehaviour), true)]
     public class LocalizedBehaviourEditor : Editor
    {
        // you don't even need to override OnInspectorGUI!

        public List<string> filteredStringNames = null;
        public Localization localizationManager = null;
        public LocalizedBehaviour localizedBehaviour = null;
        private SerializedObject lbserialized = null;

        public string[] allStringNames = null;
        string searchFilter = "";
        string selectedString;

        Vector2 listScroll = Vector2.zero;
        int _leftBarWidth = 200;
        int minLeftBarWidth = 150;

        static bool FoldOut = true;

        int leftBarWidth
        {
            get { return _leftBarWidth; }
            set { _leftBarWidth = Mathf.Max(value, minLeftBarWidth); }
        }

        public bool Contains(string s, string text) { return s.ToLower().IndexOf(text.ToLower()) != -1; }

        void FilterStrings()
        {
            filteredStringNames = new List<string>(allStringNames.Length);
            if (searchFilter.Length == 0)
                filteredStringNames = (from data in allStringNames orderby data select data).ToList();
            else
                filteredStringNames = (from data in allStringNames where Contains(data, searchFilter) orderby data select data).ToList();
        }

        public void Initialize()
        {
            if (localizationManager == null)
                localizationManager = Object.FindObjectOfType<Localization>();

            if (localizedBehaviour == null)
                localizedBehaviour = (LocalizedBehaviour)target;

            if (lbserialized == null)
                lbserialized = new SerializedObject(target);

            if (allStringNames == null)
            {
                //allStringNames = Sun.Utils.EnumNames<MotinStrings>();
                allStringNames = localizationManager.CurrentKeys.ToArray();
            }
            ////selectedMotinString = stringComponent.motinStringName;

            //if (stringComponent.motinStringName.Length == 0)
            //{
            //    stringComponent.motinStringName = ((MotinStrings)0).ToString();
            //}
            //selectedString = stringComponent.motinStringName;
            FilterStrings();
        }

        void DrawKey(LocalizedBehaviour lBehaviour)
        {
            bool newForceUpperCase = EditorGUILayout.Toggle("Force Upper Case", lBehaviour.ForceUpperCase);
            if (newForceUpperCase != lBehaviour.ForceUpperCase)
            {
                lBehaviour.ForceUpperCase = newForceUpperCase;
                //lBehaviour.UpdateLocalization();
                localizationManager.UpdateTranslations();
                SceneView.RepaintAll();
            }

            GUILayout.BeginHorizontal();

            //localizedBehaviour.KeyName =
            //      EditorGUILayout.TextField(string.Format("Key Phrase Name"), localizedBehaviour.KeyName);

            var keyProp = lbserialized.FindProperty("keyName");
            EditorGUILayout.PropertyField(keyProp);
            //EditorGUILayout.PropertyField( lbserialized.FindProperty("keyName") );

            //keyProp.stringValue = EditorGUI.TextField(left, label.text, property.stringValue, EditorStyles.boldLabel);

            if (GUILayout.Button( "List", EditorStyles.popup, GUILayout.MaxWidth(48)) == true)
            {
                var menu = new GenericMenu();

                for (var i = 0; i < localizationManager.CurrentKeys.Count; i++)
                {
                    var phraseName = localizationManager.CurrentKeys[i];

                    menu.AddItem(new GUIContent(phraseName), keyProp.stringValue == phraseName, () =>
                    { keyProp.stringValue = phraseName; keyProp.serializedObject.ApplyModifiedProperties(); });
                }

                if (menu.GetItemCount() > 0)
                {
                    menu.ShowAsContext();
                    //menu.DropDown(right);
                }
                else
                {
                    Debug.LogWarning("Your scene doesn't contain any key phrases, so the phrase name list couldn't be created.");
                }
            }

            GUILayout.EndHorizontal();
        }

        void DrawSearcher()
        {
            if (allStringNames != null)
            {
                string newSearchFilter =
                    GUILayout.TextField( searchFilter, GUI.skin.FindStyle("ToolbarSeachTextField"), 
                    /*GUILayout.MinWidth( 350)*/ GUILayout.ExpandWidth(true));
                if (newSearchFilter != searchFilter)
                {
                    searchFilter = newSearchFilter;
                    FilterStrings();
                }

                if (searchFilter.Length > 0)
                {
                    if (GUILayout.Button("", GUI.skin.FindStyle("ToolbarSeachCancelButton")))
                    {
                        searchFilter = "";
                        FilterStrings();
                        FoldOut = false;
                    }
                }
                else
                {
                    GUILayout.Label("", GUI.skin.FindStyle("ToolbarSeachCancelButtonEmpty"));
                }
            }
        }

        void DrawList(LocalizedBehaviour lBehaviour)
        {
            listScroll = GUILayout.BeginScrollView(listScroll, GUILayout.ExpandWidth(true), GUILayout.MinHeight(200));
            GUILayout.BeginVertical(GUILayout.ExpandWidth(true), GUILayout.ExpandHeight(true));
            foreach (string data in filteredStringNames)
            {
                // 0 length name signifies inactive clip
                if (data.Length == 0) continue;

                bool selected = selectedString == data;
                //bool newSelected = GUILayout.Toggle(selected, data, EditorStyles.miniLabel, GUILayout.ExpandWidth(true));
                //bool newSelected = GUILayout.Toggle(selected, data, EditorStyles.centeredGreyMiniLabel, GUILayout.ExpandWidth(true));
                //bool newSelected = GUILayout.Toggle(selected, data, EditorStyles.miniButton, GUILayout.ExpandWidth(true));
                //bool newSelected = GUILayout.Toggle(selected, data, GUI.skin.FindStyle("Toolbar", GUILayout.ExpandWidth(true)) );
                //bool newSelected = GUILayout.Toggle(selected, data, GUI.skin.FindStyle("toolbarbutton"), GUILayout.ExpandWidth(true));
                //bool newSelected = GUILayout.Toggle(selected, data, GUI.skin.FindStyle("Tag MenuItem"), GUILayout.ExpandWidth(true));
                //bool newSelected = GUILayout.Toggle(selected, data, (GUIStyle)"PR Label", GUILayout.ExpandWidth(true));
                //bool newSelected = GUILayout.Toggle(selected, data, (GUIStyle)"ObjectPickerResultsEven", GUILayout.ExpandWidth(true));
                bool newSelected = GUILayout.Toggle(selected, data, (GUIStyle)"IN ThumbnailSelection", GUILayout.ExpandWidth(true));
                //bool newSelected = GUILayout.Toggle(selected, data, (GUIStyle)"MenuItemMixed", GUILayout.ExpandWidth(true));
                if (newSelected != selected && newSelected == true)
                {
                    selectedString = data;
                    lBehaviour.KeyName = selectedString;
                    GUIUtility.keyboardControl = 0;
                    EditorUtility.SetDirty(lBehaviour);
                    AssetDatabase.SaveAssets();
                    Repaint();
                }
            }

            GUILayout.EndVertical();
            GUILayout.EndScrollView();
        }

        void DrawUpdateFallback()
        {
            localizationManager.UpdateTranslations();

            var fallProp = lbserialized.FindProperty("FallBack");
            
            if (fallProp.propertyType == SerializedPropertyType.String)  //if (fallProp.type == "string")    
            {
                fallProp.stringValue = localizationManager.GetTranslationText(localizedBehaviour.KeyName);
                if (localizedBehaviour.ForceUpperCase)
                    fallProp.stringValue = fallProp.stringValue.ToUpper();
            // Dev.Log("Update FallBack txt " + text + " - " + fallProp.stringValue);
            }
            else
            {
                fallProp.objectReferenceValue = 
                    localizationManager.GetTranslationObject(localizedBehaviour.KeyName);
                EditorGUILayout.PropertyField(fallProp);
            }
            //Dev.Log("Update FallBack txt " + text + " - " + fallProp.objectReferenceValue);
            //localizedBehaviour.UpdateLocalization();


        }

        public override void OnInspectorGUI()
        {
            Initialize();

            lbserialized.Update();
            
            GUILayout.Space(16);

            //base.DrawDefaultInspector();

            DrawKey(localizedBehaviour);

            GUILayout.Space(4);

            GUILayout.BeginHorizontal();

            if (GUILayout.Button("", EditorStyles.foldout, GUILayout.MaxWidth(16))) // StaticDropdown
                FoldOut = !FoldOut;

            DrawSearcher();
            FoldOut = (searchFilter.Length > 0 ? true : FoldOut);
            
            GUILayout.EndHorizontal();

            if (FoldOut)
            {
                var prop = lbserialized.FindProperty("keyName");
                selectedString = prop.stringValue;
                DrawList(localizedBehaviour);
            }

            GUILayout.Space(16);

            EditorGUILayout.PropertyField( lbserialized.FindProperty("FallBack") , true);

            if (GUILayout.Button("Update Fallback"))
                DrawUpdateFallback();

            GUILayout.Space(8);



            lbserialized.ApplyModifiedProperties();
              
        }
    }
}


