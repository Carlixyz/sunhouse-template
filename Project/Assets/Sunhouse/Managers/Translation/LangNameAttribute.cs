﻿using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;

namespace Sun.Local
{

    [CustomPropertyDrawer(typeof(LangAttribute))]
    public class LanguageNameDrawer : PropertyDrawer
    {
        Localization localization;

        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            var left = position; left.xMax -= 40;
            var right = position; right.xMin = left.xMax + 2;

            //EditorStyles.label.fontStyle = FontStyle.Bold;

            var origFontStyle = EditorStyles.label.fontStyle;
            EditorStyles.label.fontStyle = FontStyle.Bold;

            EditorGUI.PropertyField(left, property);
            //EditorGUI.LabelField(left, label.text, property.stringValue, EditorStyles.boldLabel);

            EditorStyles.label.fontStyle = origFontStyle;

            //EditorStyles.label.fontStyle = FontStyle.Normal;


            if (GUI.Button(right, "List") == true)
            {
                var menu = new GenericMenu();

                if (localization == null)
                    localization = Object.FindObjectOfType<Localization>();

                for (var i = 0; i < localization.CurrentLanguages.Count; i++)
                {
                    var language = localization.CurrentLanguages[i];
                    menu.AddItem(new GUIContent(language), property.stringValue == language, () => { property.stringValue = language; property.serializedObject.ApplyModifiedProperties(); });
                }

                if (menu.GetItemCount() > 0)
                {
                    menu.DropDown(right);
                }
                else
                {
                    Debug.LogWarning("Your scene doesn't contain any languages, so the language name list couldn't be created.");
                }
            }
        }
    }


}
#endif


//namespace Sun
namespace Sun.Local
{
    // This attribute allows you to select a language from all the localizations in the scene
    public class LangAttribute : PropertyAttribute
    {
    }

}