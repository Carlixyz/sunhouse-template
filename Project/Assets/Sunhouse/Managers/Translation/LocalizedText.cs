﻿using UnityEngine;
using UnityEngine.UI;

namespace Sun.Local
{
    // This component will update a Text component with localized text, or use a fallback if none is found
    [ExecuteInEditMode]
    [DisallowMultipleComponent]
    [RequireComponent(typeof(Text))]
    [AddComponentMenu("Sun/Localized Text")]
    public class LocalizedText : LocalizedBehaviour
    {
        [Tooltip("If Phrase KeyName couldn't be found, this text will be used")]
        public string FallBack;

        // This gets called every time the translation needs updating
        public override void UpdateTranslation(Translation translation)
        {
            // Get the Text component attached to this GameObject
            var text = GetComponent<Text>();

            // Use translation?
            if (translation != null)
            {
                text.text = translation.Text;
            }
            // Use fallback?
            else
            {
                text.text = FallBack;
            }

            if (ForceUpperCase)
                text.text = text.text.ToUpper();
        }

        protected virtual void Awake()
        {
            // Should we set FallbackText?
            if (string.IsNullOrEmpty(FallBack) == true)
            {
                // Get the Text component attached to this GameObject
                var text = GetComponent<Text>();
                // Copy current text to fallback
                FallBack = text.text;
            }
        }
    }
}