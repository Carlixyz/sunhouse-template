﻿using UnityEngine;
using UnityEngine.Serialization;

namespace Sun.Local
{
    // This component simplifies the updating process, extend it if you want to cause a specific object to get localized
    [ExecuteInEditMode]
    public abstract class LocalizedBehaviour : MonoBehaviour
    {
        [Tooltip("The name of the phrase we want to use for this localized component")]
        //[phraseKeyAttribute]
        [SerializeField]
        [FormerlySerializedAs("KeyName")]
        private string keyName;

        // This property is used to set the phraseName from code
        public string KeyName
        {
            set
            {
                // phraseName changed?
                if (value != keyName)
                {
                    // Update localization with new phrase
                    keyName = value;
                    UpdateLocalization();
                }
            }
            get
            {
                return keyName;
            }
        }

        public bool ForceUpperCase = false; // used only in text cases

        // This gets called every time the translation needs updating
        // NOTE: translation may be null if it can't be found
        public abstract void UpdateTranslation(Translation translation);

        // Call this to force the behaviour to get updated
        public void UpdateLocalization()
        {
            UpdateTranslation(Localization.Get.GetTranslation(keyName));
            //UpdateTranslation(Localization.GetTranslation(keyName));
        }

        protected virtual void OnEnable()
        {
            Localization.OnLocalizationChanged += UpdateLocalization;

            UpdateLocalization();
        }

        protected virtual void OnDisable()
        {
            Localization.OnLocalizationChanged -= UpdateLocalization;
        }
        
#if UNITY_EDITOR
        // This gets called from the inspector when changing 'phraseName'
        protected virtual void OnValidate()
        {
            if (isActiveAndEnabled == true)
            {
                UpdateLocalization();
            }
        }
#endif
    }
}