﻿#define XLS
#if UNITY_EDITOR && XLS
using OfficeOpenXml;    // EPPlus.dll
#endif

using UnityEngine;
using System.Collections.Generic;
using System.IO;


namespace Sun 
{
    [System.Serializable]
    public class Translation                    // data about the keys after translation to a specific language
    {
        public string Language;                 // The language of this translation
        public string Text;                     // The text finally translated 
        public Object Object;                   // Generic translated object (e.g. language specific texture)
        public Translation(string lang) { Language = lang; }// Constructor   
    }


    /// <summary>
    /// Localization System for all kind of translations 
    /// Can Load Data From an Excel .xlsx and Load/Save in custom .txt files 
    /// (combinable with textures, fonts, audio translations .cs files)
    /// </summary>

    //[ExecuteInEditMode]
    [PrefabAttribute("Localization")]
    [AddComponentMenu("Sun/Localization Manager")]
    [DisallowMultipleComponent, System.Serializable] //, AddComponentMenu("Sun/Localization Manager")]
    public class Localization : Singleton<Localization>
    {

        [System.Serializable]
        public class phraseKey                  // data about each Key to be translated into different languages
        {
            public string Name;                 // The name/description of this Key
            public List<Translation> Translations = new List<Translation>();    // All Key's translations
            public Translation FindTranslation(string language) //Find a translation or return null   
            {
                return Translations.Find(t => t.Language == language);
            }
            public Translation AddTranslation(string language)  // Add new translation or return current one
            {
                var translation = FindTranslation(language);
                if (translation == null)                            // Add it?
                    Translations.Add(translation = new Translation(language));

                return translation;
            }
        }


        [System.Serializable]
        public class Culture                    // information about an alias PostFix for a language
        {
            public string Language;             // The language of this culture PostFix 
            public string Alias;                // The list of all culture names for this language    
        }


        [Local.LangAttribute]
        public string DefaultLanguage ;
        public List<string> Languages;                              // A list of all languages defined  
        public List<Culture> Cultures;                              // A list of all cultures defined 
        public List<phraseKey> Keys;                                // A list of all Keys defined 

        private string       _currentLanguage;                      // The currently set language
        private List<string> _currentLanguages = new List<string>();//All current languages
        private List<string> _currentKeys = new List<string>();     //All current Keys
        private Dictionary<string, Translation> _currentTranslations =   
                    new Dictionary<string, Translation>();          //All Keys name with their Translations

        //----------------------------------------------------------------------------------------//			

        public string CurrentLanguage                               // Change the current language ?
        {
            set
            {
                if (CurrentLanguage != value)
                {
                    _currentLanguage = value;
                    UpdateTranslations();
                }
            }
            get
            {
                if (string.IsNullOrEmpty(_currentLanguage))
                    _currentLanguage = DefaultLanguage;
                return _currentLanguage;
            }
        }

        public void SetCurrentLanguage(string newLanguage)
        {
            CurrentLanguage = newLanguage;
        }

        public static System.Action OnLocalizationChanged;          // on any language or translations change

        public List<string> CurrentLanguages                        // languages that you can switch between     
        {
            get
            {
                return _currentLanguages;
            }
        }

        public List<string> CurrentKeys                             // Keys that you can switch between     
        {
            get
            {
                return _currentKeys;
            }
        }

        //----------------------------------------------------------------------------------------//			

        public Translation GetTranslation(string keyName, string langName)  // Get any Language's translation  
        {
            var translation = default(Translation);

            if (_currentKeys != null && keyName != null)
                translation = Keys.Find(k => k.Name == keyName).FindTranslation(langName);

            return translation;                             // or return null 
        }

        public Translation GetTranslation(string keyName)           // Get the translation of this key   
        {
            var translation = default(Translation);

            if (_currentTranslations != null && keyName != null )
            {
                _currentTranslations.TryGetValue(keyName, out translation);
            }

            return translation;                             // or return null 
        }
        
        public string GetTranslationText(string keyName)            // Get current text for this key
        {
            //return (GetTranslation(keyName)).Text;
            var translation = GetTranslation(keyName);
            if (translation != null)
                return translation.Text;

            return null;                                  // or return null         
        }

        public Object GetTranslationObject(string keyName)          // Get current Obj for this key 
        {
            var translation = GetTranslation(keyName);
            if (translation != null)
                return translation.Object;

            return null;                                    // or return null

        }

        //----------------------------------------------------------------------------------------//			

        public void AddLanguage(string language, string[] aliases = null)   // Add a new language to this localization
        {
            if (Languages == null) Languages = new List<string>();

            if (!Languages.Contains(language))              // Add this language to list ?
                Languages.Add(language);

            if (aliases != null && aliases.Length > 0)      // Add cultures to list ?     
                for (var i = 0; i < aliases.Length; i++)
                    AddCulture(language, aliases[i]);
        }

        public Culture AddCulture(string language, string alias)            // Add new Culture PostFix?
        {
            if (Cultures == null) Cultures = new List<Culture>();

            var culture = Cultures.Find(c => c.Language == language && c.Alias == alias);
            if (culture == null)
            {
                culture = new Culture();
                culture.Language = language;
                culture.Alias = alias;

                Cultures.Add(culture);
            }
            return culture;
        }

        public phraseKey AddKey(string keyName)                 // Add a new Key or return current one
        {
            if (Keys == null) Keys = new List<phraseKey>();

            var newKey = Keys.Find(k => k.Name == keyName);
            if (newKey == null)
            {
                newKey = new phraseKey();
                newKey.Name = keyName;
                Keys.Add(newKey);
            }

            return newKey;
        }

        public Translation AddTranslation(string language, string keyName)
        {
            AddLanguage(language);

            return AddKey(keyName).AddTranslation(language);
        }

        public void UpdateTranslations()
        {
        // rebuilds the dictionary for quickly map keys to translations of current language
            _currentTranslations.Clear();
            _currentLanguages.Clear();
            _currentKeys.Clear();
                
            if (Keys != null)                                   // Add all keys to currentTranslations      
                for (var j = Keys.Count - 1; j >= 0; j--)
                {
                    var key = Keys[j];
                    var keyName = key.Name;

                    if (CurrentKeys.Contains(keyName) == false)
                        CurrentKeys.Add(keyName);
                                                                // check this key hasn't already been added
                    if (_currentTranslations.ContainsKey(keyName) == false)
                    {                                           // Find the translation for this key
                        var translation = key.FindTranslation(_currentLanguage);

                        if (translation != null)                // If it exists, add it     
                            _currentTranslations.Add(keyName, translation);
                    }
                }
                
            if ( Languages != null)                             // Add all languages to currentLanguages 
                for (var j =  Languages.Count - 1; j >= 0; j--)
                {
                    var language = Languages[j];
                    if (_currentLanguages.Contains(language) == false)
                        _currentLanguages.Add(language);
                }

            if (OnLocalizationChanged != null)              // Notify changes?
                OnLocalizationChanged();
        }

        public virtual void OnEnable()
        {

            if (string.IsNullOrEmpty(_currentLanguage) == true)     // need to set an initial language?       
            {
                // Use default
                _currentLanguage = DefaultLanguage;         

                //AddLanguage(_currentLanguage);

                if (Cultures != null)                       // Set language from culture?
                {
                    var cultureInfo = System.Globalization.CultureInfo.CurrentCulture;
                    var cultureName = cultureInfo.Name;
                    for (int i = Cultures.Count - 1; i >= 0; i--)
                    {
                        var culture = Cultures[i];
                        if (culture != null && culture.Alias == cultureName)
                        {
                            _currentLanguage = culture.Language;
                            break;
                        }
                    }
                }
//#if UNITY_EDITOR
//                _currentLanguage = DefaultLanguage;
//#endif
            }
            UpdateTranslations();
        }

        public virtual void OnDisable()
        {
            UpdateTranslations();
        }

#if UNITY_EDITOR
        void AddKeyFromFile(string languageName, string keyName, string keyTranslation)
        {
            // Find or add the translation for this last key
            var translation = AddTranslation(languageName, keyName);
            // Set the translation text for this key (and Replace line markers with actual newlines)
            translation.Text = keyTranslation.Replace(newlineString, System.Environment.NewLine);
        }


        public virtual void OnValidate()                        // Inspector modified?        
        {
            UpdateTranslations();
        }

        char[] newlineCharacters    = { '\r', '\n' };
        string newlineString        = "\\n";
        string oBracket             = "[";                                  // Open/Close Bracket String
        string cBracket             = "]";

        /// This will load All localizations from a file with this custom format:
        /// [Language Name]
        /// Key Name Here = Translation Here     // Optional Comment Here
        /// [Current = Language Name2]
        public bool LoadTXT(string filePath)
        {
            if (!File.Exists(filePath))                         // if(file == null) // file as TextAsset 
                return false;

            Debug.Log( filePath);

            string FileText = File.ReadAllText(filePath, System.Text.Encoding.Default);
            // Remove all text previous to the first '[' occurence...   & split between Languages  
            string[] langGroups = FileText.Substring(FileText.IndexOf(oBracket) + 1).Trim()
                .Split(oBracket.ToCharArray(), System.StringSplitOptions.RemoveEmptyEntries);

            if (langGroups == null )                            // if(langGroups.Length == 0)           
                return false;

            foreach (var lang in langGroups)                    //for (int i = 0; i < langGroups.Length; i++)         
            {                                                    
                var lines = lang.Split(newlineCharacters, System.StringSplitOptions.RemoveEmptyEntries);

                string langName         = "";
                string keyName          = "";
                string keyTranslation   = "";

                var cBracketIndex = lines[0].IndexOf(cBracket); // Get Language Name(asumed in 1st line)
                if (cBracketIndex != -1)
                    langName = lines[0].Substring(0, cBracketIndex).Trim();

                var currentIndex = langName.IndexOf("=");       // & check if it's the current lang
                if (currentIndex != -1)                          
                    SetCurrentLanguage(langName = langName.Substring(currentIndex + 1).Trim()); // Debug.Log("Current Language setup: " + langName);

                for (int j = 1; j < lines.Length; j++)          // foreach (var line in lines)
                {
                    var commentIndex = lines[j].IndexOf("//");  // Does have a comment? Trim it 
                    if (commentIndex != -1)
                        lines[j] = lines[j].Substring(0, commentIndex).Trim();

                    var equalsIndex = lines[j].IndexOf('=');
                    if (equalsIndex != -1)                      // Check if there's a new/Different Key  
                    {
                        if (!string.IsNullOrEmpty(keyName))     // There was a previous key, Process it
                            AddKeyFromFile(langName, keyName, keyTranslation);
                                                                // Get new Key & continue
                        keyName = lines[j].Substring(0, equalsIndex).Trim();
                        keyTranslation = lines[j].Substring(equalsIndex + 1).Trim();
                    }
                    else if (!string.IsNullOrEmpty(lines[j]))   // NO '=' Key,(Asumed Still same Key)
                    {
                        keyTranslation += newlineString + lines[j];         // Add line & continue
                    }
                } 

                if (!string.IsNullOrEmpty(keyName))             // Still one last key, Finish it
                    AddKeyFromFile(langName, keyName, keyTranslation);
            }
            UpdateTranslations();

            return true;
        }

        public bool SaveTXT(string filePath)
        {
            if (!File.Exists(filePath))                         // if(file == null) // file as TextAsset 
                Debug.Log("Creating File " + filePath);
                                                                //    return false;

                using (StreamWriter sWriter = new StreamWriter( filePath, false, System.Text.Encoding.UTF8) )
            {
                foreach(var Lang in Languages)                  // Find where we want to save the file
                {
                    if (_currentLanguage == Lang)
                        sWriter.WriteLine("[Current = {0}]", Lang);  // if Current Language Mark it 
                    else
                        sWriter.WriteLine( "[{0}]", Lang );    

                    foreach (var key in Keys)   // if Current == 
                    {
                        sWriter.WriteLine( "{0} = {1}", key.Name, key.FindTranslation(Lang).Text );    
                    }

                    sWriter.WriteLine(System.Environment.NewLine);
                }
            }

            UnityEditor.AssetDatabase.Refresh();

            return true;
        }

#if XLS
        public bool LoadXLSX(string filePath)                   // Import from an Excel's .XLSX File 
        {
                                                                // (needs an EPPlus.dll library)
            if (!File.Exists(filePath))                          
                return false;

            FileInfo file = new FileInfo(filePath);
            ExcelPackage ep = new ExcelPackage(file);
            ExcelWorkbook wb = ep.Workbook;

            for (int i = 1; i <= wb.Worksheets.Count; i++)
            {
                ExcelWorksheet sheet = wb.Worksheets[i];

                string TableName = sheet.Name;
                int totalRows = sheet.Dimension.Rows;
                int totalColumns = sheet.Dimension.Columns;

                for (int row = 2; row <= totalRows; row++)
                {
                    if (sheet.Cells[row, 1].Value == null )
                        continue;

                    string keyName = sheet.Cells[row, 1].Value.ToString();

                    for (int column = 2; column <= totalColumns; column++)
                    {
                        if (sheet.Cells[1, column].Value == null || sheet.Cells[row, column].Value == null)
                            continue;

                        string langName = sheet.Cells[1, column].Value.ToString();
                        string keyTranslation = sheet.Cells[row, column].Value.ToString();

                        AddKeyFromFile(langName, keyName, keyTranslation);
                    }
                }
            }

            UpdateTranslations();

            return true;

            //Excelent.Excel xls = Excelent.Excel.Load(filePath); //xls.ShowLog();

            //foreach(var table in xls.Tables)
            //{
            //    Dev.Log("Loading Excel table: " + table.TableName);
            //    //table.ShowLog();
            //    for (int rowIndex = 2; rowIndex <= table.NumberOfRows; rowIndex++)
            //    {
            //        for (int colIndex = 2; colIndex <= table.NumberOfColumns; colIndex++)
            //        {
            //            string keyName = table.GetValue(rowIndex, 1).ToString();
            //            string langName = table.GetValue(1, colIndex).ToString();
            //            string keyTranslation = table.GetValue(rowIndex, colIndex).ToString();
            //            if( !string.IsNullOrEmpty(keyName) && 
            //                !string.IsNullOrEmpty(langName) &&
            //                !string.IsNullOrEmpty(keyTranslation) )
            //            {
            //                AddKeyFromFile(langName, keyName, keyTranslation);
            //            }
            //        }
            //    }
            //}
        }
#else
        public bool LoadXLSX(string filePath) { return false; }
#endif



#endif

    }   // Localization

}   // NameSpace

//namespace Sun.Local
//{
//    // This attribute allows you to select a language from all the localizations in the scene
//    public class KeyAttribute : PropertyAttribute { }


//    // This attribute allows you to select a language from all the localizations in the scene
//    public class LangAttribute : PropertyAttribute { }

//}