﻿using UnityEngine;


namespace Sun.Local
{
    // This component will update an AudioSource component with localized text, or use a fallback if none is found
    [ExecuteInEditMode]
    [DisallowMultipleComponent]
    [RequireComponent(typeof(AudioSource))]
    [AddComponentMenu("Sun/Localized Audio")]
    public class LocalizedAudio : LocalizedBehaviour
    {

        [Tooltip("If PhraseName couldn't be found, this clip will be used")]
        public AudioClip FallBack;

        // This gets called every time the translation needs updating
        public override void UpdateTranslation(Translation translation)
        {
            // Get the AudioSource component attached to this GameObject
            var audioSource = GetComponent<AudioSource>();

            // Use translation?
            if (translation != null)
            {
                audioSource.clip = translation.Object as AudioClip;
            }
            // Use fallback?
            else
            {
                audioSource.clip = FallBack;
            }
        }

        protected virtual void Awake()
        {
            // Should we set FallbackAudioClip?
            if (FallBack == null)
            {
                // Get the AudioSource component attached to this GameObject
                var audioSource = GetComponent<AudioSource>();

                // Copy current sprite to fallback
                FallBack = audioSource.clip;
            }
        }
    }

}
