﻿using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;

namespace Sun.Local
{

    [CustomPropertyDrawer(typeof(phraseKeyAttribute))]
    public class KeyNameDrawer : PropertyDrawer
    {
        Localization localization;

        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            var left = position; left.xMax -= 50;
            var right = position; right.xMin = left.xMax + 2;

            //EditorGUI.PropertyField(left, property);
            //EditorGUI.LabelField(left, label.text, property.stringValue, EditorStyles.boldLabel);
            property.stringValue = EditorGUI.TextField(left, label.text, property.stringValue, EditorStyles.boldLabel);

            //if (GUI.Button(position, property.stringValue, EditorStyles.popup) == true)
            //if (GUI.Button(right, "List") == true)
            if (GUI.Button(right, "List", EditorStyles.popup) == true)
            {
                var menu = new GenericMenu();

                if (localization == null)
                    localization = Object.FindObjectOfType<Localization>();

                for (var i = 0; i < localization.CurrentKeys.Count; i++)
                {
                    var phraseName = localization.CurrentKeys[i];

                    menu.AddItem(new GUIContent(phraseName), property.stringValue == phraseName, () => { property.stringValue = phraseName; property.serializedObject.ApplyModifiedProperties(); });
                }

                if (menu.GetItemCount() > 0)
                {
                    menu.DropDown(right);
                }
                else
                {
                    Debug.LogWarning("Your scene doesn't contain any phrases, so the phrase name list couldn't be created.");
                }
            }
        }

    }
}
#endif

//namespace Sun
namespace Sun.Local
{
    // This attribute allows you to select a language from all the localizations in the scene
    public class phraseKeyAttribute : PropertyAttribute
    {
    }

    public class testStatAttribute : PropertyAttribute
    {
    }
}