﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

namespace Sun.Local
{
    // This component will update an Image component with a localized sprite, or use a fallback if none is found
    [ExecuteInEditMode]
    [DisallowMultipleComponent]
    [RequireComponent(typeof(Image))]
    [AddComponentMenu("Sun/Localized Image Sprite")]
    public class LocalizedImage : LocalizedBehaviour {

        [Tooltip("If PhraseName couldn't be found, this sprite will be used")]
        public Sprite FallBack;

        // This gets called every time the translation needs updating
        public override void UpdateTranslation(Translation translation)
        {
            // Get the Image component attached to this GameObject
            var image = GetComponent<Image>();

            // Use translation?
            if (translation != null)
            {
                image.sprite = translation.Object as Sprite;
            }
            // Use fallback?
            else
            {
                image.sprite = FallBack;
            }
        }

        protected virtual void Awake()
        {
            // Should we set FallbackSprite?
            if (FallBack == null)
            {
                // Get the SpriteRenderer component attached to this GameObject
                var spriteRenderer = GetComponent<Image>();

                // Copy current sprite to fallback
                FallBack = spriteRenderer.sprite;
            }
        }
    }
}
