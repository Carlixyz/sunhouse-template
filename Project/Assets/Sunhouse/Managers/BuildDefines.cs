﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BuildDefines : MonoBehaviour {

    string text = "You Tell Me";

    public string Text
    {
        get
        {
            return text;
        }

        set
        {
            text = value;
        }
    }

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}



/// (to indicate to other scripts that my library is present).
/// This can be used safely on any platform(!), also it preserves other defines(!!!).
//using System;
//using UnityEngine;
//using UnityEditor;
 
//namespace Library
//{
//    [InitializeOnLoad]
//    public class Library
//    {
//        const string define = "LIBRARY_IS_AVAILABLE";
//        static Library()
//        { AddLibrayDefineIfNeeded(); }
//        static void AddLibrayDefineIfNeeded()
//        {
//            // Get defines.
//            BuildTargetGroup buildTargetGroup = EditorUserBuildSettings.selectedBuildTargetGroup;
//            string defines = PlayerSettings.GetScriptingDefineSymbolsForGroup(buildTargetGroup);
//            // Append only if not defined already.
//            if (defines.Contains(define))
//            {
//                Debug.LogWarning("Selected build target (" + EditorUserBuildSettings.activeBuildTarget.ToString() + ") already contains <b>" + define + "</b> <i>Scripting Define Symbol</i>.");
//                return;
//            }
//            // Append.
//            PlayerSettings.SetScriptingDefineSymbolsForGroup(buildTargetGroup, (defines + ";" + define));
//            Debug.LogWarning("<b>" + define + "</b> added to <i>Scripting Define Symbols</i> for selected build target (" + EditorUserBuildSettings.activeBuildTarget.ToString() + ").");
//        }
//    }
//}
