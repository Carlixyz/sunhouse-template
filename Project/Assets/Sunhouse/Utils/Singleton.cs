﻿/*
 * Unity Singleton MonoBehaviour
 * This file is licensed under the terms of the MIT license. 
 * 
 * Copyright (c) 2014 Kleber Lopes da Silva (http://kleber-swf.com)
 * Plus with http://www.archmagerises.com/news/2015/9/22/tips-on-game-world-state-data-serialization-in-unity-c
 */
using System;
using UnityEngine;

namespace Sun
{
    /// <summary>
    /// Special Unity Singleton (with Monobehaviour class) use it in combination with PrefabAttribute.cs
    /// </summary>
    public abstract class Singleton<T> : MonoBehaviour where T : MonoBehaviour
    {
        //protected Singleton() {;}

        public bool Persistent = true;
                    
        private static T _instance;                                 // Set [PrefabAttribute(typeof(T).Name)]

        public static T Get                                         // GET real Singleton Instance                                     
        {
            get
            {
                if ( !Instantiated) CreateInstance();                      
                    return _instance;
            }
        }   

        private static void CreateInstance()
        {
            if (Destroyed)
                return;
            var type = typeof(T);
            var objects = FindObjectsOfType<T>();
            if (objects.Length > 0)
            {
                if (objects.Length > 1)
                {
                    Debug.LogWarning("There is more than one instance of Singleton of type \"" + type +
                                     "\". Keeping the first one. Destroying the others.");
                    for (var i = 1; i < objects.Length; i++) Destroy(objects[i].gameObject);
                }
                _instance = objects[0];
                _instance.gameObject.SetActive(true);
                Instantiated = true;
                Destroyed = false;
                return;
            }

            string prefabName;
            GameObject gameObject;
            var attribute = Attribute.GetCustomAttribute(type, typeof(PrefabAttribute)) as PrefabAttribute;
            if (attribute == null || string.IsNullOrEmpty(attribute.Name))
            {                
                prefabName = typeof(T).Name;                        //prefabName = type.ToString();          
                gameObject = new GameObject(prefabName);
            }
            else
            {
                prefabName = attribute.Name;
                //  If You Had a NULL prefab Here is Because Your Prefab isn't in the right folder 
                gameObject = Instantiate(Resources.Load<GameObject>(prefabName));

                if (gameObject == null)       // Check out the  [PrefabAttribute("prefabName")]     
                    throw new Exception("Could not find Prefab \"" + prefabName +
                        "\" on Resources for Singleton of type \"" + type + "\".");

                gameObject.name = prefabName;
            }


            if (_instance == null)
                _instance = gameObject.GetComponent<T>() ?? gameObject.AddComponent<T>();
            Instantiated = true;
            Destroyed = false;
        }

        public static bool Instantiated { get; private set; }

        public static bool Destroyed { get; private set; }

        protected virtual void Awake()
        {
            if (_instance == null)
            {
                    //if (string.IsNullOrEmpty(DefaultFileName))
                    //    DefaultFileName = typeof(T).Name /*+ ".sav"*/; //Get.GetInstanceID().ToString();

                if (Persistent && !Application.isEditor)
                {
                    CreateInstance();
                    DontDestroyOnLoad(gameObject);

                }
                return;
            }

            if (Persistent) DontDestroyOnLoad(gameObject);
            if (GetInstanceID() != _instance.GetInstanceID()) Destroy(gameObject);
        }

        private void OnDestroy()
        {
            if (!Persistent)
            {
                Destroyed = true;
                Debug.LogWarning("Non-Persistent Singleton Destroyed!");
            }
            Instantiated = false;
        }

        /// -------------------------------------------------------------------------- ///


    }
}