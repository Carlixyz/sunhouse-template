﻿using System;
using UnityEngine;
using System.Collections.Generic;
using System.Collections;

namespace RuntimeGizmos
{
    public struct TransformState
    {
        public TransformState(Transform transform)
        {
            Position = transform.position;
            Rotation = transform.rotation;
            Scale = transform.localScale;
            transformRef = transform;
            toBeDeleted = false;
            prevParent = null;
        }

        public void Delete()
        {
            toBeDeleted = true;
            if (transformRef != null)
            {
                prevParent = transformRef.parent;
                transformRef.parent = GameObject.FindObjectOfType<TransformGizmo>().transform;
                transformRef.gameObject.SetActive(false);
            }
        }

        public void UnDelete()
        {
            toBeDeleted = false;
            if(transformRef != null)
            {
                transformRef.gameObject.SetActive(true);
                transformRef.parent = prevParent;
            }
        }

        public Vector3 Position;
        public Vector3 Scale;
        public Quaternion Rotation;
        public Transform transformRef;
        public bool toBeDeleted;
        public Transform prevParent;

    }

    [RequireComponent(typeof(Camera))]
	public class TransformGizmo : MonoBehaviour
	{

        CircularStack<TransformState> UndoStack = new CircularStack<TransformState>(10);
        [ShowOnly, SerializeField] int UndoIndex = 0, UndoMaxSteps = 10;

        void InitUndo()
        {
            //if (target != null)
            RegisterUndo(target);
        }

        void RegisterUndo( Transform t, bool delete = false)
        {
            //UndoStack.Push(new TransformState(t));
            var tState = new TransformState(t);

            if (delete)
                tState.Delete();

            if (UndoStack.Index == UndoStack.Capacity - 1 
                && UndoStack[0].toBeDeleted  
                && UndoStack[0].transformRef != null)
                    Destroy(UndoStack[0].transformRef.gameObject);

            UndoStack.Push(tState);
            UndoIndex = UndoStack.Index;
        }

        void PerformUndo()
        {
            if (UndoStack.Current.toBeDeleted)
                UndoStack.Current.UnDelete();

            if (UndoIndex % UndoMaxSteps > 0 && UndoStack.Count > 0 &&
                UndoStack[UndoIndex-1].transformRef != null)
            {

                UndoStack.Index--;
                TransformState tState = UndoStack.Current;
                var tRef = tState.transformRef;
                tRef.position = tState.Position;
                tRef.rotation = tState.Rotation;
                tRef.localScale = tState.Scale;
                //Debug.Log("Transform UNDO at " + tState + " || " + tRef);

                gameObject.SendMessage("cameraTargetPosition", tRef.position, SendMessageOptions.DontRequireReceiver);
                gameObject.SendMessage("cameraTargetSelection", true, SendMessageOptions.DontRequireReceiver);
                UndoIndex = UndoStack.Index;
            }

     
        }

        void PerformRedo()
        {
            if (UndoStack.Current.toBeDeleted)
                UndoStack.Current.Delete();

            if ( (UndoIndex % UndoMaxSteps) < (UndoStack.Count - 1) &&
                UndoStack[UndoIndex + 1].transformRef != null)
            {
                UndoStack.Index++;
                TransformState tState = UndoStack.Current;
                var tRef = tState.transformRef;
                tRef.position = tState.Position;
                tRef.rotation = tState.Rotation;
                tRef.localScale = tState.Scale;


                //if (tState.toBeDeleted /*&& (UndoIndex % UndoMaxSteps) != UndoStack.Count*/)
                //    tState.Delete();

                gameObject.SendMessage("cameraTargetPosition", tRef.position, SendMessageOptions.DontRequireReceiver);
                gameObject.SendMessage("cameraTargetSelection", true, SendMessageOptions.DontRequireReceiver);
                //Debug.Log("Transform REDO at " + UndoIndex + " Count " + UndoStack.Count);
                UndoIndex = UndoStack.Index;

            }
            //else if ((UndoIndex % UndoMaxSteps) == (UndoStack.Count - 1) && UndoStack.Current.toBeDeleted)
            //{
            //    UndoStack.Current.Delete();
            //    //UndoStack.Current.UnDelete();
            //}
        }

        public TransformSpace space = TransformSpace.Global;
		public TransformType type = TransformType.Move;

		//These are the same as the unity editor hotkeys
		public KeyCode SetMoveType = KeyCode.W;
		public KeyCode SetRotateType = KeyCode.E;
		public KeyCode SetScaleType = KeyCode.R;
		public KeyCode SetSpaceToggle = KeyCode.X;
		public KeyCode SetVerticalMoveToggle = KeyCode.LeftShift;

        Color xColor = new Color(1, .3f, .15f, 0.85f);
		Color yColor = new Color(.14f, 1, .36f, 0.85f);
		Color zColor = new Color(.26f, .675f, 1, 0.85f);/* #44ADFFC8*/
		Color allColor = new Color(.7f, .7f, .7f, 0.8f);
		Color selectedColor = new Color(1, 1, 0, 0.8f);

		float handleLength = .25f;
		float triangleSize = .03f;
		float boxSize = .01f;
		int circleDetail = 40;
		float minSelectedDistanceCheck = .04f;
		float moveSpeedMultiplier = 2f;
		float scaleSpeedMultiplier = 1f;
		float rotateSpeedMultiplier = 200f;
		float allRotateSpeedMultiplier = 20f;
        float gizmoSizeInOrtho = 1;

        AxisVectors handleLines = new AxisVectors();
		AxisVectors handleTriangles = new AxisVectors();
		AxisVectors handleSquares = new AxisVectors();
		AxisVectors circlesLines = new AxisVectors();
		AxisVectors drawCurrentCirclesLines = new AxisVectors();

        bool isTransforming;
		float totalScaleAmount;
		Quaternion totalRotationAmount;
        Axis selectedAxis = Axis.None;
        bool moveVerticalToggle = false;
		AxisInfo axisInfo;
		Transform target;
		Camera myCamera;

		static Material lineMaterial;

		void Awake()
		{
			myCamera = GetComponent<Camera>();
			SetMaterial();
        }

        bool KeyCtrl = false;
        bool KeyShift = false;


        void Update()
		{
            KeyCtrl = Input.GetKey(KeyCode.LeftControl)
                        || Input.GetKey(KeyCode.Mouse1)
                        || Input.GetButton("Fire1");

            KeyShift = Input.GetKey(KeyCode.LeftShift) || Input.GetKey(KeyCode.Mouse0);

            if (KeyCtrl && !KeyShift && Input.GetKeyDown(KeyCode.Z))
                PerformUndo();

            if (KeyCtrl && KeyShift && Input.GetKeyDown(KeyCode.Z))
                PerformRedo();


            SetSpaceAndType();
			SelectAxis();
			GetTarget();
			if(target == null) return;

            TransformSelected();

            if (Input.GetKeyDown(KeyCode.Delete))
            {
                RegisterUndo(target, true);
                target = null;
            }
        }

		void LateUpdate()
		{
			if(target == null) return;

			//We run this in lateupdate since coroutines run after update and we want our gizmos to have the updated target transform position after TransformSelected()
			SetAxisInfo();
			SetLines();
		}


        void OnPostRender()
		{
			if(target == null) return;

			lineMaterial.SetPass(0);

			Color xColor = (selectedAxis == Axis.X) ? selectedColor : this.xColor;
			Color yColor = (selectedAxis == Axis.Y) ? selectedColor : this.yColor;
			Color zColor = (selectedAxis == Axis.Z) ? selectedColor : this.zColor;
			Color allColor = (selectedAxis == Axis.Any) ? selectedColor : this.allColor;

			DrawLines(handleLines.x, xColor);
			DrawLines(handleLines.y, yColor);
			DrawLines(handleLines.z, zColor);

			DrawTriangles(handleTriangles.x, xColor);
			DrawTriangles(handleTriangles.y, yColor);
			DrawTriangles(handleTriangles.z, zColor);

			DrawSquares(handleSquares.x, xColor);
			DrawSquares(handleSquares.y, yColor);
			DrawSquares(handleSquares.z, zColor);
			DrawSquares(handleSquares.all, allColor);

			AxisVectors rotationAxisVector = circlesLines;
			if(isTransforming && space == TransformSpace.Global && type == TransformType.Rotate)
			{
				rotationAxisVector = drawCurrentCirclesLines;

				AxisInfo axisInfo = new AxisInfo();
				axisInfo.xDirection = totalRotationAmount * Vector3.right;
				axisInfo.yDirection = totalRotationAmount * Vector3.up;
				axisInfo.zDirection = totalRotationAmount * Vector3.forward;
				SetCircles(axisInfo, drawCurrentCirclesLines);
			}

			DrawCircles(rotationAxisVector.x, xColor);
			DrawCircles(rotationAxisVector.y, yColor);
			DrawCircles(rotationAxisVector.z, zColor);
			DrawCircles(rotationAxisVector.all, allColor);
		}

		void SetSpaceAndType()
		{
			if(Input.GetKeyDown(SetMoveType)) type = TransformType.Move;
			else if(Input.GetKeyDown(SetRotateType)) type = TransformType.Rotate;
			else if(Input.GetKeyDown(SetScaleType)) type = TransformType.Scale;

			if(Input.GetKeyDown(SetSpaceToggle))
			{
				if(space == TransformSpace.Global) space = TransformSpace.Local;
				else if(space == TransformSpace.Local) space = TransformSpace.Global;
			}

            moveVerticalToggle = Input.GetKey(SetVerticalMoveToggle);
        

            if (type == TransformType.Scale) space = TransformSpace.Local; //Only support local scale
		}

		void TransformSelected()
		{
			if(selectedAxis != Axis.None && Input.GetMouseButtonDown(0))
			{
                if (UndoStack.Count < 1)
                    InitUndo();
                StartCoroutine(TransformSelected(type));
			}
		}

		
		IEnumerator TransformSelected(TransformType type)
		{
			isTransforming = true;
            totalScaleAmount = 0;
			totalRotationAmount = Quaternion.identity;

            Vector3 originalTargetPosition = target.position;
			Vector3 planeNormal = (transform.position - target.position).normalized;
			Vector3 axis = GetSelectedAxisDirection();
			Vector3 projectedAxis = Vector3.ProjectOnPlane(axis, planeNormal).normalized;
			Vector3 previousMousePosition = Vector3.zero;

            //Sun.Events.Get.Raise( new Sun.Events.tGizmos(target.position, true, true));
            gameObject.SendMessage("cameraTargetPosition", target.position, SendMessageOptions.DontRequireReceiver);
            gameObject.SendMessage("cameraTargetTransform", true, SendMessageOptions.DontRequireReceiver);


            while (!Input.GetMouseButtonUp(0))
			{
                if (target == null)
                    yield break ;

                Ray mouseRay = myCamera.ScreenPointToRay(Input.mousePosition);
				Vector3 mousePosition = Geometry.LinePlaneIntersect(mouseRay.origin, mouseRay.direction, originalTargetPosition, planeNormal);

				if(previousMousePosition != Vector3.zero && mousePosition != Vector3.zero)
				{

					if(type == TransformType.Move)
					{
                        if (selectedAxis == Axis.Any)
                        {
                            if (moveVerticalToggle && Mathf.Abs(Vector3.Dot(Camera.main.transform.forward, Vector3.up)) < .5f)  // .35f
                            {
                                Camera Cam = Camera.main;
                                Vector2 mousePos = Input.mousePosition;
                                Ray ray = Cam.ScreenPointToRay(mousePos);
                                Vector3 fromTargetToCameraVector = (target.position - Cam.transform.position);
                                float distance;

                                if (new Plane(fromTargetToCameraVector.normalized,
                                    target.position).Raycast(ray, out distance))
                                {
                                    Vector3 rayPos = ray.GetPoint(distance);        

                                    if (Mathf.Abs(Vector3.Dot(Cam.transform.forward, Vector3.forward)) >.5f )
                                        rayPos.z = target.position.z;       // eyefish issue move correction
                                    else
                                    if (Mathf.Abs(Vector3.Dot(Cam.transform.right, Vector3.forward)) > .5f)
                                        rayPos.x = target.position.x;

                                    target.position = rayPos;

                                }

                            }
                            else
                            {
                                Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
                                float distance;

                                if (new Plane(Vector3.up, target.position).Raycast(ray, out distance))
                                {
                                    Vector3 rayPos = ray.GetPoint(distance);
                                    rayPos.y = target.position.y;
                                    target.position = rayPos;
                                }
                            }
                        }
                        else
                        {
                            float moveAmount = ExtVector3.MagnitudeInDirection(mousePosition - previousMousePosition, projectedAxis) * moveSpeedMultiplier;
                            //if(target != null)
                                target.Translate(axis * moveAmount, Space.World);
                        }


                        if (Input.GetKey(KeyCode.LeftControl))
                            target.position =
                                new Vector3((int)target.position.x + 0.5f,
                                            (int)target.position.y + 0.5f,
                                            (int)target.position.z + 0.5f);
                    }

					if(type == TransformType.Scale)
					{
						Vector3 projected = (selectedAxis == Axis.Any) ? transform.right : projectedAxis;
						float scaleAmount = ExtVector3.MagnitudeInDirection(mousePosition - previousMousePosition, projected) * scaleSpeedMultiplier;
						
						//WARNING - There is a bug in unity 5.4 and 5.5 that causes InverseTransformDirection to be affected by scale which will break negative scaling. Not tested, but updating to 5.4.2 should fix it - https://issuetracker.unity3d.com/issues/transformdirection-and-inversetransformdirection-operations-are-affected-by-scale
						Vector3 localAxis = (space == TransformSpace.Local && selectedAxis != Axis.Any) ? target.InverseTransformDirection(axis) : axis;
						
						if(selectedAxis == Axis.Any) target.localScale += (ExtVector3.Abs(target.localScale.normalized) * scaleAmount);
						else target.localScale += (localAxis * scaleAmount);
					
						totalScaleAmount += scaleAmount;
					}

					if(type == TransformType.Rotate)
					{
						if(selectedAxis == Axis.Any)
						{
							Vector3 rotation = transform.TransformDirection(new Vector3(Input.GetAxis("Mouse Y"), -Input.GetAxis("Mouse X"), 0));
							target.Rotate(rotation * allRotateSpeedMultiplier, Space.World);
							totalRotationAmount *= Quaternion.Euler(rotation * allRotateSpeedMultiplier);
						}else{
							Vector3 projected = (selectedAxis == Axis.Any || ExtVector3.IsParallel(axis, planeNormal)) ? planeNormal : Vector3.Cross(axis, planeNormal);
							float rotateAmount = (ExtVector3.MagnitudeInDirection(mousePosition - previousMousePosition, projected) * rotateSpeedMultiplier) / GetDistanceMultiplier();
							target.Rotate(axis, rotateAmount, Space.World);
							totalRotationAmount *= Quaternion.Euler(axis * rotateAmount);
						}
					}
				}

				previousMousePosition = mousePosition;

				yield return null;
			}

			totalRotationAmount = Quaternion.identity;
			totalScaleAmount = 0;
            isTransforming = false;

            //Sun.Events.Get.Raise( new Sun.Events.tGizmos(target.position, false, true));
            gameObject.SendMessage("cameraTargetPosition", target.position, SendMessageOptions.DontRequireReceiver);
            gameObject.SendMessage("cameraTargetSelection", true, SendMessageOptions.DontRequireReceiver);
            gameObject.SendMessage("cameraTargetTransform", false, SendMessageOptions.DontRequireReceiver);

            //if (target != null)
            RegisterUndo(target);

        }

        Vector3 GetSelectedAxisDirection()
		{
			if(selectedAxis != Axis.None)
			{
				if(selectedAxis == Axis.X) return axisInfo.xDirection;
				if(selectedAxis == Axis.Y) return axisInfo.yDirection;
				if(selectedAxis == Axis.Z) return axisInfo.zDirection;
				if(selectedAxis == Axis.Any) return Vector3.one;
			}
			return Vector3.zero;
		}
	
		void GetTarget()
		{
			if(selectedAxis == Axis.None && Input.GetMouseButtonDown(0) && !Input.GetKey(KeyCode.LeftAlt))
			{
				RaycastHit hitInfo; 
				if(Physics.Raycast(myCamera.ScreenPointToRay(Input.mousePosition), out hitInfo))
				{
					target = hitInfo.transform;
                    
                    //Sun.Events.Get.Raise( new Sun.Events.tGizmos(target.position, false, true) ); // Tell
                    gameObject.SendMessage("cameraTargetSelection", true, SendMessageOptions.DontRequireReceiver);
                    gameObject.SendMessage("cameraTargetPosition", target.position, SendMessageOptions.DontRequireReceiver);


//#if UNITY_EDITOR
//                    GameObject[] selections = { target.gameObject };
//                    UnityEditor.Selection.objects = selections;
//#endif
                }
                else
                {

                    target = null;
                    //Sun.Events.Get.Raise( new Sun.Events.tGizmos(null, false, false));
                    gameObject.SendMessage("cameraTargetSelection", false, SendMessageOptions.DontRequireReceiver);
                    gameObject.SendMessage("cameraTargetTransform", false, SendMessageOptions.DontRequireReceiver);

                }
            }
		}

        public Transform GetTargetSelected()
        {
            return target;
        }

        public bool GetTargetTransforming()
        {
            return isTransforming;
        }

        AxisVectors selectedLinesBuffer = new AxisVectors();
		void SelectAxis()
		{
			if(!Input.GetMouseButtonDown(0)) return;
			selectedAxis = Axis.None;

			float xClosestDistance = float.MaxValue;
			float yClosestDistance = float.MaxValue;
			float zClosestDistance = float.MaxValue;
			float allClosestDistance = float.MaxValue;
			float minSelectedDistanceCheck = this.minSelectedDistanceCheck * GetDistanceMultiplier();

			if(type == TransformType.Move || type == TransformType.Scale)
			{
				selectedLinesBuffer.Clear();
				selectedLinesBuffer.Add(handleLines);
				if(type == TransformType.Move) selectedLinesBuffer.Add(handleTriangles);
				else if(type == TransformType.Scale) selectedLinesBuffer.Add(handleSquares);

				xClosestDistance = ClosestDistanceFromMouseToLines(selectedLinesBuffer.x);
				yClosestDistance = ClosestDistanceFromMouseToLines(selectedLinesBuffer.y);
				zClosestDistance = ClosestDistanceFromMouseToLines(selectedLinesBuffer.z);
				allClosestDistance = ClosestDistanceFromMouseToLines(selectedLinesBuffer.all);

                if(allClosestDistance <= minSelectedDistanceCheck) selectedAxis = Axis.Any;
            }
			else if(type == TransformType.Rotate)
			{
				xClosestDistance = ClosestDistanceFromMouseToLines(circlesLines.x);
				yClosestDistance = ClosestDistanceFromMouseToLines(circlesLines.y);
				zClosestDistance = ClosestDistanceFromMouseToLines(circlesLines.z);
				allClosestDistance = ClosestDistanceFromMouseToLines(circlesLines.all);
			}

			if( (type == TransformType.Move || type == TransformType.Scale) && allClosestDistance <= minSelectedDistanceCheck) selectedAxis = Axis.Any;
			else if(xClosestDistance <= minSelectedDistanceCheck && xClosestDistance <= yClosestDistance && xClosestDistance <= zClosestDistance) selectedAxis = Axis.X;
			else if(yClosestDistance <= minSelectedDistanceCheck && yClosestDistance <= xClosestDistance && yClosestDistance <= zClosestDistance) selectedAxis = Axis.Y;
			else if(zClosestDistance <= minSelectedDistanceCheck && zClosestDistance <= xClosestDistance && zClosestDistance <= yClosestDistance) selectedAxis = Axis.Z;
			else if(type == TransformType.Rotate && target != null)
			{
				Ray mouseRay = myCamera.ScreenPointToRay(Input.mousePosition);
				Vector3 mousePlaneHit = Geometry.LinePlaneIntersect(mouseRay.origin, mouseRay.direction, target.position, (transform.position - target.position).normalized);
				if((target.position - mousePlaneHit).sqrMagnitude <= (handleLength * GetDistanceMultiplier()).Squared()) selectedAxis = Axis.Any;
			}
		}

		float ClosestDistanceFromMouseToLines(List<Vector3> lines)
		{
			Ray mouseRay = myCamera.ScreenPointToRay(Input.mousePosition);

			float closestDistance = float.MaxValue;
			for(int i = 0; i < lines.Count; i += 2)
			{
				IntersectPoints points = Geometry.ClosestPointsOnSegmentToLine(lines[i], lines[i + 1], mouseRay.origin, mouseRay.direction);
				float distance = Vector3.Distance(points.first, points.second);
				if(distance < closestDistance)
				{
					closestDistance = distance;
				}
			}
			return closestDistance;
		}

		void SetAxisInfo()
		{
			float size = handleLength * GetDistanceMultiplier();
			axisInfo.Set(target, size, space);

			if(isTransforming && (type == TransformType.Scale|| type == TransformType.Move) )
			{
				if(selectedAxis == Axis.Any) axisInfo.Set(target, size + totalScaleAmount, space);
				if(selectedAxis == Axis.X) axisInfo.xAxisEnd += (axisInfo.xDirection * totalScaleAmount);
				if(selectedAxis == Axis.Y) axisInfo.yAxisEnd += (axisInfo.yDirection * totalScaleAmount);
				if(selectedAxis == Axis.Z) axisInfo.zAxisEnd += (axisInfo.zDirection * totalScaleAmount);
			}
		}

		//This helps keep the size consistent no matter how far we are from it.
		float GetDistanceMultiplier()
		{
            if (Camera.main.orthographic)
                return gizmoSizeInOrtho * Camera.main.orthographicSize;

            if (target == null) return 0f;
			return Mathf.Max(.01f, Mathf.Abs(ExtVector3.MagnitudeInDirection(target.position - transform.position, myCamera.transform.forward)) * .7f);
		}

		void SetLines()
		{
			SetHandleLines();
			SetHandleTriangles();
			SetHandleSquares();
			SetCircles(axisInfo, circlesLines);
		}

		void SetHandleLines()
		{
			handleLines.Clear();

			if(type == TransformType.Move || type == TransformType.Scale)
			{
				handleLines.x.Add(target.position);
				handleLines.x.Add(axisInfo.xAxisEnd);
				handleLines.y.Add(target.position);
				handleLines.y.Add(axisInfo.yAxisEnd);
				handleLines.z.Add(target.position);
				handleLines.z.Add(axisInfo.zAxisEnd);
			}
		}

		void SetHandleTriangles()
		{
			handleTriangles.Clear();
            //handleSquares.Clear();

            if (type == TransformType.Move)
			{
				float triangleLength = triangleSize * GetDistanceMultiplier();
				AddTriangles(axisInfo.xAxisEnd, axisInfo.xDirection, axisInfo.yDirection, axisInfo.zDirection, triangleLength, handleTriangles.x);
				AddTriangles(axisInfo.yAxisEnd, axisInfo.yDirection, axisInfo.xDirection, axisInfo.zDirection, triangleLength, handleTriangles.y);
				AddTriangles(axisInfo.zAxisEnd, axisInfo.zDirection, axisInfo.yDirection, axisInfo.xDirection, triangleLength , handleTriangles.z);

                AddSquares(target.position - (axisInfo.xDirection * boxSize * GetDistanceMultiplier()), axisInfo.xDirection, axisInfo.yDirection, axisInfo.zDirection, boxSize * GetDistanceMultiplier(), handleTriangles.all);
            }
        }

		void AddTriangles(Vector3 axisEnd, Vector3 axisDirection, Vector3 axisOtherDirection1, Vector3 axisOtherDirection2, float size, List<Vector3> resultsBuffer)
		{
			Vector3 endPoint = axisEnd + (axisDirection * (size  * 2 /*2*/ ));
			Cone baseSquare = GetBaseSection(axisEnd, axisOtherDirection1, axisOtherDirection2, size * .75f);

            //resultsBuffer.Add(baseSquare.bottomLeft);   // The Quad pyramid's Base
            //resultsBuffer.Add(baseSquare.topLeft);
            //resultsBuffer.Add(baseSquare.topRight);
            //resultsBuffer.Add(baseSquare.topLeft);
            //resultsBuffer.Add(baseSquare.bottomRight);
            //resultsBuffer.Add(baseSquare.topRight);

            //---

            //resultsBuffer.Add(baseSquare.offsetDown);   // The OCt pyramid's Base
            //resultsBuffer.Add(baseSquare.bottomRight);
            //resultsBuffer.Add(axisEnd);   // The Quad pyramid's Base

            //resultsBuffer.Add(baseSquare.bottomRight);
            //resultsBuffer.Add(baseSquare.offsetRight);   // The OCt pyramid's Base
            //resultsBuffer.Add(axisEnd);   // The Quad pyramid's Base

            //resultsBuffer.Add(baseSquare.offsetRight);   // The OCt pyramid's Base
            //resultsBuffer.Add(baseSquare.topLeft);   // The OCt pyramid's Base
            //resultsBuffer.Add(axisEnd);   // The Quad pyramid's Base

            //resultsBuffer.Add(baseSquare.topLeft);   // The OCt pyramid's Base
            //resultsBuffer.Add(baseSquare.offsetUp);   // The OCt pyramid's Base
            //resultsBuffer.Add(axisEnd);   // The Quad pyramid's Base

            //resultsBuffer.Add(baseSquare.offsetUp);   // The OCt pyramid's Base
            //resultsBuffer.Add(baseSquare.bottomLeft);   // The OCt pyramid's Base
            //resultsBuffer.Add(axisEnd);   // The Quad pyramid's Base

            //resultsBuffer.Add(baseSquare.bottomLeft);   // The OCt pyramid's Base
            //resultsBuffer.Add(baseSquare.offsetLeft);   // The OCt pyramid's Base
            //resultsBuffer.Add(axisEnd);   // The Quad pyramid's Base

            //resultsBuffer.Add(baseSquare.offsetLeft);   // The OCt pyramid's Base
            //resultsBuffer.Add(baseSquare.topRight);
            //resultsBuffer.Add(axisEnd);   // The Quad pyramid's Base

            //resultsBuffer.Add(baseSquare.topRight);
            //resultsBuffer.Add(baseSquare.offsetDown);   // The OCt pyramid's Base
            //resultsBuffer.Add(axisEnd);   // The Quad pyramid's Base

            //---


            //for (int i = 0; i < 4; i++) // The 4 Tris of the pyramid
            for (int i = 0; i < 8; i++) // The 8 Tris of the Cone
                {
                resultsBuffer.Add(baseSquare[i]);
                resultsBuffer.Add(baseSquare[i + 1]);
                resultsBuffer.Add(endPoint);
            }

            //resultsBuffer.Add(baseSquare.offsetDown);   // The OCt pyramid's Base
            //resultsBuffer.Add(baseSquare.offsetRight);   // The OCt pyramid's Base
            //resultsBuffer.Add(endPoint);   // The OCt pyramid's Base

            //resultsBuffer.Add(baseSquare.offsetRight);   // The OCt pyramid's Base
            //resultsBuffer.Add(baseSquare.offsetUp);   // The OCt pyramid's Base
            //resultsBuffer.Add(endPoint);   // The OCt pyramid's Base

            //resultsBuffer.Add(baseSquare.offsetUp);   // The OCt pyramid's Base
            //resultsBuffer.Add(baseSquare.offsetLeft);   // The OCt pyramid's Base
            //resultsBuffer.Add(endPoint);   // The OCt pyramid's Base

            //resultsBuffer.Add(baseSquare.offsetLeft);   // The OCt pyramid's Base
            //resultsBuffer.Add(baseSquare.offsetDown);   // The OCt pyramid's Base
            //resultsBuffer.Add(endPoint);   // The OCt pyramid's Base

        }

        void SetHandleSquares()
		{
			handleSquares.Clear();

			if(type == TransformType.Scale)
			{
				float boxLength = boxSize * GetDistanceMultiplier();
				AddSquares(axisInfo.xAxisEnd, axisInfo.xDirection, axisInfo.yDirection, axisInfo.zDirection, boxLength, handleSquares.x);
				AddSquares(axisInfo.yAxisEnd, axisInfo.yDirection, axisInfo.xDirection, axisInfo.zDirection, boxLength, handleSquares.y);
				AddSquares(axisInfo.zAxisEnd, axisInfo.zDirection, axisInfo.xDirection, axisInfo.yDirection, boxLength, handleSquares.z);
				AddSquares(target.position - (axisInfo.xDirection * boxLength), axisInfo.xDirection, axisInfo.yDirection, axisInfo.zDirection, boxLength, handleSquares.all);
			}

            // handleTriangles.all Get the modification and handleSquares.all displays it
            if (type == TransformType.Move)
                AddSquares(target.position - (axisInfo.xDirection * boxSize * GetDistanceMultiplier()), axisInfo.xDirection, axisInfo.yDirection, axisInfo.zDirection, boxSize * GetDistanceMultiplier(),  handleSquares.all);

        }

        void AddSquares(Vector3 axisEnd, Vector3 axisDirection, Vector3 axisOtherDirection1, Vector3 axisOtherDirection2, float size, List<Vector3> resultsBuffer)
		{
			Cone baseSquare = GetBaseSection(axisEnd, axisOtherDirection1, axisOtherDirection2, size);
			Cone baseSquareEnd = GetBaseSection(axisEnd + (axisDirection * (size * 2f)), axisOtherDirection1, axisOtherDirection2, size);

			resultsBuffer.Add(baseSquare.bottomLeft);
			resultsBuffer.Add(baseSquare.topLeft);
			resultsBuffer.Add(baseSquare.bottomRight);
			resultsBuffer.Add(baseSquare.topRight);

			resultsBuffer.Add(baseSquareEnd.bottomLeft);
			resultsBuffer.Add(baseSquareEnd.topLeft);
			resultsBuffer.Add(baseSquareEnd.bottomRight);
			resultsBuffer.Add(baseSquareEnd.topRight);

			for(int i = 0; i < 4; i++)
			{
				resultsBuffer.Add(baseSquare[i]);
				resultsBuffer.Add(baseSquare[i + 1]);
				resultsBuffer.Add(baseSquareEnd[i + 1]);
				resultsBuffer.Add(baseSquareEnd[i]);
			}
		}

		Cone GetBaseSection(Vector3 axisEnd, Vector3 axisOtherDirection1, Vector3 axisOtherDirection2, float size)
		{
            float CurvFactor = .7f;
            //Square square;
            //Vector3 offsetUp = ((axisOtherDirection1 * size) + (axisOtherDirection2 * size));
            //Vector3 offsetDown = ((axisOtherDirection1 * size) - (axisOtherDirection2 * size));
            ////These arent really the proper directions, as in the bottomLeft isnt really at the bottom left...
            //square.bottomLeft = axisEnd + (offsetDown * CurvFactor) ;
            //square.topLeft = axisEnd + (offsetUp * CurvFactor) ;
            //square.bottomRight = axisEnd - (offsetDown * CurvFactor) ;
            //square.topRight = axisEnd - (offsetUp * CurvFactor) ;

            Cone cone;
            Vector3 offsetUp = ((axisOtherDirection1 * size) + (axisOtherDirection2 * size));
            Vector3 offsetDown = ((axisOtherDirection1 * size) - (axisOtherDirection2 * size));
            //These arent really the proper directions, as in the bottomLeft isnt really at the bottom left...
            cone.bottomLeft = axisEnd + (offsetDown * CurvFactor);
            cone.topLeft = axisEnd + (offsetUp * CurvFactor);
            cone.bottomRight = axisEnd - (offsetDown * CurvFactor);
            cone.topRight = axisEnd - (offsetUp * CurvFactor);

            cone.offsetUp = axisEnd + (axisOtherDirection1 * size); //  ((axisOtherDirection1 * size) + (axisOtherDirection2 * size));
            cone.offsetDown = axisEnd - (axisOtherDirection1 * size); // ((axisOtherDirection1 * size) - (axisOtherDirection2 * size));
            cone.offsetRight = axisEnd + (offsetUp - offsetDown) * 0.5f;
            cone.offsetLeft = axisEnd - (offsetUp - offsetDown) * 0.5f;

            return cone;
		}

		void SetCircles(AxisInfo axisInfo, AxisVectors axisVectors)
		{
			axisVectors.Clear();

			if(type == TransformType.Rotate)
			{
				float circleLength = handleLength * GetDistanceMultiplier();
				AddCircle(target.position, axisInfo.xDirection, circleLength, axisVectors.x);
				AddCircle(target.position, axisInfo.yDirection, circleLength, axisVectors.y);
				AddCircle(target.position, axisInfo.zDirection, circleLength, axisVectors.z);
				AddCircle(target.position, (target.position - transform.position).normalized, circleLength, axisVectors.all, false);
			}
		}

		void AddCircle(Vector3 origin, Vector3 axisDirection, float size, List<Vector3> resultsBuffer, bool depthTest = true)
		{
			Vector3 up = axisDirection.normalized * size;
			Vector3 forward = Vector3.Slerp(up, -up, .5f);
			Vector3 right = Vector3.Cross(up, forward).normalized * size;
		
			Matrix4x4 matrix = new Matrix4x4();
		
			matrix[0] = right.x;
			matrix[1] = right.y;
			matrix[2] = right.z;
		
			matrix[4] = up.x;
			matrix[5] = up.y;
			matrix[6] = up.z;
		
			matrix[8] = forward.x;
			matrix[9] = forward.y;
			matrix[10] = forward.z;
		
			Vector3 lastPoint = origin + matrix.MultiplyPoint3x4(new Vector3(Mathf.Cos(0), 0, Mathf.Sin(0)));
			Vector3 nextPoint = Vector3.zero;
			float multiplier = 360f / circleDetail;

			Plane plane = new Plane((transform.position - target.position).normalized, target.position);

			for(var i = 0; i < circleDetail + 1; i++)
			{
				nextPoint.x = Mathf.Cos((i * multiplier) * Mathf.Deg2Rad);
				nextPoint.z = Mathf.Sin((i * multiplier) * Mathf.Deg2Rad);
				nextPoint.y = 0;
			
				nextPoint = origin + matrix.MultiplyPoint3x4(nextPoint);
			
				if(!depthTest || plane.GetSide(lastPoint))
				{
					resultsBuffer.Add(lastPoint);
					resultsBuffer.Add(nextPoint);
				}

				lastPoint = nextPoint;
			}
		}

		void DrawLines(List<Vector3> lines, Color color)
		{
			GL.Begin(GL.LINES);
			GL.Color(color);

			for(int i = 0; i < lines.Count; i += 2)
			{
				GL.Vertex(lines[i]);
				GL.Vertex(lines[i + 1]);
			}

			GL.End();
		}

		void DrawTriangles(List<Vector3> lines, Color color)
		{
            GL.Begin(GL.TRIANGLE_STRIP);
            //GL.Color(color);


            for (int i = 0; i < lines.Count; i += 3)
			{
                //GL.Color(color);
                Color colorHeight = color * Mathf.Max( Mathf.Cos((i * 0.333f) * .125f), 0.75f);
                colorHeight.a = color.a;
                GL.Color(colorHeight);

                GL.Vertex(lines[i]);

                GL.Vertex(lines[i + 1]);

                GL.Vertex(lines[i + 2]);
			}
            GL.End();
        }

        void DrawSphere(Vector3 Position, Color? color = null, int details = 10, float radius = 0.5f)
        {
            Vector3 vCoord = Position;
            float alpha, beta; // Storage for coordinates and angles        

            for (alpha = 0.0f; alpha < Mathf.PI; alpha += Mathf.PI / details)
            {
                GL.Begin(GL.TRIANGLE_STRIP);
                GL.Color(color??Color.yellow);

                for (beta = 0.0f; beta < 2.01f * Mathf.PI; beta += Mathf.PI / details)
                {
                    vCoord.x = radius * Mathf.Cos(beta) * Mathf.Sin(alpha);
                    vCoord.y = radius * Mathf.Sin(beta) * Mathf.Sin(alpha);
                    vCoord.z = radius * Mathf.Cos(alpha);
                    GL.Vertex(vCoord);
                    vCoord.x = radius * Mathf.Cos(beta) * Mathf.Sin(alpha + Mathf.PI / details);
                    vCoord.y = radius * Mathf.Sin(beta) * Mathf.Sin(alpha + Mathf.PI / details);
                    vCoord.z = radius * Mathf.Cos(alpha + Mathf.PI / details);
                    GL.Vertex(vCoord);
                }
                GL.End();
            }
        }

        void DrawCone(Vector3 Position, Vector3 Direction, float radius = 0.5f, float height = 2.5f, Color? color = null)
        {
            Color col = color?? Color.yellow;
            float x = 0.0f, y = 0.0f, angle = 0.0f, angle_stepsize/* = 1.047f;//*/ = 0.7853f;
            //float x = 0.0f, y = 0.0f, angle = 0.0f, angle_stepsize = 1.047f;
            //float x = 0.0f, y = 0.0f, angle = 0.0f, angle_stepsize = .1f;

            //GL.Begin(GL.LINE_STRIP);
            GL.Begin(GL.TRIANGLE_STRIP);
            //GL.Begin(GL.TRIANGLES);

            angle_stepsize = Mathf.Max(angle_stepsize, .1f);
            //angle = 0.0f;
            angle = 2 * Mathf.PI;
            //while (angle < 2 * Mathf.PI)
            while (angle > 0.0f)
            {
                Color colorCos = col * Mathf.Max(Mathf.Cos(angle * 2), .75f);
                colorCos.a = col.a;
                GL.Color(colorCos);

                x = radius * Mathf.Cos(angle);
                y = radius * Mathf.Sin(angle);
                GL.Vertex( new Vector3(0, 0, height) );
                GL.Vertex(new Vector3( x, y, 0) );
                angle -= angle_stepsize;
            }
            //GL.Vertex(new Vector3(0, 0, height));
            GL.Vertex(new Vector3(radius, 0, 0)); // Close
            GL.End();
        }

        void DrawSquares(List<Vector3> lines, Color color)
		{
			GL.Begin(GL.QUADS);
			GL.Color(color);

			for(int i = 0; i < lines.Count; i += 4)
			{
				GL.Vertex(lines[i]);
				GL.Vertex(lines[i + 1]);
				GL.Vertex(lines[i + 2]);
				GL.Vertex(lines[i + 3]);
			}

			GL.End();
		}

		void DrawCircles(List<Vector3> lines, Color color)
		{
			GL.Begin(GL.LINES);
			GL.Color(color);

			for(int i = 0; i < lines.Count; i += 2)
			{
				GL.Vertex(lines[i]);
				GL.Vertex(lines[i + 1]);
			}

			GL.End();
		}

		void SetMaterial()
		{
			if(lineMaterial == null)
			{
				lineMaterial = new Material(Shader.Find("Custom/Lines"));
				#region Shader code
				/*
				Shader "Custom/Lines"
				{
					SubShader
					{
						Pass
						{
							Blend SrcAlpha OneMinusSrcAlpha
							ZWrite Off
							ZTest Always
							Cull Off
							Fog { Mode Off }

							BindChannels
							{
								Bind "vertex", vertex
								Bind "color", color
							}
						}
					}
				}
				*/
				#endregion
			}
		}
	}


}