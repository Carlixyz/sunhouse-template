﻿using System;
using UnityEngine;

namespace RuntimeGizmos
{
	public struct Diamond
	{
		public Vector3 bottomLeft;
		public Vector3 bottomRight;
		public Vector3 topLeft;
		public Vector3 topRight;


        public Vector3 offsetUp  ;
        public Vector3 offsetDown;
        public Vector3 offsetLeft;
        public Vector3 offsetRight;

        public Vector3 this[int index]
		{
			get
			{
				switch (index)
				{
                    // (custom diamond version instead quad)
                    case 0:
                        return this.offsetDown;
                    case 1:
                        return this.offsetRight;
                    case 2:
                        return this.offsetUp;
                    case 3:
                        return this.offsetLeft;
                    case 4:
                        return this.offsetDown; //wrap back to start 
                    default:
						return Vector3.zero;
				}
			}
		}
	}
}