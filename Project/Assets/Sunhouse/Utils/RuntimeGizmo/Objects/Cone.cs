﻿using System;
using UnityEngine;

namespace RuntimeGizmos
{
	public struct Cone
	{
		public Vector3 bottomLeft;
		public Vector3 bottomRight;
		public Vector3 topLeft;
		public Vector3 topRight;


        public Vector3 offsetUp  ;
        public Vector3 offsetDown;
        public Vector3 offsetLeft;
        public Vector3 offsetRight;

        public Vector3 this[int index]
		{
			get
			{
				switch (index)
				{
                    case 0:                         // REAL OCT SECTION ARROW
                        return this.offsetDown;
                    case 1:
                        //return this.bottomRight;
                        return this.topRight;
                    case 2:
                        //return this.offsetRight;
                        return this.offsetLeft;
                    case 3:
                        //return this.topLeft;
                        return this.bottomLeft;
                    case 4:
                        return this.offsetUp;
                    case 5:
                        //return this.bottomLeft;
                        return this.topLeft;
                    case 6:
                        //return this.offsetLeft;
                        return this.offsetRight;
                    case 7:
                        //return this.topRight;
                        return this.bottomRight;
                    case 8:
                        return this.offsetDown;
                    default:
						return Vector3.zero;
				}
			}
		}
	}
}