﻿using UnityEngine;
using System.Collections;

namespace Sun.Tools
{

    public class AnimSprite : MonoBehaviour                         // for Use with Unity sprite atlas system
    {
	    public Sprite[] sprites;                                    // Drag & lay sprites in order here via inspector 
        public bool Autorun = false;                                // Custom Prefabs optional Autorun 
        public int FPS = 16;

	    int spritesLength = 0;
        SpriteRenderer mainSprite;
		
	    void Start()
	    {
		    mainSprite = (SpriteRenderer)this.GetComponent<SpriteRenderer>();
		    spritesLength = sprites.Length;
            
            StartCoroutine(CoUpdate());                                 // Changed Because it can't hold re-activation
	    }

        IEnumerator CoUpdate()
        {
            while (Autorun)
            {
                PlayFramesAuto(0, spritesLength, FPS);              // Set Prefab's transform If you need to flip X

                yield return 0;
            }
        }

        // slighty Faster but more insecure to array index bounds overflows
        public void PlayFramesAuto(int frameStart, int totalFrames, int FPS = 16)			
	    {
		    mainSprite.sprite = sprites[frameStart +  (int)(Time.time * FPS) % totalFrames];								
	    }

        public void PlayFrames(int frameStart, int totalFrames, int flipped = 1, int FPS = 16)
        {
            // Increments from 0 to totalFrames range with the time
            int index = (int)(Time.time * FPS) % totalFrames;

            // Traverse sprite's index from frameStart to Max SpritesLength
            mainSprite.sprite = sprites[frameStart + index % (spritesLength - frameStart)];     // Check InBound

            GetComponent<Renderer>().transform.localScale = new Vector3(flipped, 1, 1);         // Flip Sprite?
        }

        void OnBecameVisible()
	    {
		    enabled = true;
	    }
	
	    void OnBecameInvisible()
	    {
		    enabled = false;
	    }

	}
}
