/// <summary>
/// Debug CAMERA! You can Use it combined with RunTimeTransformGizmo or alone as it
/// HOld Left Mouse Button and use A, W, S, D Keys for free Move
/// Alt + Mouse Button 01 to orbit around something (previously selected) press F to focus
/// HOld Mouse Wheel (Button 02) for side pan 
/// </summary>

using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Camera))]
public class UnityCameraIngame : MonoBehaviour
{
    public float distance = 5.0f;
    public float orthoDistance = 20;

    public float panSpeed = 0.1f;
    public float normalMoveSpeed = 10.0f;
    public float xSpeed = 120.0f;
    public float ySpeed = 160.0f;

//#if UNITY_EDITOR
    float GameBuildScaler = 1.0f;
//#else
//     float GameBuildScaler = 1.0f;
//#endif

    [SerializeField, ShowOnly] Vector3 camPosition;
    [SerializeField, ShowOnly] Vector3 targetPosition/* = Vector3.one * distance*/;

    Transform camTransform;
    Camera camRef;
    //RuntimeGizmos.TransformGizmo tGizmo;
    //Sun.Events.tGizmos tGizmo ;

    bool orbitEnabled = false, freeEnabled = false, sideEnabled = false;
    bool isTweening = false, isSelecting = false, isTransforming = false/*, isZooming = false*/;

    //[Space(20)]
    Quaternion sPerspectiveRotation = Quaternion.Euler(0, 0, 0);
    public bool ShouldTweenBetweenViews = true;


    public KeyCode toggleMouseSelection = KeyCode.Mouse0;
    public KeyCode toggleMouseFreeLook = KeyCode.Mouse1;
    public KeyCode toggleMouseSideMove = KeyCode.Mouse2;
    public KeyCode toggleOrbitKey = KeyCode.LeftAlt;
    public KeyCode setTargetFocus = KeyCode.F;
    public KeyCode alternateWheelZoom = KeyCode.LeftControl;// just in case for zoom disable 



    void OnEnable()
    {
        camTransform = GetComponent<Transform>();
        camRef = GetComponent<Camera>();
        var rigidbody = GetComponent<Rigidbody>();
        // Make the rigid body not change rotation
        if (rigidbody != null)
            rigidbody.freezeRotation = true;

        targetPosition = camTransform.position + (camTransform.forward * distance);
        distance = Vector3.Distance(camTransform.position, targetPosition);

        //be sure to grab the current rotations as starting points.
        camPosition = camTransform.position;

        //Sun.Events.Get.AddListener<Sun.Events.tGizmos>(ProcessSelectionTarget);
        //tGizmo = new Sun.Events.tGizmos(targetPosition, false, false);
    }

    //void OnDisable()
    //{
    //    Sun.Events.Get.RemoveListener<Sun.Events.tGizmos>( ProcessSelectionTarget);
    //}

    //void ProcessSelectionTarget(Sun.Events.tGizmos e)   
    //{
    //    tGizmo = e;
    //}

    void GetTargetPosition()
    {
        if (Input.GetMouseButtonDown(0))
        {
            RaycastHit hitInfo;
            if (Physics.Raycast(camRef.ScreenPointToRay(Input.mousePosition), out hitInfo))
            {
                targetPosition = hitInfo.transform.position;

#if UNITY_EDITOR
                Object[] selections = { hitInfo.transform.gameObject };
                UnityEditor.Selection.objects = selections;
#endif
            }
        }
        //else if (tGizmo.isSelecting)
        //    targetPosition = tGizmo.targetPosition;
        //else
        //    targetPosition = camTransform.position + camTransform.forward * distance;
        else if ( !isSelecting && !camRef.orthographic)
            targetPosition = camTransform.position + camTransform.forward * distance;

        //else if (!isSelecting && camRef.orthographic)
        //    if (Vector3.Distance(camTransform.position, targetPosition) >= orthoDistance)
        //        //camTransform.position = targetPosition - camTransform.forward * orthoDistance;
        //    //else
        //        targetPosition = camTransform.position + camTransform.forward * orthoDistance;
    }

    void Update()
    {
        orbitEnabled = (Input.GetKey(toggleOrbitKey) );
        sideEnabled = (!orbitEnabled && !freeEnabled && Input.GetKey(toggleMouseSideMove));
        freeEnabled = (!orbitEnabled && !sideEnabled 
                            && !Input.GetKey(KeyCode.LeftShift)
                            && !Input.GetKey(KeyCode.LeftControl)
                            && Input.GetKey(toggleMouseFreeLook));

        //Debug.DrawLine(camTransform.position, targetPosition, Color.magenta);

        GetTargetPosition();


        if (!isTweening && Input.GetKeyDown(KeyCode.F1))   // if Press F set Focus
            PerspCamera();

        if (!isTweening && Input.GetKeyDown(KeyCode.F2) )   // if Press F set Focus
            FrontCamera();

        if (!isTweening && Input.GetKeyDown(KeyCode.F3))   // if Press F set Focus
            TopCamera();

        if (!isTweening && Input.GetKeyDown(KeyCode.F4))   // if Press F set Focus
            LeftCamera();


        if (Input.GetKeyDown(setTargetFocus) && !isTweening )   // if Press F set Focus
            if(Input.GetKey(alternateWheelZoom))
                StartCoroutine(TweenToOrbitTarget(camTransform.position, Vector3.up *3));
            else
                StartCoroutine(TweenToOrbitTarget(camTransform.position, targetPosition));

        if (!Input.GetKey(alternateWheelZoom) && !Input.GetKey(KeyCode.LeftShift))
            if (Input.GetAxis("Mouse ScrollWheel") > 0f)
                if (camRef.orthographic)
                    camRef.orthographicSize -= normalMoveSpeed * 10 * Time.deltaTime;
                else
                    camTransform.position +=
                    camTransform.forward * normalMoveSpeed * 10 * Time.deltaTime;
            else if (Input.GetAxis("Mouse ScrollWheel") < 0f)
                if (camRef.orthographic)
                    camRef.orthographicSize += normalMoveSpeed * 10 * Time.deltaTime;
                else
                    camTransform.position -=
                    camTransform.forward * normalMoveSpeed * 10 * Time.deltaTime;

        if (!isTweening)
            camUpdate();

    }

    void camUpdate()
    {
        camPosition = Vector3.zero;

        if (sideEnabled)
        {
            //grab the rotation of the camera so we can move in a psuedo local XY space

            //camPosition = camTransform.right * -Input.GetAxis("Mouse X") // * panSpeed * camRef.orthographicSize;
            //camPosition += camTransform.up * -Input.GetAxis("Mouse Y") // * panSpeed * camRef.orthographicSize;
                camPosition = camTransform.right * -Input.GetAxis("Mouse X")  * panSpeed * camRef.orthographicSize;
                camPosition += camTransform.up * -Input.GetAxis("Mouse Y")  * panSpeed * camRef.orthographicSize;



            if (!isSelecting && camRef.orthographic)
                targetPosition = camTransform.position + camTransform.forward * orthoDistance;

                //if (!isSelecting && camRef.orthographic)
                //    if (Vector3.Distance(camTransform.position, targetPosition) < orthoDistance)
                //        camTransform.position = targetPosition - camTransform.forward * orthoDistance;
                //    else
                //        targetPosition = camTransform.position + camTransform.forward * orthoDistance;

                //targetPosition = camTransform.position + camTransform.forward * distance;
        }
        else if (freeEnabled)
        {
            camTransform.Rotate( Vector3.up,
                Input.GetAxis("Mouse X") * xSpeed * Time.deltaTime * GameBuildScaler,
                Space.World);
            camTransform.Rotate( camTransform.right,
                -Input.GetAxis("Mouse Y") * ySpeed * Time.deltaTime * GameBuildScaler,
                Space.World);

            camPosition += transform.up * normalMoveSpeed * Time.deltaTime
                * (Input.GetKey(KeyCode.E) == true ? 1 : 0);
            camPosition -= transform.up * normalMoveSpeed * Time.deltaTime
                * (Input.GetKey(KeyCode.Q) == true ? 1 : 0);

            camPosition +=
                camTransform.forward * normalMoveSpeed * Input.GetAxis("Vertical")
                * Time.deltaTime ;
            camPosition +=
                camTransform.right * normalMoveSpeed * Input.GetAxis("Horizontal") 
                * Time.deltaTime ;

            //targetPosition = camTransform.position + camTransform.forward * distance;

        }
        else if (orbitEnabled)
        {

            if (Input.GetKey(toggleMouseFreeLook))
            {
                if (camRef.orthographic)
                    camRef.orthographicSize += 
                        Time.deltaTime * panSpeed * GameBuildScaler
                    * (-Input.GetAxis("Mouse X") * xSpeed + Input.GetAxis("Mouse Y") * ySpeed);
                else
                    camPosition += camTransform.forward * Time.deltaTime * panSpeed * GameBuildScaler
                    * (Input.GetAxis("Mouse X") * xSpeed - Input.GetAxis("Mouse Y") * ySpeed);
            }

            //if (!tGizmo.isTransforming && Input.GetKey(toggleMouseSelection))
            if ( Input.GetKey(toggleMouseSelection))
            {
                if(!isTransforming)
                {
                    camTransform.RotateAround(targetPosition, Vector3.up,
                        Input.GetAxis("Mouse X") * xSpeed * distance * Time.deltaTime * GameBuildScaler);
                    camTransform.RotateAround(targetPosition, camTransform.right,
                        -Input.GetAxis("Mouse Y") * ySpeed * Time.deltaTime * GameBuildScaler);
                //  camPosition.y = ClampAngle(camPosition.y, yMinLimit, yMaxLimit);
                }
            }
        }

        camTransform.position += camPosition;
    }

    //void OnDrawGizmos()
    //{
    //    Gizmos.color = Color.blue;
    //    Gizmos.DrawLine(transform.position, targetPosition);
    //}

    ///  Usefull Tweening CoRoutines  ///

    IEnumerator TweenMatrixProjection(float ProjectionChangeTime = 0.25f)
    {
        isTweening = true;
        float _currentT = 0.0f;
        //bool ChangeProjection = false;

        var currentlyOrthographic = camRef.orthographic;
        Matrix4x4 orthoMat, persMat;


        if (currentlyOrthographic)
        {
            orthoMat = camRef.projectionMatrix;

            camRef.orthographic = false;
            camRef.ResetProjectionMatrix();
            persMat = camRef.projectionMatrix;
        }
        else
        {
            persMat = camRef.projectionMatrix;

            camRef.orthographic = true;
            camRef.ResetProjectionMatrix();
            orthoMat = camRef.projectionMatrix;
        }

        while (isTweening)
        {
            camRef.orthographic = currentlyOrthographic;

            _currentT += (Time.deltaTime / ProjectionChangeTime);

            if (_currentT < 1.0f)
            {
                if (currentlyOrthographic)
                    camRef.projectionMatrix = MatrixLerp(orthoMat, persMat, _currentT * _currentT);
                else
                    camRef.projectionMatrix = MatrixLerp(persMat, orthoMat, Mathf.Sqrt(_currentT));
            }
            else
            {
                isTweening = false;
                camRef.orthographic = !currentlyOrthographic;
                camRef.ResetProjectionMatrix();
            }
            yield return 0;
        }

        //if (!isSelecting)
        if (camRef.orthographic)
            camTransform.position = targetPosition - camTransform.forward * orthoDistance;
        //else
        //targetPosition = camTransform.position + camTransform.forward * orthoDistance;


        yield break;
    }

    IEnumerator TweenMatrixProjection(Vector3 endPos, Vector3 endUp, float ProjectionChangeTime = 0.5f)
    {
        isTweening = true;


        float time = .5f;
        float elapsed = 0f;
        var currentlyOrthographic = camRef.orthographic;
        //Matrix4x4 orthoMat, persMat;
        Matrix4x4 startMatP, endMatP;
        //Matrix4x4 startMatW, endMatW;
        Vector3 startPos = camTransform.position;
        Vector3 startUp = camTransform.up;
        //Vector3 wup = camTransform.up;


        startMatP = camRef.projectionMatrix;
        if (!currentlyOrthographic)
            camRef.orthographic = true;

        camRef.ResetProjectionMatrix();
        endMatP = camRef.projectionMatrix;

        while (elapsed < time)
        {
            elapsed += Time.deltaTime;

            camTransform.position = camPosition = Vector3.Lerp(startPos, endPos, (elapsed / time));
            //startUp = Vector3.Lerp(startUp, endUp, (elapsed / time));
            camTransform.LookAt(targetPosition, Vector3.Lerp(startUp, endUp, (elapsed / time)));
            camRef.projectionMatrix = MatrixLerp(startMatP, endMatP, (elapsed / time));
            //camRef.worldToCameraMatrix = MatrixLerp(startMatW, endMatW, (elapsed / time));
            yield return 0;
        }

        ///--------------------------------------------------------------------- ///
        //if (!isSelecting)
        //if (camRef.orthographic)
        //    camTransform.position = targetPosition - camTransform.forward * orthoDistance;
        isTweening = false;
        camRef.orthographic = true;
        camRef.ResetProjectionMatrix();

        camTransform.position = camPosition = endPos;
        camTransform.LookAt(targetPosition, endUp);

        //else
        //targetPosition = camTransform.position + camTransform.forward * orthoDistance;


        yield break;
    }

    Matrix4x4 MatrixLerp(Matrix4x4 from, Matrix4x4 to, float t)
    {
        t = Mathf.Clamp(t, 0.0f, 1.0f);
        var newMatrix = new Matrix4x4();
        newMatrix.SetRow(0, Vector4.Lerp(from.GetRow(0), to.GetRow(0), t));
        newMatrix.SetRow(1, Vector4.Lerp(from.GetRow(1), to.GetRow(1), t));
        newMatrix.SetRow(2, Vector4.Lerp(from.GetRow(2), to.GetRow(2), t));
        newMatrix.SetRow(3, Vector4.Lerp(from.GetRow(3), to.GetRow(3), t));
        return newMatrix;
    }

    IEnumerator TweenToOrbitTarget(Vector3 originPos, Vector3 targetPos)
    {
        isTweening = true;

        camPosition = originPos;
        //targetPosition = targetPos;
        Vector3 nextCamPos = targetPos - distance * camTransform.forward;
        float elapsed = 0f;

        while ((Vector3.Distance(camPosition, nextCamPos) > 0.01f))
        {
            elapsed += Time.deltaTime * 2;
            camPosition = Vector3.Lerp(originPos, nextCamPos, elapsed);
            camTransform.position = camPosition;

            yield return 0;
        }

        camPosition = camTransform.position;
        //targetPosition = targetPos;

        isTweening = false;

        yield break;
    }

    IEnumerator TweenToPosition(Vector3 targetPos, Vector3 wUp)
    {
        isTweening = true;

        float time = .5f;
        float elapsed = 0f;
        Vector3 startPos = camTransform.position;
        Vector3 endPos = targetPos;//targetPos - orthoDistance * camTransform.forward; 
        Vector3 startUp = camTransform.up;

        while (elapsed < time)
        {
            camTransform.position = camPosition = Vector3.Lerp(startPos, endPos, (elapsed / time));
            camTransform.LookAt(targetPosition, Vector3.Lerp(startUp, wUp, (elapsed / time)));
            elapsed += Time.deltaTime;

            yield return 0;
        }

        //Debug.Log("End Tween");
        camTransform.position = camPosition = endPos;
        camTransform.LookAt(targetPosition, wUp);

        isTweening = false;

        yield break;
    }


    //void StorePerspective()
    //{
    //    sPerspectiveRotation = camTransform.rotation;
    //}

    void ApplyOrthoRotation(Vector3 pos, Vector3 wUp)
    {
        if (camRef.orthographic == false)
            sPerspectiveRotation = camTransform.rotation;
        //StorePerspective();

        //if (camRef.orthographic && !isSelecting)
        //    targetPosition = (Vector3.one * distance) - (targetPosition);
        //targetPosition = (Vector3.one * distance) - (targetPosition * 0.5f);
        //targetPosition = targetPosition * 0.5f;

        /// -------------------------------------------------------------------- ///

        //if (targetPosition == camTransform.position || !isSelecting)
        //    targetPosition = targetPosition * 0.5f;

        //if (!isSelecting)
        //    //    targetPosition = Vector3.one * 2;
        //targetPosition =  targetPosition * 0.5f;
        //targetPosition = (targetPosition - camTransform.position) * 0.5f;
        //targetPosition = ( camTransform.position - targetPosition) * 0.5f;

        if (ShouldTweenBetweenViews)
            StartCoroutine(TweenMatrixProjection(/*camTransform.position, */targetPosition + pos, wUp));
        //StartCoroutine(TweenToPosition(/*camTransform.position, */targetPosition + pos, wUp));
        else
        {
            camRef.orthographic = true; //SceneView.lastActiveSceneView.orthographic = true;
            camTransform.position = targetPosition + pos;
            camTransform.LookAt(targetPosition, wUp);
        }

    }

    void FrontCamera()
    {
        ApplyOrthoRotation(Vector3.back * orthoDistance, Vector3.up);
    }

    void TopCamera()
    {
        ApplyOrthoRotation(Vector3.up * orthoDistance, Vector3.forward);
    }

    void LeftCamera()
    {
        ApplyOrthoRotation(Vector3.left * orthoDistance, Vector3.up);
    }

    void PerspCamera()
    {
        if (camRef.orthographic == true)
        {
            //if (ShouldTweenBetweenViews)
            //else
            camTransform.rotation = sPerspectiveRotation;
            camTransform.LookAt(targetPosition);
            camTransform.position = targetPosition - (camTransform.forward * distance);
            //camTransform.position += camTransform.forward * distance * 2;
            //if (!isSelecting)
            //    targetPosition = camTransform.position + camTransform.forward * distance;

            //camRef.orthographic = false;
        }
        StartCoroutine(TweenMatrixProjection());
    }

    ///  Some dummy Methods for EditorCameraIngame<->TransformGizmo Decoupling  ///

    void cameraTargetSelection( bool selected = true)
    {
        isSelecting = selected;
    }

    void cameraTargetPosition(Vector3 tPos )
    {
        targetPosition = tPos;
    }

    void cameraTargetTransform( bool transformed = true)
    {
        isTransforming = transformed;
    }


}
