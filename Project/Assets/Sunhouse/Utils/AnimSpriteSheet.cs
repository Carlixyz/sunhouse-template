using UnityEngine;
using System.Collections;

namespace Sun.Tools
{

    public class AnimSpriteSheet : MonoBehaviour        // Classic SpriteSheet's Grids without Unity atlas system Cuts
    {

        public int RowSize = 1;		                    // Input the number of Rows of the sprite sheet 
        public int ColumnSize = 1;		                // Input the number of columns of the sprite sheet 
        public int framesPerSecond = 16;		        // Speed of Sprite animation


        public void PlayFrames(int rowFrameStart, int colFrameStart, int totalframes, int flipped)
        {
            int index = (int)(Time.time * framesPerSecond);							// time control fps
            index = index % totalframes;

            Vector2 size = new Vector2(1.0f / ColumnSize, 1.0f / RowSize);	// scale

            int u = index % ColumnSize;
            int v = index / ColumnSize;

            Vector2 offset = new Vector2(((u + colFrameStart) * size.x), (1.0f - size.y) - (v + rowFrameStart) * size.y); // offset

            offset.x = ((offset.x * flipped) - size.x * (System.Convert.ToByte(flipped < 0))) * flipped;

            size.x *= flipped;


            GetComponent<Renderer>().material.mainTextureOffset = offset;		// texture offset
            GetComponent<Renderer>().material.mainTextureScale = size;		    // texture scale
        }


        public void PlayFrames(int rowFrameStart, int colFrameStart, int totalframes, int flipped, int FPS)
        {
            int index = (int)(Time.time * FPS);									// time control fps
            index = index % totalframes;

            Vector2 size = new Vector2(1.0f / ColumnSize, 1.01f / RowSize);	    // scale

            int u = index % ColumnSize;
            int v = index / ColumnSize;

            Vector2 offset = new Vector2(((u + colFrameStart) * size.x), (1.0f - size.y) - (v + rowFrameStart) * size.y); // offset

            offset.x = ((offset.x * flipped) - size.x * (System.Convert.ToByte(flipped < 0))) * flipped;
            size.x *= flipped;

            GetComponent<Renderer>().material.mainTextureOffset = offset;		// texture offset
            GetComponent<Renderer>().material.mainTextureScale = size;		// texture scale
        }

        public void PlayFramesFixed(int rowFrameStart, int colFrameStart, int totalframes, int flipped, float fraction = 1.005f, int FPS = 16) //Ej. 1.005f
        {
            int index = (int)(Time.time * FPS);							// time control fps
            index = index % totalframes;

            Vector2 size = new Vector2(1.0f / ColumnSize, fraction / RowSize);	// scale

            int u = index % ColumnSize;
            int v = index / ColumnSize;

            Vector2 offset = new Vector2(((u + colFrameStart) * size.x), (1.0f - size.y) - (v + rowFrameStart) * size.y); // offset

            offset.x = ((offset.x * flipped) - size.x * (System.Convert.ToByte(flipped < 0))) * flipped;

            size.x *= flipped;

            GetComponent<Renderer>().material.mainTextureOffset = offset;		// texture offset
            GetComponent<Renderer>().material.mainTextureScale = size;		// texture scale
        }
    }
}