﻿using UnityEngine;
using System.Collections;
using System;
using System.Reflection;
//using UnityEditor;
using System.IO;
using System.Linq;

namespace Sun
{

    public static class Utils // 
    {

        public struct Tuple<T1, T2>
        {
            public readonly T1 Item1;
            public readonly T2 Item2;
            public Tuple(T1 item1, T2 item2) { Item1 = item1; Item2 = item2; }

            public static Tuple<T1, T2> CreateTuple(T1 item1, T2 item2)
            {
                return new Tuple<T1, T2>(item1, item2);
            }
        }

        //public static Tuple<T1, T2> CreateTuple<T1, T2>(T1 item1, T2 item2)
        //{
        //    return new Tuple<T1, T2>(item1, item2);
        //}

        public static System.Random randomGenerator = null;

        public static string GetUniqueString()
        {
            return GetUniqueInteger().ToString() + GetUniqueInteger().ToString() + GetUniqueInteger().ToString();
        }

        public static int GetUniqueInteger()
        {
            if (randomGenerator == null)
                randomGenerator = new System.Random();

            int result = randomGenerator.Next();
            //		Debug.Log("GENERATED " + result);
            return result;

        }

#if UNITY_EDITOR
        static bool isPlaying = false;

        [UnityEditor.MenuItem("Tools/Run _F5")]           //[MenuItem("HotKey/Run2 _%H")]
        static void SetPlayMode()
        {
            isPlaying = !isPlaying;
            if (!isPlaying)
                UnityEditor.SceneManagement.EditorSceneManager.SaveScene(
                        UnityEngine.SceneManagement.SceneManager.GetActiveScene());

            UnityEditor.EditorApplication.ExecuteMenuItem("Edit/Play");
        }

        const string menuName = "GameObject/Create Prefab From Selected";

        static bool namesEnabler = false;

        [UnityEditor.MenuItem("Tools/Show GO Names _%F1")]           //[MenuItem("HotKey/Run2 _%H")]
        static void SetNamesEnabler()
        {
            namesEnabler = !namesEnabler;
        }

        [UnityEditor.DrawGizmo(UnityEditor.GizmoType.InSelectionHierarchy | UnityEditor.GizmoType.NotInSelectionHierarchy)]
        static void DrawGameObjectName(Transform transform, UnityEditor.GizmoType gizmoType)
        {
            if (namesEnabler)
                UnityEditor.Handles.Label(transform.position, transform.gameObject.name);
        }

        [UnityEditor.MenuItem("Tools/Clear PlayerPrefs")]
        private static void ClearAllPlayerPrefs()
        {
            if (UnityEditor.EditorUtility.DisplayDialog("Clear ALL PlayerPrefs", "Do you really want to clear ALL the PlayerPrefs\nof this project ?\n\nYou cannot undo this action.", "Yep", "Nah"))
            {
                PlayerPrefs.DeleteAll();
            }

        }       // ClearAllPlayerPrefs()

        [UnityEditor.MenuItem("Tools/Screenshot _F12")]
        public static void Capture()
        {
            if (!UnityEditor.EditorApplication.isPlaying) return;
            string filename = System.DateTime.Now.ToString("yyyy-MM-dd_hh-mm-ss") + ".png";
            Application.CaptureScreenshot(filename);
        }

        [UnityEditor.MenuItem(menuName)]
        static void CreatePrefabMenu()  // Adds a menu named "Create Prefab From Selected" to the GameObject menu.
        {

            var go = UnityEditor.Selection.activeGameObject;

            Mesh m1 = go.GetComponent<MeshFilter>().mesh;//update line1
            UnityEditor.AssetDatabase.CreateAsset(m1, "Assets/savedMesh/" + go.name + "_M" + ".asset"); // update line2


            var prefab = UnityEditor.PrefabUtility.CreateEmptyPrefab("Assets/savedMesh/" + go.name + ".prefab");
            UnityEditor.PrefabUtility.ReplacePrefab(go, prefab);
            UnityEditor.AssetDatabase.Refresh();
        }

        [UnityEditor.MenuItem(menuName, true)]
        static bool ValidateCreatePrefabMenu()           // Validates the menu.
        {
            return UnityEditor.Selection.activeGameObject != null;
        }

        /*
        [MenuItem("Tools/Generate Enum File")]
        public static void GenerateEnumFile()
        {
            string enumName = "MyEnum";
            string[] enumEntries = { "Foo", "Goo", "Hoo" };
            string filePathAndName = "Assets/Scripts/" + enumName + ".cs"; //The folder Scripts/Enums/ is expected to exist

            using (StreamWriter streamWriter = new StreamWriter(filePathAndName))
            {
                streamWriter.WriteLine("public enum " + enumName);
                streamWriter.WriteLine("{");
                for (int i = 0; i < enumEntries.Length; i++)
                {
                    streamWriter.WriteLine("\t" + enumEntries[i] + ",");
                }
                streamWriter.WriteLine("}");
            }
            AssetDatabase.Refresh();
        }
        */
#endif

        /* NEEDS PLAYMAKER
        public static PlayMakerFSM GetFsm(string name,GameObject go)
        {
            if(go==null || name.Length==0)
                return null;

            PlayMakerFSM[] fsmScripts =  go.GetComponents<PlayMakerFSM>();
            foreach(PlayMakerFSM fsm in fsmScripts)
            {
                if(name==fsm.FsmName)
                    return fsm;

            }
            return null;
        }
        */

        /*
        public static T[] FieldToArray(System.Reflection.FieldInfo field)
        {
            Array reflectArray = (Array)field.GetValue(target);					
            T[] value = new T[reflectArray.Length];	
            for(int i = 0; i< reflectArray.Length; i++)
            {
                value[i] =(MotinSerializableData) reflectArray.GetValue(i);
            }
            return value;
        }*/


        public static string AddPersistentDataPath(string fileName)
        {
            return System.IO.Path.Combine(Application.persistentDataPath, fileName );
        }

        public static string BundleFilePath(string filename)
        {
            string result;
#if UNITY_EDITOR || UNITY_STANDALONE_WIN || UNITY_STANDALONE_OSX
            //result = System.IO.Path.Combine(string.Format("file://{0}", Application.streamingAssetsPath), filename);
            result = System.IO.Path.Combine(Application.streamingAssetsPath, filename);
#elif UNITY_ANDROID
            result = System.IO.Path.Combine(Application.streamingAssetsPath, filename);
            //result = System.IO.Path.Combine("jar:file://", Application.dataPath,"!/assets/", filename);
#elif UNITY_IPHONE
            result = System.IO.Path.Combine(Application.dataPath, string.Format("Raw/{0}", filename));
#elif UNITY_WEBPLAYER
            result = System.IO.Path.Combine("StreamingAssets/", filename)); // untested
#endif
            return result;
        }

        public static float SignedAngleBetween(Vector3 a, Vector3 b, Vector3 n)
        {
            // angle in [0,180]
            float angle = Vector3.Angle(a, b);
            float sign = Mathf.Sign(Vector3.Dot(n, Vector3.Cross(a, b)));

            // angle in [-179,180]
            float signed_angle = angle * sign;

            // angle in [0,360] (not used but included here for completeness)
            //float angle360 =  (signed_angle + 180) % 360;

            return signed_angle;
        }

        public static float SignedAngle360Between(Vector3 a, Vector3 b, Vector3 n)
        {
            // angle in [0,180]
            float angle = Vector3.Angle(a, b);
            float sign = Mathf.Sign(Vector3.Dot(n, Vector3.Cross(a, b)));

            // angle in [-179,180]
            float signed_angle = angle * sign;

            // angle in [0,360] (not used but included here for completeness)
            float angle360 = (signed_angle + 180) % 360;

            return angle360;
        }

        /// Get an enum value if a string is inside a enum
        public static T StringToEnum<T>(string name)
        {
            try
            {
                return (T)Enum.Parse(typeof(T), name);
            }
            catch (ArgumentException)
            {
                Debug.LogWarning("ENUM NOT FOUND" + name);
                return (T)indexToEnum<T>(0);
            }
        }

        /// Get index if a value is inside a String Array else returns -1
        public static int StringArrayIndex(string[] stringArray, string value)
        {
            if (stringArray == null || value == null)
                return -1;

            int count = stringArray.Length;
            for (int i = 0; i < count; i++)
            {
                if (stringArray[i] == value)
                {
                    return i;
                }
            }
            return -1;
        }

        ///  Get index if a value is inside a int Array else returns -1
        public static int IntArrayIndex(int[] intArray, int value)
        {
            if (intArray == null /*|| value == null*/)
                return -1;

            int count = intArray.Length;
            for (int i = 0; i < count; i++)
            {
                if (intArray[i] == value)
                {
                    return i;
                }
            }
            return -1;
        }

        /// Get index if a string value is inside a enum else returns -1
        public static int StringToIndex<T>(string name)
        {
            string[] enumNames = Enum.GetNames(typeof(T));
            int count = enumNames.Length;

            for (int i = 0; i < count; i++)
            {
                if (enumNames[i] == name)
                {
                    return i;
                }
            }
            return -1;
        }

        /// Get index if a int value is inside a enum else returns -1
        public static T indexToEnum<T>(int index)
        {
            string[] enumNames = Enum.GetNames(typeof(T));
            return StringToEnum<T>(enumNames[index]);

        }

        /// Get a List of strings from some Enum
        public static string[] EnumNames<T>()
        {
            return Enum.GetNames(typeof(T));
        }

        public static bool IsTypeDerivedFrom(Type type, Type fromType)
        {
            if (type == fromType)
                return true;

            if (type.BaseType == null)
                return false;

            return IsTypeDerivedFrom(type.BaseType, fromType);

        }

        public static Type[] GetTypesInNamespace(Assembly assembly, string nameSpace)
        {
            return (from type in assembly.GetTypes() where type.Namespace == nameSpace select type).ToArray();
        }

        public class SunDataEnumField : Attribute
        {
            public string filePath = "";
            public SunDataEnumField(string file)
            {
                filePath = file;
            }
        }

        /////////////////////////////////////////  DRAWING UTILITIES ////////////////////////////////

        struct Line
        {
            public Vector3 start;
            public Vector3 end;
            public Color color;
            public float startTime;
            public float duration;

            public Line(Vector3 start, Vector3 end, Color color, float startTime = 0, float duration = 0)
            {
                this.start = start;
                this.end = end;
                this.color = color;
                this.startTime = startTime;
                this.duration = duration;
            }

            public bool DurationElapsed(bool drawLine)
            {
                if (drawLine)
                {
                    GL.Color(color);
                    GL.Vertex(start);
                    GL.Vertex(end);
                }
                return Time.time - startTime >= duration;
            }
        }

        static Material lineMaterial = null;

        static bool lineMatCreated = false;

        public static void CreateLineMaterial()
        {
            if (!lineMaterial)
            {
                lineMatCreated = true;
                // Unity has a built-in shader that is useful for drawing simple colored things.
                lineMaterial = new Material(Shader.Find("Hidden/Internal-Colored"));
                //lineMaterial = new Material(Shader.Find("Hidden/GridShader"));
                lineMaterial.hideFlags = HideFlags.HideAndDontSave;

                // Turn on alpha blending
                lineMaterial.SetInt("_SrcBlend", (int)UnityEngine.Rendering.BlendMode.SrcAlpha);
                lineMaterial.SetInt("_DstBlend", (int)UnityEngine.Rendering.BlendMode.OneMinusSrcAlpha);
                // Turn backface culling off
                lineMaterial.SetInt("_Cull", (int)UnityEngine.Rendering.CullMode.Off);
                // Turn off depth writes
                lineMaterial.SetInt("_ZWrite", 1);
            }
        }

        public static bool V3Equal(Vector3 a, Vector3 b)
        {
            return Vector3.SqrMagnitude(a - b) < 0.0001f;
        }

        public static void DrawLine(Vector3 start, Vector3 end, Color? color = null)
        {
            if (start == end)
                return;


            if(!lineMatCreated)
                CreateLineMaterial();

            lineMaterial.SetPass(0);                                // set the current material

            GL.Begin(GL.LINES);
            GL.Color(color ?? Color.white);

            GL.Vertex3(start.x, start.y, start.z);
            GL.Vertex3(end.x , end.y , end.z );

            //GL.Clear(true, true, Color.black);
            //GL.ClearWithSkybox(true, Camera.main);
            GL.End();

        }

        public static void DrawRay(Vector3 start, Vector3 dir, Color? color = null)
        {
            if (dir == Vector3.zero)
                return;
            DrawLine(start, start + dir, color);
        }

        /// Draw an arrow from start to a Fixed end with color .
        public static void DrawFixedArrow(Vector3 start, Vector3 end, Color? color = null, float arrowHeadLength = 0.25f, float arrowHeadAngle = 20)
        {
            DrawArrow(start, end - start, color, arrowHeadLength, arrowHeadAngle );
        }

        /// Draw an arrow from start to start + dir with color.
        public static void DrawArrow(Vector3 start, Vector3 dir, Color? color = null, float arrowHeadLength = 0.25f, float arrowHeadAngle = 20 )
        {
            if (dir == Vector3.zero)
                return;
            DrawRay(start, dir, color);
            Vector3 right = Quaternion.LookRotation(dir) * Quaternion.Euler(0, 180 + arrowHeadAngle, 0) * Vector3.forward;
            Vector3 left = Quaternion.LookRotation(dir) * Quaternion.Euler(0, 180 - arrowHeadAngle, 0) * Vector3.forward;
            DrawRay(start + dir, right * arrowHeadLength, color);
            DrawRay(start + dir, left * arrowHeadLength, color);
        }

        /// Draw a 3D volumetric Grid set start, moduled size & default color (step just as Vector3.one )
        public static void DrawGridArea(Vector3 start, Vector3 size, Vector3 step, Color? color = null)
        {
            Color[] colors  =  {    color ??    new Color(1, .26f, 0, .78f),
                                    color ??    new Color(0, 1, .26f, .5f),
                                    color ??    new Color(.26f, .675f, 1, .78f) };

            DrawGridArea(start,size,step, colors);
        }

        /// Draw a 3D volumetric Grid set start, moduled size & color (step can be just Vector3.one )
        public static void DrawGridArea(Vector3 start, Vector3 size, Vector3 step, Color[] colors)
        {
            //if (!enabled) return;
            step = Vector3.Max(step, Vector3.one * 0.025f);        // Clamp to a minimun step for GPU 

            CreateLineMaterial();
            //start = transform.position; // - new Vector3((int)gridSize.x, (int)gridSize.y, 0) * 0.5f;

            lineMaterial.SetPass(0);                                // set the current material
            GL.Begin(GL.LINES);

            for (float j = 0; j <= /*(int)*/size.y; j += step.y)                        //Layers
            {

                for (float i = 0; i <= /*(int)*/size.z; i += step.z)                    //X axis lines      
                {
                    GL.Color(colors[0]);                   //GL.Color(Color.red);
                    GL.Vertex3(start.x, start.y + j, start.z + i);
                    GL.Vertex3(start.x + /*(int)*/size.x, start.y + j, start.z + i);
                }

                for (float i = 0; i <= /*(int)*/size.x; i += step.x)                    //Z axis lines
                {
                    GL.Color(colors[2]);                   //GL.Color(Color.blue);
                    GL.Vertex3(start.x + i, start.y + j, start.z);
                    GL.Vertex3(start.x + i, start.y + j, start.z + /*(int)*/size.z);
                }
            }

            for (float i = 0; i <= /*(int)*/size.z; i += step.z)                        //Y axis lines
            {
                GL.Color(colors[1]);                     //GL.Color(Color.green);
                for (float k = 0; k <= /*(int)*/size.x; k += step.x)
                {
                    GL.Vertex3(start.x + k, start.y, start.z + i);
                    GL.Vertex3(start.x + k, start.y + /*(int)*/size.y, start.z + i);
                }
            }

            GL.End();
        }



        /// Draw a 3D volumetric TileGrid set start, tileCount size & color (step can be just Vector3.one )
        public static void DrawGridTiled(Vector3 start, Vector3 count, Vector3? step = null, Color? color = null)
        {
            Color[] colors =  {    color ??    new Color(1, .26f, 0, .78f),
                                    color ??    new Color(0, 1, .26f, .5f),
                                    color ??    new Color(.26f, .675f, 1, .78f) };

            DrawGridTiled(start, count, step ?? Vector3.one, colors);

        }

        /// Draw a 3D volumetric TileGrid set start, tileCount size & color (step can be just Vector3.one )
        public static void DrawGridTiled(Vector3 start, Vector3 count, Vector3 step, Color[] colors)
        {
            step = Vector3.Max(step, Vector3.one * 0.025f);        // Clamp to a minimun step for GPU 

            CreateLineMaterial();

            Vector3 realSize =
                new Vector3(count.x * step.x, count.y * step.y, count.z * step.z);

            lineMaterial.SetPass(0);                                // set the current material
            GL.Begin(GL.LINES);

            for (float j = 0; j <= /*(int)*/realSize.y; j += step.y)                        //Layers
            {

                for (float i = 0; i <= /*(int)*/realSize.z; i += step.z)                    //X axis lines      
                {
                    GL.Color(colors[0]);                   //GL.Color(Color.red);
                    GL.Vertex3(start.x, start.y + j, start.z + i);
                    GL.Vertex3(start.x + /*(int)*/realSize.x, start.y + j, start.z + i);
                }

                for (float i = 0; i <= /*(int)*/realSize.x; i += step.x)                    //Z axis lines
                {
                    GL.Color(colors[2]);                   //GL.Color(Color.blue);
                    GL.Vertex3(start.x + i, start.y + j, start.z);
                    GL.Vertex3(start.x + i, start.y + j, start.z + /*(int)*/realSize.z);
                }
            }

            for (float i = 0; i <= /*(int)*/realSize.z; i += step.z)                        //Y axis lines
            {
                GL.Color(colors[1]);                     //GL.Color(Color.green);
                for (float k = 0; k <= /* (int)*/realSize.x; k += step.x)
                {
                    GL.Vertex3(start.x + k, start.y, start.z + i);
                    GL.Vertex3(start.x + k, start.y + /*(int)*/realSize.y, start.z + i);
                }
            }

            GL.End();
        }




        /// Draw a 3D Plane (add it inside a Camera's PostRender or any MonoBehaviour's RenderObject)
        public static void DrawPlane(Vector3 position, Vector3 normal)
        {
            Vector3 v3;

            if (normal.normalized != Vector3.forward)
                v3 = Vector3.Cross(normal, Vector3.forward).normalized * normal.magnitude;
            else
                v3 = Vector3.Cross(normal, Vector3.up).normalized * normal.magnitude; ;

            var corner0 = position + v3;
            var corner2 = position - v3;
            var q = Quaternion.AngleAxis(90.0f, normal);
            v3 = q * v3;
            var corner1 = position + v3;
            var corner3 = position - v3;

            Color32 color = /* Color.blue +*/ Color.yellow /** 0.7f*/;
            color.a = 200;

            DrawLine(corner0, corner2, color);
            DrawLine(corner1, corner3, color);
            DrawLine(corner0, corner1, color);
            DrawLine(corner1, corner2, color);
            DrawLine(corner2, corner3, color);
            DrawLine(corner3, corner0, color);
            DrawArrow(position, normal, Color.green);
        }
        /// Draw a Polygon with a lot of faces
        public static void DrawCircle(Vector3 center, float radius, Color? color = null)
        {
            //            float degRad = Mathf.PI / 180;
            for (float theta = 0.0f; theta < (2 * Mathf.PI); theta += 0.1f)
            {
                Vector3 ci = (new Vector3(Mathf.Cos(theta) * radius + center.x, Mathf.Sin(theta) * radius + center.y, center.z));
                DrawLine(ci, ci + new Vector3(0, 0.02f, 0), color);
            }
        }


        /// Draw a square with color for a duration of time and with or without depth testing.
        public static void DrawSquare(Vector3 pos, Vector3? rot = null, Vector3? scale = null, Color? color = null)
        {
            DrawSquare(Matrix4x4.TRS(pos, Quaternion.Euler(rot ?? Vector3.zero), scale ?? Vector3.one), color);
        }

        /// Draw a square with color for a duration of time and with or without depth testing.
        public static void DrawSquare(Vector3 pos, Quaternion? rot = null, Vector3? scale = null, Color? color = null)
        {
            DrawSquare(Matrix4x4.TRS(pos, rot ?? Quaternion.identity, scale ?? Vector3.one), color);
        }

        /// Draw a square with color for a duration of time and with or without depth testing.
        public static void DrawSquare(Matrix4x4 matrix, Color? color = null)
        {
            Vector3
                    p_1 = matrix.MultiplyPoint3x4(new Vector3(.5f, 0, .5f)),
                    p_2 = matrix.MultiplyPoint3x4(new Vector3(.5f, 0, -.5f)),
                    p_3 = matrix.MultiplyPoint3x4(new Vector3(-.5f, 0, -.5f)),
                    p_4 = matrix.MultiplyPoint3x4(new Vector3(-.5f, 0, .5f));

            DrawLine(p_1, p_2, color);
            DrawLine(p_2, p_3, color);
            DrawLine(p_3, p_4, color);
            DrawLine(p_4, p_1, color);
        }

        /// Draw a cube with color for a duration of time and with or without depth testing.
        public static void DrawCube(Vector3 pos, Vector3? rot = null, Vector3? scale = null, Color? color = null)
        {
            DrawCube(Matrix4x4.TRS(pos, Quaternion.Euler(rot ?? Vector3.zero), scale ?? Vector3.one), color);
        }

        /// Draw a cube with color for a duration of time and with or without depth testing.
        public static void DrawCube(Vector3 pos, Quaternion? rot = null, Vector3? scale = null, Color? color = null)
        {
            DrawCube(Matrix4x4.TRS(pos, rot ?? Quaternion.identity, scale ?? Vector3.one), color);
        }

        /// Draw a cube with color.
        public static void DrawCube(Matrix4x4 matrix, Color? color = null)
        {
            Vector3
                    down_1 = matrix.MultiplyPoint3x4(new Vector3(.5f, -.5f, .5f)),
                    down_2 = matrix.MultiplyPoint3x4(new Vector3(.5f, -.5f, -.5f)),
                    down_3 = matrix.MultiplyPoint3x4(new Vector3(-.5f, -.5f, -.5f)),
                    down_4 = matrix.MultiplyPoint3x4(new Vector3(-.5f, -.5f, .5f)),
                    up_1 = matrix.MultiplyPoint3x4(new Vector3(.5f, .5f, .5f)),
                    up_2 = matrix.MultiplyPoint3x4(new Vector3(.5f, .5f, -.5f)),
                    up_3 = matrix.MultiplyPoint3x4(new Vector3(-.5f, .5f, -.5f)),
                    up_4 = matrix.MultiplyPoint3x4(new Vector3(-.5f, .5f, .5f));

            DrawLine(down_1, down_2, color);
            DrawLine(down_2, down_3, color);
            DrawLine(down_3, down_4, color);
            DrawLine(down_4, down_1, color);

            DrawLine(down_1, up_1, color);
            DrawLine(down_2, up_2, color);
            DrawLine(down_3, up_3, color);
            DrawLine(down_4, up_4, color);

            DrawLine(up_1, up_2, color);
            DrawLine(up_2, up_3, color);
            DrawLine(up_3, up_4, color);
            DrawLine(up_4, up_1, color);
        }

        //void testDraw( )
        //{
        //    Quaternion rotation = Quaternion.LookRotation(transform.TransformDirection(workNormal));
        //    Matrix4x4 trs = Matrix4x4.TRS(transform.TransformPoint(workPoint), rotation, Vector3.one);
        ////    GL.PushMatrix();
        ////    GL.MultMatrix(trs);
        ////    //GL.LoadProjectionMatrix(trs);
        ////    Utils.DrawPlane(Vector3.zero, Vector3.forward);
        ////    GL.LoadIdentity();
        ////    GL.PopMatrix();
        //}
    }
}