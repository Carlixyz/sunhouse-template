﻿using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using UnityEngine.EventSystems;

namespace Sun.Tools
{
    [System.Serializable]
    public class KeySet
    {
        public bool useTouches = true;
        public int numberOfTouchesNeeded = 1;
        public KeyCode[] keysToUse;
    }

    [AddComponentMenu("Sun/Slide Sequence")]
    public class SlideShow : MonoBehaviour
    {
        [System.Serializable]
        public class Logo
        {
            public Texture2D image;
            public Color background;
            public float duration = 2.0f;
            public bool skippable = true;
        }

        public RawImage back;
        public RawImage image;
        public RawImage front;
        public Canvas sceneCanvas;

        public Logo[] logos;
        public string levelToLoad;
        public KeySet exitWith;

        [Range(0, 1)]
        public float logoSize       = .85f;
        public float enterLapse     = 1;
        public float exitLapse      = 1;

        bool isFading               = false;            // Fading State Activator       
        int currentLogo             = 0;
        float logoLapse             = 0;
        float currentAlpha          = 0;
        float logoMaxWidth, logoMaxHeight, actualWidth, actualHeight, texAspect;

        Texture2D   ImageTex        = null;
        Texture2D   FadeTex         = null;
        Color       alphaColor      = new Color();      // Color object for alpha setting
        Rect logoOffset             = new Rect();

        Canvas GetCanvas()
        {
            Canvas canvas = GameObject.FindObjectOfType<Canvas>();
            if (canvas == null)
            {
                GameObject cgo = new GameObject("Canvas", 
                                                typeof(Canvas),
                                                typeof(CanvasScaler),
                                                typeof(GraphicRaycaster),
                                                typeof(EventSystem),
                                                typeof(StandaloneInputModule) );

                //cgo.GetComponent<Canvas>().re

                return cgo.GetComponent<Canvas>();
            }

            return canvas;
        }

        RawImage SetupImages(string names, Texture2D tex)
        {

            var imageGO = new GameObject(names, typeof(RawImage));
            imageGO.transform.SetParent(sceneCanvas.transform);
            RawImage img = imageGO.GetComponent<RawImage>();
            img.texture = tex;

            return img;
        }

        void Start()
        {
            FadeTex = new Texture2D(1, 1);
            FadeTex.SetPixel(1, 1, Color.black);        //Sets the 1 pixel to be white
            FadeTex.Apply();                            //Applies all the changes made

            sceneCanvas = GetCanvas();

            back = SetupImages("back", FadeTex);
            image = SetupImages("image", FadeTex);
            front = SetupImages("front", FadeTex);
            //back.texture = FadeTex;

            currentLogo = -1;
            //StartCoroutine(SwapLogo());
        }

        void Update()
        {
            if (isFading)
                return;

            logoLapse -= Time.unscaledDeltaTime;

            if ( logoLapse < 0 || logos[currentLogo].skippable && GetSkipButtonPessed() ) 
                StartCoroutine(SwapLogo());
        }
bool GetSkipButtonPessed()
        {
            if (this.exitWith.useTouches &&  Input.touches.Length == this.exitWith.numberOfTouchesNeeded)
                return true;
            foreach (KeyCode k in this.exitWith.keysToUse)
                if ( Input.GetKeyDown(k)) return true;

            return false;
        }

        void OnGUI()
        {
            if (Event.current.type == EventType.Repaint)
            {
                if (ImageTex != null)
                {
                    GUI.depth = -1000;
                    GUI.DrawTexture(logoOffset, ImageTex);
                    //GUI.DrawTexture(new Rect(0, 0, Screen.width, Screen.height), ImageTex);
                }

                if (currentAlpha > 0)                                       // Draw only if not transculent
                {
                    GUI.color = alphaColor;
                    GUI.DrawTexture(new Rect(0, 0, Screen.width, Screen.height), FadeTex);
                }
            }
        }

        IEnumerator SwapLogo()
        {
            isFading = true;
            StartCoroutine(CrossFade(currentAlpha, 1, exitLapse));
            yield return new WaitForSeconds(exitLapse);

            if ( currentLogo + 1 < logos.Length)
            {
                currentLogo++;
                logoMaxWidth = Screen.width * logoSize;
                logoMaxHeight = Screen.height * logoSize;

                texAspect = (float)((float)this.logos[this.currentLogo].image.width / (float)this.logos[this.currentLogo].image.height);
                actualWidth = this.logoMaxWidth;
                actualHeight = this.actualWidth / this.texAspect;
                if (this.actualHeight > this.logoMaxHeight)
                {
                    this.actualHeight = this.logoMaxHeight;
                    this.actualWidth = this.actualHeight * this.texAspect;
                }

                logoOffset = new Rect(
                        Screen.width * 0.5f -(this.actualWidth * 0.5f),
                        Screen.height * 0.5f - (this.actualHeight * 0.5f),
                        this.actualWidth,
                        this.actualHeight
                    );

                logoLapse = logos[currentLogo].duration;
                ImageTex = logos[currentLogo].image;
                try
                {
                    Camera.main.backgroundColor = this.logos[this.currentLogo].background;
                }
                catch { }

                StartCoroutine(CrossFade(currentAlpha, 0, enterLapse));
                yield return new WaitForSeconds(enterLapse);
                isFading = false;
            }
            else
                SceneManager.LoadScene(levelToLoad);

            //yield break;
        }

        IEnumerator CrossFade(float startVal, float endVal, float duration = 2)
        {
            float startTime = Time.unscaledTime;
            float elapsed = 0f;

            while ((Time.unscaledTime - startTime) < duration + .1f)
            {
                elapsed = (Time.unscaledTime - startTime) / duration;
                currentAlpha = Mathf.Lerp(startVal, endVal, elapsed);
                alphaColor.a = Mathf.Clamp01(currentAlpha);
                yield return 0;
            }
            //yield break;
        }

    }
}

