using System;

/// <summary>
/// Mark a method with an integer argument with this to display the argument as an enum popup in the UnityEvent
/// drawer. Use: [EnumAction(typeof(SomeEnumType))]
/// 
/// Usefull for enums popup selections inside Event Calls Inspector
/// </summary>
/// Use with UnityEventDrawer.cs inside some Editor folder

[AttributeUsage(AttributeTargets.Method)]
public class EnumActionAttribute : UnityEngine.PropertyAttribute
{
    public Type enumType;

    public EnumActionAttribute(Type enumType)
    {
        this.enumType = enumType;
    }
}

/*

Example 1:

Code (CSharp):
public class SomeClass : MonoBehaviour
{
    [EnumAction(typeof(SomeEnum))]
    public void SomeMethod(int argument)
    {
        var enumArgument = (SomeEnum)argument;
        // Implement method
    }
}

Example 2:

Code (CSharp):
public class SomeClass : MonoBehaviour
{
    public void ActualMethod(SomeEnum argument)
    {
              // Implement method
    }
 
    [EnumAction(typeof(SomeEnum))]
    public void SomeMethod(int argument)
    {
        ActualMethod((SomeEnum)argument);
    }
}

    */