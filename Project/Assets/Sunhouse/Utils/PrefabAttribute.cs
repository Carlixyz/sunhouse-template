﻿/*
 * Unity Singleton MonoBehaviour
 * This file is licensed under the terms of the MIT license. 
 * 
 * Copyright (c) 2014 Kleber Lopes da Silva (http://kleber-swf.com)
 *
 * Use this
*/
using System;
//using UnityEngine;

// put this:     [PrefabAttribute("PrefabName")] before some:
// class someSingleton : Singleton<someSingleton>() {}
// and put the Singleton inside a "Resources" Folder
// And everyTime you Call a Singleton of this class
// It will Be Instantiated with the Prefabs values

namespace Sun
{
	[AttributeUsage(AttributeTargets.Class)]
	public class PrefabAttribute : Attribute
    {
        //public GameObject Prefab;
		public readonly string Name;
		public PrefabAttribute(string name = null) { Name = name; }
		//public PrefabAttribute() { Name = null; }
		//public PrefabAttribute(GameObject prefab) { Prefab = prefab; }
    }
}