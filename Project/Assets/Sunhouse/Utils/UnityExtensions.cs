﻿using UnityEngine;
using System;
using System.Reflection;
using System.Linq;
//using UnityEditor;

public static class UnityExtensions
{
    //// Creating an instance of a ScriptableObject
    //MyScriptableObject so = ScriptableObject.CreateInstance();

    //// Setting up the editor
    //Editor myEditor = Editor.CreateEditor(so);

    //// Drawing the editor
    //myEditor.OnInspectorGUI();


    public static string[] EnumNames<T>()
    {
        return Enum.GetNames(typeof(T));
    }

    //public static void Detach(this AudioSource source) // checkUp first Arg!
    public static void Detach(this Component source) // checkUp first Arg!
    {
        //public static void OnDestroy(this AudioSource source)
        //{
        //    if (source.gameObject.activeInHierarchy)
        //        Debug.Log("Disabled");
        //    else 
        //        Debug.Log("Destroyed");
        //}
        UnityEngine.Debug.Log("My parent was about to be destroyed, so I moved out.");
        source.transform.parent = null;
    }

    public static void DestroyChildren(this Transform t)
    {
        for (int i = t.childCount - 1; i >= 0; --i)
        {
            GameObject.Destroy(t.GetChild(i).gameObject);
        }
        t.DetachChildren();
    }

    public static Component CopyComponent(Component original, GameObject destination)
    {
        Component[] m_List = destination.GetComponents<Component>();
        System.Type type = original.GetType();
        System.Reflection.FieldInfo[] fields = type.GetFields();

        //Sun.Serial.)

        foreach (Component comp in m_List)
        {
            // If we already have one of them
            if (original.GetType() == comp.GetType())
            {
                foreach (System.Reflection.FieldInfo field in fields)
                {
                    field.SetValue(comp, field.GetValue(original));
                }
                return comp;
            }
        }

        // By here, we need to add it
        Component copy = destination.AddComponent(type);

        // Copied fields can be restricted with BindingFlags
        foreach (System.Reflection.FieldInfo field in fields)
        {
            field.SetValue(copy, field.GetValue(original));
        }

        return copy;
    }

    public static T GetCopyOf<T>(this Component comp, T other) where T : Component
    {
        Type type = comp.GetType();
        if (type != other.GetType()) return null; // type mis-match
        BindingFlags flags = BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.Default | BindingFlags.DeclaredOnly;
        PropertyInfo[] pinfos = type.GetProperties(flags);
        foreach (var pinfo in pinfos)
        {
            if (pinfo.CanWrite)
            {
                try
                {
                    pinfo.SetValue(comp, pinfo.GetValue(other, null), null);
                }
                catch { } // In case of NotImplementedException being thrown. For some reason specifying that exception didn't seem to catch it, so I didn't catch anything specific.
            }
        }
        FieldInfo[] finfos = type.GetFields(flags);
        foreach (var finfo in finfos)
        {
            finfo.SetValue(comp, finfo.GetValue(other));
        }
        return comp as T;
    }

    public static T Clone<T>(T source)
    {
        if (!typeof(T).IsSerializable)
        {
            throw new ArgumentException("The type must be serializable.", "source");
        }

        // Don't serialize a null object, simply return the default for that object
        if (object.ReferenceEquals(source, null))
        {
            return default(T);
        }

        System.Runtime.Serialization.IFormatter formatter = 
        new System.Runtime.Serialization.Formatters.Binary.BinaryFormatter();
        System.IO.Stream stream = new System.IO.MemoryStream();
        using (stream)
        {
            formatter.Serialize(stream, source);
            stream.Seek(0, System.IO.SeekOrigin.Begin);
            return (T)formatter.Deserialize(stream);
        }
    }

    public static T AddComponent<T>(this GameObject go, T toAdd) where T : Component
    {
        return go.AddComponent<T>().GetCopyOf(toAdd) as T;
    }

    public static bool IsTypeDerivedFrom(Type type, Type fromType)
    {
        if (type == fromType)
            return true;

        if (type.BaseType == null)
            return false;

        return IsTypeDerivedFrom(type.BaseType, fromType);
    }

    public static Type[] GetTypesInNamespace(Assembly assembly, string nameSpace)
    {
        return (from type in assembly.GetTypes() where type.Namespace == nameSpace select type).ToArray();
    }

    //public static Sun.Serialization.sVector3 GetSerializable(this Vector3 vec3)
    //{
    //    //return new Sun.Serialization.sVector3(vec3);
    //    return vec3.GetSerializable();
    //}

    public static object ToSerializable(this object obj)
    {
        return Sun.Serialization.Adapter.ToSerializableObject(obj);
    }

    public static object FromSerializable(this object obj)
    {
        return Sun.Serialization.Adapter.FromSerializableObject(obj);
    }

    //public static object GetSerializable(this UnityEngine.Object obj)
    //{
    //    return Sun.Serialization.Adapter.ToSerializableObject(obj);
    //}


#if UNITY_EDITOR
    public static Type GetActualType(this UnityEditor.SerializedProperty prop)
    {
        switch (prop.propertyType)
        {
            case UnityEditor.SerializedPropertyType.Integer:
                return typeof(int);
            case UnityEditor.SerializedPropertyType.Float:
                return typeof(float);
            case UnityEditor.SerializedPropertyType.Boolean:
                return typeof(bool);
            case UnityEditor.SerializedPropertyType.String:
                return typeof(string);
            case UnityEditor.SerializedPropertyType.Color:
                return typeof(Color);
            case UnityEditor.SerializedPropertyType.Bounds:
                return typeof(Bounds);
            case UnityEditor.SerializedPropertyType.Rect:
                return typeof(Rect);
            case UnityEditor.SerializedPropertyType.Vector2:
                return typeof(Vector2);
            case UnityEditor.SerializedPropertyType.Vector3:
                return typeof(Vector3);
            case UnityEditor.SerializedPropertyType.ObjectReference:
                return prop.objectReferenceValue.GetType();
            default: throw new Exception("Invalid type: " + prop.propertyType.ToString());
        }
    }

#endif
}
