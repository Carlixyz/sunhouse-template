﻿using UnityEditor;
using System.CodeDom;
using System.IO;
using System.CodeDom.Compiler;

public class CreateCustomEditorSubclassMenuItem
{
    [MenuItem("Assets/Create/File/CustomEditor Subclass...", false, 12)]
    private static void CreateCustomEditorClass()
    {
        var filePath = 
            EditorUtility.SaveFilePanelInProject("Save file", "CustomEditorSubclass",
            "cs", "Enter class name", AssetDatabase.GetAssetPath(Selection.activeObject));
        if (!string.IsNullOrEmpty(filePath))
        {
            using (var streamWriter = new StreamWriter(File.Create(filePath)))
            {
                using (var tw = new IndentedTextWriter(streamWriter, "    "))
                {
                    var fileName = Path.GetFileNameWithoutExtension(filePath);
                    var unit = BuildEditorWindowSubclassUnit(fileName);
                    var provider = CodeDomProvider.CreateProvider("CSharp");
                    provider.GenerateCodeFromCompileUnit(unit, tw, new CodeGeneratorOptions());
                }
            }
        }
        AssetDatabase.Refresh();
    }

    private static CodeCompileUnit BuildEditorWindowSubclassUnit(string className)
    {
        var unit = new CodeCompileUnit();

        var @namespace = new CodeNamespace();
        unit.Namespaces.Add(@namespace);

        @namespace.Imports.Add(new CodeNamespaceImport("UnityEngine"));
        @namespace.Imports.Add(new CodeNamespaceImport("UnityEditor"));
        @namespace.Imports.Add(new CodeNamespaceImport("System.Collections"));
        @namespace.Imports.Add(new CodeNamespaceImport("System.Collections.Generic"));


        var declaration = new CodeTypeDeclaration { Name = className };
        @namespace.Types.Add(declaration);

        //----

        ////var decorator = new CodeAttributeArgument { Name = className };

        var classDecl = className.Replace("Editor", "");

        CodeAttributeArgument codeAttr =
            new CodeAttributeArgument(          
                    new CodeTypeOfExpression(   // typeof( some class without "Editor"
                            new CodeTypeReference(classDecl)  
                    )
                );

        CodeAttributeDeclaration codeAttrDecl = new CodeAttributeDeclaration("CustomEditor", codeAttr);
        declaration.CustomAttributes.Add(codeAttrDecl); // [CustomEditor(typeof( class ))]


        ///------------------------------------------

        declaration.BaseTypes.Add(new CodeTypeReference { BaseType = "Editor" });

        var methodAwake = new CodeMemberMethod
        {
            Name = "OnEnable",
            Attributes = MemberAttributes.Static
        };

        var someComments = new CodeSnippetTypeMember    // Add some help snippets Comment
        (
            "/* \n\tSerializedObject lbserialized = null;\n\t"
            + classDecl + " " + classDecl.ToLower() + "Ref = null;\n\t"

            + "void Initialize () \n\t{ \n\t\t" + "if (" + classDecl.ToLower() + "Ref == null)\n"
            + "\t\t\t" + classDecl.ToLower() + "Ref = (" + classDecl + ")target; \n\t\t"
            + "if (lbserialized == null) \n\t\t\t" + "lbserialized = serializedObject;\n\t"
            + "//  lbserialized.Update();\n\t" + "//  lbserialized.ApplyModifiedProperties();\n\t" 
            + "}\n\t*/"
        );

        declaration.Members.Add(someComments);
 

        declaration.Members.Add(methodAwake);

        var methodInspectorGUI = new CodeMemberMethod
        {
            Name = "OnInspectorGUI",
            Attributes = MemberAttributes.Public | MemberAttributes.Override

        };

        declaration.Members.Add(methodInspectorGUI);
        declaration.Members.Add(new CodeMemberMethod { Name = "OnSceneGUI" });

        return unit;
    }
}