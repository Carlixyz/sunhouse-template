﻿using UnityEditor;
using UnityEditorInternal;
using UnityEngine;
using System;
using System.Linq;

[CustomPropertyDrawer(typeof(System.Collections.Generic.ReorderableListBase), true)]
public class ReorderableListDrawer : PropertyDrawer
{
    private ReorderableList _list;

    //private bool isGenericClass = false;

    private ReorderableList GetReorderableList(SerializedProperty property)
    {
        if (_list == null)
        {
            var listProperty = property.FindPropertyRelative("List");

            _list = new ReorderableList(property.serializedObject, listProperty, true, true, true, true);

            _list.drawHeaderCallback += delegate (Rect rect)
            {
                EditorGUI.LabelField(rect, property.displayName);
            };

            _list.drawElementCallback = delegate (Rect rect, int index, bool isActive, bool isFocused)
            {
                var sProperty = listProperty.GetArrayElementAtIndex(index);


                //bool foldout = isActive;
                //float height = EditorGUIUtility.singleLineHeight * 1.25f;
                //if (foldout)
                //{
                //    height = EditorGUIUtility.singleLineHeight * 5;
                //}

                EditorGUI.PropertyField(rect, sProperty, true);

                //isGenericClass = (sProperty.propertyType == SerializedPropertyType.Generic);
            };
        }

        return _list;
    }


    public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
    {
        return GetReorderableList(property).GetHeight();
    }


    public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
    {
        var rolist = GetReorderableList(property);
        var listProperty = property.FindPropertyRelative("List");
        

        var height = 0f;
        for (var i = 0; i < listProperty.arraySize; i++)
        {
            height = Mathf.Max(height, EditorGUI.GetPropertyHeight(listProperty.GetArrayElementAtIndex(i)));
        }


        rolist.elementHeight = height;
        rolist.DoList(position);
    }


    ReorderableList CreateList(SerializedObject obj, SerializedProperty prop)
    {
        ReorderableList list = new ReorderableList(obj, prop, true, true, true, true);

        list.drawHeaderCallback = rect => {
            EditorGUI.LabelField(rect, "Items");
        };

        System.Collections.Generic.List<float> heights = 
            new System.Collections.Generic.List<float>(prop.arraySize);

        list.drawElementCallback = (rect, index, active, focused) => {
            SerializedProperty element = list.serializedProperty.GetArrayElementAtIndex(index);
            Sprite s = (element.objectReferenceValue as Sprite);

            bool foldout = active;
            float height = EditorGUIUtility.singleLineHeight * 1.25f;
            if (foldout)
            {
                height = EditorGUIUtility.singleLineHeight * 5;
            }

            try
            {
                heights[index] = height;
            }
            catch (System.ArgumentOutOfRangeException e)
            {
                Debug.LogWarning(e.Message);
            }
            finally
            {
                float[] floats = heights.ToArray();
                System.Array.Resize(ref floats, prop.arraySize);
                heights = floats.ToList();
            }

            float margin = height / 10;
            rect.y += margin;
            rect.height = (height / 5) * 4;
            rect.width = rect.width / 2 - margin / 2;

            if (foldout)
            {
                if (s)
                {
                    EditorGUI.DrawPreviewTexture(rect, s.texture);
                }
            }
            rect.x += rect.width + margin;
            EditorGUI.ObjectField(rect, element, GUIContent.none);
        };

        list.elementHeightCallback = (index) => {
            /*Repaint();*/ HandleUtility.Repaint();
            float height = 0;

            try
            {
                height = heights[index];
            }
            catch (ArgumentOutOfRangeException e)
            {
                Debug.LogWarning(e.Message);
            }
            finally
            {
                float[] floats = heights.ToArray();
                Array.Resize(ref floats, prop.arraySize);
                heights = floats.ToList();
            }

            return height;
        };

        list.drawElementBackgroundCallback = (rect, index, active, focused) => {
            rect.height = heights[index];
            Texture2D tex = new Texture2D(1, 1);
            tex.SetPixel(0, 0, new Color(0.33f, 0.66f, 1f, 0.66f));
            tex.Apply();
            if (active)
                GUI.DrawTexture(rect, tex as Texture);
        };

        list.onAddDropdownCallback = (rect, li) => {
            var menu = new GenericMenu();
            menu.AddItem(new GUIContent("Add Element"), false, () => {
                //serializedObject.Update();
                obj.Update();
                li.serializedProperty.arraySize++;
                obj.ApplyModifiedProperties();
                //serializedObject.ApplyModifiedProperties();
            });

            menu.ShowAsContext();

            float[] floats = heights.ToArray();
            Array.Resize(ref floats, prop.arraySize);
            heights = floats.ToList();
        };

        return list;
    }
}