﻿using System;
using System.Reflection;
using System.Collections.Generic;

using UnityEditor;
using UnityEngine;


public class EditorWindowFinder : EditorWindow
{
    [NonSerialized]
    List<Type> Windows;

    [NonSerialized]
    Vector2 scrollPosition;


    [MenuItem("Window/EditorWindowFinder")]
    static void Init()
    {
        EditorWindow.GetWindow<EditorWindowFinder>("Window Finder").Show();
    }

    void OnEnable()
    {
        var windowType = typeof(EditorWindow);
        Windows = new List<Type>(30);

        foreach (var assembly in AppDomain.CurrentDomain.GetAssemblies())
        {
            foreach (var type in assembly.GetTypes())
            {
                if (windowType != type && windowType.IsAssignableFrom(type))
                {
                    Windows.Add(type);
                }
            }
        }

        Windows.Sort((a, b) => a.FullName.CompareTo(b.FullName));
    }

    void OnGUI()
    {
        using (var scope = new GUILayout.ScrollViewScope(scrollPosition))
        {
            scrollPosition = scope.scrollPosition;

            foreach (var entry in Windows)
            {
                if (GUILayout.Button(entry.FullName))
                {
                    EditorWindow.GetWindow(entry);
                }
            }
        }
    }

}