﻿using UnityEngine;
using System;
using UnityEditor;
using System.Collections.Generic;
using System.Collections;

namespace Sun
{
    public static class EditorUtils // a Class for Extending with a Class Method
    {

#if UNITY_EDITOR

        /// -------------------------------------------------------------------------------------///

        static Dictionary<string, bool> UnFoldings = new Dictionary<string, bool>();

        public static string DrawFilterField(string filterEntry, string fieldName = "")
        {
            if (string.IsNullOrEmpty(fieldName))
                filterEntry = EditorGUILayout.TextField(filterEntry, GUI.skin.FindStyle("ToolbarSeachTextField"));
            else
                filterEntry = EditorGUILayout.TextField(fieldName, filterEntry, GUI.skin.FindStyle("ToolbarSeachTextField"));

            GUI.SetNextControlName("Clear");
            if (filterEntry.Length > 0)
            {
                if (GUILayout.Button("", GUI.skin.FindStyle("ToolbarSeachCancelButton")))
                {
                    filterEntry = "";
                    GUI.FocusControl("Clear");
                }
            }
            else
                GUILayout.Label("", GUI.skin.FindStyle("ToolbarSeachCancelButtonEmpty"));

            return filterEntry;
        }

        public static string DrawStringPopup(string label, string selectedName, string[] names)
        {

            if (selectedName == null || names == null || names.Length == 0)
                return "";

            int selectedIndex = names.Length - 1;
            for (int i = 0; i < names.Length; i++)
                if (selectedName == names[i])
                {
                    selectedIndex = i;
                    break;
                }

            selectedIndex = EditorGUILayout.Popup(label, selectedIndex, names);
 
            return names[selectedIndex];
 
        }

        public static GUIContent MakeLabel(System.Reflection.FieldInfo field)
        {
            GUIContent guiContent = new GUIContent();
            guiContent.text = field.Name /*.SplitCamelCase()*/;
            object[] descriptions =
               field.GetCustomAttributes(typeof(System.ComponentModel.DescriptionAttribute), true);

            if (descriptions.Length > 0)
                guiContent.tooltip =                            //just use the first one.
                   (descriptions[0] as System.ComponentModel.DescriptionAttribute).Description;

            return guiContent;
        }

        public static bool DrawFoldOut(string fieldName, string contextName = " ")
        {
            var entryName = fieldName + contextName;

            if (!UnFoldings.ContainsKey( entryName ))
                UnFoldings.Add(entryName, true);                        // Remember previous unfold state

            var origFontStyle = EditorStyles.foldout.fontStyle;
            EditorStyles.foldout.fontStyle = FontStyle.Bold;            // Make it Bolder

            UnFoldings[entryName] =
                EditorGUILayout.Foldout(UnFoldings[entryName], fieldName, true);

            EditorStyles.foldout.fontStyle = origFontStyle;

            return UnFoldings[entryName];

            /////////////------------------------------------------------------////////////

            //var origFontStyle = EditorStyles.foldout.fontStyle;
            //EditorStyles.foldout.fontStyle = FontStyle.Bold;            // Make it Bolder

            //bool fold = true;                                           // True if it's open or 1º time

            //if ( UnFoldings.ContainsKey(contextName) )                  // if there's context match
            //    foreach (var f in UnFoldings[contextName])              
            //        fold = !(f == fieldName);                           // and name match set false

            //fold = EditorGUILayout.Foldout( fold, fieldName, true);

            //if ( fold )
            //    UnFoldings.Remove(contextName, fieldName);
            //else
            //    UnFoldings.Add(contextName, fieldName);

            //EditorStyles.foldout.fontStyle = origFontStyle;

            //return fold;
        }

        public static void DrawClass(object value)
        {
            EditorGUI.indentLevel += 2;
            GUILayout.BeginVertical();
            EditorGUILayout.Separator();
            System.Reflection.FieldInfo[] fields = value.GetType().GetFields();
            int totalFields = fields.Length;
            //EditorGUILayout.LabelField("Drawing class totalFields " + totalFields);

            if (Utils.IsTypeDerivedFrom(value.GetType(), typeof(Data))) // Check it's Data Derived Class
            {
                DrawDefaultField(value, fields[fields.Length - 3]);
                DrawDefaultField(value, fields[fields.Length - 2]);     // & Displays custom base Info
                DrawDefaultField(value, fields[fields.Length - 1]);
                totalFields -= 3;
            }

            System.Reflection.FieldInfo field = null;
            for (int i = 0; i < totalFields; i++)
            {
                field = fields[i];
                if (field.IsPublic && !field.IsStatic  &&
                    field.GetCustomAttributes(typeof(HideInInspector), false).Length == 0)
                    DrawDefaultField(value, field);
            }


            GUILayout.EndVertical();
            EditorGUI.indentLevel -= 2;

            EditorGUILayout.Separator();
        }

        public static void DrawDefaultField(object value, System.Reflection.FieldInfo field)
        {
  
            //////////////////////////////////////////////////////////////////////////////////////
            object oldValue = field.GetValue(value);
            object newValue = oldValue;
   
            if (field.GetCustomAttributes(typeof(ShowOnly), false).Length == 0 )
            {
                if (field.FieldType == typeof(int))
                {
                    //if (field.GetCustomAttributes(typeof(SunDataEnumField), false).Length != 0)
                    //{
                    //    //    int tmpInt = DrawMotinDataEnumIntField(field, (int)oldValue);
                    //    //    if (tmpInt != (int)oldValue)
                    //    //        newValue = tmpInt;
                    //}
                    //else
                    {
                        newValue = (object)EditorGUILayout.IntField(MakeLabel(field), (int)oldValue);
                    }
                }
                if (field.FieldType == typeof(bool))
                {
                    newValue = (object)EditorGUILayout.Toggle(MakeLabel(field), (bool)oldValue);
                }
                else if (field.FieldType == typeof(float))
                {
                    newValue = (object)EditorGUILayout.FloatField(MakeLabel(field), (float)oldValue);
                }
                else if (field.FieldType == typeof(string))
                {
           
                    //if(field.GetCustomAttributes(typeof(MotinEditorSoundEnumField),false).Length!=0)
                    //{
                    //	SoundDefinitions.Sounds tmpSound = (SoundDefinitions.Sounds)EditorGUILayout.EnumPopup(field.Name,MotinUtils.StringToEnum<Sun.AudioLoad.Clips>((string)oldValue));
                    //	if(tmpSound.ToString() != (string)oldValue)
                    //	{
                    //		newValue = tmpSound.ToString(); 
                    //		//EditorUtility.SetDirty(stringComponent);
                    //		//AssetDatabase.SaveAssets();
                    //	}
                    //}
                    //else if(field.GetCustomAttributes(typeof(MotinEditorLocalizationEnumField),false).Length!=0)
                    //{
                    //	MotinStrings tmpString = (MotinStrings)EditorGUILayout.EnumPopup(field.Name,MotinUtils.StringToEnum<MotinStrings>((string)oldValue));
                    //	if(tmpString.ToString() != (string)oldValue)
                    //	{
                    //		newValue = tmpString.ToString(); 
                    //		//EditorUtility.SetDirty(stringComponent);
                    //		//AssetDatabase.SaveAssets();
                    //	}
                    //}
                    //else if (field.GetCustomAttributes(typeof(MotinEditorEnumField), false).Length != 0)
                    //{
                    //    MotinEditorEnumField motinEnumAttr = (MotinEditorEnumField)field.GetCustomAttributes(typeof(MotinEditorEnumField), false)[0];
                    //    string tmpString = DrawEnumField(field.Name, motinEnumAttr.enumeration, (string)oldValue);
                    //    if (tmpString != (string)oldValue)
                    //    {
                    //        newValue = tmpString;
                    //        //EditorUtility.SetDirty(stringComponent);
                    //        //AssetDatabase.SaveAssets();
                    //    }
                    //}
                    //else if (field.GetCustomAttributes(typeof(MotinEditorMotinDataEnumField), false).Length != 0)
                    //{
                    //    string tmpString = DrawMotinDataEnumField(field, (string)oldValue);
                    //    if (tmpString != (string)oldValue)
                    //    {
                    //        newValue = tmpString;
                    //    }
                    //}
                    //else
                    newValue = (object)EditorGUILayout.TextField(MakeLabel(field), (string)oldValue);

                }
                else if (field.FieldType == typeof(Vector2))
                {
                    newValue = (object)EditorGUILayout.Vector2Field(field.Name, (Vector2)oldValue);
                }
                else if (field.FieldType == typeof(Vector3))
                {
                    newValue = (object)EditorGUILayout.Vector3Field(field.Name, (Vector3)oldValue);
                }
                else if (field.FieldType == typeof(Vector4))
                {
                    newValue = (object)EditorGUILayout.Vector4Field(field.Name, (Vector4)oldValue);
                }
                else if (field.FieldType == typeof(Rect))
                {
                    newValue = (object)EditorGUILayout.RectField(field.Name, (Rect)oldValue);
                }
                else if (field.FieldType == typeof(Color))
                {
                    newValue = (object)EditorGUILayout.ColorField(field.Name, (Color)oldValue);
                }
                else if (oldValue.GetType().IsArray  || (field.FieldType.IsGenericType &&
                    field.FieldType.GetGenericTypeDefinition() == typeof(List<>)) )
                {
                    //EditorGUILayout.LabelField("A LIST ! ");
                    System.Collections.IList collection = (System.Collections.IList)field.GetValue(value);
                    DrawList(field.Name, collection);
                    newValue = oldValue;
                }
                else if ( Utils.IsTypeDerivedFrom(field.FieldType, typeof(UnityEngine.Object)))
                {
                    newValue = (object)EditorGUILayout.ObjectField(field.Name, (UnityEngine.Object)oldValue, field.FieldType, true);
                }
                else if (field.FieldType.IsEnum)
                {
                    newValue = EditorGUILayout.EnumPopup(field.Name, (System.Enum)System.Enum.ToObject(field.FieldType, oldValue));
                }
                else if (field.FieldType.IsClass && !oldValue.GetType().IsArray)
                {
                    if ( DrawFoldOut(
                        oldValue.ToString() + " " + field.Name + " ",
                        oldValue.GetHashCode().ToString()) )
                    {
                        DrawClass(oldValue);
                        newValue = oldValue;
                    }
                }
                //else if (oldValue.GetType().IsArray)
                //    EditorGUILayout.LabelField("A ARRAY ! ");

                if (newValue == null && oldValue != null)
                    field.SetValue(value, newValue);
                else if (newValue != null && !newValue.Equals(oldValue))
                    field.SetValue(value, newValue);
            }
            else
            {
                EditorGUILayout.BeginHorizontal();
                EditorGUILayout.LabelField(field.Name + " ");
                GUILayout.FlexibleSpace();
                GUILayout.Label(field.GetValue(value).ToString());
                EditorGUILayout.EndHorizontal();
            }

        }

        /// -------------------------------------------------------------------------------------/// List Utils

        public static void DrawList(string fieldName, System.Collections.IList list )
        {
            if (DrawFoldOut(fieldName, list.GetHashCode().ToString()) )
            {
                EditorGUI.indentLevel++;
                int count = list.Count;

                GUILayout.BeginHorizontal();                                /// OPEN
                if (list.IsFixedSize)
                {
                    EditorGUILayout.LabelField("count   " + count);
                    GUILayout.FlexibleSpace();
                }
                else
                {
                    count = EditorGUILayout.IntField("count ", list.Count);
                    EditorGUILayout.Separator();
                    if (GUILayout.Button("+",EditorStyles.miniButton))
                        count++;

                    if (count != list.Count)
                    {
                        if (count < list.Count)
                        {
                            while (count < list.Count)
                            {
                                if (list.Count < 1)
                                    break;
                                list.RemoveAt(list.Count - 1);
                            }
                        }
                        else if (count > list.Count)
                        {
                            while (count > list.Count)
                            {
                                if (list.GetType().GetGenericArguments()[0].ToString() == "System.String")
                                    list.Add("");   // Strings don't have constructors
                                else
                                    list.Add(System.Activator.CreateInstance(list.GetType().GetGenericArguments()[0]));
                            }
                        }
                    }
                }
                GUILayout.EndHorizontal();                                  /// CLOSE

                EditorGUI.indentLevel++;
                int index = 0;
                bool classFold = true;
                foreach (object data in list)
                {
                    if(data == null)
                    {
                        EditorGUILayout.LabelField("    NULL OBJECT: Bad Instantiation? Try with {,,}    ");
                        EditorGUI.indentLevel-=2;
                        return;
                    }

                    if (!list.IsFixedSize)
                        EditorGUILayout.BeginHorizontal();          /// List ToolBar 1) start

                    var type = data.GetType();
                    if (type.IsPrimitive || type.IsEnum || type == typeof(string)) // "item " + index
                        list[index] = DrawDefaultValue(list[index], type.Name + index, false);
                    else
                        classFold = DrawFoldOut(TryGetDataName(data));

                    if (!list.IsFixedSize)                          
                    {
                        if (DrawListItemTools(list,ref index))
                            return;

                        EditorGUILayout.EndHorizontal();            /// List ToolBar 2) close
                    }

                    if (type.IsClass && type != typeof(string))    // Special use case for classes C
                        if (classFold)
                            DrawClass(data);

                    //list[index] = DrawDefaultValue(list[index], type.Name + index);
                    index++;
                }

                

                EditorGUI.indentLevel--;
                EditorGUI.indentLevel--;
            }
        }

        public static bool DrawListToolbar(System.Collections.IList list, ref int index)    // Draws default ToolBar
        {
            EditorGUILayout.BeginHorizontal(EditorStyles.toolbar);

            if (GUILayout.Button("Remove", EditorStyles.toolbarButton))
                return ToolbarRemove(list, ref index);

            if (GUILayout.Button("up", EditorStyles.toolbarButton))
                return ToolbarUp(list, ref index);

            if (GUILayout.Button("down", EditorStyles.toolbarButton))
                return ToolbarDown(list, ref index);

            EditorGUILayout.EndHorizontal();
            return false;
        }   

        public static bool DrawListItemTools(System.Collections.IList list,ref int index)   // Draws 4 each item list
        {
            EditorGUILayout.BeginHorizontal(GUILayout.MaxWidth(128));

            if (GUILayout.Button("-", EditorStyles.miniButton))
                return ToolbarRemove(list, ref index);

            if (GUILayout.Button("up", EditorStyles.miniButtonLeft))

                return ToolbarUp(list, ref index);
            if (GUILayout.Button("down", EditorStyles.miniButtonRight))
                return ToolbarDown( list, ref index);

            EditorGUILayout.EndHorizontal();
            return false;
        }


        public static bool ToolbarRemove(System.Collections.IList list, ref int index)      // removeAt index logic
        {
            if (list.Count > 0)
            {
                list.RemoveAt(index);

                if (index > 0 && list.Count > 1)
                    index--;
                else
                    index = 0;

                return true;
            }

            return false;
        } 

        public static bool ToolbarUp(System.Collections.IList list, ref int index)          // move up index logic
        {
            if (index > 0)
            {
                object aux = list[index - 1];
                list[index - 1] = list[index];
                list[index] = aux;
                index--;

                return true;
            }

            return false;
        }      

        public static bool ToolbarDown(System.Collections.IList list, ref int index)        // move down index logic
        {
            if (index < (list.Count - 1))
            {
                object aux = list[index + 1];
                list[index + 1] = list[index];
                list[index] = aux;
                index++;

                return true;
            }

            return false;
        }


        public static object DrawDefaultValue( object value, string name = "", bool drawClasses = true)
        {
            if (string.IsNullOrEmpty(name))
                name = value.ToString();

            var type = value.GetType();
            if (type == typeof(int))
               return EditorGUILayout.IntField(name, (int)value);
            else if (type == typeof(bool))
                return EditorGUILayout.Toggle(name, (bool)value);
            else if (type == typeof(float))
                return EditorGUILayout.FloatField(name, (float)value);
            else if (type == typeof(Rect))
                return EditorGUILayout.RectField(name, (Rect)value);
            else if (type == typeof(Color))
                return EditorGUILayout.ColorField(name, (Color)value);
            else if (type == typeof(Vector2))
                return EditorGUILayout.Vector2Field(name, (Vector2)value);
            else if (type == typeof(Vector3))
                return EditorGUILayout.Vector2Field(name, (Vector3)value);
            else if (type.IsEnum /* == typeof(Enum)*/)
                return EditorGUILayout.EnumPopup(name, (Enum)value);
            else if (type == typeof(string))
                return EditorGUILayout.TextField(name, (string)value);
            else if (type == typeof(UnityEngine.Object))
                return EditorGUILayout.ObjectField(name, (UnityEngine.Object)value, type, true);
            else if ( type.IsClass && drawClasses)
                if ( DrawFoldOut(TryGetDataName(value)) )
                    DrawClass(value);

            return value;
        }

        public static string TryGetDataName(object dataClass)       /// it's 4 my Custom 'Data' Class
        {
            if( Utils.IsTypeDerivedFrom(dataClass.GetType(), typeof(Data))  ) // If is Derived from Data
            {
                Data d = (Data)dataClass;

                return  d.name != "" ? ((Data)dataClass).name :         // Try Get Name if not empty..
                    d.ToString() + " " + ((Data)dataClass).intUniqueId; // else return type + Id string
            }

            return dataClass.ToString() + " " + dataClass.GetHashCode().ToString(); // or get hashCode
        }

        public static string TypeStringBrief(System.Type t)
        {
            if (t.IsArray) return TypeStringBrief(t.GetElementType()) + "[]";
            if (t == typeof(int)) return "int";
            if (t == typeof(long)) return "long";
            if (t == typeof(float)) return "float";
            if (t == typeof(double)) return "double";
            if (t == typeof(Vector2)) return "Vector2";
            if (t == typeof(Vector3)) return "Vector3";
            if (t == typeof(Vector4)) return "Vector4";
            if (t == typeof(Color)) return "Color";
            if (t == typeof(Rect)) return "Rect";
            if (t == typeof(string)) return "string";
            if (t == typeof(char)) return "char";
            return t.Name;
        }


#endif

    }



}