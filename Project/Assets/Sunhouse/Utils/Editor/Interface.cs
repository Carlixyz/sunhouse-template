using UnityEditor;
using System.CodeDom;
using System.IO;
using System.CodeDom.Compiler;

public class CreateInterfaceMenuItem
{
    [MenuItem("Assets/Create/File/Interface...", false, 10)]
    private static void CreateInterface()
    {
        var filePath = EditorUtility.SaveFilePanelInProject("Save file", "Interface", "cs", "Enter interface name");
        if (!string.IsNullOrEmpty(filePath))
        {
            using (var streamWriter = new StreamWriter(File.Create(filePath)))
            {
                using (var textWriter = new IndentedTextWriter(streamWriter, "    "))
                {
                    var fileName = Path.GetFileNameWithoutExtension(filePath);
                    var unit = BuildInterfaceGraph(fileName);
                    var provider = CodeDomProvider.CreateProvider("CSharp");
                    provider.GenerateCodeFromCompileUnit(unit, textWriter, new CodeGeneratorOptions());

                }
            }
        }
        AssetDatabase.Refresh();
    }

    private static CodeCompileUnit BuildInterfaceGraph(string interfaceName)
    {
        var codeCompileUnit = new CodeCompileUnit();

        var codeNamespace = new CodeNamespace();
        codeNamespace.Imports.AddRange(new[] {
            new CodeNamespaceImport("UnityEngine"),
            new CodeNamespaceImport("System.Collections")
        });
        codeCompileUnit.Namespaces.Add(codeNamespace);

        var declaration = new CodeTypeDeclaration(interfaceName);
        declaration.IsInterface = true;
        codeNamespace.Types.Add(declaration);

        return codeCompileUnit;
    }
}