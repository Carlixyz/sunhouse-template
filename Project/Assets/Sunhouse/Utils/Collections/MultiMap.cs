﻿using System;
using System.Collections;
//using System.Collections.Generic;
using UnityEngine;

//namespace Sun.Collections
namespace System.Collections.Generic
{
    public class MultiMap<T, V>
    {
        // 1
        Dictionary<T, List<V>> _dictionary =
            new Dictionary<T, List<V>>();

        // 2
        public void Add(T key, V value)
        {
            List<V> list;
            if (this._dictionary.TryGetValue(key, out list))
            {
                // 2A.
                list.Add(value);
            }
            else
            {
                // 2B.
                list = new List<V>();
                list.Add(value);
                this._dictionary[key] = list;
            }
        }

        // 3
        public IEnumerable<T> Keys
        {
            get
            {
                return this._dictionary.Keys;
            }
        }

        // 4
        public List<V> this[T key]
        {
            get
            {
                List<V> list;
                if (!this._dictionary.TryGetValue(key, out list))
                {
                    list = new List<V>();
                    this._dictionary[key] = list;
                }
                return list;
            }
        }

        // 5
        public void Remove(T key, V value)
        {
            List<V> list;
            if (this._dictionary.TryGetValue(key, out list))
            {
                // 2A.
                list.Remove(value);
            }
            else
            {
                Sun.De.Log("That Value isn't in the Map");
            }
        }

        // 6
        public void Remove(T key)
        {
            this._dictionary.Remove(key);
        }

        // 7
        public void Clear()
        {
            this._dictionary.Clear();
        }

        internal bool ContainsKey(T key)
        {
            return this._dictionary.ContainsKey(key);
        }
    }
}