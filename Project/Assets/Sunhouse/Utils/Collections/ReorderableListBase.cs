﻿using System;
//using System.Collections.Generic;
using UnityEngine;


namespace System.Collections.Generic
{
    public class ReorderableListBase { }

    public class ReorderableList<T> : ReorderableListBase
    {
        public List<T> List;
    }
}


/// ADD Down There your derived Orderable Classes and then just use as you need in some MonoBehaviour as:
//  public class testScript : MonoBehaviour
//  {
//      public ReorderableVector3List vectors;
//      public ReorderableExList exs;
//  }


//[Serializable]
//public class ReorderableVector3List : ReorderableList<Vector3>
//{
//}

//[Serializable]
//public class ReorderableColorList : ReorderableList<Color>
//{
//}

//[Serializable]
//public class ReorderableExList : ReorderableList<ListItemExample>
//{
//}
