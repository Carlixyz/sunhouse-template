﻿using UnityEngine;
using System.Collections;

namespace System.Collections.Generic
{
    public class RingBuffer<T>
    {
        Queue<T> _queue;
        int _size;

        public RingBuffer(int size)
        {
            _queue = new Queue<T>(size);
            _size = size;
        }

        public void Add(T obj)
        {
            if (_queue.Count == _size)
            {
                _queue.Dequeue();
                _queue.Enqueue(obj);
            }
            else
                _queue.Enqueue(obj);
        }
        public T Read()
        {
            return _queue.Dequeue();
        }

        public T Peek()
        {
            return _queue.Peek();
        }
    }
}