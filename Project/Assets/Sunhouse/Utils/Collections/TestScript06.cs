﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

public class TestScript06 : MonoBehaviour
{

    //public ReorderableVector3List vectors;
    //public ReorderableExList exs;

    [HideInInspector] // we do this beacause we call base.OnInspectorGUI(); And we don't want that to draw the list aswell.
    public List<ListItemExample> list = new List<ListItemExample>();
}



[Serializable]
public class ListItemExample
{
    public GameObject Prefab;
    public bool boolValue;
    public string stringvalue;
    public int intValue;
}