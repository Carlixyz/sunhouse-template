﻿using UnityEngine;

#if UNITY_EDITOR
using UnityEditor;

//namespace Sun
//{

    [CustomPropertyDrawer(typeof(ShowOnly))]     //[CustomPropertyDrawer(typeof(ReadOnlyAttribute))]
 public class ShowOnlyDrawer : PropertyDrawer
 {
    SerializedProperty previousProp;

    public override void OnGUI(Rect position, SerializedProperty prop, GUIContent label)
    {

        //if (previousProp == prop)
        //    return;

        string valueStr;

        switch (prop.propertyType)
        {
            case SerializedPropertyType.Integer:
                valueStr = prop.intValue.ToString();
                break;
            case SerializedPropertyType.Boolean:
                valueStr = prop.boolValue.ToString();
                break;
            case SerializedPropertyType.Float:
                valueStr = prop.floatValue.ToString("0.00");
                break;
            case SerializedPropertyType.String:
                valueStr = prop.stringValue;
                break;
            case SerializedPropertyType.Vector2:
                valueStr = prop.vector2Value.ToString();
                break;
            case SerializedPropertyType.Vector3:
                valueStr = prop.vector3Value.ToString();
                break;
            case SerializedPropertyType.Quaternion:
                valueStr = prop.quaternionValue.ToString();
                break;
            case SerializedPropertyType.Enum:
                valueStr = prop.enumDisplayNames[prop.enumValueIndex].ToString();
                break;
            //case SerializedPropertyType.ExposedReference:
            //    valueStr = prop.exposedReferenceValue.name;
            //    break;
            case SerializedPropertyType.ObjectReference:
                if(prop.objectReferenceValue == null)
                    valueStr = prop.displayName + ": Empty";
                else
                    valueStr = prop.objectReferenceValue.ToString();
                break;
            default:
                valueStr = "(not supported) " + prop.displayName;
                break;
        }
        //EditorGUI.BeginDisabledGroup(true);
        GUI.enabled = false;
        EditorGUI.LabelField(position, label.text, valueStr);
        GUI.enabled = true;
        //EditorGUI.EndDisabledGroup();

        //previousProp = prop;
    }

    //public override float GetPropertyHeight(SerializedProperty property,
    //                                        GUIContent label)
    //{
    //    return EditorGUI.GetPropertyHeight(property, label, true);
    //}

    //public override void OnGUI(Rect position,
    //                           SerializedProperty property,
    //                           GUIContent label)
    //{
    //    GUI.enabled = false;
    //    EditorGUI.PropertyField(position, property, label, true);
    //    GUI.enabled = true;
    //}
}

//}

#endif

//namespace Sun
//{
public class ShowOnly : PropertyAttribute
{
    //void function()
    //{

    //}
}
//}

//[CustomEditor(typeof(Pool))]
//public class SunHouse_Editor : Editor
//{
//    [MenuItem("GameObject/Sun/Pool", false, 1)]
//    public static void CreateLocalization()
//    {
//        var gameObject = Selection.activeGameObject;
//        if (gameObject == null )
//        {
//            gameObject = new GameObject(typeof(Sun.Pool).Name);
//            Selection.activeGameObject = gameObject;
//        }
//        UnityEditor.Undo.RegisterCreatedObjectUndo(gameObject, "Create Pool");
//        gameObject.AddComponent<Sun.Pool>();
//    }
//}

//public class Test
//{
//    [ReadOnly] public string a;
//    [ReadOnly] public int b;
//    [ReadOnly] public Material c;
//    [ReadOnly] public List<int> d = new List<int>();
//}
