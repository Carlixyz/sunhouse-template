﻿
// Popup list created by Eric Haines
// ComboBox Extended by Hyungseok Seo.(Jerry) sdragoon@nate.com
// Refactored by zhujiangbo jumbozhu@gmail.com
// Slight edit for button to show the previously selected item AndyMartin458 www.clubconsortya.blogspot.com
// 
// -----------------------------------------------
// This code working like ComboBox Control.
// I just changed some part of code, 
// because I want to seperate ComboBox button and List.
// ( You can see the result of this code from Description's last picture )
// -----------------------------------------------
//


using UnityEngine;



public class ComboBox
{
    private static bool forceToUnShow = false;
    private static int useControlID = -1;
    private bool isClickedComboButton = false;
    public bool isClickedButton { get { return isClickedComboButton; } }
    private int selectedItemIndex = 0;
    private bool changed = false;
    public bool initialized = false;
    public bool isChanged {
        get
        {
            if (!changed)
                return false;

            changed = false;
            return true;
        }
    }

    private Rect rect;
    private GUIContent buttonContent;
    private GUIContent[] listContent;
    public int TotalItems { get { return listContent.Length; } }

    private string buttonStyle;
    private string boxStyle;
    private GUIStyle listStyle;

    public Rect GetRect
    {
        get { return rect; }
        set { rect = value; }
    }

    public ComboBox()
    {

        this.rect = new Rect(0,200,200,22);


        buttonContent = new GUIContent("Empty");
        this.buttonStyle = "button";
        this.boxStyle = "box";

        listStyle = new GUIStyle();
        listStyle.normal.textColor = Color.white;
        listStyle.onHover.background =
        listStyle.hover.background = new Texture2D(2, 2);
        listStyle.padding.left =
        listStyle.padding.right =
        listStyle.padding.top =
        listStyle.padding.bottom = 4;

        //Debug.Log("Created Combo at " + this.rect);

        initialized = false;
    }

    public ComboBox(Rect rect, GUIContent buttonContent, GUIContent[] listContent, GUIStyle listStyle)
    {

        this.rect = rect;
        this.buttonContent = buttonContent;
        this.listContent = listContent;
        this.buttonStyle = "button";
        this.boxStyle = "box";
        this.listStyle = listStyle;
        initialized = true;

    }

    public ComboBox(Rect rect, GUIContent buttonContent, GUIContent[] listContent, string buttonStyle, string boxStyle, GUIStyle listStyle)
    {
        this.rect = rect;
        this.buttonContent = buttonContent;
        this.listContent = listContent;
        this.buttonStyle = buttonStyle;
        this.boxStyle = boxStyle;
        this.listStyle = listStyle;
        initialized = true;

    }

    public ComboBox(Rect rect, GUIContent buttonContent, GUIContent[] listContent)
    {
        this.rect = rect;
        this.buttonContent = buttonContent;
        this.listContent = listContent;
        this.buttonStyle = "button";
        this.boxStyle = "box";
        this.listStyle = new GUIStyle();
        this.listStyle.normal.textColor = Color.white;
        this.listStyle.onHover.background =
        this.listStyle.hover.background = new Texture2D(2, 2);
        this.listStyle.padding.left =
        this.listStyle.padding.right =
        this.listStyle.padding.top =
        this.listStyle.padding.bottom = 4;

        initialized = true;


        Debug.Log("Called Contructor ComboBox");
    }

    public ComboBox(Rect rect, string[] listNames)
    {
        this.rect = rect;

        this.listContent = new GUIContent[listNames.Length];
        for (int i = 0; i < listNames.Length; i++)
            this.listContent[i] = new GUIContent(listNames[i]);
        this.buttonContent = listContent[0];    ////////////////////////////////////////
        this.buttonStyle = "button";

        this.boxStyle = "box";
        this.listStyle = new GUIStyle();
        this.listStyle.normal.textColor = Color.white;
        this.listStyle.alignment = TextAnchor.MiddleCenter;
        this.listStyle.onHover.background =
        this.listStyle.hover.background = new Texture2D(2, 2);
        this.listStyle.padding.left =
        this.listStyle.padding.right =
        this.listStyle.padding.top =
        this.listStyle.padding.bottom = 4;

        //Debug.Log("Created Combo FULL at " + this.rect);

        initialized = true;

    }

    /*
    public ComboBox(Rect rect, string[] listNames, GUIStyle listStyle)
    {
        this.rect = rect;

        this.listContent = new GUIContent[listNames.Length];
        for (int i = 0; i < listNames.Length; i++)
            this.listContent[i] = new GUIContent(listNames[i]);
        this.buttonContent = listContent[0];    ////////////////////////////////////////
        this.buttonStyle = "button";

        this.boxStyle = "box";
        this.listStyle = listStyle;
        this.listStyle.normal.textColor = Color.white;
        this.listStyle.onHover.background =
        this.listStyle.hover.background = new Texture2D(2, 2);
        this.listStyle.padding.left =
        this.listStyle.padding.right =
        this.listStyle.padding.top =
        this.listStyle.padding.bottom = 4;
    }
    */

    public int Show()
    {
        //if (!initialized)
        //{
        //    GUI.enabled = false;
        //    GUI.Button(rect, buttonContent, buttonStyle);
        //    GUI.enabled = true;

        //    return 0;
        //}

        if (forceToUnShow)
        {
            forceToUnShow = false;
            isClickedComboButton = false;
        }

        bool done = false;
        int controlID = GUIUtility.GetControlID(FocusType.Passive);

        switch (Event.current.GetTypeForControl(controlID))
        {
            case EventType.mouseUp:
                {
                    if (isClickedComboButton)
                    {
                        done = true;
                    }
                }
                break;
        }

         if (GUI.Button(rect, buttonContent, buttonStyle))
        {

            if (useControlID == -1)
            {
                useControlID = controlID;
                isClickedComboButton = false;
            }

            if (useControlID != controlID)
            {
                forceToUnShow = true;
                useControlID = controlID;
            }
            isClickedComboButton = true;
        }

        if (isClickedComboButton && initialized)
        {
            Rect listRect = new Rect(rect.x, rect.y + listStyle.CalcHeight(listContent[0], 1.0f),
                      rect.width, listStyle.CalcHeight(listContent[0], 1.0f) * listContent.Length);

            GUI.Box(listRect, "", boxStyle);
            int newSelectedItemIndex = GUI.SelectionGrid(listRect, selectedItemIndex, listContent, 1, listStyle);
            if (newSelectedItemIndex != selectedItemIndex)
            {
                selectedItemIndex = newSelectedItemIndex;
                buttonContent = listContent[selectedItemIndex];
                changed = true;
            }
        }

        if (done)
            isClickedComboButton = false;

        return selectedItemIndex;
    }

    public int ShowIn(float posX, float posY)
    {
        //if (!initialized)
        //{
        //    GUI.enabled = false;
        //    GUI.Button(rect, buttonContent, buttonStyle);
        //    GUI.enabled = true;

        //    return 0;
        //}

        if (forceToUnShow)
        {
            forceToUnShow = false;
            isClickedComboButton = false;
        }

        bool done = false;
        int controlID = GUIUtility.GetControlID(FocusType.Passive);

        switch (Event.current.GetTypeForControl(controlID))
        {
            case EventType.mouseUp:
                {
                    if (isClickedComboButton)
                    {
                        done = true;
                    }
                }
                break;
        }

        if (GUI.Button(rect, buttonContent, buttonStyle))
        {
            if (useControlID == -1)
            {
                useControlID = controlID;
                isClickedComboButton = false;
            }

            if (useControlID != controlID)
            {
                forceToUnShow = true;
                useControlID = controlID;
            }
            isClickedComboButton = true;
        }

        if (isClickedComboButton && initialized)
        {
            Vector2 size = listStyle.CalcSize(listContent[0]);

            ////Rect listRect = new Rect(rect.x, rect.y + listStyle.CalcHeight(listContent[0], 1.0f),
            ////    rect.width, listStyle.CalcHeight(listContent[0], 1.0f) * listContent.Length);

            //Rect listRect = new Rect(posX, posY, size.x, size.y * listContent.Length);
            Rect listRect = new Rect(posX, posY, rect.width, size.y * listContent.Length);

            GUI.Box(listRect, "", boxStyle);
            int newSelectedItemIndex = GUI.SelectionGrid(listRect, selectedItemIndex, listContent, 1, listStyle);
            if (newSelectedItemIndex != selectedItemIndex)
            {
                selectedItemIndex = newSelectedItemIndex;
                buttonContent = listContent[selectedItemIndex];
                changed = true;
            }
        }

        if (done)
            isClickedComboButton = false;

        return selectedItemIndex;
    }

    public int SelectedItemIndex
    {
        get
        {
            return selectedItemIndex;
        }
        set
        {
            if (value != selectedItemIndex && initialized && listContent != null)
            {
                selectedItemIndex = value;
                buttonContent = listContent[selectedItemIndex];
                //changed = true;
            }
            //selectedItemIndex = value;
        }
    }

    public bool CheckListChanged(string[] newStrings)
    {
        if (!initialized && newStrings.Length >0 || listContent.Length != newStrings.Length)
            return true;

        int i = 0;
        foreach (var c in listContent)
            if (newStrings[i++] != c.text)
                return true;


        return false;
    }
}


/*
 * 
// === usage ======================================
using UnityEngine;
using System.Collections;
 
public class ComboBoxTest : MonoBehaviour
{
	GUIContent[] comboBoxList;
	private ComboBox comboBoxControl;// = new ComboBox();
	private GUIStyle listStyle = new GUIStyle();
 
	private void Start()
	{
		comboBoxList = new GUIContent[5];
		comboBoxList[0] = new GUIContent("Thing 1");
		comboBoxList[1] = new GUIContent("Thing 2");
		comboBoxList[2] = new GUIContent("Thing 3");
		comboBoxList[3] = new GUIContent("Thing 4");
		comboBoxList[4] = new GUIContent("Thing 5");
 
		listStyle.normal.textColor = Color.white; 
		listStyle.onHover.background =
		listStyle.hover.background = new Texture2D(2, 2);
		listStyle.padding.left =
		listStyle.padding.right =
		listStyle.padding.top =
		listStyle.padding.bottom = 4;
 
		comboBoxControl = new ComboBox(new Rect(50, 100, 100, 20), comboBoxList[0], comboBoxList, "button", "box", listStyle);
	}
 
	private void OnGUI () 
	{
		comboBoxControl.Show();
	}
}
 
*/
