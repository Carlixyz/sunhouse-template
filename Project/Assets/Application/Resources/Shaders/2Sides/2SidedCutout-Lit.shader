﻿Shader "Custom/2Sided/2SidedCutoutLit" {
	Properties{
		_Color("Main Color", Color) = (1,1,1,1)
		_MainTex("Base (RGB) Trans (A)", 2D) = "white" {}
	_Cutoff("Alpha cutoff", Range(0,1)) = 0.5
		//_BumpMap("Normal Map", 2D) = "bump" {}
	}

		SubShader{
		Tags{ "Queue" = "AlphaTest" "IgnoreProjector" = "True" "RenderType" = "TransparentCutout" }
		LOD 200
		Cull Off

		CGPROGRAM
#pragma surface surf Lambert alphatest:_Cutoff //addshadow fullforwardshadows

		sampler2D _MainTex;
		fixed4 _Color;
		sampler2D _Bump;			// * GREAT SHIT 

	struct Input {
		float2 uv_MainTex;
		float2 uv_Bump;				// * GREAT SHIT 
		float3 viewDir;				// * GREAT SHIT 
	};

	void surf(Input IN, inout SurfaceOutput o) {
		fixed4 c = tex2D(_MainTex, IN.uv_MainTex) * _Color;
		o.Albedo = c.rgb;
		o.Alpha = c.a;
		// Suggested by imageNoxWings in:
		// http://answers.unity3d.com/questions/187630/writing-a-double-sided-shader-how-to-reverse-the-n.html
		float3 n = UnpackNormal(tex2D(_Bump, IN.uv_Bump));
		o.Normal = dot(IN.viewDir, float3(0, 0, 1)) > 0 ? n : -n;
	}
	ENDCG
	}

		Fallback "Transparent/Cutout/VertexLit"
}

/*
Shader "Custom/2SidedCutoutLit" {
Properties{
_Color("Main Color", Color) = (1,1,1,1)
_MainTex("Base (RGB) Trans (A)", 2D) = "white" {}
_Cutoff("Alpha cutoff", Range(0,1)) = 0.5
}

SubShader{
Tags{ "Queue" = "AlphaTest" "IgnoreProjector" = "True" "RenderType" = "TransparentCutout" }
LOD 200
Cull Off

CGPROGRAM
#pragma surface surf Lambert alphatest:_Cutoff

sampler2D _MainTex;
fixed4 _Color;

struct Input {
float2 uv_MainTex;
};

void surf(Input IN, inout SurfaceOutput o) {
fixed4 c = tex2D(_MainTex, IN.uv_MainTex) * _Color;
o.Albedo = c.rgb;
o.Alpha = c.a;
}
ENDCG
}

Fallback "Transparent/Cutout/VertexLit"
}
*/