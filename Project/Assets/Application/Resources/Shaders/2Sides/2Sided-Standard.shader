﻿// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

/*
Shader "Custom/DoubleSided" {
	Properties
	{
		_MainTex("Particle Texture", 2D) = "white" {}
	}

		Category{
		Tags{ "Queue" = "Transparent" "IgnoreProjector" = "True" "RenderType" = "Transparent" }
		Blend SrcAlpha One
		Cull Off Lighting On ZWrite Off

		SubShader{
		Pass{

		CGPROGRAM
#pragma vertex vert
#pragma fragment frag
#pragma multi_compile_fog

#include "UnityCG.cginc"

		sampler2D _MainTex;

	struct appdata_t {
		float4 vertex : POSITION;
		fixed4 color : COLOR;
		float2 texcoord : TEXCOORD0;
	};

	struct v2f {
		float4 vertex : SV_POSITION;
		fixed4 color : COLOR;
		float2 texcoord : TEXCOORD0;
		UNITY_FOG_COORDS(1)
	};

	float4 _MainTex_ST;

	v2f vert(appdata_t v)
	{
		v2f o;
		o.vertex = UnityObjectToClipPos(v.vertex);
		o.color = v.color;
		o.texcoord = TRANSFORM_TEX(v.texcoord,_MainTex);
		UNITY_TRANSFER_FOG(o,o.vertex);
		return o;
	}

	fixed4 frag(v2f i) : SV_Target
	{
		fixed4 col = i.color * tex2D(_MainTex, i.texcoord);
	UNITY_APPLY_FOG_COLOR(i.fogCoord, col, fixed4(0,0,0,0)); // fog towards black due to our blend mode
	return col;
	}
		ENDCG
	}
	}
	}
}

*/


Shader "Custom/2Sided/2Sided-Standard" {
	Properties{
		_Color("Color", Color) = (1,1,1,1)
		_Glossiness("Smoothness", Range(0,1)) = 0.5
		_Metallic("Metallic", Range(0,1)) = 0.0
		_MainTex("Albedo (RGB)", 2D) = "white" {}
	_EmitPow("Emission Power", Float) = 1.0
		_Emission("Emission", 2D) = "black" {}
	_BumpMap("Normal Map", 2D) = "bump" {}
	_SpecMap("Specularity Map (A)", 2D) = "white" {}
	_Cutoff("Cutoff", Range(0,1)) = 0.7
		_Mask("Alpha Mask (A)", 2D) = "white" {}
	}
		SubShader{
		Tags{ "RenderType" = "Opaque" }
		LOD 200

		Cull Front

		CGPROGRAM
		// Physically based Standard lighting model, and enable shadows on all light types
#pragma surface surf Standard fullforwardshadows alphatest:_Cutoff
#pragma vertex vert

		// Use shader model 3.0 target, to get nicer looking lighting
#pragma target 3.0

		sampler2D _MainTex;
	sampler2D _Emission;
	sampler2D _BumpMap;
	sampler2D _SpecMap;
	sampler2D _Mask;

	struct Input {
		float2 uv_MainTex;
		float2 uv_Emission;
		float2 uv_BumpMap;
		float2 uv_SpecMap;
		float2 uv_Mask;
	};

	half _EmitPow;
	half _Glossiness;
	half _Metallic;
	fixed4 _Color;

	void vert(inout appdata_full v) {
		v.normal *= -1;
	}

	void surf(Input IN, inout SurfaceOutputStandard o) {
		// Albedo comes from a texture tinted by color
		fixed4 c = tex2D(_MainTex, IN.uv_MainTex) * _Color;
		o.Albedo = c.rgb;
		o.Emission = tex2D(_Emission, IN.uv_Emission) * _EmitPow;
		o.Normal = UnpackNormal(tex2D(_BumpMap, IN.uv_BumpMap));
		// Metallic and smoothness come from slider variables
		o.Metallic = _Metallic * tex2D(_SpecMap, IN.uv_SpecMap).a;
		o.Smoothness = _Glossiness * tex2D(_SpecMap, IN.uv_SpecMap).a;
		o.Alpha = tex2D(_Mask, IN.uv_Mask).a;
	}
	ENDCG

		Cull Back

		CGPROGRAM
		// Physically based Standard lighting model, and enable shadows on all light types
#pragma surface surf Standard fullforwardshadows alphatest:_Cutoff

		// Use shader model 3.0 target, to get nicer looking lighting
#pragma target 3.0

		sampler2D _MainTex;
	sampler2D _Emission;
	sampler2D _BumpMap;
	sampler2D _SpecMap;
	sampler2D _Mask;

	struct Input {
		float2 uv_MainTex;
		float2 uv_Emission;
		float2 uv_BumpMap;
		float2 uv_SpecMap;
		float2 uv_Mask;
	};

	half _EmitPow;
	half _Glossiness;
	half _Metallic;
	fixed4 _Color;

	void surf(Input IN, inout SurfaceOutputStandard o) {
		// Albedo comes from a texture tinted by color
		fixed4 c = tex2D(_MainTex, IN.uv_MainTex) * _Color;
		o.Albedo = c.rgb;
		o.Emission = tex2D(_Emission, IN.uv_Emission) * _EmitPow;
		o.Normal = UnpackNormal(tex2D(_BumpMap, IN.uv_BumpMap));
		// Metallic and smoothness come from slider variables
		o.Metallic = _Metallic * tex2D(_SpecMap, IN.uv_SpecMap).a;
		o.Smoothness = _Glossiness * tex2D(_SpecMap, IN.uv_SpecMap).a;
		o.Alpha = tex2D(_Mask, IN.uv_Mask).a;
	}
	ENDCG
	}
		FallBack "Diffuse"
}

