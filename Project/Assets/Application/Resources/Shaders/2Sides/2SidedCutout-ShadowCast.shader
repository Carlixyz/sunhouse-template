Shader "Custom/2Sided/2SidedCutout-ShadowCast" {	// FULL Shadows system working
	Properties {
		_Color ("Color", Color) = (1,1,1,1)
		[PerRendererData]_MainTex ("Sprite Texture", 2D) = "white" {}
		_Cutoff("Shadow alpha cutoff", Range(0,1)) = 0.5
	}
	SubShader {
		Tags 
		{ 
			"Queue"="Geometry"
			"RenderType"="TransparentCutout"
		}
		LOD 200
		Cull Off

		CGPROGRAM
		// Lambert lighting model, and enable shadows on all light types
		#pragma surface surf Lambert addshadow fullforwardshadows

		// Use shader model 3.0 target, to get nicer looking lighting
		#pragma target 3.0

		sampler2D _MainTex;
		fixed4 _Color;
		fixed _Cutoff;
		sampler2D _Bump;					// GREAT SHIT ADDED

		struct Input
		{
			float2 uv_MainTex;
			float2 uv_Bump;					// GREAT SHIT ADDED
			float3 viewDir;					// GREAT SHIT ADDED
		};

		void surf (Input IN, inout SurfaceOutput o) {
			fixed4 c = tex2D (_MainTex, IN.uv_MainTex) * _Color;
			o.Albedo = c.rgb;
			o.Alpha = c.a;
			clip(o.Alpha - _Cutoff);

			// Suggested by imageNoxWings in:
			// http://answers.unity3d.com/questions/187630/writing-a-double-sided-shader-how-to-reverse-the-n.html
			float3 n = UnpackNormal(tex2D(_Bump, IN.uv_Bump));
			o.Normal = dot(IN.viewDir, float3(0, 0, 1)) > 0 ? n : -n;
		}
		ENDCG
	}
	//FallBack "Diffuse"
	Fallback "Transparent/Cutout/VertexLit"
}
