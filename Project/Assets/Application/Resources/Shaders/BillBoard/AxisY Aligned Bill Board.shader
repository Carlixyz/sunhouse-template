﻿//	you will have to use it on rect that has vertices aligned in xz plane,
//and properly oriented face normal - otherwise it will be culled / invisible).
//It works with object rotation, so it can be used for bullets and such... 
//and you will probably have to disable dynamic batching.

Shader "Custom/Billboard/AxisY Aligned Billboard CG"
{
   Properties {
      _MainTex ("Texture Image", 2D) = "white" {}
   }
   SubShader {
        Tags {
	   "Queue"="Transparent"
		"IgnoreProjector"="True"
	   "RenderType"="Transparent"
	   "DisableBatching" = "True" 
   }
        Lighting Off ZWrite Off Ztest LEqual Fog { Mode Off }
        Cull Back
        //Blend SrcAlpha One //additive blend
        //Blend SrcAlpha OneMinusSrcAlpha
        LOD 200
 
      Pass {
         CGPROGRAM
         #pragma vertex vert
         #pragma fragment frag
     #pragma fragmentoption ARB_precision_hint_fastest
         uniform sampler2D _MainTex;      
         struct vertexInput {
            float4 vertex : POSITION;
            float2 tex : TEXCOORD0;
         };
         struct vertexOutput {
            float4 pos : SV_POSITION;
            float2 tex : TEXCOORD0;
         };
         vertexOutput vert(vertexInput i)
         {
            vertexOutput o;
            float3 objSpaceCamPos = mul(unity_WorldToObject, float4(_WorldSpaceCameraPos.xyz, 1)).xyz;
            float3 offsetDir = normalize(cross(float3(0.0f, 1.0f, 0.0f), objSpaceCamPos));
 
            o.pos.xz = i.vertex.x * offsetDir.xz;
            o.pos.yw = i.vertex.yw;
 
            o.pos = UnityObjectToClipPos(o.pos);
            o.tex = i.tex;
            return o;
         }
         float4 frag(vertexOutput input) : COLOR
         {
            return tex2D(_MainTex, float2(input.tex.xy));
         }
         ENDCG
      }
   }
}