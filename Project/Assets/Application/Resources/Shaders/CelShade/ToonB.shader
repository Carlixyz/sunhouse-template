﻿// Upgrade NOTE: replaced '_Object2World' with 'unity_ObjectToWorld'
// Upgrade NOTE: replaced '_World2Object' with 'unity_WorldToObject'
// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'
// https://en.wikibooks.org/wiki/Cg_Programming/Unity/Smooth_Specular_Highlights
Shader "Custom/Anim/ToonCB" {
	Properties{
	_LightTex("Light Texture", 2D) = "white" {}
	_SSSTex("SSS Texture", 2D) = "white" {}
	_SpecTex("Spec Texture", 2D) = "white" {}
	_AOTex("AO Texture", 2D) = "white" {}
	
	_Color("Diffuse Material Color", Color) = (1,1,1,1)
		_SpecColor("Specular Material Color", Color) = (1,1,1,1)
		_Shininess("Shininess", Float) = 10
		_SpecMix("Spec Mix", Float) = 0.5
		_CutoutThresh("Shininess", Float) = 0.5
		_AOOffset("AO Offset", Float) = 0.5
		_AOMultiplier("AO Multiplier", Float) = 1
		_LightThreshold("Light Threshold", Float) = 0
	}
		SubShader{
		Pass{
		Tags{ "LightMode" = "ForwardBase" }
		// pass for ambient light and first light source

		CGPROGRAM

#pragma vertex vert  
#pragma fragment frag 

#include "UnityCG.cginc"
		uniform float4 _LightColor0;
	// color of light source (from "Lighting.cginc")

	// User-specified properties
	uniform float4 _Color;
	uniform float4 _SpecColor;
	uniform float _Shininess;
	float _SpecMix;
	float _CutoutThresh;
	float _AOOffset;
	float _AOMultiplier;
	float _LightThreshold;

	sampler2D _LightTex;
	sampler2D _SSSTex;
	sampler2D _SpecTex;
	sampler2D _AOTex;
	
	struct vertexInput {
		float4 vertex : POSITION;
		float3 normal : NORMAL;
		float4 uv : TEXCOORD0;
	};
	struct vertexOutput {
		float4 pos : SV_POSITION;
		float4 uv : TEXCOORD0;
		float3 normalDir : TEXCOORD1;
		float4 posWorld : TEXCOORD2;
	};

	vertexOutput vert(vertexInput input)
	{
		vertexOutput output;

		float4x4 modelMatrix = unity_ObjectToWorld;
		float4x4 modelMatrixInverse = unity_WorldToObject;

		output.posWorld = mul(modelMatrix, input.vertex);
		output.normalDir = normalize(
			mul(float4(input.normal, 0.0), modelMatrixInverse).xyz);
		output.pos = UnityObjectToClipPos(input.vertex);
		output.uv = input.uv;
		return output;
	}

	float4 frag(vertexOutput input) : COLOR
	{

		float pixelAlpha = tex2D(_LightTex, input.uv).a;

	clip(pixelAlpha - _CutoutThresh);

	float3 normalDirection = normalize(input.normalDir);

	float3 viewDirection = normalize(
		_WorldSpaceCameraPos - input.posWorld.xyz);
	float3 lightDirection;
	float attenuation;

	if (0.0 == _WorldSpaceLightPos0.w) // directional light?
	{
		attenuation = 1.0; // no attenuation
		lightDirection = normalize(_WorldSpaceLightPos0.xyz);
	}
	else // point or spot light
	{
		float3 vertexToLightSource =
			_WorldSpaceLightPos0.xyz - input.posWorld.xyz;
		float distance = length(vertexToLightSource);
		attenuation = 1.0 / distance; // linear attenuation 
		lightDirection = normalize(vertexToLightSource);
	}

	float3 ambientLighting =
		UNITY_LIGHTMODEL_AMBIENT.rgb * _Color.rgb;
	ambientLighting = float3(0,0,0); // disable ambient lighting

	float3 diffuseReflection =
		attenuation * _LightColor0.rgb /* * _Color.rgb*/
		* max(0.0, dot(normalDirection, lightDirection));

	float3 specularReflection;
	if (dot(normalDirection, lightDirection) < 0.0)
		// light source on the wrong side?
	{
		specularReflection = float3(0.0, 0.0, 0.0);
		// no specular reflection
	}
	else // light source on the right side
	{
		/* specularReflection = attenuation * _LightColor0.rgb
		* _SpecColor.rgb * pow(max(0.0, dot(
		reflect(-lightDirection, normalDirection),
		viewDirection)), _Shininess);*/

		specularReflection = attenuation * pow(max(0.0, dot(
			reflect(-lightDirection, normalDirection),
			viewDirection)), _Shininess);
	}

	float lightDir = dot(normalDirection, lightDirection);
	float specMult = tex2D(_SpecTex, input.uv);
	specularReflection = specularReflection * specMult;

	float ao = tex2D(_AOTex, input.uv).r;
	ao = ao - (_AOOffset * _AOMultiplier);

	lightDir = lightDir + (ao);

	if (specularReflection.r > 0.1)
	{
		float4 colorA = tex2D(_LightTex, input.uv);
		float4 colorB = float4(1, 1, 1, 1);
		//float mix = 0.6;
		float4 colorC = float4(_SpecMix, _SpecMix, _SpecMix, 1);
		return (colorA + colorC) / 2.0;
		//return colorA;// (colorA*_SpecMix + colorB*(1 - _SpecMix)) / 2.0;
	}
	else if (lightDir > _LightThreshold)
	{
		float4 lightColor = tex2D(_LightTex, input.uv);
		return lightColor;
	}
	else
	{
		float4 darkColor = tex2D(_LightTex, input.uv) * tex2D(_SSSTex, input.uv);// float4(0.5, 0.5, 0.5, 1);
		return darkColor;
	}

	return float4(ambientLighting + diffuseReflection
		+ specularReflection, 1.0);
	}

		ENDCG
	}


	}
		Fallback "Specular"
}