﻿using UnityEngine;
using System.Collections;

public class BillboardList : MonoBehaviour {

    Transform thisTransform;
    //Camera m_Camera;
    Transform camTransform;
    public Transform[] childsTransform;
    public bool alignNotLook = true;

    public enum Axis { up, down, left, right, forward, back };
    public bool reverseFace = false;
    public Axis axis = Axis.up;

    // return a direction based upon chosen axis
    public Vector3 GetAxis(Axis refAxis)
    {
        switch (refAxis)
        {
            case Axis.down:
                return Vector3.down;
            case Axis.forward:
                return Vector3.forward;
            case Axis.back:
                return Vector3.back;
            case Axis.left:
                return Vector3.left;
            case Axis.right:
                return Vector3.right;
        }

        // default is Vector3.up
        return Vector3.up;
    }

    //Use this for initialization
    void Start ()
    {
        thisTransform = transform;
        camTransform = Camera.main.transform;
        StartCoroutine(CoUpdate());
        //childsTransform = GetComponentsInChildren<Transform>();
    }

    //Update is called once per frame
   IEnumerator CoUpdate()
    {
        while (thisTransform)
        {
            thisTransform.LookAt(thisTransform.position + camTransform.rotation * Vector3.forward,  // You can use this shit with a parent
                  camTransform.rotation * Vector3.up);

            foreach (Transform child in childsTransform)
            {
                if (alignNotLook)
                    child.forward = camTransform.forward;
                else
                {
                //    child.LookAt(camTransform, Vector3.up);
                    Vector3 targetPos = child.position + camTransform.rotation * (reverseFace ? Vector3.forward : Vector3.back);
                    Vector3 targetOrientation = camTransform.rotation * GetAxis(axis);
                    child.LookAt(targetPos, targetOrientation);
                }
            }

            yield return 0;
        }
    }


    //void Update()
    //{
    //    transform.LookAt(transform.position + m_Camera.transform.rotation * Vector3.forward,
    //          m_Camera.transform.rotation * Vector3.up);
    //}
}
