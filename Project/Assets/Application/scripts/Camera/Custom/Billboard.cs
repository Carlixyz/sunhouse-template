﻿using UnityEngine;
using System.Collections;

public class Billboard : MonoBehaviour
{
    Transform thisTransform;
    Transform camTransform;
    public bool alignNotLook = true;


    //Use this for initialization
    void Start()
    {
        thisTransform = transform;
        camTransform = Camera.main.transform;
        StartCoroutine(CoUpdate());
    }

    //Update is called once per frame
    IEnumerator CoUpdate()
    {
        while (thisTransform)
        {
            if (alignNotLook)
                thisTransform.forward = camTransform.forward;
            else
                thisTransform.LookAt(camTransform, Vector3.up);

            yield return 0;
        }
    }


    //void Update()
    //{
    //    transform.LookAt(transform.position + m_Camera.transform.rotation * Vector3.forward,
    //          m_Camera.transform.rotation * Vector3.up);
    //}
}
