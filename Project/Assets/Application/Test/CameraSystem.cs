﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class CameraSystem : MonoBehaviour 
{

    public Camera VerticalCam;
    public Camera HorizontalCamRight;
    public Camera HorizontalCamLeft;

    DeviceOrientation currentOrientation;

    int ResolutionWidth = 384;
    int ResolutionHeight = 216;

    void Awake()
    {
        Screen.SetResolution(ResolutionHeight, ResolutionWidth, true, 60);
        //Screen.SetResolution(ResolutionWidth, ResolutionHeight, true, 60);
        //if(Input.deviceOrientation == DeviceOrientation.Unknown)
        //    currentOrientation = DeviceOrientation.Portrait;

        UpdateOrientation();
    }

    void UpdateOrientation()
    {
        HorizontalCamLeft.enabled = (Input.deviceOrientation == DeviceOrientation.LandscapeLeft);

        HorizontalCamRight.enabled = (Input.deviceOrientation == DeviceOrientation.LandscapeRight);

        VerticalCam.enabled = (Input.deviceOrientation == DeviceOrientation.Portrait ||
                Input.deviceOrientation == DeviceOrientation.PortraitUpsideDown );


        currentOrientation = Input.deviceOrientation;

        Debug.Log(currentOrientation);
    }

    // Update is called once per frame
    void Update()
    {

        if (currentOrientation != Input.deviceOrientation)
            UpdateOrientation();

    }

    /*
    void UpdateResolution()
    {
        //Camera.main.pixelRect = new Rect(Screen.width - 384, Screen.height - 216, 384, 216);

        if (Screen.orientation == ScreenOrientation.Landscape ||
                Screen.orientation == ScreenOrientation.LandscapeRight)
        {
            //RePositionate();
            setRatio(9.0f / 16.0f);

            //Camera.main.rect = new Rect(0, 0, 1.7778f, 0.5625f);
            //RePositionate();
            Debug.Log("Landscape ");
        }
        //else
        else if (Screen.orientation == ScreenOrientation.Portrait ||
                Screen.orientation == ScreenOrientation.PortraitUpsideDown)
        {
            setRatio();

            //Debug.Log("Orientation is Portrait " + Camera.main.pixelRect);
            Debug.Log("Portrait ");
        }

        currentOrientation = Screen.orientation;

        Debug.Log(Camera.main.pixelRect.width + ", " + Camera.main.pixelRect.height);
    }
*/

    public void setRatio(float targetaspect = 16.0f / 9.0f)
    {

        // determine the game window's current aspect ratio
        float windowaspect = (float)Screen.width / (float)Screen.height;
        // current viewport height should be scaled by this amount
        float scaleheight = windowaspect / targetaspect;
        // obtain camera component so we can modify its viewport
        //Camera camera = GetComponent<Camera>();
        Camera camera = Camera.main;
        // if scaled height is less than current height, add letterbox
        if (scaleheight < 1.0f)
        {
            Rect rect = camera.rect;
            rect.width = 1.0f;
            rect.height = scaleheight;
            rect.x = 0;
            rect.y = (1.0f - scaleheight) / 2.0f;
            camera.rect = rect;
        }
        else // add pillarbox
        {
            float scalewidth = 1.0f / scaleheight;
            Rect rect = camera.rect;
            rect.width = scalewidth;
            rect.height = 1.0f;
            rect.x = (1.0f - scalewidth) / 2.0f;
            rect.y = 0;
            camera.rect = rect;
        }


    }

}
