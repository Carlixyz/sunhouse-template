﻿using UnityEngine;
using UnityEditor;
using System;

/// <summary>
/// Excel Editor using EPPlus.dll with an Excel Wrapper for all *.xlsx files
/// code based on the link from:
/// https://github.com/joexi/Excel4Unity
/// </summary>

namespace Sun.Excelent
{
    public class ExcelEditor : EditorWindow
    {

        private static Excel mExcel = null;
        private ExcelTable mTable;
        private int selectIndex;
        private static string filePathAndName = "";
        private const float WINDOW_MIN_WIDTH = 400.0f, WINDOW_MIN_HEIGHT = 200.0f;

        [MenuItem("Tools/ExcelEditor")]
        public static void GetWindow()
        {
            Type projectType = Type.GetType("UnityEditor.ProjectBrowser,UnityEditor.dll");
            ExcelEditor window = GetWindow<ExcelEditor>(typeof(ExcelEditor).Name, true, projectType);
            window.minSize = new Vector2(WINDOW_MIN_WIDTH, WINDOW_MIN_HEIGHT);
            //window.Show();
        }

        //private void OnEnable()
        //{
        //    //mExcel = Excel.Create("ExcelFile.xlsx");
        //    //selectIndex = 0;
        //}

        //private void OnDisable()
        //{
        //    //mExcel.Save("ExcelFile.xlsx");
        //    AssetDatabase.Refresh();
        //    //SceneView.RepaintAll();
        //}

        void OnGUI()
        {
            DrawFileLoadAndSaver();

            if (mExcel != null)
            {
                DrawTableTab(mExcel, ref selectIndex);
                mTable = mExcel.Tables[selectIndex];
                DrawTable(mTable);
                DrawButtons();
                //Show(mExcel);
            }
        }

        private void DrawFileLoadAndSaver(/*Excel excel*/)
        {
            EditorGUILayout.Separator();
            EditorGUILayout.BeginHorizontal(GUI.skin.FindStyle("Toolbar"));

            filePathAndName = EditorGUILayout.TextField("Excel File", filePathAndName, GUI.skin.FindStyle("ToolbarSeachTextField"));

            GUI.SetNextControlName("Clear");
            if (  filePathAndName.Length > 0)
            {
                //if (GUILayout.Button("x", EditorStyles.miniButtonRight, GUILayout.Width(16)))
                if (GUILayout.Button("", GUI.skin.FindStyle("ToolbarSeachCancelButton")))
                {
                    filePathAndName = "";
                    GUI.FocusControl("Clear");
                }
            }
            else
                GUILayout.Label("", GUI.skin.FindStyle("ToolbarSeachCancelButtonEmpty"));

            if (GUILayout.Button("New File", EditorStyles.miniButton, GUILayout.Width(64)))    // Scratch all
            {
                selectIndex = 0;
                mExcel = null;
                mExcel = new Excel();
                mExcel.Tables.Add(new ExcelTable("Sheet 0"));
            }

            GUILayout.Space(8);

            if (GUILayout.Button("Load .xlsx File", EditorStyles.miniButtonLeft, GUILayout.Width(128)))
            {
                if (string.IsNullOrEmpty(filePathAndName) )
                    filePathAndName = UnityEditor.EditorUtility.OpenFilePanel(
                        "Choose a xlsx Excel File from where to Load Data",
                        Application.dataPath, "xlsx");   // Find where we want to Load the file

                //ExcelEditor window = EditorWindow.GetWindowWithRect<ExcelEditor>(new Rect(0, 0, 800, 400));

                if (string.IsNullOrEmpty(filePathAndName))
                    return;

                mExcel = Excel.Load(filePathAndName);
            }

            if (GUILayout.Button("Save .xlsx File", EditorStyles.miniButtonRight, GUILayout.Width(128) ))
            {
                if (string.IsNullOrEmpty(filePathAndName) || !System.IO.File.Exists(filePathAndName))
                {
                    filePathAndName = UnityEditor.EditorUtility.SaveFilePanelInProject(
                        "Choose a txt Translation File from where to Save", Application.dataPath,
                        "xlsx", "Chaque Tu Archivo!");
                }

                if (!string.IsNullOrEmpty(filePathAndName) && mExcel != null)
                    mExcel.Save(filePathAndName);
            }              
              
            EditorGUILayout.EndHorizontal();
            EditorGUILayout.Separator();
        }

        public void DrawButtons()
        {

            EditorGUILayout.BeginHorizontal();
 
            if (GUILayout.Button("Remove Row", EditorStyles.miniButtonLeft, GUILayout.Width(128)) )
                mTable.NumberOfRows = (mTable.NumberOfRows > 0 ? mTable.NumberOfRows - 1 : 0);

            if (GUILayout.Button("Add Row", EditorStyles.miniButtonRight, GUILayout.Width(128)) )
                mTable.NumberOfRows++;

            //GUILayout.Space(256);
            GUILayout.FlexibleSpace();

            //[ContextMenu("Remove Column")]
            if (GUILayout.Button("Remove Column", EditorStyles.miniButtonLeft, GUILayout.Width(128)) )
            {
                //mTable.NumberOfColumns--;
                mTable.NumberOfColumns = (mTable.NumberOfColumns > 0 ? mTable.NumberOfColumns - 1 : 0);
            }

            if (GUILayout.Button("Add Column", EditorStyles.miniButtonRight, GUILayout.Width(128)) )
                mTable.NumberOfColumns++;

            EditorGUILayout.EndHorizontal();
            EditorGUILayout.Separator();
        }

        public static void DrawTableTab(Excel xls, ref int selectIndex)
        {
            GUILayout.BeginHorizontal();
            {
                if (GUILayout.Button(new GUIContent(" R1 "), EditorStyles.toolbarButton, GUILayout.Width(32)))
                    xls.Tables[selectIndex].AlternateRowLocking();

                if (GUILayout.Button(new GUIContent(" C1 "), EditorStyles.toolbarButton, GUILayout.Width(32)))
                    xls.Tables[selectIndex].AlternateColLocking();

                for (int i = 0; i < xls.Tables.Count; i++)
                {
                    if (GUILayout.Toggle(selectIndex == i, xls.Tables[i].TableName, EditorStyles.toolbarButton))
                        selectIndex = i;
                }

                if (GUILayout.Button( new GUIContent(" - "), EditorStyles.toolbarButton, GUILayout.Width(32)))
                    if (xls.Tables.Count > 1)
                    {
                        xls.Tables.RemoveAt(selectIndex);
                        selectIndex = (selectIndex > 0 ? selectIndex - 1 : 0);
                    }

                if ( GUILayout.Button(new GUIContent(" + "), EditorStyles.toolbarButton, GUILayout.Width(32)))
                    xls.Tables.Add(new ExcelTable("Sheet " + (int)(xls.Tables.Count)) );
                //xls.Tables.Add(new ExcelTable("Sheet " + ++selectIndex));
            }
            GUILayout.EndHorizontal();
        }

        public static void DrawTable(ExcelTable table)
        {
            if (table != null)
            {
                table.Position = EditorGUILayout.BeginScrollView(table.Position);
                for (int i = 1; i <= table.NumberOfRows; i++)
                {
                    bool continued = false;

                    if (continued)
                    {
                        continue;
                    }
                    EditorGUILayout.BeginHorizontal();

                    for (int j = 1; j <= table.NumberOfColumns; j++)
                    {
                        DrawCell(table, i, j);
                    }
                    EditorGUILayout.EndHorizontal();
                }
                EditorGUILayout.EndScrollView();
            }
        }

        public static void DrawCell(ExcelTable table, int row, int column)
        {
            ExcelTableCell cell = table.GetCell(row, column);
            if (cell != null)
            {
                switch (cell.Type)
                {
                    case ExcelTableCellType.TextField:
                        {
                            string s = EditorGUILayout.TextField(cell.Value.ToString(), GUILayout.MinWidth(cell.width));
                            //string s = EditorGUILayout.TextField(cell.Value.ToString(), GUILayout.MaxWidth(cell.width));
                            //string s = EditorGUILayout.TextField(cell.Value.ToString());
                            cell.Value = s;
                            break;
                        }
                    case ExcelTableCellType.Label:
                        {
                            EditorGUILayout.LabelField(cell.Value.ToString(), GUILayout.MinWidth(cell.width));
                            //EditorGUILayout.LabelField(cell.Value.ToString(), GUILayout.MaxWidth(cell.width));
                            //EditorGUILayout.LabelField(cell.Value.ToString());
                            break;
                        }
                    case ExcelTableCellType.Popup:
                        {
                            int selectIndex = cell.ValueSelected.IndexOf(cell.Value);
                            if (selectIndex < 0)
                            {
                                selectIndex = 0;
                            }
                            selectIndex = EditorGUILayout.Popup(selectIndex, cell.ValueSelected.ToArray(), GUILayout.MaxWidth(cell.width));
                            cell.Value = cell.ValueSelected[selectIndex];
                            break;
                        }
                    default:
                        {
                            break;
                        }
                }
            }
            else
            {
                string s = EditorGUILayout.TextField(table.GetValue(row, column).ToString());
                table.SetValue(row, column, s);
            }

        }

        //public static void DrawButton(string title, Action onClick)
        //{
        //    DrawButtonHorizontal(title, onClick);
        //}

        //public static void DrawButtonHorizontal(string title, Action onClick, bool horizontal = true)
        //{
        //    if (horizontal)
        //    {
        //        EditorGUILayout.BeginHorizontal();
        //    }
        //    if (GUILayout.Button(title))
        //    {
        //        if (onClick != null)
        //        {
        //            onClick();
        //        }
        //    }
        //    if (horizontal)
        //    {
        //        EditorGUILayout.EndHorizontal();
        //    }
        //}
    }
}
