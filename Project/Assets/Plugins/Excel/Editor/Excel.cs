﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using OfficeOpenXml;
using System.IO;

/// <summary>
/// Excel Wrapper using EPPlus.dll for loading all *.xlsx files
/// code based on the link from:
/// https://github.com/joexi/Excel4Unity
/// </summary>

namespace Sun.Excelent
{

    public class Excel
    {
        public List <ExcelTable> Tables = new List<ExcelTable>();

        public Excel()                                                          // Create clean Sheet from scratch
        {
            //Tables.Add(new ExcelTable("Sheet 0"));
        }

        public Excel(ExcelWorkbook wb)                                          // Create from Existent Workbook
        {
            //Debug.Log("wb.Worksheets.Count " + wb.Worksheets.Count);
            for (int i = 1; i <= wb.Worksheets.Count; i++)
            {
                ExcelWorksheet sheet = wb.Worksheets[i];
                ExcelTable table = new ExcelTable(sheet);
                Tables.Add(table);
                //Debug.Log("Tables + " + i);
            }
        }                               

        public Excel(string path)                                               // Create from Existent File Path
        {
            FileInfo file = new FileInfo(path);
            ExcelPackage ep = new ExcelPackage(file);
            ExcelWorkbook wb = ep.Workbook;

            for (int i = 1; i <= wb.Worksheets.Count; i++)
            {
                ExcelWorksheet sheet = wb.Worksheets[i];
                ExcelTable table = new ExcelTable(sheet);
                Tables.Add(table);
            }
        }


        //-----------------------------------------------------------------

        public static Excel Load(string path)                                   // Load Excel Data from file
        {
            FileInfo file = new FileInfo(path);
            if (file.Exists )
            {
                ExcelPackage ep = new ExcelPackage(file);
                return new Excel(ep.Workbook);
            }

            //Debug.LogWarning("Excel File at: " + path + " not Found!");
            //return new Excel();
            return null;
        }

        public static Excel Create(string path)                                 // Create Excel from scratch
        {
            Excel xls = default(Excel);
            ExcelPackage ep = new ExcelPackage();
            ep.Workbook.Worksheets.Add("sheet");
            xls = new Excel(ep.Workbook);
            xls.Save(path);
            return xls;
        }

        public void Save(string path)                                           // Saves all to an Excel File
        {
            FileInfo output = new FileInfo(path);
            ExcelPackage ep = new ExcelPackage();
            for (int i = 0; i < this.Tables.Count; i++)
            {
                ExcelTable table = this.Tables[i];
                ExcelWorksheet sheet = ep.Workbook.Worksheets.Add(table.TableName);
                for (int row = 1; row <= table.NumberOfRows; row++)
                {
                    for (int column = 1; column <= table.NumberOfColumns; column++)
                    {
                        sheet.Cells[row, column].Value = table.GetValue(row, column);
                    }
                }
            }
            ep.SaveAs(output);
        }

        //-----------------------------------------------------------------

        public void ShowLog()
        {
            for (int i = 0; i < Tables.Count; i++)
            {
                Tables[i].ShowLog();
            }
        }

        public void AddTable(string name)
        {
            ExcelTable table = new ExcelTable();
            table.TableName = name;
            Tables.Add(table);
        }

    }
}
