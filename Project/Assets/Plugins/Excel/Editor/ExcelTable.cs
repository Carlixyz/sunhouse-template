﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using OfficeOpenXml;

/// <summary>
/// Excel Objects using EPPlus.dll to help Excel Wrapper
/// ExcelTableCellType enum (for Editor Drawing customization)
/// ExcelTableCell
/// ExcelTable
/// code based on the link from:
/// https://github.com/joexi/Excel4Unity
/// </summary>

namespace Sun.Excelent
{


    public enum ExcelTableCellType
    {
        None = 0,
        TextField = 1,
        Label = 2,
        Popup = 3,
    }

    public class ExcelTableCell
    {
        public int RowIndex;
        public int ColumnIndex;

        public string Value;
        public List<string> ValueSelected = new List<string>();

        public float width = 50f;

        public ExcelTableCellType Type = ExcelTableCellType.TextField;

        public ExcelTableCell(int row, int column, string value)
        {
            RowIndex = row;
            ColumnIndex = column;
            Value = value;
        }
    }

    public class ExcelTable
    {
        private Dictionary <int, Dictionary<int, ExcelTableCell>> cells = new Dictionary<int, Dictionary<int, ExcelTableCell>>();

        public string TableName;
        public int NumberOfRows;
        public int NumberOfColumns;

        public Vector2 Position;

        public ExcelTable()
        {

        }

        public ExcelTable(string name)
        {
            TableName = name;
            NumberOfRows = 4;
            NumberOfColumns = 8;
            for (int row = 1; row <= NumberOfRows; row++)
                for (int column = 1; column <= NumberOfColumns; column++)
                    SetValue(row, column, " ");
        }

        public ExcelTable(ExcelWorksheet sheet)
        {
            TableName = sheet.Name;
            NumberOfRows = sheet.Dimension.Rows;
            NumberOfColumns = sheet.Dimension.Columns;
            for (int row = 1; row <= NumberOfRows; row++)
            {
                for (int column = 1; column <= NumberOfColumns; column++)
                {
                    if (sheet.Cells[row, column].Value == null)
                        continue;

                    string value = sheet.Cells[row, column].Value.ToString();
                    SetValue(row, column, value);
                }
            }
            AlternateRowLocking();
            AlternateColLocking();
        }

        public ExcelTableCell SetValue(int row, int column, string value)
        {
		    CorrectSize(row, column);

            if (!cells.ContainsKey(row))
                cells[row] = new Dictionary<int, ExcelTableCell>();

            if (cells[row].ContainsKey(column))
            {
                cells[row][column].Value = value;
                return cells[row][column];
            }
            else
            {
                ExcelTableCell cell = new ExcelTableCell(row, column, value);
                cells[row][column] = cell;
                return cell;
            }
        }

        public object GetValue(int row, int column)
        {
            ExcelTableCell cell = GetCell(row, column);
            if (cell != null)
                return cell.Value;

            return SetValue(row, column, "").Value;

            //return null;
        }

        public ExcelTableCell GetCell(int row, int column)
        {
            if (cells.ContainsKey(row))
                if (cells[row].ContainsKey(column))
                    return cells[row][column];

            return null;
        }

        public void CorrectSize(int row, int column)
        {
            NumberOfRows = Mathf.Max(row, NumberOfRows);
            NumberOfColumns = Mathf.Max(column, NumberOfColumns);
        }

        public void SetCellTypeRow(int rowIndex, ExcelTableCellType type)
        {
            for (int column = 1; column <= NumberOfColumns; column++)
            {
                ExcelTableCell cell = GetCell(rowIndex, column);
                if (cell != null)
                    cell.Type = type;
            }
        }

        public void SetCellTypeColumn(int columnIndex, ExcelTableCellType type, List<string> values = null)
        {
            for (int row = 1; row <= NumberOfRows; row++)
            {
                ExcelTableCell cell = GetCell(row, columnIndex);
                if (cell != null)
                {
                    cell.Type = type;
                    if (values != null)
                        cell.ValueSelected = values;
                }
            }
        }

        public void ShowLog() {
            string msg = "";
            for (int row = 1; row <= NumberOfRows; row++)
            {
                for (int column = 1; column <= NumberOfColumns; column++)
                {
                    msg += string.Format("{0} ", GetValue(row, column));
                }
                msg += "\n";
            }
            Debug.Log(msg);
        }

        public bool FirstRowLock = false;

        public bool FirstColLock = false;

        public void AlternateRowLocking()
        {
            FirstRowLock = !FirstRowLock;
            SetCellTypeRow(1, (FirstRowLock ? ExcelTableCellType.Label : ExcelTableCellType.TextField));
        }

        public void AlternateColLocking()
        {
            FirstColLock = !FirstColLock;
            SetCellTypeColumn(1, (FirstColLock ? ExcelTableCellType.Label : ExcelTableCellType.TextField));
        }
    }

}
