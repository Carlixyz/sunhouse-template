# SunHouse Template

A Base Project for starting work anything

## Getting Started

The gitIgnore is included and fully working Just Remember to change the Project Folder Name & check "Force Text" for Asset serialization + Visible MetaFiles.
You Can Clean Up th Application Folder and it will be ready for any project!

### USE CASE

Explain what these tests test and why

```
A code example
```

## Small tasks

Improve the Windows Manager Coherence (next Update)


## Include

* Tiler 2D/3D Editor
* AudioManager
* Logger
* a couple of powerfull Serialization Systems
* Translation System with excel importer and custom .txt format import/exporter
* Lean Tween extended with completion events 
* a Window Manager

## Built With

* [Unity](https://unity3d.com/es) - The engine framework used
* [Transform Gizmos](https://maven.apache.org/) - InGame GameObject Manipulation
* [LeanTween](http://dentedpixel.com/category/developer-diary/) - Used to animate Tweens
* [Unity Standalone File Browser](https://github.com/gkngkc/UnityStandaloneFileBrowser) - Used to open/save files win Standalone

## Consult

Please visit [SunHouse](http://sunhouse.com.ar/) for details on our code of conduct, and the process for submitting pull requests to us.

## Authors

* **CarliXYZ** - *Initial work* - [MAIL](carlixyz@yahoo.com.ar)

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details

